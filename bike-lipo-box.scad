// -*- C -*-

include <commitid.scad>
include <utils.scad>
include <sealing-box.scad>
include <bike-lipo-box-gland.scad>

pxp6012_rad = 22.5 / 2 + 0.5; // make circular hole this size in outer wall
pxp6012_rad_outer = 32.0 / 2 - 0.5;

s1930_y = 30.2 + 0.2;
s1930_x =   22 + 0.2;
s1930_y_outer = 36.4 + 0.2;
s1930_x_outer = 27.6 + 0.2;

s1930_recess = 3;
s1930_around = 3;
s1930_behind = 3;

jdae12pa_rad = 12 / 2 + 0.5;
jdae12pa_rad_outer = 19 / 2 + 0.5; // head of an "M12 bolt"

totx_inner = 180;
toty_outer = 95;
totz_inner = 27.0;

wallthick = 2.5;

cabledia = 8.7;

strap_w = 5 + 1;
strap_th = 4 + 1;
strap_pillar = 3;
strap_pillard = 5;
strap_over = 2;

lipokeeper_w = 10;
lipokeeper_h = 8;
lipokeeper_d_min = 2;
lipokeeper_slope = 0.75;
lipokeeper_end_h = 12;
lipokeeper_end_d_min = 15;

straps_at_box = [45, 95, 125, 160];
straps_every = 30;

// calculated

totx_outer = totx_inner + wallthick*2;
toty_inner = toty_outer - wallthick*2;
totz_outer = totz_inner + wallthick*2;

sb_box_sz = [totx_outer, totz_outer, toty_inner];

// origin is at centre on outer face wall
// outside is towards positive x
// mounting is vertical
module S1930_Positive(){
  d = s1930_recess + s1930_behind;
  translate([-d/2, 0,0])
    cube([d,
	  s1930_x_outer + s1930_around,
	  s1930_y_outer + s1930_around], center=true);
}
module S1930_Negative(){
  cube([60, s1930_x, s1930_y],
       center=true);
  translate([1, 0,0])
    cube([s1930_recess*2+2, s1930_x_outer, s1930_y_outer],
	 center=true);
}

module TestWall(){ ////toplevel
  sw_ctr = [25, wallthick, 25];

  rotate([0,0,-90]){
    difference(){
      union(){
	cube([50, wallthick, 42]);
      }

      translate([30, -1, 20])
	rotate([-90,0,0])
	cylinder(r = pxp6012_rad, h=10, $fn=60);

      rotate([90,0,0])
	Commitid_BestCount([15,40]);
    }
  }

  difference(){
    union(){
      cube([50, wallthick, 50]);
      translate(sw_ctr)
	rotate([0,0,90])
	S1930_Positive();
    }

    translate(sw_ctr) {
      rotate([0,0,90])
	S1930_Negative();
    }
  }    
}

ts_totx = 30;
ts_toty = 25;
ts_totz_inner = 8;

ts_box_sz = [ts_totx, ts_toty, ts_totz_inner];

$sealingbox_wallth = wallthick;
$sealingbox_floorth = wallthick;
$sealingbox_ceilth = wallthick;

module TestSealBox(){ ////toplevel
  $sealingbox_sz = ts_box_sz;

  SealingBox_RectBox();
  ts_cidoff = ($sealingbox_cnrrad * (1-.7) + wallthick * .8) * [1,1];
  translate(ts_cidoff)
    Commitid_BestCount([ts_totx,ts_toty] - 2*ts_cidoff);
}

module TestSealLid(){ ////toplevel
  $sealingbox_sz = ts_box_sz;

  difference(){
    SealingBox_RectLid();

    translate([ts_totx * .75, ts_toty/2, 0])
      cylinder(h=100, r=5);
    
    translate([-wallthick + $sealingbox_cnrrad*.5,
	       $sealingbox_cnrrad*.5 - wallthick,
	       ts_totz_inner + $sealingbox_ceilth])
      Commitid_BestCount([ts_totx * .75 - 2.5 - ($sealingbox_cnrrad*.5),
			  ts_toty - ($sealingbox_cnrrad*.5 - wallthick)*2]);
  }
}

module TestSealLidPrint(){ ////toplevel
  rotate([180,0,0]) TestSealLid();
}

module ProfileDemos(){ ////toplevel
  $sealingbox_sz = ts_box_sz;

  SealingBox_WallProfile();
  color("blue") SealingBox_FloorProfile();
  SealingBox_LidProfile();
  color("blue") SealingBox_CeilProfile();
  color("red") translate([-5,0]) square([1,ts_totz_inner]);
}

module AtGlands(){
  for (dgy=[-15,-45]) {
    translate([totx_inner + wallthick - $sealingbox_cnrrad * .3,
	       toty_inner + dgy,
	       totz_inner/2])
      children();
  }
}

module StrapKeepers(at){
  strap_x_tot = strap_w + strap_pillar*2;

  for (sx= at) {
    echo("strapkeeper at ",sx);
    translate([sx - strap_x_tot, 0, 0])
      difference(){
      translate([0,0, -0.1])
	cube([strap_x_tot, strap_pillard, strap_th + strap_over]);
      translate([strap_pillar, -1, 0])
	cube([strap_w, strap_pillard+2, strap_th]);
    }
  }
}

chargingconn_x = pxp6012_rad_outer + 1 + $sealingbox_cnrrad;
switch_x = chargingconn_x + pxp6012_rad_outer
  + s1930_y_outer/2 + s1930_around;

module AtSealingBox(){
  rotate([90,0,0])
    translate([-wallthick,-wallthick, -toty_inner])
    children();
}

module Box(){ ////toplevel
  $sealingbox_sz = sb_box_sz;

  difference(){
    union(){
      AtSealingBox()
	SealingBox_RectBox();

      translate([switch_x, toty_inner, totz_inner/2])
	rotate([90,0,90])
	S1930_Positive();

      // keepers for lipo
      for (keepers= [[ 35, lipokeeper_d_min,     lipokeeper_h,
		       [ 40, 80, 120, 150 ] ],
		     [ 10, lipokeeper_end_d_min, lipokeeper_end_h,
		       [ 25 ] ]
		     // each entry: [ y, d_min, h, [ x, ...] ]
		     ])
	for (kx= keepers[3]) {
	  translate([kx, keepers[0], -1])
	    hull(){
	      cube([lipokeeper_w, keepers[1], keepers[2] +1]);
	      cube([lipokeeper_w,
		    keepers[1] + keepers[2] / lipokeeper_slope,
		    1]);
	    }
      }

      AtGlands()
	GlandPositive(cabledia);

      translate([0, toty_inner+wallthick, -wallthick])
	rotate([180, 0,0])
	StrapKeepers(straps_at_box);
    }

    // charging connector
    translate([chargingconn_x,
	       toty_inner - (pxp6012_rad_outer + 5),
	       10])
      cylinder(r= pxp6012_rad, h= totz_outer);

    // vent connector
    translate([chargingconn_x,
	       toty_inner - (pxp6012_rad_outer*2 + 5 + 15 +
			     jdae12pa_rad_outer),
	       10])
      cylinder(r= jdae12pa_rad, h= totz_outer);

    translate([switch_x, toty_inner, totz_inner/2])
      rotate([90,0,90])
      S1930_Negative();

    AtGlands()
      GlandNegative(cabledia);

    translate(-$sealingbox_cnrrad * [1,1,0] +
	      [totx_inner, toty_inner/2, -wallthick])
      rotate([0,0,180])
      scale([2,2,1])
      Commitid_Full16_M();
  }
}

module BoxPrint(){ ////toplevel
  rotate([-90,0,-90])
    Box();
}

module Lid(){ ////toplevel
  $sealingbox_sz = sb_box_sz;
  difference(){
    union(){
      AtSealingBox()
	SealingBox_RectLid();
      translate([0, -wallthick, -SealingBox_lidbigger()])
	mirror([0,0,1])
	StrapKeepers([ straps_every : straps_every
		       : totx_inner-straps_every ]);
    }

    translate($sealingbox_cnrrad * [1,0,1])
      rotate([90,0,0])
      scale([1.5, 1.5, 1])
      Commitid_Small16_M();
  }
}

module LidPrint(){ ////toplevel
  rotate([90,0,-90])
    Lid();
}

module Demo(){ ////toplevel
  color("blue") Box();
  color("red") Lid();
}

//TestWall();
//ProfileDemos();
//TestSealBox();
//TestSealLid();
//FArcSegment_mask(350);
//StrapKeepers();
