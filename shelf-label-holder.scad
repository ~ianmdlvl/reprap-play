// -*- C -*-

prong_nomdepth = 15;
prong_curverad = 30;
prong_thick = 0.7;
prong_maxdepth = 18;

front_thick = 2.5;

//nom_shelf = 14.54 + 0.5;
nom_shelf = 20.315 + 0.5;

interference = 0.75;

length = 60;

// calculated

interference_angle = atan2(interference, prong_nomdepth);

module ProngElevationUnrotated(){
  intersection(){
    union(){
      translate([ prong_nomdepth, prong_curverad ])
	circle( prong_curverad , $fa=0.5 );
      translate([ -10, 0 ])
	square([ prong_nomdepth + 10, 10 ]);
    }
    translate([-5, -5])
      square([ prong_maxdepth + 5, prong_thick + 5]);
  }
}

module Elevation(){
  difference(){
    union(){
      rotate(-interference_angle)
	ProngElevationUnrotated();
      translate([0, -nom_shelf])
	mirror([0,1])
	rotate(-interference_angle)
	ProngElevationUnrotated();
      translate([-10, -nom_shelf - prong_thick/2])
	square([10, nom_shelf + prong_thick]);
    }
    mirror([1,0])
      translate([ front_thick, -100 ])
      square([ 50, 200 ]);
  }
}

module Main(){
  linear_extrude(height=length)
    Elevation();
}

module Print(){
  rotate([0,-90,0])
    Main();
}

//ProngElevationUnrotated();
//Elevation();
Main();
//Print();
