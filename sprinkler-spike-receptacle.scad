// -*- C -*-

main_height = 95;

spike_web_thick = 2.52 + 0.75;

spike_top_width = 21.04 + 1.5;

spike_botpos_height = 9.5;
spike_botpos_width = 11.68 + 0.00;

topwall_width = 1.5;

mount_dist = 20;
mount_width = 10;
mount_height = 5;
mount_hole_dia = 4.5;
mount_head_dia = 7.5;
mount_hole_th = 2.5;

strap_height = main_height * 0.5;

strap_width = 5.5;
strap_thick = 2.5;
strap_around = 2.5;
strap_fixing_height = 4.0;
strap_fixing_slope = 1.0;

// calculated

main_width = spike_top_width + topwall_width*2;

pos_web_thick = spike_web_thick + topwall_width*2;

module NegativePlan(){
  x4z =
    (spike_top_width - spike_botpos_width) /
    (main_height - spike_botpos_height);

  x0 = (spike_botpos_width - x4z * spike_botpos_height)/2;
  x1 =  spike_top_width/2;
  z1 = main_height;

  polygon([[ x0, -5],
	   [ x0, 0],
	   [ x1, z1],
	   [ x1, z1+5],
	   [-x1, z1+5],
	   [-x1, z1],
	   [-x0, 0],
	   [-x0, -5]]);
}

module SomeMidRounding(sq_size, z_extra) {
  translate([0,0,-z_extra])
    linear_extrude(height= main_height + z_extra*2)
    rotate(45)
    square( sq_size, center=true );
}

module PositiveMidRounding(){
  SomeMidRounding(pos_web_thick*2, 0);
}

module NegativeMidRounding(){
  SomeMidRounding(spike_web_thick*2.5, 5);
}

module PositivePlan(){
  w = main_width;
  translate([ -w/2, 0 ])
    square([ w, main_height ]);
}

module MultiplySolidifyPlan(th){
  for (r=[0,90]) {
    rotate([0,0,r])
      rotate([90,0,0])
      translate([0,0,-th/2])
      linear_extrude(height=th)
      children(0);
  }
}

module MultiplyForFixings(){
  for (r=[0:90:270])
    rotate([0,0,r])
    children(0);
}

module FixingsPositive(){
  // mount
  translate([ -1,
	      -mount_width/2,
	      0 ])
    cube([ mount_dist + mount_width/2 + 1,
	   mount_width,
	   mount_height ]);

  // strap
  for (m=[0,1]) mirror([0,m,0]) {
      translate([main_width/2, 0, strap_height]) {
	hull(){
	  translate([ -strap_around,
		      -pos_web_thick/2,
		      -(strap_thick + strap_around) / strap_fixing_slope ])
	    cube([ strap_around,
		   pos_web_thick/2 - strap_width/2,
		   0.5 ]);
	  translate([ -strap_around,
		      -(strap_around + strap_width/2),
		      0 ])
	    cube([ strap_around*2 + strap_thick,
		   strap_around,
		   strap_fixing_height ]);
	}
	mirror([0,1,0])
	  translate([ strap_thick,
		      -strap_width/2,
		      0 ])
	  cube([ strap_around,
		 strap_around + strap_width,
		 strap_fixing_height ]);
      }
    }
}

module FixingsNegative(){
  // mount hole
  translate([ mount_dist, 0,0 ]) {
    translate([0,0, -1])
      cylinder(r= mount_hole_dia/2, h= 20, $fn=20);
    translate([0,0, mount_hole_th])
      cylinder(r = mount_head_dia/2, h=20, $fn=20);
  }
}

module Main(){
  difference(){
    union(){
      MultiplySolidifyPlan(pos_web_thick) PositivePlan();
      PositiveMidRounding();
      MultiplyForFixings() FixingsPositive();
    }
    MultiplySolidifyPlan(spike_web_thick) NegativePlan();
    NegativeMidRounding();
    MultiplyForFixings() FixingsNegative();
  }
}

module PlanTest(){
  linear_extrude(height=2.0){
    difference(){
      PositivePlan();
      NegativePlan();
    }
    difference(){
      circle(r = spike_botpos_width/2 + 5);
      circle(r = spike_botpos_width/2);
      translate([-50, 0]) square([100,50]);
    }
  }
  linear_extrude(height=4.0){
    difference(){
      translate([ -main_width/2, 0 ]) square([ main_width, 2 ]);
      NegativePlan();
    }
  }
}

module MainFitTest(){
  for (top = [0,1]) {
    translate([ top * (mount_dist*2 + mount_width), 0,0 ]){
      intersection(){
	translate([0, 0, (-main_height + 0.5) * top])
	  Main();
	translate([-50,-50,0])
	  cube([100,100, spike_botpos_height + 1.5]);
      }
    }
  }
}

module Tests(){
  translate([-mount_dist*3, 0,0])
    PlanTest();
  MainFitTest();
}

module StrapFixingTest(){
  intersection(){
    Main();
    translate([ -10, -10, 40 ])
      cube([ 20, 40, 15 ]);
  }
}

//Tests();
//StrapFixingTest();
Main();
