// -*- C -*-

// Per gym ring strap retainer print
//     HHookA
//     GeneralB
//     Pin

//// toplevels-from:
include <scaffold-clamp-common.scad>

th = 3;
hhook_th = 3;
hinge_units = 2;
nbolts = 1;
hinge_unit = 5;
bolt_dia = 3;
bolt_flat = 7 + 1;

hhook_inside = 35;
hhook_l = 50;

module DemoA(){ HHookA(); }
