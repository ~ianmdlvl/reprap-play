// -*- C -*-

pump_dia = 27 + 0.9;
seatstay_mindia = 14 + 0.5;
seatstay_maxdia = 19 + 0.7;
pump_seatstay_gap = 12.3;
pump_seatstay_delta = 0.1;
pump_ridge_width = 8 + 2.0;

body_thick = 5;

pin_workdepth = 16 - 1.0;
pin_width = 11 + 1.0;
pin_thick = 3 + 0.7;
pin_base = 25;

lock_manouvre_thick = 3.7 + 0.6;
lock_manouvre_len = 18;
lock_hang_width = 17.5;
lock_manouvre_len_smaller = 13;

body_depth_each = 5;
clatter_gap = 0.5;

roof_extent = 7;
roof_thick = 2;

// fudgeish

cut_rotation = 2;
holes_rotation = 9;
pin_y_offset = 5.5;
pin_x_offset = 0.5;
ridge_rotation = 8.5;
lock_hang_ratio = 7;

// computed

body_depth = pin_width + body_depth_each*2;

module Holes(forbody=false){
  translate([0, -pump_dia/2]);
  rotate(-holes_rotation){
    translate([-(pump_seatstay_gap/2 + pump_dia/2),
	       0]) {
      if (!forbody)
	rotate(-ridge_rotation + holes_rotation)
	  translate([-50, -pump_ridge_width/2])
	  square([50, pump_ridge_width]);
      circle(r=pump_dia/2, $fn=80);
    }
    translate([+(pump_seatstay_gap/2 + seatstay_mindia/2),
	       pump_dia/2 -seatstay_maxdia/2 -pump_seatstay_delta]) {
      hull(){
	for (ud=[-1,1])
	  translate([0, ud * (seatstay_maxdia-seatstay_mindia)/2])
	    circle(r=seatstay_mindia/2, $fn=50);
      }
    }
  }
}

module BodyPlan(){
   minkowski(){
    circle(body_thick);
    hull()
      Holes(true);
  }
}

module Body(){
  translate([0,0,body_depth/2])mirror([0,0,1]){
    linear_extrude(height=body_depth){
      difference(){
	BodyPlan();
	Holes();
      }
    }
    linear_extrude(height=roof_thick){
      difference(){
	hull(){
	  BodyPlan();
	  translate([0,-roof_extent,0]) BodyPlan();
	}
	Holes();
      }
    }
  }
}

module Pin(){
  translate([pin_x_offset, pin_y_offset, 0]) rotate([0,90,0]){
    translate([0, 0, -pin_thick/2])
      linear_extrude(height=pin_thick){
      translate([-pin_base/2, 0]) square([pin_base, 50]);
      translate([-pin_width/2, -100]) square([pin_width, 101]);
    }
    hull() for (d=[0,10]) {
      translate([d*lock_hang_ratio,-d,0])
	translate([-lock_manouvre_thick/2,
		   -pin_workdepth-100,
		   -lock_manouvre_len_smaller])
	cube([lock_manouvre_thick, 100,
	      lock_manouvre_len + lock_manouvre_len_smaller]);
    }
  }
}

module All(){
  difference(){
    Body();
    Pin();
  }
}

module Piece(pc,interval){
  translate([0,-pc*interval,0])
    intersection(){
      rotate([0,0,pc*180-cut_rotation])
      translate([-200,clatter_gap/2,-200]) cube([400,400,400]);
    All();
  }
}

module PiecePrint(pc){
  rotate([0,0,90]) rotate([0,180,0])
    Piece(pc,4);
}

module PiecesPrint(){
  PiecePrint(0);
  PiecePrint(1);
}

module Demo(){
  for (pc=[0,1])
    Piece(pc,0);
}

//Holes();
//Demo();
//All();
//Pin();
//Pieces();
PiecesPrint();
//PiecePrint(0);
//PiecePrint(1);
