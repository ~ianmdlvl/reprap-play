// -*- C -*-

module RecessScrewCutout_RecessCylinder(recessdia,zbelow, h){
  translate([0,0,-zbelow]) cylinder(r=recessdia/2, h=h+1, $fn=40);
}

RecessedScrewCutout_defaultrecessdepth_flat = -0.30;
RecessedScrewCutout_defaultrecessdepth_hex = -0.60;

function RecessedScrewCutout_recessdepth(recessdia,
        recessdepth_arg=RecessedScrewCutout_defaultrecessdepth_flat) =
  recessdepth_arg >= 0 ? recessdepth_arg : -recessdepth_arg * recessdia;

function RecessedScrewCutout_totaldepth(recessdia,
        recessdepth_arg=RecessedScrewCutout_defaultrecessdepth_flat) =
  RecessedScrewCutout_recessdepth(recessdia, recessdepth_arg) +
  + 0.5*recessdia + 0.1;

module RecessedScrewCutout(shaftdia, recessdia, shaftlen,
        zbelow=1,
        recessdepth_arg=RecessedScrewCutout_defaultrecessdepth_flat) {
  // pass recessdepth_arg=-1 for the default for flat heads
  // pass recessdepth_arg=-1 for the default for flat heads
  recessdepth = RecessedScrewCutout_recessdepth(recessdia, recessdepth_arg);
  recesstopz = RecessedScrewCutout_totaldepth(recessdia, recessdepth_arg);

  translate([0,0,-zbelow]) cylinder(r=shaftdia/2, h=shaftlen+zbelow, $fn=20);
  RecessScrewCutout_RecessCylinder(recessdia,zbelow, recessdepth);
  intersection(){
    cube([recessdia + 1, shaftdia + 0.1, recesstopz*2 + 1], center=true);
    translate([0, -recessdia, recesstopz])
      rotate([0,135,0]) cube([recessdia, recessdia*2, 10]);
    RecessScrewCutout_RecessCylinder(recessdia,zbelow, recesstopz+1);
  }
}

//              nom.   shaft
//              shaft   slop
screw_info_M2   = [2,   1.2];
screw_info_M3   = [3,   1.2];
screw_info_M4   = [4,   1.1];
screw_info_M5   = [5,   1.0];
screw_info_M6   = [6,   1.2];

function screw_shaft_dia_nom(info)      = info[0];
function screw_shaft_dia_use(info)      = info[0] + info[1];
function screw_recess_dia_use(info)     = info[0] * 2.50 + 1.0;
function screw_recess_depth(info)       = info[0] * 1.00 + 0.50;
function screw_recess_depth_allen(info) = info[0] * 1.55 + 0.50;

function RecessedScrewCutoutStandard_totaldepth(info) =
  RecessedScrewCutout_totaldepth(screw_recess_dia_use(info),
				 screw_recess_depth(info));

function RecessedScrewCutoutStandardAllen_totaldepth(info) =
  RecessedScrewCutout_totaldepth(screw_recess_dia_use(info),
				 screw_recess_depth_allen(info));

module RecessedScrewCutoutStandard(info, shaftlen, zbelow=1) {
  RecessedScrewCutout(screw_shaft_dia_use(info),
		      screw_recess_dia_use(info),
		      shaftlen, zbelow,
		      screw_recess_depth(info));
}				 

module RecessedScrewCutoutStandardAllen(info, shaftlen, zbelow=1) {
  RecessedScrewCutout(screw_shaft_dia_use(info),
		      screw_recess_dia_use(info),
		      shaftlen, zbelow,
		      screw_recess_depth_allen(info));
}				 

tests = [
	 screw_info_M2,
	 screw_info_M3,
	 screw_info_M4,
2	 screw_info_M5,
	 screw_info_M6
	 ];

function Test_blocksz(t) = screw_recess_dia_use(t) + 7;

module OneTestCore(t, h, ymul, labelnumber=false){
  blocksz = Test_blocksz(t);
  translate([0, ymul * (blocksz*0.5 - 1.5), 0]) {
    difference(){
      translate([-blocksz/2, -blocksz/2, 0])
	cube([blocksz, blocksz, h]);
      child();
    }
    if (labelnumber) {
      rotate([90,0,0])
	translate([-blocksz/4,blocksz/5, blocksz/2-1])
	linear_extrude(height=0.3+1)
      import(file=str("screw-recess-test-number-s",t[0],".dxf"), convexity=100);
    }
  }
}

module OneTest(t){
  h = RecessedScrewCutoutStandard_totaldepth(t) + 3;
  ha = RecessedScrewCutoutStandardAllen_totaldepth(t) + 3;
  OneTestCore(t, h, 1){
    RecessedScrewCutoutStandard(t, h+1);
  }
  OneTestCore(t, ha, -1, true){
    RecessedScrewCutoutStandardAllen(t, ha+1);
  }
}

function Test_x(i) = i<=0 ? 0 :
  Test_x(i-1) + Test_blocksz(tests[i-1])/2 + Test_blocksz(tests[i])/2 - 3;

module Tests(){
  for (i = [0:len(tests)-1]) {
    echo(i, Test_x(i));
    translate([Test_x(i), 0, 0])
      OneTest(tests[i]);
  }
}

//OneTest(tests[1]);
Tests();
//Hole();
//Holes();
