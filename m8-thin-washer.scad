// -*- C -*-

include <utils.scad>

$fa = 1;
$fs = 0.1;

linextr(0, 0.425 + 0.125 * 2) {
  difference(){
    circle(r=  15        /2);
    circle(r= (8 + 0.5)  /2);
  }
}
