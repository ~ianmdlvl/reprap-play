// -*- C -*-

include <utils.scad>

wall_th = 2;
hook_th = 4;
hook_hole = 4;
hook_w_min = 6;
hook_hook = 12;

plug_entry_gap = 2.0;

plug_l_d = [[ 27.78,
	      10.62 + 0.75 ],
	    [ 40.88,
	      8.56 + 0.75 ],
	    ];

plug_stem = [ 2.72 + 0.50,
	      5.20 + 0.50 ];

palmrest_from_plug_z = 3.98;
laptop_th = 16.31 + 0.75;

tongue_len = 50;

// calculated

hook_th_plug_holder =
  plug_l_d[0][1]/2 + wall_th * sin(22.5);

hook_tongue_h = hook_hole + wall_th*2;

plug_l_d_smallest = plug_l_d[len(plug_l_d)-1];
plug_hook_x_min = -plug_l_d_smallest[0] - wall_th;
plug_hook_z_start = -plug_l_d_smallest[1]/2 - wall_th;

z_laptop_base = palmrest_from_plug_z - laptop_th;
z_hook_min = z_laptop_base - hook_tongue_h;

module PlugMainPlan() {
  for (l_d = plug_l_d) {
    l = l_d[0];
    d = l_d[1];
    rectfromto([ -l, -d/2 ],
	       [  0, +d/2 ]);
  }
}

module PlugHolderPlan() {
  intersection(){
    hull()
      offset(r= wall_th)
      PlugMainPlan();

    rectfromto([-100,-100], [-plug_entry_gap,+100]);
  }
}

module PlugHookHookPlan(){
  polygon([ [ plug_hook_x_min, 0 ],
	    [ plug_hook_x_min, plug_hook_z_start ],
	    [ plug_hook_x_min + (plug_hook_z_start - z_hook_min),
	      z_hook_min ],
	    [ -plug_entry_gap, z_hook_min ],
	    [ -plug_entry_gap, 0 ],
	    ]);
}

module TonguePlan(){
  difference(){
    rectfromto([ -plug_entry_gap - 1, z_hook_min ],
	       [ tongue_len, z_laptop_base ]);
    translate([ tongue_len - wall_th - hook_hole/2,
		z_hook_min + wall_th + hook_hole/2 ])
      circle(r = hook_hole/2);
  }
}

module FarHookPlan(){
  mirror([1,0,0])
    TonguePlan();

  rectfromto([ 0, z_hook_min ],
	     [ hook_w_min, palmrest_from_plug_z + 0.1]);

  translate([0, palmrest_from_plug_z])
    rectfromto([ -hook_hook, 0 ],
	       [ hook_w_min, hook_w_min ]);
}

module RotateIntersect(n=6){
  intersection_for (r = [0:n-1]) {
    rotate([r/n * 360,0,0])
      linextr(-100,100) children(0);
  }
}

module PlugHolder(){
  difference(){
    union(){
      RotateIntersect(8)
	PlugHolderPlan();

      rotate([0,0,180]) {
	linextr_y_xz(-hook_th_plug_holder/2,
		     +hook_th_plug_holder/2)
	  PlugHookHookPlan();

	linextr_y_xz(-hook_th/2,
		     +hook_th/2)
	  TonguePlan();
      }
    }

    RotateIntersect(6)
      PlugMainPlan();

    linextr(-plug_stem[1]/2, 100)
      rectfromto([ -100, -plug_stem[0]/2 ],
		 [ +100, +plug_stem[0]/2 ]);
  }
}

module PlugHolderPrint(){ ////toplevel
  render() PlugHolder();
}

module FarHookPrint(){ ////toplevel
  linextr(0, hook_th)
    FarHookPlan();
}

module DemoPlan() { ////toplevel
  translate([0,0,-10]) color("grey") PlugHolderPlan();
  PlugMainPlan();
  translate([0,0,-5]) color("blue") {
    PlugHookHookPlan();
    TonguePlan();
  }

  translate([0,40,0]) {
    FarHookPlan();
  }
}
