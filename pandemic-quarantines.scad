// -*- C -*-

prisml = 13;
triedge = 13;

etchdepth = 1.0;

figboxsize = 13;

// calculated

triheight = triedge / 2 * sqrt(3);
tricentre = triedge / 2 * tan(30);

module Number(number) {
  translate([-figboxsize/2, -figboxsize/2])
    import(file=str("pandemic-quarantine-l",number,".dxf"), convexity=100);
}

module FaceTriangle(){
  x = triedge / 2;
  y = triheight;
  polygon([[-x,  0],
	   [ 0,  y],
	   [ x,  0]]);
}

module Body(){
  translate([0, prisml/2, 0])
    rotate([90,0,0])
    linear_extrude(height=prisml) FaceTriangle();
}

module NumberCut(number){
  translate([0,0, -etchdepth])
    linear_extrude(height= etchdepth + 1)
    Number(number);
}

module Etchings(){
  for (rot=[0,180]) {
    rotate([0,0, rot])
      translate([0, -prisml/2, triedge * 0.3])
      rotate([90, 0, 0])
      NumberCut(2);
  }
  for (rot=[0,120,240]) {
    translate([0,0, tricentre])
      rotate([0, rot, 0])
      translate([0,0, -tricentre])
      rotate([0,180,0])
      rotate([0,0, rot==240 ? 90 : -90])
      NumberCut(1);
  }
}

module Counter(){
  difference(){
    Body();
    Etchings();
  }
}

Counter();
//NumberCut(1);
