// -*- C -*-

// High Detail
// support enabled
//   distance x/y 2.5mm
// fill density 30%

laptop_w       = 310;
laptop_th_rear  = 14;
laptop_th_front = 11;
laptop_halfdepth_real = 125;
laptop_halfdepth = 115; //125;
laptop_inner_x_clear = 95;

include <utils.scad>
include <camera-mount.scad>

bar_w = 15;
bar_th = 12;
flex_allow = 3;
claw_th = 6;
claw_overlap = 15;
mount_dia = 50;

min_ceil = 1;

//$test=true;
$test=false;

module ClawProfile(laptop_th){
  laptop_zmin = bar_th + flex_allow;
  laptop_zmax = laptop_zmin + laptop_th;
  difference(){
    rectfromto([-claw_overlap, 0],
	       [claw_th, laptop_zmax + claw_th]);
    rectfromto([-claw_overlap-1, laptop_zmin ],
	       [ 0, laptop_zmax ]);
  }
}

module ClawBar(inner_len, laptop_th){
  rotate([0,0,180]) linextr_y_xz(-bar_w/2, +bar_w/2) {
    rectfromto([0,0],
	       [inner_len, bar_th]);
    translate([inner_len, 0])
      ClawProfile(laptop_th);
  }
}

module Body(){
  translate([0, laptop_halfdepth - laptop_halfdepth_real, 0])
    for (m=[0,1]) {
      mirror([m,0]) {
	ClawBar(laptop_w/2, laptop_th_rear);
	translate([ laptop_w/2 - laptop_inner_x_clear + bar_w/2, 0])
	  rotate([0,0,-90])
	  ClawBar(laptop_halfdepth, laptop_th_front);
      }
    }
  cylinder(r = mount_dia/2, h= bar_th);
}

module Bracket(){
  difference(){
    Body();
    rotate([0,180,0])
      CameraMountThread( bar_th + 2 );
  }
  translate([0,0, bar_th-min_ceil])
    cylinder(r= mount_dia/2-1, h=min_ceil);
}

//ClawProfile(laptop_th_front);
//ClawBar(125, laptop_th_front);
//Body();
Bracket();
