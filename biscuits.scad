
scale=0.75;
rad=30*scale;
hbase=28.4*scale;
voff=10*scale;
height=70*scale;

wallheight = 15;
wallthick=0.8;

module flatsolid() {
	circle(r=rad,$fn=50);
	polygon(points=[[-hbase,voff],[hbase,voff],[0,height]]);
}

module mink() {
	minkowski() {
		flatsolid();
		circle(r=wallthick/2);
	}
}   

module hollow() {
	difference() {
		mink();
		flatsolid();
	}
}

linear_extrude(height=wallheight) hollow();
