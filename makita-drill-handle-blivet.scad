// -*- C -*-

include <utils.scad>

hex_across = 12.70 - 0.3;
screw_dia = 8.0 + 0.0;

min_th = 0.425;
extra_th = 1.0;

// calculated

total_th = min_th + extra_th;
hex_rad = hex_across / 2 / cos(30);

module Plan(){
  difference(){
    circle(r = hex_rad, $fn = 6);
    circle(r = screw_dia/2);
  }
}

module Elevation(){
  hull(){
    rectfromto([ -hex_rad, -1],
	       [ 0.1, min_th]);
    translate([ hex_rad, 0 ])
      rectfromto([ 0, -1 ],
		 [ 1, total_th]);
  }
}

module Blivet(){
  intersection(){
    linextr(0, total_th + 1)
      Plan();
    linextr_y_xz(-hex_across, hex_across)
      Elevation();
  }
}

Blivet();
