// -*- C -*-

spoolinnerdia = 32;
spoolwidth = 88.0;
spoolinnerrad = (spoolinnerdia - 0.2) / 2;
spoolouterrad = spoolinnerrad + 61.5;

include <doveclip.scad>
include <axlepin.scad>

spoolradclear = 10;
spoolradslop = 2;

spoolinnerslop = 3;
axleslop = 0.5;

axlerad = 7;
barwasherrad = 17;

hubbasethick = 4;
hubmainthick = 15;
hubbaseweb = 1.2;
hubbasestalkwidth = 4;
hubwalls = 2.5;
hubpillarw = 4;
hubbaserad = spoolinnerrad + 10;
hubmainrad = spoolinnerrad - spoolradslop;

legw = 12;
plugl = 20;
plugwmin = 3;
plugh = 10;
plugslope = 0.5;
plugwmax = plugwmin + plugh * plugslope * 2;

trestlefoot = 15;

trestlelegw = 10;
trestlebaseh = 10;
trestleplugd = 1;

topblockthick = 3;
topblockbasedepth = 5;

pinbasew = 4.0;
pinminh = 1.0;
pinmaxh = 3.5;
pindh = 1.75;
pindwidth = 1.75;

pintaperlen = 20;
pinstraightlen = 30-pintaperlen;

spoolouterpad = AxlePin_holerad()*2 * 1.5;
spoolbarlen = spoolwidth +
  2*(Washer_thick() + hubbasethick + AxlePin_holerad()
     + spoolinnerslop + spoolouterpad);
  barz = axlerad * 0.5;
axlepin_x = spoolwidth/2 + hubbasethick +
  Washer_thick() + spoolinnerslop + AxlePin_holerad()*0.5;

trestleheight = spoolouterrad + spoolradclear - barz;
trestlebase = trestleheight * 1.2;

module Plug(d=0){
  dw = d;
  dh = d;
  dhb = d*2;
  a = atan(plugslope);
  bdy = -dhb;
  bdx = dw / cos(a) + bdy * plugslope;
  tdy = dh;
  tdx = bdx + tdy * plugslope;
  translate([-d,0,0]) rotate([90,0,90]) linear_extrude(height=plugl+0.1+d*2){
    polygon([[-(plugwmin/2 + bdx),  bdy],
	     [-(plugwmax/2 + tdx),  plugh + tdy],
	     [+(plugwmax/2 + tdx),  plugh + tdy],
	     [+(plugwmin/2 + bdx),  bdy]]);
  }
}

module Bar(){ ////toplevel
  spoolw = spoolbarlen;
  biggestw = spoolw + 50;

  intersection(){
    for (mir=[0,1]) {
      mirror([mir,0,0]) {
	translate([spoolw/2, 0, 0])
	  Plug();
	translate([-1, -50, -50])
	  cube([spoolw/2+1.1, 100, 100]);
      }
    }
    difference(){
      translate([-biggestw/2, -50, 0])
	cube([biggestw, 100, 100]);
      for (mir=[0,1]) {
	mirror([mir,0,0])
	  translate([axlepin_x, 0, -50])
	  cylinder(r=AxlePin_holerad(), 100, $fn=15);
      }
    }
    translate([0,0,barz]) {
      translate([-100,0,0])
	rotate([0,90,0]) cylinder(r=axlerad, h=200, $fn=60);
    }
  }
}

module FtAxlePin(){ ////toplevel
  AxlePin(axlerad, (axlerad + barwasherrad*2)/3 * 2);
}

module AxleWasher(){ ////toplevel
  Washer(axlerad, barwasherrad);
}

module Trestle(){ ////toplevel
  legang = atan2(trestlebase/2, trestleheight);
  eplen = sqrt(trestleheight*trestleheight + trestlebase*trestlebase*0.25);
  topblockw = plugwmax + trestleplugd*2 + topblockthick*2;

  pinholebasew = pinbasew + pindwidth*2;
  pinholeh =     pinmaxh +  pindh;

  difference(){
    union(){
      for (mir=[0,1]) {
	mirror([mir,0,0]) {
	  rotate([0,0, -90-legang])
	    ExtenderPillars(length=eplen+trestlelegw,
			    width=trestlelegw,
			    height=legw,
			    baseweb=true);

	  translate([-trestlebase/2, -trestleheight, 0])
	    cylinder(r=trestlelegw/2*1.2, h=trestlefoot);
	}
      }
      translate([-topblockw/2, -topblockbasedepth, 0])
	cube([topblockw,
	      topblockbasedepth + plugh + topblockthick
	      + (pinmaxh - pinminh)*0.5,
	      plugl]);

      translate([-trestlebase/2, -trestleheight, 0])
	ExtenderPillars(length=trestlebase, width=trestlebaseh*2, height=legw);
    }
    translate([-300, -trestleheight-50, -1])
      cube([600, 50, 52]);

    rotate([-90,-90,0])
      Plug(d=trestleplugd);

    for (rot=[0,180]) {
      translate([0,0,plugl/2]) rotate([0,rot,0]) translate([0,0,-plugl/2]) {
	translate([0,
 	           plugh - (pinmaxh - pinminh)*1.00,
		   (plugl - pinholebasew*2)/3]) {
	  translate([-(topblockw*0.25+1), 0, pinholebasew/2])
	    rotate([-90,0,0]) %Pin();
	  translate([-(topblockw+1), 0, 0]) {
	    rotate([0,90,0]) {
	      linear_extrude(height = topblockw*2.0+2) {
		polygon([[-1.0 * pinholebasew, -0.01],
			 [-0.5 * pinholebasew, pinholeh],
			 [ 0                 , -0.01]]);
	      }
	    }
	  }
	}
      }
    }
  }
}

module Pin(){ ////toplevel
  rotate([90,0,90]) {
    hull(){
      for (mir=[0,1]) {
	mirror([mir,0,0]) {
	  linear_extrude(height=0.1) {
	    polygon([[-0.01, 0],
		     [-0.01, pinminh],
		     [pinbasew*0.5*(pinminh/pinmaxh), 0]]);
	  }
	  translate([0,0,pintaperlen])
	    linear_extrude(height=pinstraightlen) {
	    polygon([[-0.01, 0],
		     [-0.01, pinmaxh],
		     [pinbasew*0.5, 0]]);
	  }
	}
      }
    }
  }
}

module HubEnd(){ ////toplevel
  thick = hubmainthick+hubbasethick;
  difference(){
    union(){
      for (ang=[0 : 60 : 359]) {
	rotate([0,0,ang]) {
	  translate([hubmainrad - hubwalls/2, -hubbasestalkwidth/2, 0])
	    cube([hubbaserad - (hubmainrad - hubwalls/2),
		  hubbasestalkwidth, hubbasethick]);
	  ExtenderPillar(length = hubmainrad-hubwalls/2,
			 height = hubbasethick + hubmainthick,
			 pillarw = hubpillarw);
	}
      }
      cylinder(r=axlerad+hubwalls, h=thick);
      cylinder(r=hubmainrad-0.1, h=hubbaseweb);
      difference(){
	cylinder(r=hubmainrad, h=thick, $fn=100);
	translate([0,0,-1])
	  cylinder(r=hubmainrad-hubwalls, h=thick+2);
      }
      difference(){
	cylinder(r=hubbaserad, h=hubbasethick, $fn=50);
	translate([0,0,-1])
	  cylinder(r=hubbaserad-hubwalls, h=hubbasethick+2);
      }
    }
    translate([0,0,-1])
      cylinder(r=axlerad+axleslop, h=thick+2, $fn=50);
  }
}


module TestKit(){ ////toplevel
  translate([60,0,0]) mirror([1,0,0]) Pin();
  translate([60,15,0]) mirror([1,0,0]) Pin();
  translate([0,40,0]) intersection(){
    Trestle();
    translate([-50,-10,-1]) cube([100,100,100]);
  }
  intersection(){
    translate([-60,10,0]) Bar();
    cube(50,center=true);
  }
  %translate([50,40, AxlePin_zoffset()]) FtAxlePin();
  %translate([0,-20,0]) AxleWasher();
}

module DemoSpool(){
  rotate([0,90,0]) translate([0,0,-spoolwidth/2])
    difference(){
      cylinder(r=spoolouterrad, h=spoolwidth);
      translate([0,0,-1]) cylinder(r=spoolinnerrad, h=spoolwidth+2);
    }
}

module Demo(){ ////toplevel
  color("blue") Bar();
  for (mir=[0,1]) {
    mirror([mir,0,0]) {
      color("red") translate([spoolbarlen/2,0,0])
	rotate([90,0,90]) Trestle();
      color("orange")
	translate([spoolwidth/2 + hubbasethick + spoolinnerslop*2/3, 0, barz])
	rotate([0,90,0]) AxleWasher();
      color("orange") translate([axlepin_x, 0, barz])
	rotate([90,0,90]) FtAxlePin();
      color("cyan")
	translate([spoolwidth/2 + hubbasethick + spoolinnerslop*1/3, 0, barz])
	rotate([0,-90,0]) HubEnd();
    }
  }
  %translate([0,0,barz]) DemoSpool();
}

//Bar();
//FtAxlePin();
//AxleWasher();
//Trestle();
//Pin();
//TestKit();
//Plug(d=1);
//ExtenderPillars(80,12,8, baseweb=true);
//HubEnd();
//Demo();
