size=1.7;
scale([size,size,size])
difference() {
    cube(size=[10,10,4.0], center=true);
    translate([0,0,2.0]) sphere(r=4.5, $fn=50);
}
