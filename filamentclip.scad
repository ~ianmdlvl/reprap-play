include <cliphook.scad>
include <filamentteeth.scad>

rad=19;
h=3.5;
w=2.5;

looprad=2.5;
loopw=w;

fdia=1.77;
//fdia=3;

d=0.01;

module our_ClipHook(ye){
  ClipHook(h=h, w=w, g=0.6, k=1.5, g=0.6, ye=ye, cupcaph=0.5, cupcapg=0.8);
}

module FilamentClip() {
  rotate([0,0,-70]) {
    translate([0,rad-1.5,0]) {
      rotate([0,0,8])
	our_ClipHook(ye=-1.3);
    }
  }

  rotate([0,0,-35]) {
    translate([0,rad,0]) {
      rotate([0,0,180])
	our_ClipHook(ye=0.8);
    }
  }

  linear_extrude(height=h) {
    assign($fn=80) {
      FlatArc(0,0, rad-w/2,rad+w/2, 80,350);
    }
    assign($fn=30) {
      FlatArc(0,rad+looprad+w, looprad,looprad+loopw);
    }
  }

  for (mir=[0,1]) {
    mirror([mir,0,0])
      rotate([0,0,-40])
      translate([rad+w*0.3+teethw*0.3+fdia/2, 0, 0])
      rotate([0,0,95])
      FilamentTeeth(fdia=fdia, h=h);
  }
}

FilamentClip();
