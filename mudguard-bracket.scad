// -*- C -*-

include <utils.scad>

wings_dist_x = 40;
wings_cup_angle = 40;
wings_cup_depth = 5;
wings_cup_inner = 3;
wings_inner_th = 1;
wings_main_th = 3.5;
wings_cup_z = -34;
retainer_z = -20; // XXXX
retainer_adjust = 15;
retainer_radius = 30;
bracket_adjust = 3;

tubeswidth_top = 23.5;
tubeswidth_bot = 29;
tubeswidth_dz = 26;
tube_dia = 18;
screw_hole_z = 13;

screw_dia = 4 + 0.75;

beam_h = 13;
beam_th = 4.5;
wings_height = 15;
retainer_th = 3;
clamp_main_th = 4;
clamp_depth = 4;
bracket_buttress = 2.5;

retainer_x_gap = 0.25;
retainer_big_gap = 2.5;

$fs = 0.1;
$fa = 3;

// calculated

function tubeswidth_at_z(z) =
  tubeswidth_bot +
  (tubeswidth_top - tubeswidth_bot) * (z / tubeswidth_dz);

bracket_bot_z = screw_hole_z - beam_h/2;
bracket_top_z = screw_hole_z + beam_h/2;
bracket_bot_xsz = tubeswidth_at_z(bracket_bot_z) + tube_dia * 2.5;
bracket_top_xsz = tubeswidth_at_z(bracket_top_z) + tube_dia * 2.5;

wing_top_ea_x = -tubeswidth_top/2 - wings_main_th/2;

retainer_ea_x = wing_top_ea_x + wings_main_th/2 + retainer_x_gap;
retainer_z_ctr = retainer_z - retainer_radius;
retainer_void_w = -retainer_ea_x*2 - retainer_th*2;;
retainer_tab = wings_height - beam_th;

tube_angle = atan2((tubeswidth_bot - tubeswidth_top)/2, tubeswidth_dz);

tube_ctr_y = tube_dia/2;
clamp_near_y = tube_ctr_y + tube_dia/2 - clamp_depth;
clamp_far_y = tube_ctr_y + tube_dia/2 + clamp_main_th;

buttress_x_ea = wing_top_ea_x + wings_main_th/2;

wing_corners = [
//		[ -wings_dist_x/2, wings_cup_z/2 ],
		[ wing_top_ea_x, 0 ],
		[ wing_top_ea_x, bracket_top_z - wings_main_th/2 ]
		];

wing_elevation_cup_outside_circle =
  [ -wings_cup_inner/2 - wings_main_th/2, wings_cup_inner/2 ];

module Oval(r, adjust_total){
  hull(){
    for (dy = [-1,+1] * adjust_total * 0.5)
      translate([ 0, dy ])
	circle(r = r);
  }
}

module AtWingCupElevation () {
  translate([ -wings_dist_x/2, wings_cup_z ])
    rotate(-wings_cup_angle)
    children();
}

module WingCupElevationNegative () {
  AtWingCupElevation(){
    hull(){
      translate([ 0, wings_cup_inner/2 ])
	circle(r = wings_cup_inner/2);
      translate([0, 20])
	square(wings_cup_inner, center=true);
    }
  }
}

module WingCupElevationPositive () {
  AtWingCupElevation(){
    hull(){
      translate([ wings_cup_inner/2 + wings_inner_th/2, 0 ]){
	translate([ 0, wings_cup_depth - wings_inner_th/2 ])
	  circle(r = wings_inner_th/2);
	translate([ 0, wings_cup_inner/2 ])
	  square([wings_inner_th, 0.1], center=true);
      }
    }
    translate([ (wings_inner_th - wings_main_th)/2,
		wings_cup_inner/2 ]){
      intersection(){
	circle(r = (wings_inner_th + wings_main_th + wings_cup_inner)/2);
	rectfromto([-100, -100], [100, 0]);
      }
    }
  }
}

module WingCircle(){
  circle(r = wings_main_th/2);
}

module WingElevationCupToWingJoinCircle(){
  translate(wing_elevation_cup_outside_circle + [ 0, wings_cup_depth ])
    WingCircle();
}

module WingElevation(){
  hull(){
    AtWingCupElevation(){
      translate(wing_elevation_cup_outside_circle)
	WingCircle();
      WingElevationCupToWingJoinCircle();
    }
  }
  hull(){
    AtWingCupElevation()
      WingElevationCupToWingJoinCircle();
    translate(wing_corners[0])
      WingCircle();
  }
  for (i = [0: len(wing_corners)-2]) {
    hull(){
      for (j = [0, 1]) {
	translate(wing_corners[i + j])
	  WingCircle();
      }
    }
  }
}

module BracketButtressPlan(){
  difference(){
    hull(){
      rectfromto([ -bracket_top_xsz/2, -beam_th ],
		 [ +bracket_top_xsz/2, 0        ]);
      rectfromto([ -wing_top_ea_x, -wings_height ],
		 [ +wing_top_ea_x, 0 ]);
    }
    rectfromto([ -buttress_x_ea, -beam_th - retainer_th - retainer_big_gap ],
	       [ +buttress_x_ea, -beam_th ]);
  }
}

module BracketBeamElevation(){
  hull(){
    translate([ 0, bracket_bot_z ])
      rectfromto([ -bracket_bot_xsz/2, 0 ],
		 [ +bracket_bot_xsz/2, 1 ]);
    translate([ 0, bracket_top_z ])
      rectfromto([ -bracket_top_xsz/2, -1 ],
		 [ +bracket_top_xsz/2, 0 ]);
  }
}

module Bracket(){ ////toplevel
  difference(){
    union(){
      linextr(0, wings_height) {
	for (m=[0,1])
	  mirror([m, 0]) {
	    WingElevation();
	    WingCupElevationPositive();
	  }
      }
      linextr(0, beam_th)
	BracketBeamElevation();
      linextr_y_xz(bracket_bot_z, bracket_bot_z + bracket_buttress)
	rotate(180)
	BracketButtressPlan();
      linextr_y_xz(bracket_top_z - bracket_buttress, bracket_top_z)
	rotate(180)
	BracketButtressPlan();
    }
    linextr(-1, wings_height+1) {
      for (m=[0,1])
	mirror([m, 0])
	  WingCupElevationNegative();
      translate([ 0, screw_hole_z ])
	Oval(screw_dia/2, bracket_adjust);
    }
  }
}

module RetainerElevation(){
  difference(){
    union(){
      rectfromto([ -retainer_ea_x, retainer_z_ctr ],
		 [ +retainer_ea_x, bracket_top_z + retainer_adjust/2 ]);
      // TODO make it round at bottom
    }
    translate([ 0, screw_hole_z ])
      Oval( screw_dia/2, retainer_adjust );
    translate([ 0, retainer_z_ctr ])
      circle(r = retainer_radius);
    rectfromto([ -retainer_void_w/2, retainer_z + retainer_th ],
	       [ +retainer_void_w/2, 
		 screw_hole_z - retainer_adjust/2 ]);
  }
}

module Retainer(){ ////toplevel
  linextr(0, retainer_th) {
    RetainerElevation();
  }
  linextr(0, retainer_tab) {
    intersection(){
      RetainerElevation();
      translate([ 0, retainer_z_ctr ])
	circle(r = retainer_radius + retainer_th);
    }
  }
}

module Tubes(dr=0, dyz=0){ ////toplevel
  for (m=[0,1]) {
    mirror([m,0,0])
      translate([ -tubeswidth_bot/2 - tube_dia/2, tube_ctr_y, 0 ])
      rotate([ 0, tube_angle, 0])
      linextr(-50, 100)
      translate(dyz * [1,1])
      circle(r = tube_dia/2 + dr);
  }
}

module Clamp(){ ////toplevel
  difference(){
    intersection(){
      hull(){
	Tubes(dr=clamp_main_th);
	Tubes(dr=clamp_main_th, dyz=tube_dia/2 );
      }
      linextr(bracket_bot_z, bracket_top_z)
	rectfromto([ -100, clamp_near_y ],
		   [ +100, clamp_far_y ]);
    }
    Tubes();
    translate([0, 0, screw_hole_z])
      linextr_y_xz(-50, 50)
      circle(r = screw_dia/2);
  }
}

module ElevationDemo(){ ////toplevel
  translate([0,0,2]) color("red") WingCupElevationNegative();
  translate([0,0,1]) color("blue") WingElevation();
  WingCupElevationPositive();
  BracketBeamElevation();
  AtWingCupElevation(){
    translate([0,0,-2]) color("grey") {
      rectfromto([0,0], [10,10]);
      rectfromto([0,0], -[10,10]);
    }
  }
}

module Demo(){ ////toplevel
  color("blue")
    rotate([90,0,0])
    Bracket();
  color("red")
    rotate([90,0,0])
    translate([ 0, 0, beam_th + 1 ])
    Retainer();
  color("green")
    Clamp();
  %Tubes();
}
