// -*- C -*-
// edit nutbos.scad.m4, not nutbos.scad!
// shaft, nut_across, nut_thick, nut_recess, wall, ceil

nutbox_data_M4 = [
		   4.0 + 0.5,
		   6.89 + 0.45,
		   3.10 + 0.75,
		   0.75,
		   2.0,
		   2.5
		   ];

nutbox_data_M3 = [
		   3.0 + 0.5,
		   5.48 + 0.45,
		   2.26 + 0.75,
		   0.75,
		   1.8,
		   2.0
		   ];

m4_define(shaft,      (nutbox_data[0]))
m4_define(nut_across, (nutbox_data[1]))
m4_define(nut_thick,  (nutbox_data[2]))
m4_define(nut_recess, (nutbox_data[3]))
m4_define(wall,       (nutbox_data[4]))
m4_define(ceil,       (nutbox_data[5]))

m4_define(nut_dia, (nut_across / cos(30)))
m4_define(outer_size, (nut_dia + wall * 2))
m4_define(h_base, (ceil + nut_thick + nut_recess))

function NutBox_shaft(nutbox_data) = shaft;
function NutBox_outer_size(nutbox_data) = outer_size;
function NutBox_h_base(nutbox_data) = h_base;
function NutBox_wall(nutbox_data) = wall; // not sure why anyone needs this

module NutBox(nutbox_data, h, h_above_extra=0) {
  // origin is centre of top of mount
  // entrance is to positive y
  // height is h which must be at least h_base
  // can be mad extra tall (with hole all the way through) with h_above_extra
  w = outer_size;
  difference(){
    mirror([0,0,1]) translate([-w/2,-w/2, -h_above_extra])
      cube([w,w, h + h_above_extra]);
    mirror([0,0,1]) translate([0,0,-1 -h_above_extra])
      cylinder(r = shaft/2, h = h+2 + h_above_extra, $fn=20);
    for (offset = [ [0,0, -nut_recess],
		    [0, outer_size, 0] ]) {
      hull(){
	for (toffset = [[0,0,0], offset]) {
	  translate(toffset)
	    translate([0,0, -ceil])
	    mirror([0,0,1])
	    rotate([0,0, 360/6/2])
	    cylinder(r = nut_dia/2,
		     h = nut_thick,
		     $fn = 6);
	}
      }
    }
  }
}
