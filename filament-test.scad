// -*- C -*-
translate([3,3,0]) mirror([1,1,0]) cube([15,15,1]);

multmatrix([[	1,	0,	0,	0	],
	[	0,	1,	1.0,	0	],
	[	0,	0,	1,	0	],
	[	0,	0,	0,	1	]])
 cylinder(r=6.1/2, h=8);

w=0.5;

translate([15,0])
difference(){
  cube([8,8,8]);
  translate([w,w,-1]) cube([8-w*2, 8-w*2, 8+2]);
}
