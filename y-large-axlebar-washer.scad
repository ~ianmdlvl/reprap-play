// -*- C -*-

$fa=3;
$fs=0.1;

r0 = 12 + 0.75;
r1 = 22;
h = 7.36 - 0.20;

linear_extrude(height=h, convexity=3) {
  difference(){
    circle(r = r1/2);
    circle(r = r0/2);
  }
}
