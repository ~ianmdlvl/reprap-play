// -*- C -*-
//
// filamentteeth.scad
//
// 3D design for clips to hold FFF filament
// Copyright 2012,2016 Ian Jackson
//
// This work is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This work is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this work.  If not, see <http://www.gnu.org/licenses/>.


d=0.01;
teethw=1.5;

module FilamentTeeth(fdia, h, teethw=teethw,
			 stembendd=0.5,	stembendl=7, teethxl=1.5) {
  gapw = fdia-stembendd*2;
  teethbigw = gapw + teethw*2;
  basew = fdia+teethw*2-stembendd*2;
  based = basew/3;

  translate([-based, -basew/2, 0]) cube([based, basew, h]);
  difference() {
    union() {
      translate([-d, -teethbigw/2, 0])
	cube([d+stembendl + teethw, teethbigw, h]);
//  translate([
//  stembigw = fdia + stembend
//  translate([-d, -stemw, 
      linear_extrude(height=h) {
	translate([stembendl-fdia/2, 0]) circle(fdia/2+teethw, $fn=30);
      }
    }
    translate([0,0,-1]) {
      translate([0,-gapw/2])
	cube([stembendl+teethxl+1, gapw, h+2]);
      linear_extrude(height=h+2) {
	translate([stembendl-fdia/2, 0]) circle(fdia/2, $fn=30);
      }
    }
  }

  for (mirr=[0:1]) {
    mirror([0,mirr,0]) {
      translate([stembendl + teethw, gapw/2, 0])
	rotate([0,0,30])
	cube([teethxl, teethw, h]);
    }
  }
}

