// -*- C -*-

include <commitid.scad>

cable_dias = [6.5, 8.2];

cd = cable_dias[1] + 0.5;
wall = 2.5;

function Gland_xlen(cabledia)    = cabledia * 1.5;
function Gland_xdia(cabledia)    = cabledia * 2.0;
function Gland_xoutdia(cabledia) = Gland_xdia(cabledia) * 1.1 + 0.5;

// origin is centre, on outside
// outside is in direction of positive X axies
module GlandNegative(cabledia){
  xlen = Gland_xlen(cabledia);
  xdia = Gland_xdia(cabledia);

  hull(){
    rotate([0,90,0]) cylinder(r= cabledia/2, h=1);
    translate([xdia,0,0]) rotate([0,90,0]) cylinder(r= xdia/2, h=1);
  }
  translate([-10,0,0])
    rotate([0,90,0])
    cylinder(r= cabledia/2, h=11);
}

module GlandPositive(cabledia){
  translate([-0.1, 0,0])
    rotate([0,90,0])
    cylinder(r= Gland_xoutdia(cabledia)/2, h= Gland_xlen(cabledia) + 0.1);
}  

platesz = [wall, 24, 28];
plateoff = [-platesz[0]/2, -platesz[1]/2, -platesz[2] + platesz[1]/2];

module Plate(){
  difference(){
    union(){
      GlandPositive(cd);
      translate(plateoff)
	cube(platesz);
    }
    GlandNegative(cd);
  }
}

module Test(){ ////toplevel
  Plate();
  translate(plateoff){
    difference(){
      cube([15, 20, 1.2]);
      Commitid_BestCount_M([15, 20]);
    }
  }
}

//Test();
