// -*- C -*-

// shape parameters

r1 =  2.85;
r2 =  4; a2 = 27;
r3 = r2;
r4 =  4; a4 = 18;
r5 = 30;
h6 = 7; a6 = 4;

// coordinates

                                z1 =  0;
h2 = r2 * sin(a2);              z2 = z1 - h2;
h3 = r3 * sin(a2);              z3 = z2 - h3;
h4 = r4 * sin(a4);              z4 = z3 - h4;

zc5 = z4 - r5 * sin(a4);
z5 = zc5 + r5 * sin(a6);        z6 = z5 - h6;

x1 =  0 - r1;
x2 = x1 + r2 * (1-cos(a2));
x3 = x2 + r3 * (1-cos(a2));
x4 = x3 - r4 * (1-cos(a4));

xc5 = x4 + r5 * cos(a4);
x5 = xc5 - r5 * cos(a6);

x6 = x5 - h6 * tan(a6);

htotal = r1 - z6;
echo("height", htotal);

d = 0.01;
dx = 0.00;

$fa=2;
$fs=0.2;

module SegmentBasisSquare(zmin, zmax, xmin){
  translate([xmin, zmin-d]) square([-xmin+dx, zmax-zmin+d*2]);
}
module ConvexSegment(xc, zc, r, zmin, zmax){
  intersection(){
    translate([xc,zc]) circle(r=r);
    SegmentBasisSquare(zmin,zmax,-50);
  }
}
module ConcaveSegment(xc, zc, r, zmin, zmax){
  difference(){
    SegmentBasisSquare(zmin,zmax, xc);
    translate([xc,zc]) circle(r=r);
  }
}

module PawnTemplate(){
  ConvexSegment(  x1 + r1,  z1,   r1, z1, 50);
  ConvexSegment(  x1 + r2,  z1,   r2, z2, z1);
  ConcaveSegment( x3 - r3,  z3,   r3, z3, z2);
  ConcaveSegment( x3 - r4,  z3,   r4, z4, z3);
  ConvexSegment(  xc5,      zc5,  r5, z5, z4);
  polygon([[x6, z6],
	   [x5, z5+d],
	   [dx, z5+d],
	   [dx, z6]]);
}

module Pawn(h=htotal){
  scale(h/htotal) {
    rotate_extrude(convexity=10, $fn=50){
      assign($fn=undef){
	PawnTemplate();
      }
    }
  }
}

Pawn(h=30);
//PawnTemplate();
