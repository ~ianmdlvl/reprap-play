// -*- C -*-

tokenrad=13;
tokenthick=1.9;

joinwidth=1.0;

circlerad=15;

module Letter() {
  translate([-circlerad,-circlerad])
    import("question-question.dxf", convexity=100);
}

module Token() { ////toplevel
  rotate([0,180,0])
  linear_extrude(height=tokenthick) union(){
    difference(){
      %circle(tokenrad);
      Letter();
    }
  }
}

Token();
