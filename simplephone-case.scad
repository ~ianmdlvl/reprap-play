// -*- C -*-

psz = [
       120,
       56 + 5 - 3.75,
       15 + 3,
       ];

thick = [
	 2,
	 2,
	 1.5,
	 ];

btn_x = 59.6;
btn_dia = 13;
btn_y = 14.03;

abtn_x = 46.85;
abtn_sz = [ 11, 13 ];

screen_xbot = 79;
screen_sz = [ 35, 46 ];

thumb_xbot = 90;
thumb_dia = 25;

vol_xbot = 86.5;
vol_xtop = 107.5;
vol_depth = 1.0;
vol_zsz = 9;
vol_zoff = 0;

rail_ysz = 2.5;
rail_zsz = 2.5;

stay_height = 1.49;

case_x_less = 0; //case_x_less = 10;

inner_cnr_rad = 4.0;

// calculated

btn_yprop = btn_y / psz[1];
echo(btn_yprop);

ym = psz[1]/2;
outer_cnr_rad = inner_cnr_rad + thick[2];

x_sliced = outer_cnr_rad * (1-sin(45));

$screen = true;

module RoundedProfile(sz, cnr_rad){
  hull(){
    for (x=[ cnr_rad, sz[0]-cnr_rad ])
      for (y=[ cnr_rad, sz[1]-cnr_rad ])
	translate([x,y])
	  circle(r= cnr_rad, $fn=20);
  }
}

module RoundedCube(sz, cnr_rad){
  if ($test)
    cube(sz);
  else hull(){
    for (x=[ cnr_rad, sz[0]-cnr_rad ])
      for (y=[ cnr_rad, sz[1]-cnr_rad ])
	for (z=[ cnr_rad, sz[2]-cnr_rad ])
	  translate([x,y,z])
	    sphere(r= cnr_rad, $fn=40);
  }
}

module Stay(xbot, xtop, width, midgap_width) {
  translate([ (xbot+xtop)/2, psz[1]/2, psz[2] ]){
    difference(){
      cube([ xtop-xbot, width, stay_height*2 ], center=true);
      if (midgap_width > 0)
	cube([ 200, midgap_width, 10 ], center=true);
    }
  }
}

module Stays(){
  Stay(  76, 82, 10, 0);
  Stay(-0.1, 55, 10, 0);
  Stay( 113,125, 70, 15);
}

module Case(){
  difference(){
    mirror([1,0,0])
      translate(-thick +
		- [1,0,0] * x_sliced)
      RoundedCube(psz
		  + 2*thick
		  - [1,0,0] * (thick[0])
		  + [1,0,0] * (x_sliced)
		  - [case_x_less, 0, 0],
		  outer_cnr_rad);

    for (yp= [ btn_yprop, 1-btn_yprop ])
      translate([ -btn_x,
		  yp * psz[1],
		  0.5 * psz[2] ])
	cylinder(r= btn_dia/2, h=20);

    translate([ -abtn_x,
		btn_yprop * psz[1],
		psz[2] ])
      cube(concat(abtn_sz, [ thick[2]*3 ]), center=true);

    if ($screen)
      mirror([1,0,0])
      translate([ screen_xbot,
		  (psz[1] - screen_sz[1])/2,
		  psz[2]-3 ])
      cube(concat(screen_sz, [ thick[2]+6 ]));

    hull(){
      for (x=[ thumb_xbot+thumb_dia/2, psz[0]+10 ])
	translate([ -x,
		    ym,
		    -thick[2]-1 ])
	  cylinder(r= thumb_dia/2,
		   h= thick[2] + 2,
		   $fn= 20);
    }

    mirror([1,0,0])
      translate([ (vol_xbot+vol_xtop)/2, 0, psz[2]/2 + vol_zoff ])
      cube([ vol_xtop-vol_xbot, vol_depth*2, vol_zsz ], center=true);

    translate([ thick[0], -10, -10 ])
      cube([ 10, psz[1]+20, psz[2]+20 ]);

    //translate([-50,-50,10]) cube([100,100,100]);

    mirror([1,0,0])
      difference(){
	RoundedCube(psz + [1,0,0],
		    inner_cnr_rad);

        Stays();

	if (0) for (m=[0,1]) {
	  translate([0,ym,0]) mirror([0,m,0]) translate([0,-ym,0])
	    translate([-1,-1, psz[2]-rail_zsz])
	    cube([psz[0]+1, rail_ysz+1, rail_zsz+1]);
	}
      }
  }
}

module TestLoop(){
  intersection(){
    Case($screen=false);
    translate([ -vol_xbot, 0,0 ])
      cube([ 4, 200,200 ], center=true);
  }
}

Case();
//TestLoop();
//RoundedCube(psz, inner_cnr_rad);
