// -*- C -*-

holespc = 20;

len = 50;
width = 20;
backspc = 8;
extra_height = 2;
light_height = 12.5;
hole_from_back = 7.2 + 0.5;
hole_dia = 3.5 + 0.5;
bolthead_dia = 7 + 1.0;
bolthead_depth = 5 + 20 - 12 + 2.7/2;

plasfix_dia = 4.5 + 0.5 + 1.1;
plasfix_head = 8.7 + 0.5 + 1.1;
plasfix_headdep = plasfix_dia;
plasfix_sink = 8;

height = light_height + backspc + extra_height;

octagon_fudge = 1/cos(22.5);

echo("remain", width - bolthead_depth);

module Bracket(){
  difference(){
    translate([-len/2, 0, 0])
      cube([len, width, height]);
    for (xsgn=[-1,+1]) {
      translate([xsgn * holespc/2, -1,
		 light_height - hole_from_back + extra_height]) {
	rotate([-90,0,0]) {
	  rotate([0,0,360/8/2]) {
	    cylinder(r=bolthead_dia/2 * octagon_fudge,
		     h= bolthead_depth +1, $fn=8);
	    cylinder(r=hole_dia/2 * octagon_fudge,
		     h=50, $fn=8);
	  }
	}
      }
    }
    translate([0, width/2, 0]) {
      cylinder(r= plasfix_dia/2, h=50, $fn=20);
      translate([0,0,-1])
	cylinder(r= plasfix_head/2, h= plasfix_sink + 1, $fn=20);
    }
  }
}

module BracketPrint(){
  rotate([0,180,0])
    Bracket();
}

BracketPrint();
