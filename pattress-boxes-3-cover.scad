// -*- C -*-

patbox_side = 87;
patbox_centres = 60.3;

lid_thinboxbase_overlap = 5;
lid_fatbox_overlap = 12;

lid_thinbox_h = 9;
lid_fatbox_h = 24;
lid_fatbox_switches_h = 6+4;

lid_max_switches_w = 70;
lid_switches_y_slop = 3;

total_len = 260;
thinbox_len = 87;

rail_overlap = 8;

lid_top_wall = 1.5;
lid_front_wall = 1.5;
lid_side_wall = 1.5;

peg_engage_depth = 1;
peg_engage_dia = 6.5;
peg_main_dia = 9;
peg_max_dia = 15;
peg_inner_dia = 3.5;
peg_top_thick = 1;
peg_straight_len = 3;

$peg_inner_slop = 0.75;
$peg_outer_slop = -0.9;
$peg_outer_slop_engage = 0.1;

peg_slope = 1;

lid_side_slop = 0.5;
lid_rail_behindslop = 0.5;
lid_rail_strongwall = 2.5;

// computed

lid_inner_max_h =
  lid_thinboxbase_overlap + lid_fatbox_h + lid_fatbox_switches_h;
lid_inner_min_h = lid_fatbox_overlap + lid_fatbox_switches_h;

lid_inner_kink = [thinbox_len, thinbox_len*2];

lid_inner_w_nom = patbox_side;
lid_inner_w = lid_inner_w_nom + lid_side_slop * 2;

lid_seatline_w = (lid_inner_w - lid_max_switches_w)/2 - lid_switches_y_slop;

lid_seatline_h = lid_fatbox_switches_h;

peg_main_height = peg_straight_len + (peg_max_dia - peg_main_dia)/2/peg_slope;

thinbox_front_z = lid_fatbox_switches_h + lid_fatbox_h - lid_thinbox_h;

raillen = patbox_side/2 + rail_overlap;

module LidSideProfile(){
  polygon([[-lid_top_wall,     lid_inner_max_h],
	   [min(lid_inner_kink[0],total_len), lid_inner_max_h],
	   [min(lid_inner_kink[1],total_len), lid_inner_min_h],
	   [total_len,         lid_inner_min_h],
	   [total_len,         -lid_front_wall],
	   [-lid_top_wall,     -lid_front_wall]]);
}

module RailProfile(lslop=0.1){
  yt_base = thinbox_front_z;
  yt = yt_base - lid_rail_behindslop;
  pegx = (lid_inner_w_nom - patbox_centres)/2;
  sw = lid_rail_strongwall;
  
  polygon([[-lslop,                  yt],
	   [pegx - peg_main_dia/2, yt],
	   [pegx - peg_main_dia/2, yt_base - peg_straight_len],
	   [sw,
	    yt_base - peg_straight_len - (pegx - peg_main_dia/2)/peg_slope
	    +sw],
	   [sw,
	    lid_seatline_h - 1],
	   [-lslop,
	    lid_seatline_h - 1]]);
}

module LidSide(){
  overlap = [0.1, 0.1, 0.1];

  // main side profile
  rotate([90,0,0])
    linear_extrude(height= lid_side_wall)
    LidSideProfile();

  // seatline
  translate(-overlap)
    cube(overlap + [total_len, lid_seatline_w, lid_seatline_h]);

  // lid front
  translate([-0.1, -0.1, -lid_front_wall])
    cube([total_len+0.1, lid_inner_w/2 + 0.2, lid_front_wall]);

  // lid top
  translate([-lid_top_wall, -lid_side_wall, -lid_front_wall])
    cube([lid_top_wall, lid_inner_w/2 + 10, lid_front_wall + lid_inner_max_h]);

  // rail
  rotate([90,0,90])
    translate([0,0,-0.1])
    linear_extrude(height=raillen+0.1) //todo
    RailProfile();

  // rail end
  translate([raillen, 0,0])
    intersection(){
      rotate_extrude($fn=50)
	RailProfile(lslop=0);
      translate([0, 25-0.1, 0])
	cube(50, center=true);
    }
}

module Lid(){ ////toplevel
  for (m=[0,1])
    mirror([0,m,0])
      translate([0, -lid_inner_w/2, 0]) LidSide();
}

module PegProfile(){
  polygon([[-peg_engage_depth, (peg_engage_dia - $peg_outer_slop_engage)/2],
	   [0,                 (peg_engage_dia - $peg_outer_slop_engage)/2],
	   [0,                 (peg_main_dia - $peg_outer_slop)/2],
	   [peg_straight_len,  (peg_main_dia - $peg_outer_slop)/2],
	   [peg_main_height,   (peg_max_dia - $peg_outer_slop)/2],
	   [peg_main_height+peg_top_thick, (peg_max_dia - $peg_outer_slop)/2],
	   [peg_main_height+peg_top_thick, (peg_inner_dia + $peg_inner_slop)/2],
	   [-peg_engage_depth,  (peg_inner_dia + $peg_inner_slop)/2]]);
}

module Peg(){ ////toplevel
  echo($peg_outer_slop_engage);
  rotate_extrude($fn=50)
    rotate([0,0,-90])
    PegProfile();
}

module Pegs(){ ////toplevel
  baseslop = 0.1;
  dslops = [0, -0.5, -1.0, -1.5];
  stride = peg_max_dia + 4;
  for (i=[0:len(dslops)-1]) {
    translate([i*stride,0,0])
      assign($peg_outer_slop_engage= baseslop + dslops[i])
      Peg();
  }
}

module AtFixingCentres(){
  for (fc=[-1,+1]) {
    translate([patbox_side/2 + fc*patbox_centres/2,
	       patbox_side/2,
	       0])
      children();
  }
}

module TopPattressBox(){
  difference(){
    translate([0,0, -lid_thinbox_h])
      cube([patbox_side, patbox_side, lid_thinbox_h]);
    AtFixingCentres(){
      translate([0,0,-10]) cylinder(r=peg_engage_dia/2, h=20);
    }
  }
}

module Demo(){
  Lid();
  translate([0,0, thinbox_front_z])
    rotate([0,180,0]) translate([0, -patbox_side/2, 0])
    rotate([0,0,90]) union(){
      %TopPattressBox();
      color("blue") AtFixingCentres(){
	rotate([180,0,0]) Peg();
      }
  }
}

//LidSide();
//PegProfile();
//Peg();
//Pegs();
//TopPattressBox();
//RailProfile();
//Demo();
//Lid();
//translate([0,0,-lid_fatbox_switches_h]) Lid();
