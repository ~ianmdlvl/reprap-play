// -*- C -*-

frameth=0.8;

l0= 0.425;
l1= 0.25;

del=l0+l1*2;

for (r=[0,90,180,270])
  rotate([0,0,r])
  translate([10,-(10+frameth),0])
    cube([frameth, 20+2*frameth, 0.4]);

translate([-11,-11,0])
cube([2,2, del + l1]);

translate([-3,-3, del])
  cube([6,6, l1*2]);
