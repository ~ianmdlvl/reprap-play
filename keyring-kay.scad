// -*- C -*-

letterheight = 25;
linewidth = 3.5;
letterthick = 2.5;

basethick = 2.5;

xborder = 5;
yborder = 5;

kdiag = 1;
kprop = 0.50;

diaglinewidth = linewidth * sqrt(1 + kdiag*kdiag);

ringholerad = 2.5;
ringedgewidth = 3;

module kay_leg (transamount, llen, mir) {
  translate([0,transamount,0])
    mirror([0,mir,0])
    translate([0,-0.1,0])
    multmatrix([[1,kdiag,0,0],
		[0,1,0,0],
		[0,0,1,0],
		[0,0,0,1]])
    cube([diaglinewidth, llen + 0.1, letterthick]);
}  

module kay () {
  translate([0.1,0,0])
    cube([linewidth, letterheight, letterthick]);
  kay_leg(letterheight*kprop, letterheight*(1-kprop), 0);
  kay_leg(letterheight*kprop, letterheight*kprop, 1);
}

totalw = letterheight*kprop + diaglinewidth + xborder*2;
totalh = letterheight + yborder*2;
basez = -(basethick-0.1);

module main () {
  kay();
  translate([-xborder, -yborder, basez])
    cube([totalw, totalh, basethick]);
}

module ring (rad, extra) {
  translate([totalw/2 - xborder, totalh - yborder, basez-extra])
    cylinder(r=rad, h=basethick + extra*2, $fn=30);
}

difference(){
  union() {
    main();
    ring(ringholerad + ringedgewidth/2, 0);
  }
  ring(ringholerad, 1);
}
