# reprap-objects Makefile
#
# Build scripts for various 3D designs
# Copyright 2012-2023 Ian Jackson
#
# This work is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This work is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this work.  If not, see <http://www.gnu.org/licenses/>.

FILAMENTSPOOL_AUTOS = filamentspool filamentspool-lt filamentspool-sm
FILAMENTSPOOL_AUTOS += filamentspool-storarm3

QUACKSES = $(addprefix quacks-ingredients-L, 1 2 3 4 5)
QUACKS_SCADS = $(addsuffix .scad, $(QUACKSES))

USING_AUTOS ?= $(FILAMENTSPOOL_AUTOS) xeno-drivebay-bracket dungeonquest-cone anke-gps-bracket cable-hole-trunking-cover anglepoise-neck crossbar-computer-led-mount wardrobe-hook knifeblock pandemic-counter pattress-boxes-3-cover bike-lipo-box earring-stand bike-stalk-led-mount sewing-table sewing-table-test sewing-table-jig maglite-holder poster-tube-lid poster-tube-lid-coarse fairphone-case fairphone-battery-case fairphone4-case fairphone4-case-coarse lock-inframe-bracket ksafe-base $(QUACKSES) quacks-ingredients-demos mic-table-clamp nook-case nook-case-test scaffold-clamp-common scaffold-clamp-tensioner scaffold-clamp-linear-bracket scaffold-clamp-straphook powerbank-bike-clamp topeak-mtx-tortec-expeditionrack-adapter lipo-flat-mount laptop-sound-cable-hooks digispark-with-cable chimney-cable-retainer mudguard-bracket waring-blender-motor-coupler $(foreach x,500 1000,adafruit-powerboost-$x)

AUTO_INCS += sealing-box.scad sewing-table.scad nutbox.scad \
	     powerbank-anker-10000.dxf \
	     poster-tube-lid-parametric.scad \
	     $(QUACKS_SCADS)

AUTO_STLS_INCS += poster-tube-lid,CatchPostDistort-fa3.stl
AUTO_STLS_INCS += poster-tube-lid,CatchPostDistort-fa20.stl

include diziet-utils/reprap-objects.make

dovecliptest.stl: doveclip.scad $(AUTO_INCS)

KNIFEBLOCK_KNIVES= 0 1 2
KNIFEBLOCK_TEMPLATES= bl hl
KNIFEBLOCK_TEMPLATE_FILES=\
	$(foreach k,$(KNIFEBLOCK_KNIVES), \
	$(foreach t,$(KNIFEBLOCK_TEMPLATES), \
	knifeblock-knives-t$k$t.dxf))

knifeblock-knives-templates knifeblock.stl: $(KNIFEBLOCK_TEMPLATE_FILES)

.PRECIOUS: knifeblock-knives-t%.dxf
knifeblock-knives-t%.dxf: knifeblock-knives-filter knifeblock-knives-trace.fig
		./$< $(notdir $*) <$(filter %.fig, $^) >$@.tmp.fig
		fig2dev -D -30 -L eps <$@.tmp.fig >$@.tmp.eps
		pstoedit -dt -f "dxf: -polyaslines -mm" $@.tmp.eps $@

PANDEMICCOUNTER_LETTERS=30 31 32 33 34 35
PANDEMICCOUNTER_DXFS=$(foreach l,$(PANDEMICCOUNTER_LETTERS), \
	pandemic-counter-l$l.dxf)

pandemic-counter-letters: $(PANDEMICCOUNTER_DXFS)
pandemic-counter%.stl: $(PANDEMICCOUNTER_DXFS)

.PRECIOUS: pandemic-counter-l%.eps
pandemic-counter-l%.eps: pandemic-counter-letters.fig
		fig2dev -D +$(notdir $*) -L eps <$< >$@.tmp
	        @$i

.PRECIOUS: maglite-holder-torch-curve.eps
maglite-holder-torch-curve.eps: maglite-holder-torch.fig
		fig2dev -D +1:70 -L eps <$< >$@.tmp
	        @$i

maglite-holder-torch-curve.dxf: maglite-holder-torch-curve.eps
		pstoedit -dt -flat 0.05 -f "dxf: -polyaslines -mm" $< $@

powerbank-anker-10000.dxf: powerbank-anker-10000.eps
		pstoedit -dt -f "dxf: -polyaslines -mm" $< $@

PANDEMICQUARANTINES_NUMBERS=1 2
PANDEMICQUARANTINES_DXFS=$(foreach l,$(PANDEMICQUARANTINES_NUMBERS), \
	pandemic-quarantine-l$l.dxf)

pandemic-quarantine-numbers: $(PANDEMICQUARANTINES_DXFS)
pandemic-quarantine%.stl: $(PANDEMICQUARANTINES_DXFS)

.PRECIOUS: pandemic-quarantine-l%.eps
pandemic-quarantine-l%.eps: pandemic-quarantine-numbers.fig
		fig2dev -D +$(notdir $*) -L eps <$< >$@.tmp
	        @$i

FILAMENTSPOOL_NUMBERS=$(shell seq 300 100 1500)
filamentspool-number-n%.eps:	filamentspool-number.eps.pl
	./$< $* $o

FILAMENTSPOOL_DXFS=$(foreach n,$(FILAMENTSPOOL_NUMBERS), \
	filamentspool-number-n$n.dxf)

$(addsuffix .auto.stl, $(foreach f,$(FILAMENTSPOOL_AUTOS),$(shell \
	$(DUTILS)/toplevel-find $(CWD)/$f))): $(FILAMENTSPOOL_DXFS)

filamentspool-numbers filamentspool.stl: $(FILAMENTSPOOL_DXFS)

SCREWRECESSTEST_SIZES= 2 3 4 5 6
SCREWRECESSTEST_DXFS=$(foreach s,$(SCREWRECESSTEST_SIZES), \
	screw-recess-test-number-s$s.dxf)

screw-recess-test-number-s%.fig: screw-recess-test-number.fig.pl
	./$< $* $o

screw-recess-test-number-s%.eps: screw-recess-test-number-s%.fig
		fig2dev -L eps <$< >$@.tmp
	        @$i

screw-recess-test-numbers screw-recess-test.stl: $(SCREWRECESSTEST_DXFS)

question-question.eps: question-question.fig
		fig2dev -L eps <$< >$@.tmp
	        @$i

sewing-table%.stl: sewing-table-rear-profile.dxf
sewing-table%.stl: sewing-table-front-profile.dxf
sewing-table%.stl: sewing-table-end-profile.dxf

sewing-table-%-profile.eps: sewing-table-%-profile.fig
		fig2dev -L eps -D +40 <$< >$@.tmp
	        @$i

question-token.stl: question-question.dxf

lemon-stand.stl: lemon-stand.scad

electron-token.stl: electron-token.scad

quacks-scads: $(addsuffix .auto.scads, $(QUACKSES))
quacks-scads: quacks-ingredients-demos.auto.scads

quacks-stls: $(addsuffix .auto.stls, $(QUACKSES))

.PRECIOUS: $(SCREWRECESSTEST_DXFS) $(SCREWRECESSTEST_DXFS) \
	$(foreach s,$(SCREWRECESSTEST_SIZES), \
		screw-recess-test-number-s$s.fig \
		screw-recess-test-number-s$s.eps)

poster-tube-lid,CatchAssembly.auto.stl: poster-tube-lid,CatchPostDistort-fa3.stl
poster-tube-lid-coarse,CatchAssembly.auto.stl: poster-tube-lid,CatchPostDistort-fa20.stl

poster-tube-lid,CatchPostDistort-fa%.stl: \
		distort-stl poster-tube-lid,CatchPreDistort.auto.stl
	./distort-stl <poster-tube-lid,CatchPreDistort.auto.stl \
		set-fa $(notdir $*) project-cylinder 100 >$@.tmp
	$i

