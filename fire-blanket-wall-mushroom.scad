// -*- C -*-

fudge=0.15;

screwrad = 4.5 / 2 + fudge; // xxx check
shaftrad = 7.5 / 2 - fudge;

diskrad = 12.0 / 2 - fudge;

csinkpart = 0.5;

shaftlen = 8; // xxx check

diskthick = 1.5;

disktaperratio = 2;

// computed

disktaperrad = diskrad - diskthick / disktaperratio;
totallen = shaftlen + diskthick;

module SidePlan(){
  polygon([[-screwrad, 0],
	   [-disktaperrad, 0],
	   [-diskrad, -diskthick],
	   [-shaftrad, -diskthick],
	   [-shaftrad, -totallen],
	   [-screwrad, -totallen]]);
}

module Bush(){
  rotate_extrude($fn=25, convexity=3){
    SidePlan();
  }
}

//SidePlan();
rotate([0,180,0])
  Bush();
