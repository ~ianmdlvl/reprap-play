// -*- C -*-

tokenrad=13;
tokenthick=1.9;

joinwidth=1.0;

circlerad=15;

module Letter(depth) {
  translate([-circlerad,-circlerad])
    import(file=str("pandemic-counter-l",depth,".dxf"), convexity=100);
}

module Token(depth) {
  rotate([0,180,0])
  linear_extrude(height=tokenthick) union(){
    difference(){
      circle(tokenrad);
      Letter(depth);
    }
    child();
  }
}

module Token_CDC(){ ////toplevel
  Token(30){};
}
module Token_Lab(){ ////toplevel
  Token(31){};
}
module Token_Act(){ ////toplevel
  Token(32){
    translate([0, 1])
      square([tokenrad*.75, joinwidth], center=true);
  }
}
module Token_Spec(){ ////toplevel
  Token(33){};
}
//module Token_Terr(){ ////toplevel
//  Token(34){};
//}
//module Token_TerrMove(){ ////toplevel
//  Token(35){
//    translate([-tokenrad*.75, -1])
//      square([tokenrad*.75, joinwidth]);
//  };
//}

spacing = tokenrad * 2 + 2;

module Tokens(rows=1,cols=1) {
  for (i=[0:rows-1])
    for (j=[0:cols-1])
      translate([j*spacing, i*spacing, 0])
	child(0);
}

module Tokens_Act(){ ////toplevel
  // Print *twice*, LAPIS BLUE or SQUEEZED ORANGE
  // ordinary actions
  //  up to 4 for 5 players, plus 2 for Borrowed Time plus 1 for Generalist
  //  so need 23, make 24
  Tokens(4,3) Token_Act();
}

module Tokens_Spec(){ ////toplevel
  // ELECTRIC BLUE or MELLOW YELLOW
  // once-per-turn special action, one each for 5 players
  Tokens(3) Token_Spec();
  translate([spacing,0,0]) Tokens(2) Token_Spec();
}

module Tokens_CDC(){ ////toplevel
  // STORM GREY
  // CDC
  // 1 action per turn + 2 Borrowed Time
  Tokens(3) Token_CDC();
}

module Tokens_Lab(){ ////toplevel
  // WHITE
  // free Lab action (on building research station, etc)
  // make 2 (probably want less than that)
  Tokens(2) Token_Lab();
}

//module Tokens_Terr(){ ////toplevel
//  // FIRE TRUCK RED
//  // Bioterrorist general actions
//  Tokens(2) Token_Terr();
//}

//module Tokens_TerrMove(){ ////toplevel
//  // CLASSIC BLACK
//  // Bioterrorist drive/ferry
//  Tokens(1) Token_TerrMove();
//}

module PosToken(i,j){
  translate([j*spacing, i*spacing, 0]) child();
}

module Demo(){ ////toplevel
  PosToken(0,0) Token_CDC();
  PosToken(1,0) Token_Lab();
  PosToken(2,0) Token_Act();
  PosToken(3,0) Token_Spec();
//  PosToken(1,1) Token_Terr();
//  PosToken(2,1) Token_TerrMove();
}

//Tokens_Act();
//Demo();
