// -*- C -*-

basel = 16;
basew = 24;
baset = 4.0;

wallt = 2.5;

wallh = 42;

baseholesz = 3.7;
baseholeslot = 6.5;
baseholeslop = -0.5;

holeslop = 0.5;

webt = 2.5;

pad = false;
padw = 12;

padt = webt;
padl = padw;
padholesz = 3.0;

wallholeh = 6+14+2;
wallholesz = 3.0;
wallholeslot = 4.5;

walll = basel + webt + (pad ? padl : -0.1);

webw = min(basew, pad ? padw : padt);

module slothole(sz, slot, thick, csunk=true, slop=holeslop) {
  hull(){
    for (y = [-slot/2,slot/2]) {
      translate([0,y,-0.15])
	cylinder(r1=sz/2 + slop,
		 r2=sz/2 + (csunk ? thick : 0) + slop,
		 h=thick+0.30);
    }
  }
}

module Bracket(){
  difference(){
    translate([0, -basew, 0])
      cube([basel, basew, baset]);

    translate([basel/2, -(basew+wallt)/2, 0])
      slothole(baseholesz, baseholeslot, baset, slop=baseholeslop);
  }

  difference(){
    translate([0.1, 0.3, 0.1])
    rotate([90,0,0]) {
      linear_extrude(height=wallt){
	polygon([[0,0],
		 [0, wallh/2 + wallholesz/2 + wallt + wallt],
		 [basel, wallh],
		 [walll, wallh],
		 [walll, wallh - padt - padt],
		 [basel + webt, 0]]);
      }
    }

    translate([basel/2, 0, wallholeh])
      rotate([90,90,0])
      slothole(wallholesz, wallholeslot, wallt, csunk=false);
  }

  translate([basel-0.01, 0, 0]) {
    rotate([90,0,90]) {
      linear_extrude(height=webt+0.02) {
	polygon([[-basew, 0],
		 [-basew, baset],
		 [-webw, wallh],
		 [0, wallh],
		 [0, 0]]);
      }
    }
  }

  if (pad) {
    translate([basel+webt, -padw, wallh-padt]) {
      difference(){
	cube([padl, padw, padt]);
	translate([padl/2, padw/2, -1])
	  cylinder(r=padholesz/2 + holeslop, h=padt+2);
      }
    }
  }
}

module BracketR(){ ////toplevel
  rotate([-90,0,0]) Bracket();
}

module BracketL(){ ////toplevel
  mirror([1,0,0]) BracketR();
}

protinnerh = 47;
protinnerw = 53;
protd = 45;
protbaset = 4;
protwallt = 2;
protlidt = protwallt;
protwingd = 28;
protwingw = 23;

module RearCableProtector(){
  for (x = [-protwallt, protinnerw]) {
    translate([x, 0, 0]) {
      cube([protwallt, protd, protinnerh+protlidt]);
    }
  }
  translate([-(protwallt-0.1), 0, protinnerh])
    cube([protinnerw + (protwallt-0.1)*2, protd, protlidt]);
  for (lr = [1,0]) {
    translate([(lr ? -(protwingw + protwallt) : protinnerw), 0, 0]) {
      difference(){
	translate([0, 0, 0])
	  cube([protwingw, protwingd, protbaset]);
	translate([protwingw/2, protwingd/2, 0])
	  rotate([0,0, lr ? 45 : -45])
	  slothole(baseholesz, baseholeslot, baset, slop=baseholeslop);
      }
    }
  }
}

module RearCableProtectorT(){ ////toplevel
  rotate([90,0,0]) RearCableProtector();
}

chabd = 20;
chablidw = 40;
chabinnerh = 11;
chabwallt = 2;
chablidt = 2;
chabwebt = 2.5;
chabbaset = baset;
chabbasew = 20;
chabslot = 3;
chablidholed = 3;
chabwebh = 5;

module ChannelBracket(){
  translate([0, -chabd, 0])
    cube([chabwallt, chabd, chabinnerh+chablidt]);
  translate([-chablidw, -chabd, chabinnerh]) {
    difference(){
      cube([chablidw + chabwallt - 0.1, chabd - 0.1, chablidt]);
      translate([chablidw/2, chabd/2, -1])
	cylinder(r=chablidholed/2, h=chablidt+2, $fn=20);
    }
  }
  translate([chabwallt-0.1, -chabd, 0]) {
    difference(){
      cube([chabbasew, chabd-0.1, chabbaset]);
      translate([chabbasew/2, (chabd-chabwebt)/2, 0])
	rotate([0,0,90])
	slothole(baseholesz, chabslot, baset, slop=baseholeslop);
    }
  }
  rotate([90,0,0]) linear_extrude(height=chabwebt) {
    polygon([[-chablidw, chabinnerh],
	     [-chablidw, chablidt+chabinnerh],
	     [-chabwebh, chablidt+chabinnerh+chabwebh],
	     [+chabwebh, chablidt+chabinnerh+chabwebh],
	     [+chabbasew, chabbaset],
	     [+chabbasew, 0],
	     [0, 0],
	     [0, chabinnerh]]);
  }
}

module ChannelBracketT(){
  rotate([-90,0,0]) ChannelBracket();
}

module Kit(){ ////toplevel
  for (y=[0, -wallh-5]) {
    translate([0,y,0]) {
      translate([5,0,0]) BracketR();
      BracketL();
    }
  }
}

//Kit();
//BracketR();
//RearCableProtectorT();
//ChannelBracketT();
