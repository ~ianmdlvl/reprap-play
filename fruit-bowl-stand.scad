// -*- C -*-

include <utils.scad>

across = 12.5;
r0 = 71;
h = 36;
feet = 5;

// calculated

r_eff_across = across/2 * cos(360/8/2);

effective_corner = across/2 * [ sin(360/8/2), cos(360/8/2) ];

r_mid = r0 + effective_corner[0];

h_mid = h - effective_corner[1] - r_eff_across;;

module XSection(){
  rotate(360/8/2)
    circle(r= across/2, $fn=8);
}

module Ring(){
  rotate_extrude($fa=1)
    translate([r_mid, 0,0])
    XSection();
}

module Foot(){
  rotate([180,0,0])
    linear_extrude(h_mid)
    XSection();
}

module Stand(){
  Ring();
  intersection(){
    for (a=[0:feet-1]) {
      rotate([0,0, 360*a/feet])
	translate([-r_mid, 0,0])
	Foot();
    }

    linextr(-across - h, across)
      circle(r= r_mid + r_eff_across, $fa=1);
  }
}

Stand();
