// -*- C -*-

// brk_*: "bracket", the Topeak MTX bracket
// rack_*: the Tortec rack
// adapt_*: the adapter, ie this file

include <utils.scad>

// strength factor, set to 1 for real prints
//$strf = 0.33;
$strf = 1;

brk_recess_actual = 5.20;

rack_rail_dia = 10.40 + 0.30;
rack_width_inner = 115.86 - 1.0; // between insides of rails

rear_elevation_nominal = 10.04;
// ^ top of rack to bottom of bracket, at rack cross rail (fam)
rear_to_front_distance = 230; // rack cross rail (fam) to very front end
rear_to_cross_rail = 43.05; // bolt centre to rail centre, rail to rear
rear_bolt_to_front_bolt = 155.4;
front_elevation_nominal = 0; // this parameter adjusts rear too somehow?

cross_rail_distance = 232.09;

general_gap_y = 1.0;
support_bridge_gap_z = 1.0;

strap_w = 8.0 + 1.0;
strap_th = 2.5;
strap_barrel_dia = 14;
strap_guide_sz = 1;

brk_block_xw = 68.5;
brk_block_z = 14.55 - 0.00;

brk_bolt_dia = 5.0 + 0.5;
brk_nearbolt_recess_dia = 8.86 + 1.5;
brk_nearbolt_recess_depth = 1.09 + 0.25;

bolt_nut_around = 5;
bolt_nut_around_y_extra = 3;

brk_bolt_eff_len = 11.78; // inside of recess, to end of bolt
brk_bolt_len_slop = 0.5;
brk_bolt_nut_th = 3.89;
brk_bolt_nut_across_flats = 7.86 + 0.50;

brk_overall_w = 90.07;

fit_slope_len = 5;

// "foreaftmaint" aka "fam" is the hook-like part that stops
// the adapter sliding forwards/backwards along the rails
foreaftmaint_r_slop = 0.0;
foreaftmaint_y_slop = -0.25;
foreaftmaint_top_block_zs = [34.0, 39.0]; // rearwards from bolt hole

main_sz_y = $strf * 12;
grasp_sz = $strf * 6;
grasp_thin_sz = $strf * 0.5;
beside_strap_sz = $strf * 8;
main_sz_core_z = $strf * 12;

// "length" in for-aft direction of interaction with rack rail
min_on_rail_sz_z = $strf * 18;

// when printer produces support
support_around = 1.7; // how far does the support extend around (in XY)
support_remnant = 0.75; // how much frass remains attached (Z height)

$fa=10;
$fs=1;

// calculated

bolt_z = -brk_block_z/2;

front_to_rear_elevation_change =
  rear_elevation_nominal - front_elevation_nominal;

main_sz_rhs_z = max(min_on_rail_sz_z, beside_strap_sz*2 + strap_w);
main_sz_lhs_z = min_on_rail_sz_z;

main_sz_x_fam = main_sz_y;

brk_bottom_y = -brk_recess_actual;
adapt_main_top_y = brk_bottom_y - general_gap_y;

// on LHS, so -ve
rack_rail_x = -(rack_width_inner/2 + rack_rail_dia/2);
rack_rail_outer_x = -(rack_width_inner/2 + rack_rail_dia);

grasp_large_r = (rack_rail_dia + grasp_sz)/2;
grasp_small_r = (rack_rail_dia + grasp_thin_sz)/2;
grasp_large_x = rack_rail_outer_x + grasp_large_r;
grasp_small_x = rack_rail_outer_x + grasp_small_r;

block_x = grasp_large_x + grasp_large_r;
block_y_min = adapt_main_top_y - main_sz_y;

strap_barrel_x = rack_width_inner/2 + strap_barrel_dia/2;

rack_shear_ratio = - front_to_rear_elevation_change / rear_to_front_distance;

front_to_cross_rail =
  cross_rail_distance * sqrt(1 - rack_shear_ratio * rack_shear_ratio)
  - rear_bolt_to_front_bolt
  - rear_to_cross_rail
  - sqrt( pow( cross_rail_distance * rack_shear_ratio, 2 )
	  - pow(  front_to_rear_elevation_change, 2 ) )
  ;

brk_bolt_nut_top_y = -brk_nearbolt_recess_depth
  - brk_bolt_eff_len + brk_bolt_nut_th + brk_bolt_len_slop;
		       
brk_bolt_nut_r = brk_bolt_nut_across_flats/2 / cos(360/12);

function elevation_of_bolt_for(z) = rear_elevation_nominal
  + front_elevation_nominal
  + (z - brk_block_z/2) * rack_shear_ratio;

function rack_rail_y_of_elevation(elevation_nominal) =
  brk_bottom_y - elevation_nominal - general_gap_y  - rack_rail_dia/2;

echo(rack_shear_ratio);

module GraspElevation(){
  hull(){
    translate([ grasp_large_x, adapt_main_top_y - grasp_large_r ])
      circle(grasp_large_r);

    translate([ grasp_small_x, $rack_rail_y - rack_rail_dia/2 ])
      circle(grasp_small_r);

    translate([ rack_rail_x + grasp_large_r/2,
		$rack_rail_y - rack_rail_dia/2 ])
      circle(grasp_small_r);

    translate([ grasp_large_x, $rack_rail_y + rack_rail_dia/2 ])
      circle(grasp_large_r);

    translate([ grasp_large_x + grasp_large_r/2,
		$rack_rail_y + rack_rail_dia/2 ])
      circle(grasp_large_r);
  }
}

module BlockElevation(){
  hull(){
    rectfromto([ +block_x, adapt_main_top_y ],
	       [ -block_x, block_y_min ]);
    rectfromto([ -grasp_large_x, adapt_main_top_y ],
	       [ +grasp_large_x, adapt_main_top_y - 0.1 ]);
  }
  hull(){
    rectfromto([ +block_x, adapt_main_top_y ],
	       [ -block_x, block_y_min ]);
    rectfromto([ grasp_large_x, block_y_min ],
	       [ 0, block_y_min + 0.1 ]);
  }
}

module MainExtrude(z){
  linextr(0, z)
    children();
}
module RackShear(){
  s = rack_shear_ratio * $reverse_sign;
  multmatrix([ [ 1, 0,  0, 0 ],
	       [ 0, 1, s , 0 ],
	       [ 0, 0,  1, 0 ],
	       [ 0, 0,  0, 1 ] ])
    children();
}

module GraspFixingElevation(){
  intersection(){
    union(){
      hull(){
	mirror([1,0]) {
	  GraspElevation();
	}
	translate([ -block_x, block_y_min ] + [0,0.1]*1 )
	  circle(0.1);
      }
      translate([ strap_barrel_x, $strap_barrel_y ])
	circle(strap_barrel_dia/2 + strap_guide_sz);
    }
    union(){
      rectfromto([0, $rack_rail_y],
		 [rack_width_inner, 50]);
      intersection(){
	translate([ rack_rail_x, $rack_rail_y ])
	  circle(r = rack_width_inner/2 - rack_rail_x);
	polygon([ [ -block_x-0.1, 0 ],
		  [ rack_width_inner/2, 0 ],
		  $rail_fixing_fit_corner,
		  $rail_fixing_fit_corner + [-1,-1] * fit_slope_len,
		  [ -grasp_large_x - grasp_large_r*2, block_y_min ],
		  [ -block_x-0.1 -2, block_y_min +2 ]]);
      }
    }
  }
}

module StrapBarrelElevation(){
  translate([ strap_barrel_x, $strap_barrel_y ])
    circle(strap_barrel_dia/2);
}

// Bracket support block, goes up inside bracket
// Z origin is bolt hole
module BrkBlock(){
  difference(){
    linextr( -brk_block_z/2,
	     +brk_block_z/2 ) {
      rectfromto([ -brk_block_xw/2, adapt_main_top_y - 0.1 ],
		 [ +brk_block_xw/2, 0 ]);
    }
    linextr_y_xz( -50, 10 ) {
      translate([ 0, brk_block_z + bolt_z ])
	square(center=true,
	       [ main_sz_x_fam + support_around*2,
		 support_remnant *2 ]);
    }
  }
}

// Z origin is bolt hole
module BoltHole(){
  linextr_y_xz( -100, 10 )
    circle(brk_bolt_dia/2);

  linextr_y_xz( -brk_nearbolt_recess_depth, 10)
    circle(brk_nearbolt_recess_dia/2);

  linextr_y_xz( -100, brk_bolt_nut_top_y ) {
    hull()
      for (dz = [0, support_bridge_gap_z])
	translate([0, dz])
	  circle( r= brk_bolt_nut_r, $fn = 6 );
  }
}

module IfFam(){
  if ($foreaftmaint_dz) {
    children();
  }
}

module FamLinextr(){
  IfFam()
    linextr_x_yz(-main_sz_x_fam/2, +main_sz_x_fam/2)
    rotate(-90)
    children();
}

module FamGraspElevation(){
  difference(){
    hull(){
      ybot = $rack_rail_y - rack_rail_dia/2 + grasp_large_r
	- fit_slope_len * 0.5;
      for (y = [
		ybot,
		adapt_main_top_y - grasp_large_r
		])
	for (dx= [/*-1,*/ +1] * rack_rail_dia/2)
	  translate([ -$foreaftmaint_rail_z + dx, y ])
	    circle(r= grasp_large_r);
    }
    if ($foreaftmaint_cutoff) {
      translate([ -$foreaftmaint_rail_z, 0 ])
	rectfromto([-100, -100],
		   [0, 100]);
    }
  }
}

module FamStemElevation(){
  hull(){
    rectfromto([ -$foreaftmaint_rail_z
		 , adapt_main_top_y ],
	       [ 0, block_y_min]);
    translate([
	       -$foreaftmaint_rail_z,
	       $rack_rail_y +
	       rack_shear_ratio * $foreaftmaint_rail_z * $reverse_sign,
	       ])
      square([0.1, rack_rail_dia * 0.5], center=true);
  }
}

module Principal(){
  // calculated
  $rack_rail_y = rack_rail_y_of_elevation($elevation_nominal);

  $strap_barrel_y = $rack_rail_y + rack_rail_dia/2 + strap_barrel_dia/2;

  $rail_fixing_fit_corner = [
    rack_width_inner/2,
    $rack_rail_y - rack_rail_dia/2
  ];

  $foreaftmaint_rail_z = brk_block_z/2 + $foreaftmaint_dz - foreaftmaint_y_slop;

  translate([0,0,brk_block_z/2])
  mirror([0,0, $reverse_sign > 0 ? 0 : 1])
  translate([0,0,-brk_block_z/2])
  difference(){
    union(){
      MainExtrude(main_sz_lhs_z){
	GraspElevation();
      }
      RackShear() MainExtrude(main_sz_rhs_z){
	StrapBarrelElevation();
      }
      translate([ 0,0, brk_block_z/2]) {
	BrkBlock();
      }

      difference(){
	union(){
	  MainExtrude(main_sz_core_z){
	    BlockElevation();
	  }
	  if ($strf<1) {
	    MainExtrude(max(brk_block_z, main_sz_rhs_z)){
	      rectfromto([-8, adapt_main_top_y + 0.1],
			 [+8, block_y_min]);
	      rectfromto([-block_x -5,  adapt_main_top_y],
			 [-grasp_large_x, block_y_min]);
	    }
	  }
	  RackShear() MainExtrude(main_sz_rhs_z){
	    GraspFixingElevation();
	  }
	}

	translate([0,0, main_sz_rhs_z/2]) linextr(-strap_w/2, +strap_w/2) {
	  translate([ rack_width_inner/2 - strap_th, 0 ])
	    rectfromto([ 0, -50 ], [ 50, 50 ]);
	}
      }

      FamLinextr(){
	if ($foreaftmaint_top_block) {
	  rectfromto([ -foreaftmaint_top_block_zs[0] + bolt_z, 0 ],
		     [ -foreaftmaint_top_block_zs[1] + bolt_z, block_y_min] );
	}
	FamGraspElevation();
      }
      intersection(){
	union(){
	  RackShear()
	    FamLinextr()
	    FamGraspElevation();
	  FamLinextr()
	    FamStemElevation();
	}
	translate([ 0,
		    adapt_main_top_y - 50,
		    $foreaftmaint_rail_z ])
	  cube(center=true, 100);
      }

      linextr_y_xz( block_y_min - bolt_nut_around_y_extra , adapt_main_top_y )
	intersection(){
	  translate([ 0, brk_block_z/2 ])
	    circle(r = bolt_nut_around + brk_bolt_nut_r );
	  rectfromto([-100, 0], [+100,+100]);
      }
    }

    RackShear() linextr(-10, main_sz_lhs_z+main_sz_rhs_z) {
      for (mx=[0,1]) {
	mirror([mx,0]) {
	  translate([ rack_rail_x, $rack_rail_y ]){
	    hull(){
	      for (dx = [-rack_rail_dia, 0])
		translate([dx, 0])
		  circle(r= rack_rail_dia/2);
	    }
	  }
	}
      }
    }

    RackShear() IfFam(){
      // Distance from bolt hole, in backwards direction
      cr = rack_rail_dia/2 + foreaftmaint_r_slop;
      translate([ 0, $rack_rail_y, $foreaftmaint_rail_z ])
	linextr_x_yz(+rack_rail_x,
		     -rack_rail_x) {
	hull(){
	  for (dy=[0,50]) {
	    translate([-dy,0])
	      circle(r= cr);
	  }
	}
	hull(){
	  for (dd=[[0,0], [-1,-1], [-1,+1]]) {
	    translate(
		      [-1, 0] * (rack_rail_dia - fit_slope_len)
		      + 20 * dd
		      )
	      circle(r= cr);
	  }
	}
      }
    }

    translate([ 0,0, brk_block_z/2]) BoltHole();
  }
}

module ForRackForDemo(){
  elevation = elevation_of_bolt_for(rear_to_cross_rail);
  rack_rail_y = rack_rail_y_of_elevation(elevation);

  rotate([atan(
	       front_to_rear_elevation_change /
	       cross_rail_distance
	       ), 0,0])
    translate([0, rack_rail_y, brk_block_z/2 + rack_rail_y*rack_shear_ratio])
    children();
}

module RackForDemoRails(){
  ForRackForDemo() {
    for (m=[0]) mirror([m,0,0]) {
      linextr(-(50 + cross_rail_distance), 50 + rear_to_cross_rail)
	translate([rack_rail_x, 0])
	circle(r= rack_rail_dia/2);
    }
  }
}

module RackForDemoCrosses(){
  ForRackForDemo() {
    for (z = [
	      rear_to_cross_rail,
	      rear_to_cross_rail - cross_rail_distance,
	      ]) {
      translate([0,0,z])
	linextr_x_yz(rack_rail_x, -rack_rail_x)
	circle(r= rack_rail_dia/2, $fn=8);
    }
  }
}

module Front(){ ////toplevel
  rotate([180,0,0])
  Principal($reverse_sign = -1,
	    $foreaftmaint_top_block = false,
	    $foreaftmaint_cutoff = true,
	    $elevation_nominal=
      elevation_of_bolt_for(rear_to_cross_rail + rear_bolt_to_front_bolt),
	    $foreaftmaint_dz= front_to_cross_rail);
}

module Rear(){ ////toplevel
  Principal($reverse_sign = +1,
	    $foreaftmaint_top_block = true,
	    $foreaftmaint_cutoff = false,
	    $elevation_nominal=
      elevation_of_bolt_for(rear_to_cross_rail),
	    $foreaftmaint_dz= rear_to_cross_rail);
}

module SomeDemo(){
  rotate([90, 0, 0]){
    children();

    color("blue")
      translate([ 0, -2, -4 ])
      square(center=true, [ brk_overall_w, 1 ]);

    color("red")
      translate([ 0, -brk_nearbolt_recess_depth, -4 ])
      linextr_y_xz(-brk_bolt_eff_len, 0)
      circle(r = brk_bolt_dia/2);

  }
}

module FrontDemo(){ ////toplevel
  SomeDemo() rotate([180,0,0]) Front();
}
module RearDemo(){ ////toplevel
  SomeDemo() Rear();
}
module RearRackDemo(){ ////toplevel
  rotate([atan(rack_shear_ratio),0,0]) SomeDemo() {
    Rear();
    translate([0, 0, -rear_bolt_to_front_bolt])
      rotate([180,0,0]) Front();
    %RackForDemoRails();
    color("blue") RackForDemoCrosses();
  }
}
