// -*- C -*-

// should rename this to actual name of the product

include <utils.scad>

mount_lip_height = 2.0 - 0.15 - 0.15;
mount_lip_depth = 2.5 /*?*/ - 0.30;
mount_neck_width = 26.5 - 0.55 - 0.15;
mount_neck_length = 1.5 + 0.50;

mount_diag_outer = 34.8        - 0.50;
mount_diag_inner = 34.6 - 0.20 - 0.50;

mount_slope = .65;
mount_extra_slope = 3;

mount_demo_ceil = 4;

// calculated

mnep0 = [0,0];
mnep1 = mnep0 + [0,1] * mount_neck_length;
mnep7 = mnep0 + [1,0] * mount_lip_depth;
mnep2 = [ mnep7[0] + mount_extra_slope, mnep1[1] + mount_slope * (mnep7[0] + mount_extra_slope - mnep1[0]) ];
mnep3 = mnep2 + [0, 0.1];
mnep4 = [ mnep0[0]-1, mnep3[1] ];
mnep6 = mnep7 + [0,-1] * mount_lip_height;
mnep5 = [ mnep4[0], mnep6[1] ];
mnepm = [ mnep0[0], mnep3[1] ];

mount_total_height = mnep2[1] - mnep6[1];
mnep_z_offset = -mnep2[1];
mnep_side_offset = [ mount_neck_width/2, mnep_z_offset ];

module MountNeckEdgePlan() {
  polygon([ mnep0,
	    mnep1,
	    mnep2,
	    mnep3,
	    mnep4,
	    mnep5,
	    mnep6,
	    mnep7 ]);
}

module MountNeckSquare() {
  intersection_for (r=[0,90]) {
    rotate([0,0,r]){
      linextr_y_xz(-100,100,convexity=10){
	for (m=[0,1]) {
	  mirror([m,0]) {
	    translate(mnep_side_offset) MountNeckEdgePlan();
	    rectfromto([-0.1, -mount_total_height],
		       mnep_side_offset + mnepm);
	  }
	}
      }
    }
  }
}

module MountDiagonal() {
  rotate([0,0,45]){
    translate([0,0, -mount_total_height]){
      linextr(0, mount_lip_height)
	square(center=true, mount_diag_outer);
      linextr(0, mount_total_height)
	square(center=true, mount_diag_inner);
      linextr(mount_lip_height + mount_neck_length,
	      mount_total_height + 1)
	square(center=true, 100);
    }
  }
}

module MountDemoCeil() {
  c = mount_demo_ceil + mount_extra_slope;
  linextr(0, 0.8) {
    square(mount_neck_width + 2*(mount_demo_ceil + mount_extra_slope),
	   center=true);
  }
}

module Mount(){
  intersection(){
    MountNeckSquare();
    MountDiagonal();
  }
}

module MountDemo(){ ////toplevel
  Mount();
  MountDemoCeil();
}

//MountNeckEdgePlan();
//MountNeck();
//MountDemoCeil();
//MountDiagonal();
//MountDemo();
