// -*- C -*-

include <commitid.scad>

rnom = 3.5 / 2;

// alpha is slope angle, which is half of inner concave angle that
//  wire sits in
alpha = 40; // degrees

// mu is minimum number of cable radii that cable tangent point (line)
//  with splint ought to be away from edge of split
mu = 1/4;

// wall thickness, and base width as fraction of cable size
wall_r = 3.5 / 6.5;
base_r = 0.75;

total_len = 60;

strap_width = 3.0 + 0.5;

strap_count = 4;

// for cross-section calculations:
//
// origin O is at intersection of straight line segments forming walls
// C is centre of circle (wire x-section) (of radius r or radius 1)
//       which is tangent to lines
// T is said tangent points
// B is inner base point, which is extension of line from B by mu*r

sina = sin(alpha);
cosa = cos(alpha);
tana = sina/cosa;

// blah_r is blah where r=1
// d_AB is distance AB
// dy_AB is " " " vertical component

d_OT_r = tana;
d_OB_r = tana + mu;

d_OC_r = 1/cosa;

dy_OB_r = d_OB_r * sina;

// *0 and *1 relate to smallest and largest wire
// r[01] is radius
// r10 is radius ratio

r10 = d_OC_r / dy_OB_r;

r0 = rnom / sqrt(r10);
r1 = rnom * sqrt(r10);

x_B_r =   d_OB_r * cosa;
y_B_r = -dy_OB_r;

x_T_r =         sina;
y_T_r = -tana * sina;

wall_x_r = wall_r / tan(90-alpha);

top = wall_r * r1 - (d_OC_r - 1) * r0;
basew = base_r * rnom;

echo("dias", r0*2, r1*2, "ratio",r1/r0);

module CrossSectionHalf(plus=0) {
  difference(){
    polygon([[-0.1,                                y_T_r * r0],
	     [x_T_r * r0,                          y_T_r * r0],
	     [x_B_r * r1,                          y_B_r * r1],
	     [x_B_r * r1 + wall_x_r * rnom + plus, y_B_r * r1],
	     [basew                        + plus, top],
	     [-0.1,                                top]]);
    translate([0, -d_OC_r * r0])
      circle(r = r0, $fn=20);
  }
}

module CrossSection(plus=0) {
  for (m=[0,1]) {
    mirror([m,0])
      CrossSectionHalf(plus);
  }
}

module CrossSectionDemo(){ ////toplevel
  color("black") CrossSection(2);
  CrossSection();
  for (rc=[["red", r1],
	   ["blue",r0]]) {
    color(rc[0]) translate([0, -d_OC_r * rc[1]]) circle(r = rc[1]);
  }
}

strap_wall_h = 1.5;
strap_wall_l = 2.0;

writing_dx = total_len / 3;
writing_dy = basew*2;

module HalfClamp(){ ////toplevel
  difference(){
    rotate([90,0,0])rotate([0,90,0]){
      linear_extrude(height=total_len)
	CrossSection();

      for (i=[0 : strap_count]){
	if (i*2 != strap_count) {
	  translate([0, 0,
		     total_len * (i + 0.5) / (strap_count + 1)])
	    for (m=[0,1]){
	      mirror([0,0,m])
		translate([0,0, strap_width/2])
		linear_extrude(height=strap_wall_l)
		CrossSection(strap_wall_h);
	    }
	}
      }
    }

    translate([0, -basew, top])
      Commitid_BestCount([writing_dx, writing_dy]);
  }
}

module HalfClampPrint(){ ////toplevel
  rotate([180,0,0])
    HalfClamp();
}

//CrossSection();
//CrossSectionDemo();
//HalfClamp();
HalfClampPrint();
