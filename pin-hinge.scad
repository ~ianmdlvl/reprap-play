// -*- C -*-

include <utils.scad>

$hinge_pin_dia = 0.795 + 0.75;
$hinge_main_dia = 4.0;
$hinge_inter_gap = 0.50;
$hinge_prong_minwidth = 3.0;
$hinge_noncrit_gap = 1.0;

$fa = 3;
$fs = 0.05;

module HingePinPlan(){
  circle(r= $hinge_pin_dia/2);
}

module HingeProngPlan(behind){
  hull(){
    circle(r= $hinge_main_dia/2);
    polygon([[0,0],
	     [-$hinge_main_dia/2, -behind],
	     [+$hinge_main_dia/2, -behind]]);
  }
}

module HingeGapPlan() {
  circle(r = $hinge_main_dia/2 + $hinge_inter_gap);
}

module PlanDemo(){
  HingeProngPlan(5);
  %HingeGapPlan();
  translate([0,0,1]) color("red") HingePinPlan();
}

module HingePinUnitCell(l) {
  eff_l = l + $hinge_inter_gap;
  pairs = floor(eff_l / (2*($hinge_prong_minwidth + $hinge_inter_gap)));
  stride = eff_l / pairs;
  $hinge__prong_width = stride/2 - $hinge_inter_gap;
  for (i=[0:pairs-1]) {
    translate(stride * i * [1,0,0])
      children(0);
  }
}

module HingePositive(l, behind){
  HingePinUnitCell(l){
    linextr_x_yz(0, $hinge__prong_width)
      HingeProngPlan(behind);
  }
}

module HingeNegative(l){
  linextr_x_yz(-0.1, l+0.1)
    HingePinPlan();
  HingePinUnitCell(l){
    linextr_x_yz($hinge__prong_width,
		 $hinge__prong_width*2 + 2*$hinge_inter_gap)
      HingeGapPlan();
  }
}

test_l = 30;
test_wb = 12;
test_h = 12;

test_face_gap = 0.75;

module Demo(){
  difference(){
    HingePositive(test_l, test_h/2);
    %HingeNegative(test_l);
  }
}

module TestBody(){
  linextr_x_yz(0, test_l){
    offset(delta = -test_face_gap/2)
      polygon([[0,0],
	       [-test_wb/2, -test_h],
	       [+test_wb/2, -test_h]]);
  }
}

module Test(){
  difference(){
    union(){
      TestBody();
      HingePositive(test_l, test_h/2);
    }
    HingeNegative(test_l);
  }
}

//PlanDemo();
//Demo();
//TestBody();
Test();
