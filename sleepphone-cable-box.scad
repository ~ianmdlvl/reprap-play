// -*- C -*-

include <funcs.scad>

wall = 0.75 * [1,1,1];
wall_bot = 1.0;

// old model:
//phone = [ 76.40, 30.96, 6.00 ]; // includes socket
// model bought in 2024:
phone = [ 77.32, 32.00, 6.93 ]; // includes socket
phone_button_z = 6.58;
minwall = 0.50;

expose = 4.00;

cutout_dia = 7;
cutout_between = 5;

button_dz = 1.35;
button_dz_centre = 1.35 + 0.75;
button_dz_outer  = 1.35;

button_dy_outer = 28.42;
button_dy_inner = 19.05;
button_dy_centre = 5.65;

nrbutton_brace_x = 37.5;

phone_slop = 0.5 * [1,1,0]
           + 0.5 * [0,0,1];

// old model:
//led = [25.9, 9.44]; // y is from edge
// model bought in 2024:
led = [phone[0] - 62, phone[1]/2]; // y is from edge
led_dia = 4.4;

// next values include slop
plug_maxw = 10.95 + 0.35;
plug_minw=   6.53 + 0.35;
plug_sllen=  6.50;
plug_totlen = 84.90 + 1.5 - 2.0; // to maxw, including phone

plug_h = 6.5;
plug_tooth_h = 0.5;
plug_tooth_dy = 0.5;

keeper_prong = 2;
keeper_stalk_basewidth = 12;
keeper_stalk_len = 70;
keeper_stalk_gap = 1;
keeper_stalk_thick = wall_bot;

keeper_stalk_base_reinforce_len = 5;
keeper_stalk_base_reinforce_thick = 2.0;

// calculated

top_z = max( phone[2] + wall[2],
	     phone_button_z + minwall )
  + phone_slop[2];

plugkeeper_x_maxw = phone[0] - plug_totlen;

plugkeeper_p_max = [ 0, plug_maxw/2 ];
plugkeeper_p_min = [ -plug_sllen, plug_minw/2 ];;
plugkeeper_d_u = unitvector2d(
                 clockwise2d(
                 vecdiff2d( plugkeeper_p_max, plugkeeper_p_min )
			     )
			     );

module MainProfileInnerHalf(){
  p = phone + phone_slop;
  pbc = p[2] + button_dz_centre;
  pbo = p[2] + button_dz_outer;
  polygon([[ -2,                 0    ],
	   [ p[1]/2,             0    ],
	   [ p[1]/2,             p[2] ],
	   [ button_dy_outer/2,  p[2] ],
	   [ button_dy_outer/2,  pbo  ],
	   [ button_dy_inner/2,  pbo  ],
	   [ button_dy_inner/2,  p[2] ],
	   [ button_dy_centre/2, p[2] ],
	   [ button_dy_centre/2, pbc  ],
	   [ -2,                 pbc  ]]);
}

module MainProfile(){
  p = phone + phone_slop;
  difference(){
    for (m=[0,1]) mirror([m,0]) {
	minkowski(){
	  translate([ -wall[1], -wall_bot ])
	    square([ wall[1]*2, wall_bot + wall[2] ]);
	  MainProfileInnerHalf();
	}
      }
    for (m=[0,1]) mirror([m,0]) {
	MainProfileInnerHalf();
      }
  }
}

module BraceProfileInitial(){
  p = phone + phone_slop;
  pbo = p[2] + button_dz_outer;
  pbc = p[2] + button_dz_centre;
  polygon([[ button_dy_outer/2 + 0.2,  p[2]           ],
	   [ button_dy_outer/2 + 0.2,  pbo  + wall[2] ],
	   [ button_dy_outer/2      ,  pbo  + wall[2] ],
	   [ button_dy_outer/2      ,  p[2]           ],
	   ]);
}  

module BraceProfileFinal(){
  p = phone + phone_slop;
  pbo = p[2] + button_dz_outer;
  pbc = p[2] + button_dz_centre;
  polygon([[ -1,                       p[2]           ],
	   [ -1,                       pbc  + wall[2] ],
	   [ 0,                        pbc  + wall[2] ],
	   [ 0,                        p[2]           ]
	   ]);
}  

module Brace(){
  for (m=[0,1]) mirror([0,m,0]) {
      hull(){
	for (e=[0,1]) {
	  translate([ nrbutton_brace_x + e * phone[1]/2,
		      0, 0 ]){
	    rotate([ 90,0,90 ]){
	      linear_extrude(height= (e==0 ? wall[0] : 0.1)){
		hull(){
		  BraceProfileInitial();
		  if (e==0) BraceProfileFinal();
	      }
	      }
	    }
	  }
	}
      }
    }
}

module BoxMain(){
  rotate([0,0,90]) rotate([90,0,0]) {
    translate([0,0, expose])
      linear_extrude(height = phone[0] + wall[0] - expose, convexity=20)
      MainProfile();
    translate([0,0, phone[0]])
      linear_extrude(height = wall[0], convexity=20)
      hull() MainProfile();
  }
}

module PlugKeeperProfileHalf(){
  p_max = plugkeeper_p_max;
  p_min = plugkeeper_p_min;
  d = plugkeeper_d_u * keeper_prong;
  
  translate([ plugkeeper_x_maxw, 0 ]) {
    polygon([ p_min,
	      p_max,
	      p_max + d,
	      p_min + d ]);
  }
}

module PlugKeeperStalkProfile(){
  hull(){
    for (m=[0,1]) mirror([0,m,0]) PlugKeeperProfileHalf();
    translate([ plugkeeper_x_maxw + keeper_stalk_len, 0,0 ])
      square([ 0.1, keeper_stalk_basewidth ], center=true);
  }
}

module PlugKeeper(){
  for (m=[0,1]) mirror([0,m,0]) {
      translate([0,0, -keeper_stalk_thick])
	linear_extrude(height=plug_h + keeper_stalk_thick)
	PlugKeeperProfileHalf();

      translate([0, 0, plug_h - plug_tooth_h])
	linear_extrude(height= plug_tooth_h)
	translate(plugkeeper_d_u * -plug_tooth_dy)
	PlugKeeperProfileHalf();

    }
}

module Box(){
  sidewall_cutout_z = phone[2] + phone_slop[2] + button_dz_outer;

  difference(){
    union(){
      BoxMain();
      Brace();
    }

    translate([ led[0], phone[1]/2 - led[1], 1 ])
      rotate([0,0, 360/8/2])
      cylinder(r = led_dia/2 / cos(360/8/2), h= phone[2]*2, $fn=8);

    for (ys=[-1,+1]) {
      translate([ -0.1, ys * keeper_stalk_gap, -wall[2]*2])
	linear_extrude(height = wall[2]*3)
	PlugKeeperStalkProfile();

      translate([ phone[0] + wall[0],
		  ys * (cutout_between/2 + cutout_dia/2),
		  -10 ])
	cylinder( r= cutout_dia/2, h = 50, $fn = 20 );

      translate([expose, ys*phone[1]/2, sidewall_cutout_z/2])
	rotate([90,0,0])
	translate([0,0,-3])
	cylinder( r= sidewall_cutout_z/2 - 0.1, h=6 , $fn=20 );
    }
  }

  PlugKeeper();

  translate([0,0, -keeper_stalk_thick])
    linear_extrude(height = keeper_stalk_thick)
    PlugKeeperStalkProfile();

  translate([ plugkeeper_x_maxw + keeper_stalk_len +
	       -keeper_stalk_base_reinforce_len/2,
	       -keeper_stalk_basewidth/2,
	       0 ])
    mirror([0,0,1])
    cube([ keeper_stalk_base_reinforce_len,
	   keeper_stalk_basewidth,
	   keeper_stalk_base_reinforce_thick ]);
}

module BoxPrint(){
  // This makes' Cura's support more optimal: specifically,
  // it then doesn't seem to touch the back (bottom) wall
  translate([0,0,phone[0]])
    rotate([0,90,0])
    Box();
}

//MainProfileInnerHalf();
//MainProfile();
//translate([0,0,1]) color("black") BraceProfileInitial();
//translate([0,0,1]) color("black") BraceProfileFinal();
//Brace();
//Box();
BoxPrint();
