// -*- C -*-

include <commitid.scad>

stalk_dia = 6.4 + 0.25;

length = 50;
width = 12;

strap_below = 2;
strap_thick = 2;
strap_width = 5;
strap_above = 0.25;

arch_above = 2;

inside_gap = 0.5;

// calculated

height_base = stalk_dia/2 - inside_gap/2;
above_height = height_base + arch_above;
below_height = height_base + max(arch_above,
				 strap_below + strap_thick + strap_above);

module StalkCutout(){
  translate([-length,0,0])
    rotate([0,90,0])
    cylinder(r= stalk_dia/2, h=length*2, $fn=40);
}

module SomeBlockBase(height){
  translate([0,0, height/2 + inside_gap/2]) {
    difference(){
      cube([length, width, height], center=true);
      translate([-length/2, 0, height/2])
	Commitid_BestCount([length*.66, width/2]);
    }
  }
}

module BlockAbove(){ ////toplevel
  difference(){
    SomeBlockBase(above_height);
    StalkCutout();
  }
}

module BlockBelow(){ ////toplevel
  difference(){
    SomeBlockBase(below_height);
    StalkCutout();
    translate([0,0, inside_gap/2 + strap_above + stalk_dia/2 + strap_thick/2])
      cube([strap_width, width*2, strap_thick], center=true);
  }
}

module BlockAbovePrint(){ ////toplevel
  rotate([180,0,0]) BlockAbove();
}

module BlockBelowPrint(){ ////toplevel
  rotate([180,0,0]) BlockBelow();
}

module Demo(){ ////toplevel
  BlockAbove();
  rotate([180,0,0]) BlockBelow();
}

//Demo();
