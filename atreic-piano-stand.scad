// -*- C -*-
height = 40;
depth = 20;
thick = 3;
width = 40;

difference(){
  cube([width, depth, height]);
  translate([thick, -1, thick])
    cube([width - thick*2, depth+2, height]);
}
