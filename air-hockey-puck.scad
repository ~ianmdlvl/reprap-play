// -*- C -*-

main_dia = 34.0;
post_base_dia = 14;
post_top_dia = 11;
overall_height = 31;
rim_height = 5;
rim_wall_th = 2;
base_th = 2;

$fs= 0.1;
$fa = 3;

module Elevation(){
  hull(){
    translate([0, overall_height - post_top_dia/2])
      circle(r = post_top_dia/2);
    square([ post_base_dia/2, 0.1 ]);
  }
  difference(){
    square([ main_dia/2, rim_height ]);
    translate([ -rim_wall_th, base_th ])
      square([ main_dia/2, rim_height ]);
  }
}

module Bat(){
  rotate_extrude(){
    intersection(){
      Elevation();
      square([100, 100]);
    }
  }
}

Bat();
