// -*- C -*-

include <funcs.scad>

// MainLoop

main_thick = 9.0;
main_in_dia = 28.9;

horn_ext_dia = 20 - 0.5;

horn_c_x = -4.6;
horn_c_dy= -4;

blhook_start_ang = 45;
blhook_in_rad = 1.85;
blhook_str_len = 2.9;

width = 20;

// Attach

at_bolt_into = 13.0 + 0.5;
at_tube_dia = 16.7 + 0.5;
at_prong_minw = 4;
at_rear_thick = 4.5;
at_bolt_dia = 5 + 0.5;

at_rear_width = at_tube_dia;
at_stem_len = main_in_dia/2 * 0.3;

at_prong_depth = at_bolt_into * 2;
at_gap_width = at_tube_dia * 0.75;

// computed

blhook_mid_rad = blhook_in_rad + main_thick/2;
mc_mid_rad = main_in_dia/2 + main_thick/2;

mc_bl = circle_point([0,0], mc_mid_rad, 270-blhook_start_ang);

at_block_x = at_tube_dia + at_prong_minw * 2;
at_block_y = at_prong_depth + at_rear_thick;
at_block_z = width;

at_stem_yy = at_stem_len + mc_mid_rad;

at_offset_y = at_block_y + at_stem_len + mc_mid_rad;

$fs=0.05;

horn_thick = main_thick;

module MainLoop(){
  intersection(){
    difference(){
      circle(r= main_in_dia/2 + main_thick, $fn=50);
      circle(r= main_in_dia/2, $fn=50);
    }
    polygon([[0,0],
	     3*mc_bl,
	     [0, -100],
	     [100,-100],
	     [100,100],
	     [0,100]]);
  }
  translate(mc_bl)
    circle(main_thick/2);
  translate([horn_c_x, mc_mid_rad + horn_c_dy])
    intersection(){
    difference(){
      circle(horn_ext_dia/2);
      intersection(){
	circle(horn_ext_dia/2 - horn_thick);
	polygon([[-50,-50],
		 [-50,-horn_c_dy],
		 [50,-horn_c_dy],
		 [50,-50]]);
      }
    }
    polygon([[0,0],
	      [-50,0],
	      [0,50]]);
  }
  translate([0,main_in_dia/2]) mirror([1,0])
    square([-horn_c_x + horn_ext_dia/2 * 0.75, main_thick]);
  translate(mc_bl){
    translate([-blhook_str_len/2, 0])
      square(center=true, [blhook_str_len, main_thick]);
    translate([-blhook_str_len, blhook_mid_rad]){
      intersection(){
	difference(){
	  circle(r=blhook_mid_rad + main_thick/2);
	  circle(r=blhook_mid_rad - main_thick/2);
	}
	mirror([1,1]) square(50);
      }
    }
  }
}

module MainLoopTest(){
  linear_extrude(height=1.6)
    MainLoop();
}

module Attach(){
  difference(){
    translate([0, at_block_y/2, 0])
      cube(center=true, [at_block_x, at_block_y, at_block_z]);
    translate([0, at_prong_depth/2-1, 0])
      cube(center=true, [at_gap_width, at_prong_depth+2, at_block_z+1]);
    translate([0,-1,0])
      rotate([-90,0,0])
      cylinder(r= at_tube_dia/2, h= at_prong_depth+1);
    translate([-50, at_prong_depth-at_bolt_into, 0])
      rotate([0,90,0])
      cylinder(r= at_bolt_dia/2, h= 100);
  }
  difference(){
    translate([0, at_block_y + at_stem_yy/2 - 0.1, 0])
      cube(center=true, [at_tube_dia, at_stem_yy + 0.2, at_block_z]);
    translate([0, at_offset_y, -50])
      cylinder(r = mc_mid_rad, 100);
  }
}

module Combine(){
  rotate([0,0,45]) translate([0,-main_thick/2,0]){
    linear_extrude(height=width)
      translate([0,at_offset_y,0])
      MainLoop();
    translate([0,0, width/2])
      Attach();
  }
}

Combine();
