// -*- C -*-

include <camera-mount.scad>

tr_cube_offset = 20;
tr_cube_sz = [20, 20, 15];
tr_around = 10;

module Mount(){
  translate([0,  - tr_cube_sz[1], 0])
  difference(){
    translate([0, tr_cube_sz[1]/2 - tr_cube_offset/2, tr_cube_sz[2]/2])
      cube(tr_cube_sz + [0, tr_cube_offset, 0], center=true);
    translate([0, tr_cube_sz[1]/2 - tr_cube_offset, 0])
      rotate([180,0,0])
      render() CameraMountThread(tr_cube_sz[2] + 1);
  }
}

module CaseMounted(){ ////toplevel
  difference(){
    render() Case();
    translate([ phone_width/2, -phone_height/2 ])
      linextr(-50, 50)
      square([phone_width, phone_height] - tr_around * 2 * [1,1],
	     center=true);
  }
  translate([ phone_width,
	      -phone_height + tr_cube_sz[0] * 0.7,
	      epp3[1] - case_th_bottom ])
    rotate([0,0,90])
    Mount();
}

//// toplevels-from:
include <fairphone4-case.scad>
$suppress_hinge = true;
