// -*- C -*-

include <commitid.scad>

baseh= 1;
basex = 31;
basey= 20;
basexpos = -12;
baseypos = -4;

module Body(){
  mirror([0,0,1])
    translate([basexpos, baseypos])
    cube([basex, basey, baseh]);
}

difference(){
  Body();
  translate([basexpos, baseypos, -baseh])
    Commitid_BestCount_M([basex,basey], margin=3);

  translate([-6, 6, -0.4])
    cylinder(r=5, h=3, $fn=50);
}

translate([-6, 6, -0.5])
  cylinder(r=4, h=3.5, $fn=50);

echo("pause height (mm)", baseh+0.01);

Commitid_FontDemo();
