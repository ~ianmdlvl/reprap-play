include <cliphook.scad>
include <filamentteeth.scad>

rad=7.7;
smrad=2.0;
h=3.5;
w=2.5;
teethw=1.5;

looprad=2.5;
loopw=w;

fdia=1.77;
//fdia=3;

d=0.01;

module FilamentClip() {
  linear_extrude(height=h) {
    assign($fn=80) {
      FlatArc(0,0, rad-w/2,rad+w/2, 80,361);
    }
    assign($fn=30) {
      FlatArc(0,rad+looprad+w, looprad,looprad+loopw);
    }
    FlatArc(0, rad-smrad, smrad-w/2,smrad+w/2, -55,91);
    FlatArc(rad-smrad, 0, smrad-w/2,smrad+w/2, 145,-1);
  }

  for (mir=[0,1]) {
    mirror([0,mir,0])
      mirror([1,0,0])
      rotate([0,0,-56])
      translate([rad+w*0.3+teethw*0.3+fdia/2 -0.6, -1.1, 0])
      rotate([0,0,95])
      FilamentTeeth(fdia=fdia);
  }
}

FilamentClip();
