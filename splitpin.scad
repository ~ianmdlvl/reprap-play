// -*- C -*-

include <cliphook.scad>

tau = 6.28318530718;
function deg2rad(deg) = deg/360 * tau;
function rad2deg(rad) = rad/tau * 360;

module SplitPin(w=1.5, holeminrad=2.50, thick=3, deviationrad=1.5,
		mainlen=15, handlerad=20, handlelen=12) {
  spare = holeminrad*2 - deviationrad - w*2;
  echo("splitpin spare",spare);
  %translate([0,mainlen+handlelen,0]) cylinder(r=spare, h=thick);
  %translate([0,mainlen,thick/2]) rotate([90,0,0])
     cylinder(r=holeminrad, h=thick);

  bent_dx = holeminrad;
  unbent_dx = bent_dx + deviationrad;

  unbent_subang = atan(unbent_dx / mainlen);
  unbent_rad = mainlen / deg2rad(unbent_subang);

  corner_x = unbent_rad * (1 - cos(unbent_subang));
  corner_y = unbent_rad * sin(unbent_subang);

  main_cx = unbent_rad;

//  translate([w*1.5, 0, 0]) {
//    translate([corner_x, corner_y, 10]) %cube([10,10,10]);
//    translate([bent_dx, 0, 10]) %cube([10,10,10]);
//    translate([unbent_dx, 5, 10]) %cube([10,10,10]);
//  }

  linear_extrude(height=thick) {
    for (xmir=[0,1]) mirror([xmir,0,0])
      FlatArc(0,0, w*0.5, w*1.5, 270-1,360);
    translate([w*1.5, 0, 0]) {
      FlatArc($fa=1, main_cx,0, unbent_rad, unbent_rad+w,
	      180-unbent_subang, 180);
      translate([corner_x, corner_y]) rotate([0,0,-unbent_subang]) {
	rotate([0,0,10])
	  translate([w*0.2,0,0])
	  translate([-(w + deviationrad), -0.1])
	  square(size=[w + deviationrad, w+0.1]);
	FlatArc(-deviationrad + handlerad, w,
		handlerad, handlerad+w,
		180-rad2deg(handlelen/handlerad), 180+rad2deg(w/handlerad),
		$fa=0.25, $fn=60);
      }
    }
    mirror([1,0,0]) translate([w*1.5, 0, 0])
      FlatArc($fa=1, main_cx,0, unbent_rad, unbent_rad+w,
	      180-(unbent_subang + rad2deg((handlelen+w)/unbent_rad)), 180);
  }
}

module SplitPinCavity(w=1.5, holeminrad=2.50, thick=3, deviationrad=1.5,
		      mainlen=15, slop=0.5, insertby = 5) {
  smallgap2 = holeminrad;
  biggap2 = smallgap2 + deviationrad + slop;
  toegap2 = w*1.5 + slop;
  toeend = -mainlen-insertby;

  translate([0,thick/2,0]) rotate([90,0,0]) {
    linear_extrude(height = thick + slop*2) {
    for (xmir=[0,1]) mirror([xmir,0]) {
	polygon([[-0.1, 1],
		 [(smallgap2+biggap2)/2, 1],
		 [smallgap2, -insertby],
		 [biggap2, -insertby],
		 [toegap2, toeend-1],
		 [-0.1, toeend-1]]);
      }
    }
  }
}

SplitPin();
translate([0,15+5,-10])
  rotate([-90,0,0])
  SplitPinCavity();
