// -*- C -*-

hoopthick = 3;

hinnerrad = 15;
houterrad = hinnerrad + hoopthick;
hcentredist = 10;

blockdepth = 5;
blockwidth = hcentredist*2 + 6;

height = 20;

roundedgedia = 7.5;

ziglen = hcentredist/2;

feedxgap = 5;
feedzgap = 5;
ribsgap = 1;

ribdepth = 3;
ribheight = 4;

backxgap = 1;

blockoverlapcnr = 5;

screwholedia = 4 + 0.5;

module Oval(centredist, rad) {
  hull() {
    translate([-centredist/2,0,0]) circle(r=rad);
    translate([+centredist/2,0,0]) circle(r=rad);
  }
}  

module VExtrude(){
  translate([0,0, -height/2])
    linear_extrude(height=20)
    children(0);
}

module OuterOval(){
  Oval(hcentredist, houterrad);
}

module Hoop(){
  difference(){
    hull(){
      OuterOval();
      translate([0, (blockdepth + hoopthick)/2 + hinnerrad])
	square([blockwidth,
		blockdepth + hoopthick],
	       center=true);
    }
    Oval(hcentredist, hinnerrad);
  }
}

module RoundEdges(){
  intersection(){
    VExtrude()
      OuterOval();

    for (xi=[-1,1]) {
      hull(){
	for (yi=[-1,1]) {
	  translate([xi * (hcentredist/2 + hinnerrad),
		     houterrad,
		     yi * (height/2 - roundedgedia / 4 * sqrt(2))])
	    rotate([90,0,0])
	    cylinder(r=roundedgedia/2, h=houterrad*2, $fn=20);
	}
      }
    }
  }
}

module Positive(){
  difference(){
    VExtrude()
      Hoop();

    rotate([90,0,0])
      translate([0,0,-50])
      cylinder(r=screwholedia/2, h=100);
  }

  RoundEdges();
}

module Ribs(){
  imax = ceil(height*2 / ribheight);
  for (i=[-imax:imax]) {
    hull(){
      translate([-ribdepth/2,
		 ribheight*i,
		 0])
	polygon([[0,          0],
		 [ribdepth, -ribheight],
		 [ribdepth, +ribheight]]);
      translate([50, 0])
	square([1, height*2], center=true);
    }
  }
}	    

module Division(cutmore) {
  mirror([0,0,1]) {
    translate([0, 0, -cutmore*feedzgap/2]) {
      translate([-ziglen + -cutmore*feedxgap/2, -100, 0])
	cube([100, 100, 50]);
    }
  }
  translate([blockwidth/2 - blockoverlapcnr + -cutmore*backxgap/2,
	     -1,
	     -50])
    cube([100, 100, 100]);

  translate([ziglen + -cutmore*feedxgap/2,
	     -50,
	     -50])
    cube([100, 51, 100]);

  translate([50,
	     hinnerrad/2 + houterrad/2 + blockdepth/2 + -cutmore*ribsgap/2,
	     0])
    rotate([-90,0,90])
    linear_extrude(height=100)
    Ribs();
}

module SDemo(){
  //difference(){
  %  Positive();
  //  Division(0);
  //}
  Division(-1);
}

module A(){
  difference(){
    Positive();
    Division(+1);
  }
}

module B(){
  intersection(){
    Positive();
    Division(-1);
  }
}

module Demo(){
  color("red") A();
  color("blue") B();
}

module APrint(){ ////toplevel
  rotate([0,180,0])
    A();
}

module BPrint(){ ////toplevel
  B();
}

module Kit(){ ////toplevel
  translate([0, hinnerrad, 0])
    APrint();
  rotate([0,0,180])
    BPrint();
}

//Ribs();
//Demo();

//A();
//B();
//%Division(+1);

//Hoop();

//Demo();
//BPrint();

//Kit();
