// -*- C -*-

h = 15;
w = 20;
l = 30;
roof = 10;
eave = 1;
peakw = 1;
overhang = 3.0;
chimnd= 7;
chimnhr = 1.00;

$fs = 0.1;

module Hotel(){
  cube([w,l,h + 0.1]);
  hull(){
    translate([0,0, h] + overhang * [-1,-1,0])
      cube([w,l,eave] + overhang * [2,2,0]);
    translate([0,0, h] + overhang * [0,-1,0]
	      + (w-peakw) * 0.5 * [1,0,0])
      cube([peakw, l, roof] + overhang * [0,2,0]);
  }
  translate([w/4, l/2, h] + overhang * [-0.5, 0,0])
    cylinder(r= chimnd/2, h = roof * chimnhr);
}

scale(1.11)
Hotel();
