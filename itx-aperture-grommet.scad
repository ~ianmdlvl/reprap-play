// -*- C -*-

include <utils.scad>
include <threads.scad>

ap_width = 21.30;
ap_height = 16.45;
tot_height = 22.74 + 1.0;
screw_ctr_to_tr = [ 7.89, 3.87 ];

tab_th = 2.5;
wall_th = 1;
wall_h = 2;

app_slop = 0.60; // total across both sides

// calculated

tab_h = tot_height - ap_height;

real_main_sz = [ ap_width, ap_height ] - app_slop * [ 1,1 ];
real_all_sz = real_main_sz + tab_h * [0,1];
real_inner_sz = real_main_sz - wall_th * [ 2,1 ];

screw_pos = real_all_sz - (screw_ctr_to_tr - 0.5 * app_slop * [1,1]);

module GapPlan() {
  rectfromto([ wall_th, -1 ],
	     real_main_sz - wall_th * [1,1]);
}

module MainPlan() {
  rectfromto([0,0],
	     real_main_sz);
}

module AllPlan() {
  rectfromto([0,0], real_all_sz);
}

module Grommet(){ ////toplevel
  difference(){
    union(){
      linextr(0, tab_th + wall_h) MainPlan();
      linextr(0, tab_th) AllPlan();
    }
    linextr(-1, tab_th + wall_h + 1) GapPlan();
    translate(concat(screw_pos, [-1]))
      english_thread(diameter = 0.1380,
		     threads_per_inch = 32,
		     length = tab_th + 2,
		     internal = true);
  }
}

Grommet();
