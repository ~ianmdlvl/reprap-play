// -*- C -*-

// use shell thickness 1.50
// use fill density 40%

include <funcs.scad>

tube_dia = 27.5 + 1.625 + 1.32;
lock_w = 42.5 + 0.5;
lock_d = 28.0 + 0.5;
main_h = 45.0;
backflange_d = 12;
backflange_hole_dy = -1;
lockshaft_dia = 14.35;

cliprecess_h = 16;
total_h = 75;

back_gap = 12.5;
main_th = 4.50;
tube_th = 5.50;

midweb_d = 3;
clip_th = 3.5;
clip_gap = 2.5;
clip_d = 22.0;

mountscrew_dia = 4 + 0.5;
clipbolt_dia = 5 + 0.6;

mountscrew_washer = 10;

backflange_th = 4.5;

$fn=50;

join_cr = 9;

tube_rear_extra_th = 1;

divide_shaft_w = 1.75;
divide_shaft_l = 1.5;
divide_head_dx = 1.75;
divide_head_th = 1.5;
divide_gap = 0.75;

divide_angle = 26;
divide_fudge_r = 4.75;

backflange_angle = 20;

// calculated

lockshaft_r = [1, 1] * lockshaft_dia / 2;
front_th = main_th;

tube_or = tube_dia/2 + tube_th;
back_ohw = back_gap/2 + backflange_th;
backflange_ymin = tube_dia/2 + backflange_d;

lock_0y = tube_dia/2 + lock_d/2 + midweb_d;
lock_0x = lock_w/2 - lock_d/2;
lock_0 = [lock_0x,lock_0y];

lock_or = [lock_w, lock_d]/2 + [front_th,front_th];

module oval(sz){ // sz[0] > sz[1]
  xr = sz[0];
  yr = sz[1];
  hull(){
    for (sx=[-1,+1]) {
      translate([sx * (xr-yr), 0])
	circle(r=yr);
    }
  }
}

module JoinCircs(jr){
  // http://mathworld.wolfram.com/Circle-CircleIntersection.html
  R = tube_or + join_cr;
  r = lock_or[1] + join_cr;
  d = dist2d( [0,0], lock_0 );
  x = (d*d - r*r + R*R) / (2*d);
  y = sqrt( R*R - x*x );

  echo(lock_0x, lock_0y, R,r, d, x,y);

  for (m=[0,1]) {
    mirror([m,0]) {
      rotate(atan2(lock_0y, lock_0x)) {
	translate([x,-y])
	  circle(r= jr);
      }
    }
  }
}

module DivideHook(){ ////toplevel
  w = tube_th/2;
  d = divide_gap;

  translate([-1,0] * (w + d + w)){
    for (sx=[-1,+1])
      translate([-(w + w+d) * sx, 0]) circle(r= w);
    difference(){
      circle(r = 3*w + d);
      circle(r =   w + d);
      translate([-10*w, -10*w]) square([20*w, 10*w]);
    }
  }
}

module DivideCut(){
  w = tube_th/2;
  d = divide_gap;
  br = tube_dia/2 + tube_th;

  difference(){
    offset(r=divide_gap) DivideHook();
    DivideHook();
    translate([-2*w,0]) mirror([0,1]) square([4*w, 4*w]);
  }
}

module DivideCutB(){
  w = tube_th/2;
  d = divide_gap;
  br = tube_dia/2 + tube_th;
  
  intersection(){
    translate([br - tube_th/2,0]) {
      difference(){
	circle(r=br + d);
	circle(r=br);
      }
    }
    translate([-2*w, 0]) mirror([0,1]) square(4*w);
  }
}

module DivideSurround(){
  w = tube_th/2;
  d = divide_gap;

  offset(r= w*2) {
    hull() {
      DivideCut();
      translate([-(4*w + 2*d), 8*w]) circle(r=w);
    }
  }
}

module DivideInPlace(){
  rotate([0,0, -divide_angle])
    translate([ -tube_dia/2 -tube_th/2, 0])
    children();
}

module MainPlan(){ ////toplevel
  difference(){
    union(){
      difference(){
	union(){
	  hull(){
	    for (t=[0, tube_rear_extra_th])
	      translate([0, -t])
		circle(r = tube_or);
	  }
	  rotate([0,0, backflange_angle])
	    translate([-back_ohw,0]) mirror([0,1])
	    square([back_ohw*2, backflange_ymin]);

	  translate([0, lock_0y]){
	    oval(lock_or);
	  }

	  hull(){
	    JoinCircs(0.01);
	    polygon([[0,0], lock_0, [-lock_0[0], lock_0[1]]]);
	  }
	}

	rotate([0,0, backflange_angle])
	  translate([-back_gap/2,1]) mirror([0,1])
	  square([back_gap, backflange_ymin+2]);

	JoinCircs(join_cr);
      }

      DivideInPlace() DivideSurround();
    }
    translate([0, lock_0y]){
      oval([lock_w/2, lock_d/2]);
    }

    circle(r = tube_dia/2);

    DivideInPlace() DivideCut();
    DivideInPlace() DivideCutB();
  }
}

lockshaft_or = lockshaft_r + [clip_th,clip_th];
cliprecess_ymax = cliprecess_h - lockshaft_r[1];
clip_ymin = cliprecess_ymax - main_h;
clip_ogap = clip_gap + clip_th*2;

module ClipElevationPositive(){
  hull(){
    oval(lockshaft_or);
    translate([0, -lockshaft_or[1] * sqrt(2)])
      square(center=true, 0.5);
  }
  translate([-lockshaft_or[0], 0])
    square([lockshaft_or[0]*2, cliprecess_ymax]);
  translate([-clip_ogap/2, 0]) mirror([0,1]) square([clip_ogap, -clip_ymin]);
}

module ClipElevationNegative(){
  hull(){
    for (y=[0, cliprecess_ymax+1])
      translate([0, y])
	oval(lockshaft_r);
  }
  translate([-clip_gap/2, 1]) mirror([0,1]) square([clip_gap, 2-clip_ymin]);
}

module ClipElevation(){
  difference(){
    ClipElevationPositive(1);
    ClipElevationNegative(0);
  }
}

module ExtrudeClipElevation(extra=0){
  translate([0,
	     lock_0y + lock_d/2 + clip_d + extra,
	     -clip_ymin])
    rotate([90,0,0])
    linear_extrude(height= clip_d + extra*2, convexity=100)
    children(0);
}

module ThroughHole(r, y, z, x=-50) {
  translate([x, y, z])
    rotate([0, 90, 0])
    cylinder(r=r, h=100, $fn=20);
}

module MountingHoleCylinders(r, x=-50){
  for (z=[ 1/4, 3/4 ]) {
    rotate([0,0, backflange_angle])
      ThroughHole( r,
		   -tube_dia/2 -0.5*backflange_d + backflange_hole_dy,
		   total_h * z,
		   x);
  }
}

module ThroughHoles(){
  MountingHoleCylinders(mountscrew_dia/2);

  ThroughHole( clipbolt_dia/2,
	       lock_0y + lock_d/2 + clip_d/2 + front_th/2,
	       main_h - cliprecess_h - clip_th - clip_d/2 );
}

module SlopeTrimElevation(){
  far_corner_nom = [ lock_0y + lock_d/2, main_h ];
  round_centre = far_corner_nom + lock_d/2 * [0,1];
  hull(){
    translate(round_centre) circle(r= lock_d/2);
    translate([ lock_0y - lock_d/2, total_h ]) square([ lock_d + clip_d, 1 ]);
    translate(far_corner_nom) square([clip_d*2, 1]);
  }
}

module SlopeTrim(){
  rotate([0,90,0])
    rotate([0,0,90])
    translate([0,0, -lock_w])
    linear_extrude(convexity=100, height=lock_w*2)
    SlopeTrimElevation();
}
    
module MainPositive(){
  difference(){
    union(){
      linear_extrude(height=total_h, convexity=100) MainPlan();
      ExtrudeClipElevation() ClipElevationPositive();
    }
    ExtrudeClipElevation(1) ClipElevationNegative();
  }
}

module Bracket(){ ////toplevel
  difference(){
    MainPositive();
    ThroughHoles();
    SlopeTrim();
  }
}

module TestTopEdge(){ ////toplevel
  intersection(){
    translate([0,0, -total_h])
      translate([0,0, 4])
      Bracket();
    translate([-200,-200,0])
      cube([400,400,100]);
  }
}

module TestClipBoltHole(){ ////toplevel
  intersection(){
    union(){
      translate([0, 0, -5])
	Bracket();
      translate([-4, lock_0y + lock_d/2 + 1, 0])
	cube([8, 4, 1.5]);
    }
    translate([-200, lock_0y + lock_d/2 + 0.1])
      cube([400, 400, total_h-20]);
  }
}

module Demo(){ ////toplevel
  Bracket();
  color("blue") MountingHoleCylinders(mountscrew_dia/2 - 0.1);
  color("black") MountingHoleCylinders(mountscrew_washer/2,
				       back_ohw + 0.25);
}

module DivideDemo(){ ////toplevel
  color("black") translate([0,0,-2]) MainPlan();
  color("grey") DivideInPlace() DivideHook();
  color("blue") translate([0,0,-4]) DivideInPlace() DivideCut();
}

//MainPlan();
//ClipElevationPositive();
//ClipElevation();
//MainPositive();
//%ThroughHoles();
//TestTopEdge();
//TestClipBoltHole();

//Bracket();

