// -*- C -*-

// This file can be used in two ways:
//
// A. Rectangular boxes
//      1. include <sealing-box.scad>
//      2. assign() values to (xxx these should be $ variables)
//           $sealingbox_wallth
//           $sealingbox_sz[0] (outer dimension)
//           $sealingbox_sz[1] (outer dimension)
//           $sealingbox_sz[2] (inner dimension)
//           $sealingbox_ceilth
//           $sealingbox_floorth
//           $sealingbox_wallth
//      3. use the modules
//           SealingBox_RectBox
//           SealingBox_RectLid
//           (origin is notional outside corner, but at level of
//            inside of base; box extends to positive x,y,z)
//
// B. Complicated shapes, but harder work
//      1. Be a .m4 file and m4_include sealing-box.scad.m4
//      2. Define your own BoxDoShapeSomething like BoxDoShapeRect
//      3. Invoke BoxUseShape
//      4. Use the Box and Lid modules generated
//
// Other settings
//  $sealingbox_cnrrad

$sealingbox_cnrrad = 10;
$sealingbox_crude = false;
$sealingbox_inner_slop = 0.2;

m4_define(`BoxLocals',`
  xbox = $sealingbox_sz[0];
  ybox = $sealingbox_sz[1];
  zbox = $sealingbox_sz[2];
  wall = $sealingbox_wallth;
  floorth = $sealingbox_floorth;
  ceilth = $sealingbox_ceilth;
  cnrrad = $sealingbox_cnrrad;

  xbox_lin = xbox - cnrrad*2;
  ybox_lin = ybox - cnrrad*2;
')

m4_define(`innertube', `(1.0 + 0.2)')
m4_define(`lidoverlap', `1.5')
m4_define(`lidoverhang', `6')
m4_define(`tubesealrad', `2.0')

m4_define(`BoxFn',`$fn= $sealingbox_crude ? ($2) : ($1)')

m4_dnl Box_Part($1=transl_x,$2=transl_y, $3=rot_z,$4=mirror_xy)
m4_dnl          $5=kind, $6=kindargs, $7=profile(profileargsargs))
m4_define(`Box_Part',`
  translate([($1),($2)])
    rotate([0,0,($3)])
    mirror([($4),0,0])
    BoxPart_Extrude_$5($6, $7)') m4_dnl

boxpart_d = 0.01;

m4_dnl BoxPart_Extrude_Linear(dist, `profile(...);');
m4_define(`BoxPart_Extrude_Linear',`
  rotate([90,0,0])
    translate([0,0, -($1)])
    linear_extrude(height= boxpart_d + ($1)) {
      $2
    }
')

m4_dnl BoxPart_Extrude_Arc(x0_radius, swept_angle, `profile(...);')
m4_dnl  arc starting at transl_x, transl_y, moving towards positive
m4_dnl  y at first and then bending towards negative x, until
m4_dnl  use negative x0_radius to inciate bending towards positive x
m4_dnl  swept_angle is reached
m4_dnl  x0_radius is the radius of the extruded part at x=0, not of the box
m4_define(`BoxPart_Extrude_Arc',`
  translate([-($1),0,0])
    intersection(){
      translate([0,0,-500])
        linear_extrude(height=1000)
        scale(500)
        mirror([($1)<0, 0,0])
        FArcSegment_mask($2);
      rotate_extrude(convexity=10, $fs=1, BoxFn(36,8))
        mirror([($1)<0, 0,0])
        translate([+($1),0,0]){
          $3
        }
    }
')

m4_dnl BoxDoShapeRect(`profile(profileargs)');
m4_define(`BoxDoShapeRect',`
  Box_Part(0,           cnrrad,         0,0, Linear,`ybox_lin', `$1' )
  Box_Part(0,           ybox-cnrrad,    0,0, Arc,`-cnrrad,90' , `$1' )
  Box_Part(cnrrad,      ybox,         -90,0, Linear,`xbox_lin', `$1' )
  Box_Part(xbox-cnrrad, ybox,         -90,0, Arc,`-cnrrad,90' , `$1' )
  Box_Part(xbox,        ybox-cnrrad, -180,0, Linear,`ybox_lin', `$1' )
  Box_Part(xbox,        cnrrad,      -180,0, Arc,`-cnrrad,90' , `$1' )
  Box_Part(xbox-cnrrad, 0,           -270,0, Linear,`xbox_lin', `$1' )
  Box_Part(cnrrad,      0,           -270,0, Arc,`-cnrrad,90' , `$1' )
')

m4_dnl '

module SealingBox_WallProfile(){
  BoxLocals
  z = zbox - innertube - tubesealrad;
  translate([0, -0.1]) square([wall, z]);
  translate([tubesealrad, z]) circle(r=tubesealrad, BoxFn(20,6));
}

module SealingBox_FloorProfile(){
  BoxLocals
  mirror([0,1]) square([wall, floorth]);
}

function SealingBox_lidbigger() = lidoverlap + innertube;

module SealingBox_LidProfile(){
  BoxLocals
  rad = tubesealrad + innertube;
  morex = wall;
  inner_buttress_h = tubesealrad*1.5 + innertube + ceilth;

  difference(){
    translate([0, zbox + ceilth]) mirror([0,1]) {
      translate([-SealingBox_lidbigger(),
		 0])
      square([lidoverlap + innertube + tubesealrad,
	      lidoverhang + innertube + ceilth]);
      square([tubesealrad*2 + innertube + lidoverlap,
	      inner_buttress_h]);
    }
    hull(){
      translate([tubesealrad,
		 zbox - innertube - tubesealrad])
	for (t=[ [0,0],
		 [0, -zbox]
		 ]) {
	  translate(t)
	    circle(r= tubesealrad + innertube, BoxFn(20,6));
	}
    }
  }
  translate([tubesealrad*2 + $sealingbox_inner_slop,
	     zbox + ceilth]) {
    mirror([0,1]) {
      square([lidoverlap + innertube,
	      inner_buttress_h]);
    }
  }
}

module SealingBox_CeilProfile(){
  BoxLocals
  translate([0, zbox])
    square([wall*2, ceilth]);
}

// BoxDoShape(Basename,BoxDoShapeSomething)
// generates modules BasenameBox and BasenameLid
m4_define(`BoxUseShape',`
  module $1Box(){
    BoxLocals
    $2(SealingBox_WallProfile(););
    hull(){ $2(SealingBox_FloorProfile();); }
  }

  module $1Lid(){
    BoxLocals
    $2(SealingBox_LidProfile(););
    hull(){ $2(SealingBox_CeilProfile();); }
  }
')

BoxUseShape(`SealingBox_Rect',`BoxDoShapeRect')
