// -*- C -*-

//
// git clean -xdff
// ./quacks-ingredients-update-levels
// make autoincs
//
// make -j8 quacks-stls 2>&1 | tee log
// OR EG
// make -j8 quacks-ingredients-L{1,2,3,4,5},Base_Yellow.auto.stl
//
// ./quacks-ingredients-make-copy-gcodes Base_Yellow   etc.
//
//   Print Q-P-1 in spots, Q-P-2 in main colour, Q-P-3 in spots
//
// For colours which only have zero-spot counters, print only Q-P-2

token_dia = 20;
spot_dia = 4.3;
spot_gap = spot_dia / 3.0;

thick = 3.0;

multicolour_gap = 0.075; // each side
initial_layer_thick = 0.400;
initial_layer_width = 0.750;
final_layer_thick = 0.500;
multicolour_post = 4;

$fs=0.1;
$fa=1;

// calculated

token_pitch = token_dia + 3;

// autoadjusted

$spots_absent = false;
$spots_plusgap = false;

module Spots_Extrude_Lower(){
  d = $spots_plusgap ? 1 : 0;
  translate([0,0,-d])
    linear_extrude(height= initial_layer_thick + d)
    children(0);
}

module Spots_Extrude_Upper(){
  d = $spots_plusgap ? 1 : 0;
  translate([0,0, thick + d])
    mirror([0,0, 1])
    linear_extrude(height= final_layer_thick + d)
    children(0);
}

module SpotAt(condition, xy) {
  if (condition == !$spots_absent) {
    translate(xy * (spot_gap + spot_dia) * sqrt(0.5))
      circle(r= spot_dia/2 +
	     ($spots_plusgap ? multicolour_gap : 0));
  }
}

module Token_Spots(){
  SpotAt(($nspots % 2) > 0,  [0,0]);
  SpotAt($nspots >= 2, [ 1, 1]);
  SpotAt($nspots >= 2, [-1,-1]);
  SpotAt($nspots >= 4, [ 1,-1]);
  SpotAt($nspots >= 4, [-1, 1]);
}

module Token_Spots_All(){
  $nspots = 5;
  Token_Spots();
}

module Token_L1(){
  Spots_Extrude_Lower()
    Token_Spots();
}

module Token_L2(){
  $spots_absent = true;
  Spots_Extrude_Lower()
    Token_Spots();
}

module Token_L3(){
  $spots_plusgap = true;
  difference(){
    linear_extrude(height=thick)
      circle(r=token_dia/2);
    Spots_Extrude_Lower() Token_Spots_All();
    Spots_Extrude_Upper() Token_Spots_All();
  }
}

module Token_L4(){
  $spots_absent = true;
  Spots_Extrude_Upper()
    Token_Spots();
}

module Token_L5(){
  Spots_Extrude_Upper()
    Token_Spots();
}

module Frame(phase, base_sz) {
  zs = [ initial_layer_thick,
	 initial_layer_thick,
	 thick,
	 thick,
	 thick ];

  sz = base_sz + phase * initial_layer_width * 2 * [1,1];
  linear_extrude(height= initial_layer_thick) {
    difference(){
      square(center=true, sz + initial_layer_width * 2 * [1,1]);
      square(center=true, sz);
    }
  }
  // Priming tower
  translate([-base_sz[0]/2, (2.8-phase)*(multicolour_post*1.7)])
    linear_extrude(height= zs[phase-1])
    square(multicolour_post);
}

module Tests(){
  for ($nspots = [1,2,3,4]) {
    translate(($nspots - 2) * token_pitch * [1,0])
      children();
  }
}

module Tests_L() { ////toplevel
  Frame($phase, token_dia * [ 6, 1.5 ]);
  Tests() Token_L();
}

//// toplevels-from:
include <quacks-ingredients-counts.scad>

//Demo();
