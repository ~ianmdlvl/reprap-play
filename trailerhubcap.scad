// Copyright (C)2012 Ian Jackson
// Licenced under the GNU General Public Licence, version 3, or
// (at your option) any later version.  There is NO WARRANTY.

maindia = 29.7;
poleholeh = 5.0;
polecovth = 0.4;
poleholerad = 6;

mainoverlap = 1.5;

hookbasew = 5.0;
hookfullw = 7;
hookheight = 4.5;
hookwidth = 8;
hookbevelw = 0.75;
hookbevelh = 1.5;

fingernaildepth = 5;
fingernailheight = 2.5;
fingernailwidth = 6;

bigrad = maindia/2 + mainoverlap;
mainth = poleholeh + polecovth;
hooklessdepth = hookfullw - hookbasew;

module base() {
  rotate_extrude(convexity=10)
    mirror([1,0,0])
    polygon(points=[[-bigrad, 0],
		    [-bigrad + mainth, -mainth],
		    [0, -mainth],
		    [0, -poleholeh],
		    [-poleholerad, -poleholeh],
		    [-poleholerad, 0]]);
}

module fingernails() {
  for (ang=[60,180,300])
    rotate([0,0,ang])
      translate([bigrad - fingernaildepth,
		 -fingernailwidth/2,
		 -fingernailheight])
      cube([fingernaildepth + 1, fingernailwidth, fingernailheight + 1]);
}

module hookrim() {
  rotate_extrude(convexity=10)
    mirror([1,0,0])
    translate([-maindia/2, 0, 0])
    polygon(points=[[hooklessdepth, 0],
		    [hookfullw, 0],
		    [hookfullw*0.33, hookheight],
		    [hookbevelw, hookheight],
		    [0, hookheight-hookbevelh],
		    [0, hooklessdepth]]);
}

module hooktriangles() {
  for (ang=[0,120,240]) {
    rotate([0,0,ang]) {
      translate([0,0,-1]) {
	linear_extrude(height=hookheight+2) {
	  polygon(points=[[0, 0],
			  [maindia/2 + 1, -hookwidth],
			  [maindia/2 + 1, +hookwidth]]);
	}
      }
    }
  }
}

difference(){
  base();
  fingernails();
}
intersection(){
  hookrim();
  hooktriangles();
}
