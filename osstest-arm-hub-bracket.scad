// -*- C -*-

len = 80;
basethick = 4;
sidewall = 5;
width = 40;

strapthick = 4;
strapwidth = 7;

strapbotgap = 1;
strapsidegap = 4;
overstrap = 4;

wallheight = strapbotgap + strapthick + overstrap;

availlen = (len - strapsidegap);
numstraps = floor(availlen / (strapwidth + strapsidegap));
strapstride = availlen / numstraps;
echo(numstraps, strapstride);

module Bracket(){
  difference(){
    cube([len, width, basethick+wallheight]);
    translate([-1, sidewall, basethick])
      cube([len+2, width-sidewall*2, wallheight+1]);
    for (i=[0:numstraps-1]) {
      translate([ (0.5+i)*strapstride + strapsidegap/2,
		  width/2,
		  basethick + strapbotgap + strapthick/2 ])
	cube([strapwidth, width*2, strapthick], center=true);
    }
  }
}

Bracket();
