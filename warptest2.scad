// -*- C -*-
rotate([90,0,0])
linear_extrude(height=50){
  polygon([[-3/2, 0],
	   [-11/2, 8],
	   [+11/2, 8],
	   [+3/2, 0]]);
}
