// -*- C -*-

include <utils.scad>

mainwall_th = 3.0;
smallwall_th = 2.0;

seal_th = 0.3 + 0.6 + 0.6 - 0.4 - 0.4 + 0.2; // total gap for seal etc.
behind_recess = 1.5;

recess_gap_end = 0.4;

lid_edge_th = 0.5;

battery_len = 66.55 + 1.25 -.55;
battery_th = 6.55 + 0.75 - .60;
battery_wdth = 44.38 + 0.75 -.55;

battery_base_indent = 0.94 + 0.50;
battery_base_indent_fromside_outside = 4;
battery_base_indent_fromside_inside = 10;

handle_height = 3.5;
handle_inward = 10;
handle_len = 5;

pushhole_ell_sz = 4.75;
pushhole_ell_th = 1.75;
pushhole_circle_dia = 4.0;

// for testing:
//battery_len = 3;
//battery_wdth = 15;
//battery_base_indent_fromside_inside = 6;

// calculated

bpp0 = [0,0];
bpp1 = bpp0 + [ 0, mainwall_th - behind_recess ];
lppA = bpp1 + [ seal_th, -recess_gap_end ];
lppB = lppA + [ lid_edge_th, 0 ];
bpp2 = [ lppB[0], bpp1[1] ];
bpp3 = [ bpp2[0] + (bpp1 - bpp0)[1], bpp0[1] ];
bpp4 = [ bpp3[0], bpp0[1] + mainwall_th ];
lppC = bpp3 + [ 0, -recess_gap_end ];

lppF = lppC + [ handle_height, 0 ];

s0 = battery_wdth/2;
s0i = s0 - battery_th/2;
s1 = s0 + smallwall_th;

l1 = s1 - handle_inward;
l0 = l1 - handle_len;

echo(
     bpp0,
     bpp1,
     bpp2,
     bpp3,
     bpp4,
     bpp5,
     bpp6,
     bpp7,
     bpp8
);

echo(
     lppA,
     lppB,
     lppC,
     lppD,
     lppE
);

bpp8 = bpp0 + [ -battery_len,0 ];
bpp5 = [ bpp8[0] - smallwall_th, bpp4[1] ];
bpp9 = [ bpp0[0], bpp0[1] - battery_th/2 - 1.0 ];
bpp7 = [ bpp8[0], bpp9[1] ];
bpp6 = [ bpp5[0], bpp9[1] ];
lppE = [ lppA[0], bpp9[1] ];
lppD = [ lppC[0], bpp9[1] ];

module BaseHalfPlan(indent=0){
  polygon([ bpp0,
	    bpp1,
	    bpp2,
	    bpp3,
	    bpp4,
	    bpp5,
	    bpp6,
	    bpp7 + indent * [1,0],
	    bpp8 + indent * [1,0]
	    ]);
}

module SideHalfPlan(){
  polygon([ bpp5,
	    bpp6,
	    bpp9,
	    bpp1
	    ]);
}

module LidHalfPlan(){
  polygon([ lppA,
	    lppE,
	    lppD,
	    lppC,
	    lppB
	    ]);
}

module HandleHalfPlan(){
  translate(lppE)
    square(lppF - lppE);
}

module ExtrudePlan(from,to){
  rotate([0,-90,0])
  for (mj=[0,1]) {
    mirror([0,0,mj]) translate([0,0,from]){
      linear_extrude(height= to-from, convexity=5){
	for (mi=[0,1]) {
	  mirror([0,mi])
	    translate([0, battery_th/2])
	    children(0);
	}
      }
    }
  }
}

module PushHolePlan(){ ////toplevel
  translate(-(pushhole_ell_th * 0.10 +
	      pushhole_ell_sz * 0.10) * [1,1]) {
    for (r=[0,90])
      rotate(r)
	translate(-pushhole_ell_th * 0.5 * [1,1])
	square([ pushhole_ell_sz, pushhole_ell_th ]);
  }
  circle(pushhole_circle_dia/2, $fn=40);
}

module PlanDemo(){ ////toplevel
  color("blue") BaseHalfPlan();
  color("red") LidHalfPlan();
  translate([0,0,-1]) color("lightblue") SideHalfPlan();
}

module Base(){ ////toplevel
  difference(){
    ExtrudePlan(0,s1) BaseHalfPlan();
    linextr(-(10+battery_len), battery_len+10, convexity=5) PushHolePlan();
  }
  difference(){
    union(){
      ExtrudePlan(s0i, s1) SideHalfPlan();
      ExtrudePlan(s0 - battery_base_indent_fromside_inside,
		  s0 - battery_base_indent_fromside_outside
		  ) BaseHalfPlan(indent = battery_base_indent);
    }
    for (m=[0,1])
      mirror([m,0,0])
	translate([s0i, 0, bpp7[0] - 0.1])
	cylinder(r= battery_th/2, h=100, $fs=0.5);
  }
}

module BaseHalfTest(){ ////toplevel
  intersection(){
    Base();
    translate([-100,0,-100])
      cube([200,200,200]);
  }
}

module Lid(){ ////toplevel
  ExtrudePlan(0,s1) LidHalfPlan();
  ExtrudePlan(l0,l1) HandleHalfPlan();
}

module Demo(){ ////toplevel
  %Base();
  Lid();
}

//PlanDemo();
//Demo();
//Base();
