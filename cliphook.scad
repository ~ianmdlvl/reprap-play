// -*- C -*-
//
// cliphook.scad
//
// 3D design for a small clippy hook
// Copyright 2012,2016 Ian Jackson
//
// This work is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This work is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this work.  If not, see <http://www.gnu.org/licenses/>.

function ClipHook_r2(w,g,l,ye,k) = w/2 + g + w/2;
function ClipHook_r3(w,g,l,ye,k) = k * (ClipHook_r2(w,g,l,ye,k) + w + g);
function ClipHook_yd(w,g,l,ye,k) = g/2 + w + g + w/2 - ClipHook_r3(w,g,l,ye,k);
function ClipHook_xe(w,g,l,ye,k) =
        l*1.5 + w +
	sqrt(pow( ClipHook_r3(w,g,l,ye,k),      2) -
	     pow( ClipHook_yd(w,g,l,ye,k) - ye, 2));

module FlatArc(cx,cy,r1,r2,a1,a2=361,$fn=$fn) {
  astep = (a2-a1)/6;
  size = 5*(r2/2);
  translate([cx,cy,0]) {
    intersection() {
      difference() {
	circle(r=r2);
	translate([0,0,-1])
	  circle(r=r1);
      }
      scale(size) {
	for (ai=[0:4]) {
	  //echo(" jarc ", a1,a2, astep, ai, a1 + astep*ai );
	  rotate(a1 + astep*ai) {
	    polygon([ [0,0], [1,0],
		      [cos(astep*2),sin(astep*2)] ]);
	  }
	}
      }
    }
  }
}

module ClipHook_2D(w,g,l,ye,k,h) {
  r2 =  ClipHook_r2(w,g,l,ye,k);
  r3 =  ClipHook_r3(w,g,l,ye,k);
  yd =  ClipHook_yd(w,g,l,ye,k);
  xe =  ClipHook_xe(w,g,l,ye,k);

  xd = l*1.5 + w;
  xc = -l/2;
  yc = g/2 + w/2;

  alpha = atan2((xe-xd)/r3, (ye-yd)/r3);

  echo("ClipHook(w g l ye k h) ", w, g, l, ye, k, h);
  echo("ClipHook r2 r3 xd yd xe =", r2,r3, xd,yd, xe);

  $fn = 20;

  module jcirc(x,y) { translate([x,y,0]) circle(r=w/2); }
  module jbox(y,x1,x2) { translate([x1,y-w/2,0]) square(size=[x2-x1, w]); }
  module jarc(cx,cy,r,a1=0,a2=360) { FlatArc(cx,cy,r-w/2,r+w/2,a1,a2); }

  jcirc(-xc, -yc);
  jbox(-yc, xc, -xc);
  jarc(xc, yc, r2, 90, 270);
  jbox(yc+r2, xc, xd);
  jarc(xd, yd, r3, 90-alpha, 90);
  jcirc(xe,ye);
}

module ClipHook(w=1.2, g=0.2, l=0.0, ye=0, k=2.0, h=3.5, demo=false,
		cupcaph=0, cupgapg=0) {
  difference() {
    linear_extrude(height=h)
      ClipHook_2D(w,g,l,ye,k);
    if (cupcapg != 0) {
      translate([-g+0.01,-(w+g),h-cupcapg])
      cube([w,(w+g),cupcaph+1]);
    }
  }
  if (cupcaph != 0) {
    translate([-l/2, g/2+w/2, h-0.01])
      intersection() {
        cylinder(r=ClipHook_r2(w,g,l,ye,k)+w*0.4, h=cupcaph, $fn=16);
	translate([-50-g,-50,-1]) cube([50,100,h+2]);
      }
  }
}

if (ClipHook_demo) {
  ClipHook(l=0, k=1, cupcaph=1, cupcapg=0.4);
  %translate([l+w,0,0]) rotate(180) ClipHook(l=0, k=1, cupcaph=1, cupcapg=0.4);
}
