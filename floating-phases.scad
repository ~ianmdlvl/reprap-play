// -*- C -*-

// caller should define
//
//   z_pause = 2;    // higher than any thing in the model
//   total_sz = [ 200, 141 ];
//   phases=4;   // how many phases
//   colours = [
//	   "blue",
//	   "black",
//	   "red",	   
//	   "yellow",
//	   ];
//
// Pause height		z_pause value from caller
// head park X		15
// head park Y		0
// head move Z		1
// min head park Z	1
// retraction		1
// re-home		[ ] [ ]

// when z pause occurs
//   * set feed rate to 10% (or whatever minimum is)
//   * press knob, printer will start
//   * quickly, press again, select "stop"
//   * set temp
//   * set feed rate back to 100%
//   * now change filament etc., start next file

include <utils.scad>

$test_thicker = 1.0; // 2.5;

th_l0 = .425 * $test_thicker;
th_l1 = .250 * $test_thicker;
frame_w = 0.8;
tower_w = 2.0;

th_most = th_l0 + th_l1*2;

noz_gap = 8;


multicolour_gap = 0.15; // each side

underlayer_smaller = 0.35; // each side

total_sz_actual = $test ? $test : total_sz;

module Interlayer_Bigger(){
  offset(r=multicolour_gap, $fn=20){
    union(){ children(); }
  }
}

module Interlayer_Smaller(){
  offset(r=-multicolour_gap, $fn=20){
    union(){ children(); }
  }
}

module Underlayer_Smaller(){
  offset(r=-underlayer_smaller, $fn=20){
    union(){ children(); }
  }
}

module FloatingPhaseFraming(phase, zmin) {
  frame_inner =
    total_sz_actual
    + noz_gap * [2,2] * (phase > 0 ? phase : 0.5);

  frame_outer =
    frame_inner
    + frame_w * [2,2];    

  tower_pos =
    -0.5 * frame_inner
    + noz_gap * [1,0] * (phases - phase)
    + -[1,0] * tower_w + 0.5 * [0,-1] * frame_w;

  // frame for alignment
  linear_extrude(height= th_l0)
    difference(){
      square(frame_outer, center=true);
      square(frame_inner, center=true);
  }

  // tower to avoid diagonal traverse to start position
  linear_extrude(height= zmin + th_l1)
    translate(tower_pos)
    square([1,1] * tower_w);

  // trick to pause rather than finishing
  if (phase != phases-1)
    translate([0,0, z_pause+th_l1])
      linear_extrude(th_l1)
      translate(tower_pos)
      square([1,1] * tower_w);
}

echo(str("SET PAUSE AT Z HEIGHT TO ",z_pause));
