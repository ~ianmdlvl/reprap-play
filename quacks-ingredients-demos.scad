// -*- C -*-

include <quacks-ingredients.scad>

sandingframe_gap = 0.3; // to be added to radius
sandingframe_nw = 5;
sandingframe_nl = 6;
sandingframe_th = thick*2/3;

module Demo(){ ////toplevel
  $nspots = 3;
  color("red") { Token_L3(); }
  color("white") { Token_L1(); Token_L5(); }
  color("black") { Token_L2(); Token_L4(); }
}

module SandingFrame(){ ////toplevel
  stridel = token_dia + 5;
  nl = sandingframe_nl;
  nw = sandingframe_nw;
  stridew = stridel * cos(30);
  stride = [stridel,stridew];

  linear_extrude(height = sandingframe_th) {
    difference(){
      translate((token_dia/2 + stridel) * 0.5 * [-1,-1])
	square([ stridel * (nl + 0.5),
		 stridew * nw + stridel * 0.5 ]);
      for (j = [0 : nw-1]) {
	eo = j % 2;
	for (i = [0 : nl-1-0.5*eo]) {
	  translate([stridel * (i + 0.5 * eo),
		     stridew * j])
	    circle(r = token_dia/2 + sandingframe_gap);
	}
      }
    }
  }
}
