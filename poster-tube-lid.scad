// -*- C -*-

// Print, for each end:
//
//   CoverPrint
//   StrapMount
//   CatchAssembly
//
// For attaching tube to wall, with base, for storing sticks etc.
//
//   WallMount             goes near top
//   WallMountForBase      goes at bottom
//   WallMountBase         attaches to bottom, glue some neoprene to it
//   WallMountBaseCutJig   jig for cutting neoprene

include <funcs.scad>
include <utils.scad>

coarse = false;
enable_head_cups = false;

main_dia = 71.2 + 0.50 - 2.26;
top_thick_middle = 4;
top_thick_by_oring = 3.0;
top_middle_dr = 11;

main_cnr = 6.0;

min_wall = 3;

rivet_posn = 6.0 + 0.30;
rivet_thick = 1.67;
rivet_width = 4.15 + 1.0;
rivet_tall = 5.51 + 1.49;

over_rivet_wall = 1.0;
side_rivet_gap = 1.5;
inside_rivet_gap = 1.5;

bayo_interf = 0.30;
bayo_behind = 8.5;
bayo_interf_width = 2.0;
bayo_interf_slope = 0.5;

oring_thick = 5.0;
oring_bore = 62.0;

oring_upper_embed_angle = 80;
oring_compress = 0.1; // proportion
oring_compress_more = 0.2;

oring_rm_beside = 8;
oring_rm_scale = 2.0;
oring_rm_angle = 20;

side_taper = 1.0;

bayo_gap = 6.0;

bayo_entry = 1.167;
bayo_inramp = 0.9;

bayo_slice_size = coarse ? 5 : 1;

brace_hole_width = 1.0;
brace_above_below = 1.2;
brace_end_shorter = 0.3;

jig_thick = 1.4;
jig_hole_dia = 3.0;
jig_rim = 5;
jig_mark = 5;

strap_loop_thick = 6;
strap_loop_inside = 10;
strap_loop_strlen = 10;
strap_loop_elevation = 45;

sm_inner_circum = 218 - 1.90 - 1.00 - 0.50;
sm_main_thick = 2.0;
sm_main_width = 20;

sm_bolt_dia = 3.5 + 0.1;
sm_bolt_shaft = 21.0;
sm_bolt_head_dia = 6.94 + 1.0;
sm_bolt_head_thick = 2.14;
sm_bolt_nut_width = 5.89 + 0.25;
sm_bolt_nut_thick = 3.68;
sm_bolt_tighten_allow = 2.0;

sm_bolt_y_clear = 0.75;
sm_bolt_y_over = 0.5;

sm_closure_cnr = 3.0;

wm_thick = 5;
wm_screw_dia = 4.5; // Timco wood screw 40mm, use brown plug
wm_screwdriver_dia = 6.3 + 1.5;
wm_screw_around = 5.0;
wm_screw_slot = 3.5;
wm_screw_head = 8.0;

wmb_screw_dia = 5;
wmb_screw_head_dia = 8.7 + 0.5;
wmb_screw_around_x = 4;
wmb_screw_around_z = 6;
wmb_screw_depth_min = 10;
web_screw_len = 16 + 0.5;
wmb_nut_across = 7.82 + 0.35;
wmb_nut_around_min = 2;
wmb_nut_behind_min = 5;
wmb_nut_th = 3.84 + 0.75;
wmb_mount_wall = 4.5;
wmb_mount_gap_xy = 0.1;
wmb_mount_gap_z = 0.2;
wmb_mount_y_width = 10;
wmb_bottom_gap = 35; // includes allowance for padding, etc.
wmb_bottom_th = 7;
wmb_bottom_th_min = 1;
wmb_ring_gap = 1.0;
wmb_base_extra_rad = 10;
wmb_jig_th = 1;
wmb_jig_around_gap = 1;

catch_stalk_h = 4.5;
catch_stalk_len = 50;
catch_tip_th = 4;
catch_head_th = 3;

catch_pin_slop = 0.25; // each side, and above
catch_pin_slop_x_extra = 0.0; // only on one side
catch_stalk_above_gap = 1.5;
catch_stalk_eff_bend_rad = catch_stalk_len * 0.75;

catch_strap_width = 18;
catch_stalk_base_width = 15;

catch_knob_dia = 6;
catch_knob_above_gap = 5;
catch_knob_height = 3.0;

catch_stalk_below_gap = 1.0;
catch_stalk_beside_gap = 2.0;

// calculated

TAU = PI*2;

bayo_entry_x = bayo_entry;
bayo_entry_z = bayo_entry;
bayo_inramp_x = bayo_inramp;
bayo_inramp_z = bayo_inramp;

oring_mid_dia = oring_bore + oring_thick;
oring_outer_dia = oring_mid_dia + oring_thick;

oring_oblate = (1 - oring_compress);

oring_y_rad = oring_thick/2 * oring_oblate;
oring_x_rad = oring_thick/2 / oring_oblate;

by_oring_z = oring_y_rad * (1 + cos(oring_upper_embed_angle));

side_height = rivet_posn + bayo_behind + rivet_thick/2;
side_thick = rivet_tall + over_rivet_wall;

top_z = top_thick_by_oring + oring_y_rad + by_oring_z;

middle_bot_z = top_z - top_thick_middle;

bayo_top_z = bayo_behind + bayo_gap;

bayo_nom_rad = main_dia/2 + side_thick;
bayo_real_rad = main_dia/2 + rivet_tall;

rivet_entry_width = rivet_width + side_rivet_gap;

jig_mark_rad = jig_mark + main_dia/2 + jig_thick;

handling_dia = oring_bore + oring_thick*2 + min_wall*2;
handling_angle = 45;

sm_inner_rad = (sm_inner_circum + sm_bolt_tighten_allow/2) / TAU;
sm_outer_rad = sm_inner_rad + sm_main_thick;

wm_main_width = sm_main_width;
wm_y_min = sqrt( pow(sm_inner_rad, 2) -
		 pow(sm_inner_rad - (wm_thick - sm_main_thick), 2) );
wm_y_screw = wm_y_min + wm_screw_around + wm_screw_dia/2;
wm_y_max = wm_y_screw + wm_screw_dia/2 + wm_screw_around;
wm_lhs_y_min = -wm_y_max;
wm_y_slotc_screw = wm_y_screw + wm_screw_slot/2;
wm_y_slot1_screw = wm_y_screw + wm_screw_slot;
wm_y_slot1_max = wm_y_max + wm_screw_slot/2;
wm_z_slot0_screw = wm_main_width + wm_screwdriver_dia/2;
wm_z_slotc_screw = wm_z_slot0_screw + wm_screw_slot/2;
wm_z_slot1_screw = wm_z_slot0_screw + wm_screw_slot;
wm_z_max = wm_z_slot1_screw + wm_screw_around;

wmb_mount_cut_rad = sm_outer_rad + wmb_ring_gap;
wmb_nut_rad = wmb_nut_across / cos(30) * 0.5;
wmb_x_screw_plus_around_r = max(
				wmb_screw_around_x + wmb_screw_dia/2,
				wmb_nut_around_min + wmb_nut_across/2
				);
wmb_x_screw = -sm_outer_rad + wmb_x_screw_plus_around_r;
wmb_x_outer = -sm_outer_rad + wmb_x_screw_plus_around_r * 2;
function wmb_screw_thing_y_min(dia) = sqrt(
		       pow(wmb_mount_cut_rad, 2) -
		       pow(wmb_x_screw + dia/2, 2)
		       );
wmb_y_screw_end = wmb_screw_thing_y_min(wmb_screw_dia);
wmb_y_nut_min = max(
    wmb_screw_thing_y_min(wmb_nut_across + wmb_nut_around_min*2),
    wm_y_slot1_max
		    );
wmb_y_mount_max = max(
		      wmb_y_nut_min + wmb_nut_th + wmb_nut_behind_min,
		      wmb_y_screw_end + wmb_screw_depth_min
		      );
wmb_z_screw = max(
		  wmb_screw_around_z + wmb_screw_dia/2,
		  wmb_nut_around_min + wmb_nut_rad
		  );
wmb_z_max = wmb_z_screw * 2;
wmbb_y_max = max(
		 wmb_y_mount_max + wmb_mount_gap_xy + wmb_mount_wall,
		 wmb_y_screw_end + web_screw_len
		 );
wmbb_x_outer = wmb_x_outer + (wmb_mount_gap_xy + wmb_mount_wall);
wmbb_z_flat_max = -wmb_bottom_gap;
wmbb_z_flat_whole_min = wmbb_z_flat_max - wmb_bottom_th_min;
wmbb_z_min      = wmbb_z_flat_max - wmb_bottom_th;
wmbb_r_top = main_dia/2 + wmb_base_extra_rad;
wmbb_r_bottom = wmbb_r_top - (wmb_bottom_th - wmb_bottom_th_min);

smc_pos = [ 0, sm_inner_rad, 0 ];

smc_bolt_nut_dia = sm_bolt_nut_width / cos(30);
smc_bolt_nut_eff_thick = sm_bolt_nut_thick + sm_bolt_tighten_allow;

smc_bolt_y = sm_bolt_dia/2 + sm_bolt_y_clear;
smc_max_y = smc_bolt_y + sm_bolt_y_over
  + max(sm_bolt_head_dia/2, smc_bolt_nut_dia/2);
smc_cnr_c_x = sm_bolt_shaft/2 - sm_closure_cnr
  + sm_bolt_head_thick/2 + smc_bolt_nut_eff_thick/2;

catch_cr = catch_knob_dia/2 + catch_stalk_beside_gap;
catch_strap_thick = sm_main_thick;

echo("R ", sm_inner_rad, bayo_real_rad, bayo_nom_rad);

$fs= coarse ? 2.5 : 0.5;
$fa= coarse ? 5 : 1;

include <poster-tube-lid-parametric.scad>

// bayonet definition

bayo_a = [ bayo_entry_x, 0 ];
bayo_p = [ 0, bayo_entry_z ];
bayo_n = [ 0, bayo_behind-bayo_inramp_z ];
bayo_m = [ bayo_inramp_x, bayo_behind ];
bayo_l = bayo_m + bayo_interf * [ 1/bayo_interf_slope,  1 ];
bayo_k = bayo_l + [ bayo_interf_width, 0 ];
bayo_j = bayo_k + bayo_interf * [ 1/bayo_interf_slope, -1 ];
bayo_i = bayo_j + [ rivet_width + inside_rivet_gap, 0 ];
bayo_h = [ bayo_i[0], bayo_behind + bayo_gap + bayo_interf ];
bayo_g = [ bayo_m[0] - rivet_width, bayo_h[1] ];

bayo_e = [-bayo_p[0], bayo_p[1]] - [rivet_entry_width,0];
bayo_d = [-bayo_a[0], bayo_a[1]] - [rivet_entry_width,0];
bayo_c = bayo_d + [0,-5];
bayo_b = bayo_a + [0,-5];

bayo_f = [ bayo_e[0], bayo_g[1] + (bayo_e[0] - bayo_g[0]) ];

bayo_polygon = [ bayo_a,
		 bayo_b,
		 bayo_c,
		 bayo_d,
		 bayo_e,
		 bayo_f,
		 bayo_g,
		 bayo_h,
		 bayo_i,
		 bayo_j,
		 bayo_k,
		 bayo_l,
		 bayo_m,
		 bayo_n,
		 bayo_p ];

echo(bayo_polygon);

// CATCH

cppxC = 0.41 * sm_inner_rad * TAU;

// catch pin

cpp_adj = (bayo_n[0] - bayo_f[0]) * (1 - sm_inner_rad / bayo_nom_rad);
// radius scaling due to nom and actual radius difference in
// bayo entry construction

cppa = bayo_f + [1,-1] * catch_pin_slop + [1,0] * cpp_adj;
cppb = bayo_g + [1,-1] * catch_pin_slop + [1,0] * cpp_adj;
cppd = [ bayo_n[0]
	 - catch_pin_slop - catch_pin_slop_x_extra,
	 -catch_stalk_above_gap ];
cppi = [ cppa[0], cppd[1] ];
cppc = [ cppd[0], cppb[1] ];
cpph = cppd + [0,-1] * catch_stalk_h;
cppe = cppd + [0,-1] * (catch_knob_above_gap + catch_knob_dia/2);
cppf = [ cppa[0], cppe[1] ];
cppg = [ cppa[0], cpph[1] ];
cppB = 0.5 * (cppf + cppe);

echo("RR", sm_inner_rad / bayo_nom_rad);

// catch assembly depression below pin

cppy6 = cppB[1] - (catch_knob_dia/2
		   + (cppc[1] - cppd[1])
		   + catch_stalk_below_gap);
cpp7 = [ cppB[0], cppy6 + catch_cr ];
cpp11 = cpp7 + [1,0] * catch_cr;
cppy9 = cppy6 + catch_strap_width * 1/3;
cpp9 = [ cpp7[0] + catch_cr * 2, cppy9 ];
cpp8 = cpp9 + [0,-1] * catch_cr;
cpp10 = cpp8 + [-1,0] * catch_cr;
cppC = [ cppxC, cpp9[1] ];
cppD = cppC + [0,-1] * catch_strap_width;

// catch assembly stalk and so on

catch_cr3 = catch_cr + catch_stalk_h;

cppF = [ cppg[0] - catch_stalk_eff_bend_rad, cppd[1] ];
cpp4 = [ cppg[0] - catch_stalk_len, cpph[1] ] + [1,-1] * catch_cr;
cpp5 = [ cpp4[0], cppC[1] + catch_cr ];
cpp2 = cpp5 + [-1,0] * (catch_cr * 2 + catch_stalk_base_width);
cpp2r = cpp2 + [1,0] * catch_cr;
cpp2d = cpp2 + [0,-1] * catch_cr;
cpp3 = [ cpp2[0] + catch_cr + catch_cr3, cppd[1] - catch_cr3 ];
cppA = [ -cppxC, cpp9[1] ];
cppE = [ cppA[0], cppD[1] ];

catch_assembly_dy = -cppy9 + catch_strap_width;


module MainProfile(){
  main_cnr_pos = [ side_thick, top_z ] - [1,1]*main_cnr;
  difference(){
    union(){
      translate(main_cnr_pos){
	intersection(){
	  difference(){
	    circle(r = main_cnr);
	    circle(r = main_cnr * 0.5);
	  }
	  square([10,10]);
	}
      }
      polygon([[ -top_middle_dr,        middle_bot_z      ],
	       [ -top_middle_dr,        top_z             ],
	       [ main_cnr_pos[0],       top_z             ],
	       [ side_thick,            main_cnr_pos[1]   ],
	       [ side_thick,            -side_height      ],
	       [ side_taper,            -side_height      ],
	       [ 0,                     -rivet_posn       ],
	       [ 0,                     by_oring_z        ],
	       [ -oring_x_rad,          by_oring_z        ],
	       ],
	      convexity=10);
    }
    translate([ oring_mid_dia/2 - main_dia/2, 0 ])
      hull(){
      translate([ 0, oring_y_rad ])
	scale([ 1/oring_oblate * (oring_compress_more+1) , oring_oblate ])
	circle(oring_thick/2);
      translate([ 0, oring_y_rad*2 - oring_thick/2 ])
	circle(oring_thick/2);
    }
  }
}

module StrapLoopProfile(){
  circle(r = strap_loop_thick/2);
}

module StrapLoop(){ ////toplevel
  bigrad = strap_loop_inside/2 + strap_loop_thick/2;
  extralen = strap_loop_thick * 5;

  intersection(){
    rotate([strap_loop_elevation, 0,0]){
      for (x= [ -1, +1 ] * bigrad) {
	translate([x, -extralen, 0])
	  rotate([-90,0,0])
	  linear_extrude(height= extralen + strap_loop_strlen + 0.1,
			 convexity=10)
	  StrapLoopProfile();
      }
      translate([0, strap_loop_strlen, 0]){
	intersection(){
	  rotate_extrude(convexity=10)
	    translate([bigrad, 0,0])
	    StrapLoopProfile();
	  translate([0,50,0])
	    cube([100,100,100], center=true);
	}
      }
    }
    translate([0, 50, 0])
      cube(100, center=true);
  }
}

module RotateProjectSlice(offset, slice_size, nom_rad, real_rad){
  // nom_rad > real_rad
  rotate([0,0, atan2(offset, nom_rad) ]){
    intersection(){
      translate([-offset, -10, 0])
	rotate([90,0,0])
	linear_extrude(height= nom_rad*2, convexity=50)
	children(0);
      translate([0,0, -25])
	cylinder(h=50, r= real_rad);
      translate([0,0, -25])
	linear_extrude(height= 50, convexity=50)
	polygon([ [ 0,0 ],
		  [ -slice_size, -real_rad*2 ],
		  [ +slice_size, -real_rad*2 ] ]);
    }
  }
}

module RotateProject(x_min, x_max, slice_size, nom_rad, real_rad){
  offs = [ for (i=[ x_min :
		    slice_size :
		    x_max + slice_size ]) i ];
  echo (offs);
  for (off=offs)
    RotateProjectSlice(off, slice_size, nom_rad, real_rad)
    children(0);
}

module BayonetCutout(){
  RotateProject(bayo_c[0], bayo_i[0], bayo_slice_size,
		bayo_nom_rad, 
		bayo_real_rad)
    translate([-0.5 * (bayo_a[0] + bayo_d[0]), 0])
    polygon(bayo_polygon, convexity=10);
}

module ProfilesDemo(){ ////toplevel
  translate([-10,0]) MainProfile();
  translate([+10, -side_height]) polygon(bayo_polygon, convexity=10);
}

module LimitForHandling(){ ////toplevel
  hull() for (r=[0,180])
    rotate([0,0,r]) {
      for (rs=[-1,+1]) {
	for (xd=[0,1]) {
	  rotate([0,0, rs * handling_angle/2]) {
	    translate([rs * xd * main_dia/2 * tan(handling_angle/2),
		       main_dia/2 + side_thick - main_cnr,
		       top_z - main_cnr]) {
	      mirror([0,0,1])
		cylinder(r= main_cnr, h=50);
	      sphere(main_cnr);
	    }
	  }
	}
      } 
    }
  hull() rotate_extrude(convexity=10){
    translate([ handling_dia/2 - main_cnr, top_z - main_cnr ]) {
      circle(r = main_cnr);
      mirror([0,1]) square([ main_cnr, 50 ]);
    }
  }
  //cylinder(r= handling_dia/2, h=20);
}

module Cover(){ ////toplevel
  render() difference(){
    intersection(){
      union(){
	rotate_extrude(convexity=10)
	  translate([main_dia/2, 0])
	  MainProfile();
	translate([0,0, middle_bot_z])
	  cylinder(h= top_thick_middle, r = main_dia/2 - top_middle_dr + 1);
      }
      LimitForHandling();
    }
    for (r=[0,180]){
      rotate([0,0, r])
	translate([0,0, -side_height])
	BayonetCutout();
      rotate([0,0, r + asin((-oring_rm_beside) / (main_dia/2))])
	translate([0,
		   oring_mid_dia/2 + oring_thick/4 * oring_rm_scale,
		   oring_y_rad * 1.5])
	rotate([-oring_rm_angle, 0, 0])
	mirror([0,0,1])
	cylinder(r = oring_thick/4 * oring_rm_scale, h=20);
    }
    for (r=[0 : 60 : 179]) {
      rotate([0,0, r]) {
	height = top_thick_middle - brace_above_below*2;
	translate([0,0, middle_bot_z + brace_above_below + height/2 ])
	cube(center=true, [ oring_bore - brace_end_shorter,
			    brace_hole_width, height ]);
      }
    }
  }
  if (enable_head_cups)
    for (r=[0,180])
      rotate([0,0,r])
	translate([-implheadcup_large_dia * .5 - implheadcup_thick/2,
		   -implheadcup_large_dia * .0,
		   middle_bot_z + 0.1])
	ImplHeadCup();

//  translate(strap_loop_thick * [-0.5, 0, +1])
//    translate([handling_dia/2, 0, -side_height])
//    rotate([0,180,0]) rotate([0,0,90])
//    StrapLoop();
}

module SavingHole(){
  translate([0,0, -10])
    cylinder(r= main_dia/2 - jig_rim, h=20);
}

module Jig(){ ////toplevel
  difference(){
    union(){
      translate([0,0, -side_height]){
	cylinder(r= main_dia/2 + jig_thick, h= side_height + jig_thick);
      }
      translate([-jig_mark_rad, 0, jig_thick - jig_mark])
	cube([jig_mark_rad*2, jig_mark, jig_mark]);
    }
    translate([0,0, -side_height-1])
      cylinder(r= main_dia/2, h= side_height + 1);
    SavingHole();
    translate([0,0, -rivet_posn])
      rotate([90, 0,0])
      translate([0,0, -100])
      cylinder(r= jig_hole_dia/2, h = 200);
  }
}

module CoverPrint(){ ////toplevel
  rotate([0,180,0]) Cover();
}

module CoverTest2(){ ////toplevel
  difference(){
    Cover();
    SavingHole();
  }
}

module CoverTest1(){ ////toplevel
  difference(){
    CoverTest2();
    difference(){
      for (r= [ 40, 147 ]){
	rotate([0,0, r]){
	  translate([0,0, -10]) {
	    cube([ main_dia*3, main_dia * .53, 18], center=true);
	  }
	}
      }
//      translate([ 50, 0, 0 ])
//	cube([ 100,
//	       strap_loop_inside + strap_loop_thick*2 + 1,
//	       100 ],
//	     center=true);
    }
  }
}

module ImplHeadCupTest(){ ////toplevel
  for (r=[0,180])
    rotate([0,0,r])
      translate([-17,0,0])
      ImplHeadCup();
}

module SomeStrap(width, cut_width=0){
  // children(0) is to add, (1) subtract
  difference(){
    union(){
      cylinder(r=sm_outer_rad, h=width);
      StrapMountProtrusion(smc_cnr_c_x + sm_closure_cnr,
			   smc_max_y,
			   sm_closure_cnr,
			   width);
      children(0);
    }
    translate([0,0,-1])
      cylinder(r=sm_inner_rad, h=max(width+2, cut_width));
    translate(smc_pos)
      StrapMountBolt(5, width);
    translate(smc_pos)
      cube([ sm_bolt_tighten_allow, 40,100 ], center=true);
    children(1);
  }
}

module StrapMountBolt(l_delta, strap_width){ ///toplevel
  // positioned relative to smc_pos
  translate([(smc_bolt_nut_eff_thick - sm_bolt_head_thick)/2,
	     smc_bolt_y,
	     strap_width/2]){
    translate([ -sm_bolt_shaft/2-1, 0,0 ]){
      rotate([0,90,0]) cylinder(r= sm_bolt_dia/2, h= sm_bolt_shaft+2);
    }
    translate([ -sm_bolt_shaft/2, 0,0 ])
      rotate([0,-90,0])
      cylinder($fn=6, r=smc_bolt_nut_dia/2,
	       h=smc_bolt_nut_eff_thick + l_delta);
    translate([ sm_bolt_shaft/2, 0,0 ])
      rotate([0,90,0])
      cylinder(r=sm_bolt_head_dia/2, h=sm_bolt_head_thick + l_delta);
  }
}

module StrapMountProtrusion(half_x, max_y, cnr, width){
  translate(smc_pos){
    linear_extrude(height=width, convexity=10){
      hull(){
	for (m = [0,1]) mirror([m,0,0]) {
	  translate([-(half_x - cnr), max_y - cnr])
	    circle(r=cnr);
	  translate([-half_x, -sm_inner_rad])
	    square([1,1]);
	}
      }
    }
  }
}

module StrapMount(){ ////toplevel
  SomeStrap(sm_main_width){
    rotate([0,0,180]){
      StrapMountProtrusion(strap_loop_inside/2 + strap_loop_thick,
			   strap_loop_thick,
			   sm_closure_cnr,
			   sm_main_width);
      translate(smc_pos +
		[0,0, sm_main_width] +
		strap_loop_thick * [ 0, 0.5, -1.0 ])
	StrapLoop();
    }
    union(){ };
  }
}

module WallScrewHoleSlot(){ ////toplevel
  ds = [-1,+1] * wm_screw_slot/2;
  linextr_x_yz(-(wm_thick + 1), 1) {
    hull(){
      for (d = ds)
	translate([d, 0])
	  circle(r = wm_screw_dia/2);
    }
  }
  hull(){
    for (d = ds){
      translate([0, d, 0]){
	linextr_x_yz(0, 1)
	  circle(r = wm_screw_head/2);
	linextr_x_yz(-(wm_screw_head - wm_screw_dia)/2, 0)
	  circle(r = wm_screw_dia/2);
      }
    }
  }
}

module WallMountMounts(){
  linextr(0, wm_z_max){
    translate([ -sm_outer_rad, 0 ])
      rectfromto([ 0,        wm_lhs_y_min ],
		 [ wm_thick, wm_y_slot1_max ]);
  }
}
module WallMountScrewHoles(){
  translate([ -sm_outer_rad + wm_thick, 0, wm_z_slotc_screw ]) {
    translate([ 0, wm_y_slotc_screw, 0 ])
      WallScrewHoleSlot();
    translate([ 0, -wm_y_slotc_screw, 0 ])
      rotate([90,0,0])
      WallScrewHoleSlot();
  }
}

module WallMount(){ ////toplevel
  SomeStrap(sm_main_width, wm_z_max + 2){
    WallMountMounts();
    WallMountScrewHoles();
  }
}

module WallMountBaseRingCut(){
  circle(r = wmb_mount_cut_rad);
}

module WallMountBaseMounts(){
  linextr(0, wmb_z_max) {
    difference(){
      rectfromto([ -sm_outer_rad, -wmb_y_mount_max ],
		 [ wmb_x_outer,   +wmb_y_mount_max ]);
      WallMountBaseRingCut();
    }
  }
}

// screws, nuts, slots for nuts to go down into
module WallMountBaseScrewsEtc(){ ////toplevel
  for (my=[0,1]) {
    mirror([0, my, 0]) {
      translate([wmb_x_screw, 0, wmb_z_screw]) {
	linextr_y_xz(wmb_y_screw_end,
		     wmb_y_screw_end + 50)
	  circle(r = wmb_screw_dia/2);
	linextr_y_xz(wmb_y_screw_end + web_screw_len,
		     wmb_y_screw_end + 50)
	  circle(r = wmb_screw_head_dia/2);
	linextr_y_xz(wmb_y_nut_min,
		     wmb_y_nut_min + wmb_nut_th) {
	  hull(){
	    rotate(30)
	      circle(r = wmb_nut_rad, $fn = 6);
	    translate([0, 50])
	      square(wmb_nut_across, center=true);
	  }
	}
      }
    }
  }
}

module WallMountForBase(){ ////toplevel
  SomeStrap(sm_main_width, wm_z_max + 2){
    union(){
      WallMountMounts();
      WallMountBaseMounts();
    }
    union(){
      WallMountScrewHoles();
      WallMountBaseScrewsEtc();
    }
  }
}

module WallMountForBaseFixingsTest(){ ////toplevel
  intersection(){
    WallMountForBase();
    linextr(-100,100)
      rectfromto([ -sm_outer_rad-10, -wm_y_min ],
		 [ wmb_x_outer + 1, -100 ]);
  }
}

module WallMountBaseFixingsTest(){ ////toplevel
  intersection(){
    WallMountBase();
    linextr(-2,100)
      rectfromto([ -sm_outer_rad-10, -wm_y_min ],
		 [ wmbb_x_outer + 1, -100 ]);
  }
}

module WallMountBasePillarsPlan(){
  for (my = [0,1]) {
    mirror([0, my]) {
      rectfromto([ -sm_outer_rad, wmbb_y_max - wmb_mount_y_width ],
		 [ wmbb_x_outer, wmbb_y_max ]);
    }
  }
}

// trim parts that are would foul the wall
module WallMountTrimWallFoulPlan(){
    translate([ -sm_outer_rad, 0])
    rectfromto([ -wmbb_r_top, -(wmbb_r_top + 1) ],
	       [ 0,           +(wmbb_r_top + 1) ]);
}

module WallMountBase(){ ////toplevel
  difference(){
    union(){
      // vertical blocks rising to join to wall mount
      linextr(wmbb_z_min, wmb_z_max) {
	difference(){
	  WallMountBasePillarsPlan();
	  WallMountBaseRingCut();
	}
      }

      hull(){
	linextr(wmbb_z_flat_whole_min, wmbb_z_flat_max)
	  circle(r = wmbb_r_top);
	linextr(wmbb_z_min, wmbb_z_flat_max)
	  circle(r = wmbb_r_bottom);
      }
      linextr(wmbb_z_min, wmbb_z_flat_max) {
	hull(){
	  WallMountBasePillarsPlan();
	  circle(r = wmbb_r_bottom);
	}
      }
    }

    // cutaway for mount part
    linextr(-wmb_mount_gap_z, wmb_z_max+1) {
      for (my = [0,1]) {
	mirror([0, my])
	  rectfromto([ -sm_outer_rad-1, wmb_y_mount_max + wmb_mount_gap_xy ],
		     [ wmb_x_outer + wmb_mount_gap_xy, 1 ]);
      }
    }

    linextr(wmbb_z_min - 1, wmb_z_max + 1)
      WallMountTrimWallFoulPlan();
    WallMountBaseScrewsEtc();
  }
}

module WallMountBaseCutJigPlan(){ ////toplevel
  difference(){
    union(){
      circle(r = wmbb_r_top);
    }

    translate([ wmb_jig_around_gap, 0 ])
      WallMountTrimWallFoulPlan();

    offset(delta = wmb_jig_around_gap)
      WallMountBasePillarsPlan();
  }
}

module WallMountBaseCutJig(){ ////toplevel
  translate([ 0,0, wmbb_z_flat_max + 0.5 ])
    linextr(0, wmb_jig_th)
    WallMountBaseCutJigPlan();
}

module WallMountForBaseDemo(){ ////toplevel
  render() WallMountForBase();
  color("blue") render() WallMountBase();
  %WallMountBaseScrewsEtc();
  %WallMountBaseCutJig();
}

module CatchAssemblyCoreProfile(){
  difference(){
    union(){
      hull(){
	translate(cpp3) circle(r= catch_cr3);
	polygon([ cpp3,
		  cpp2r,
		  cpp5,
		  cpph,
		  cppd
		  ]);
      }
      polygon([cppD,
	       cppC,
	       cpp9,
	       cpp10,
	       cpp11,
	       cpp4,
	       cpp2r,
	       cpp2d,
	       cppA,
	       cppE
	       ]);
      translate(cpp8) circle(r= catch_cr);
    }
    hull(){
      translate(cpp4) circle(r= catch_cr);
      translate(cpp5) circle(r= catch_cr);
      translate(cpp7) circle(r= catch_cr);
      polygon([cpp4,
	       cppg,
	       cpph,
	       cpp10,
	       cpp11,
	       ]);
    }
    translate(cpp2) circle(r= catch_cr);
  }
  // if cpp11 is above cpp10, the subtracted hull above
  // can go down too far.  Ensure we do not cut off below cppy6.
  polygon([ cppE,
	    cppD,
	    cpp9,
	    [ cpp9[0],            cppy6 ],
	    [ cpp7[0] - catch_cr, cppy6 ],
	    cpp2d
	    ]);
}

module CatchTipProfile(dy){
  ddy = [0,dy];
  intersection(){
    translate(cppF){
      difference(){
	circle(r = dist2d(cppF, cppd));
	//circle(r = dist2d(cppF, cppa));
      }
    }
    polygon([ cppa,
	      cppi + ddy,
	      cppd + ddy,
	      cppc,
	      cppb ]);
  }
}

module CatchHeadProfile(){
  polygon([ cppd,
	    cppd,
	    cppi,
	    cppf,
	    cppe,
	    cpph ]);
}


module CatchCore(){ /////toplevel
  linear_extrude(height=catch_strap_thick, convexity=10)
    CatchAssemblyCoreProfile();

  hull(){
    linear_extrude(height=catch_head_th, convexity=10)
      CatchTipProfile(0);
    linear_extrude(height=catch_tip_th, convexity=10)
      CatchTipProfile(catch_tip_th - catch_head_th);
  }

  linear_extrude(height=catch_head_th, convexity=10)
    CatchHeadProfile();

  translate(concat(cppB,[0])) hull(){
    translate([0,0, catch_knob_height + catch_head_th - catch_knob_dia/2])
      sphere(r = catch_knob_dia/2);
    cylinder(r = catch_knob_dia/2, h = 0.1);
  }
}

module CatchPreDistort(){ /////toplevel
  scale(100 / sm_inner_rad)
    rotate([90,0,0])
    CatchCore();
}

module CatchAssembly(){ /////toplevel
  rotate([0,0, -(cppe[0] + cppB[0] + catch_pin_slop) / sm_inner_rad * 360/TAU])
    translate([0,0, catch_assembly_dy])
    scale(sm_inner_rad / 100)
    import(str("poster-tube-lid,CatchPostDistort-fa",
	       (coarse ? 20 : 3),
	       ".stl"),
	   convexity=20);

  SomeStrap(catch_strap_width){
    union(){ }
    union(){
      translate([-200, -200, -200])
	cube([400, 200, 400]);
    }
  }
}

module CatchDemo(){ /////toplevel
  color("blue") translate([0,0,
			   -catch_assembly_dy
	     ])
    CatchAssembly();
  translate([0,0,+side_height
	     ])
    Cover();
}

module CatchDemoS(){ /////toplevel
  color("blue") translate([0,0,
	     -catch_assembly_dy
	     ])
    CatchAssembly();
  intersection(){
    translate([0,0,+side_height
	       ])
      Cover();
    mirror([0,1,0]) translate([-250,33,0]) cube([500,500,500]);
  }
  color("black")
    translate([0,-33,0])
    cube([6.15, 2,2], center=true);
}

module CatchPinProfileDemo(){ /////toplevel
  translate([0, 0 * -bayo_behind,0]) {
    echo("G ",
	 bayo_n[0] - bayo_e[0]);
    color("blue") translate([0,0,
			     +1,
	       ]) {
      CatchAssemblyCoreProfile();
      CatchHeadProfile();
    }
    translate([0,0,10])
      color("red")
      CatchTipProfile(0);

    polygon(bayo_polygon, convexity=10);

    // adhoc show a position
    color("purple")
    translate(concat(
		     cppa,
		     10
		     )) difference(){ circle(2.5); circle(2.0); }

 }
}

//ProfilesDemo();
//BayonetCutout();
//MainProfile();
//Cover();
//Jig();
//CoverTest();
