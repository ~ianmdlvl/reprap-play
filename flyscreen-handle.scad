// -*- C -*-

opening_height = 7.84;
opening_depth = 7.88;
openingedge_dia = 2.00;
opening_protrh = 2.00;

pivot_x = 6;
inside_len = 4;

pivoting_gap = 0.1;

outside_gap = 3;
outside_len = 13;
outend_height = 3;

outside_len_bot = 23;

outside_pushh = 4;
outside_pushslope = 1.4;
outside_push_inadj = 0.82;

ourcirc_r = 0.5 / 2;

ribble_dia = 2.2;;

opening_protr_slop = 0.1;

intooth_top_slop = 0.1;
inside_h_xgap = 1;

pivot_r = 2;
pivot_slop = 0.25;

strap_above = 0.1;
strap_th = 2.5;
strap_below = 3;
strap_width = 5;

width = 35;
nstraps = 2;

test_width = 5;

// calculated

inside_h = opening_height/2 - opening_protrh - inside_h_xgap/2;

edge_or = openingedge_dia/2 + opening_protr_slop;

Q0 = [ openingedge_dia/2,
       openingedge_dia/2 + opening_height/2 ];

p4p5d = [edge_or + ourcirc_r, 0];

P0 = [ pivot_x, pivoting_gap ];
P4 = Q0 - p4p5d;
P3t = [ P4[0], Q0[1] - openingedge_dia/2 + opening_protrh
	- intooth_top_slop - ourcirc_r ];
P2 = P4 + [ -(inside_len - ourcirc_r*2), 0 ];
P1 = [ P2[0], P3t[1] - (inside_h + ourcirc_r*2) ];

P5 = Q0 + p4p5d;

P8t = [ outside_len - ourcirc_r, P5[1] ];
P9t = P8t + [ 0, -(strap_above + strap_th + strap_below - ourcirc_r*2) ];

P9b = [ P9t[0], -P9t[1] + outside_gap ];
P8b = P9b + [ 0, outend_height ];

P89eadj = [ outside_len_bot - outside_len, 0 ];
P8eb = P8b + P89eadj;
P9eb = P9b + P89eadj;

P6t = P5 + [ 0, outside_pushh - ourcirc_r*2 ];
P7 = [ P6t[0] + (P6t[1] - P1[1]) / outside_pushslope,
       P1[1] ];

P3a = P3t + [ -outside_push_inadj, 0 ];
P6a = P6t + [ -outside_push_inadj, 0 ];

outside_push_inadj_slope = (P3t[1]-P4[1]) / (P6a[1]-P5[1]);

ribble_rad = ribble_dia/2;

kit_adj_shift = -opening_height - 2.0;

module ExtrusionSect(){
  cr = openingedge_dia/2;
  toph = opening_height/2 + opening_protrh;

  for (my=[0,1]) {
    mirror([0,my]) {
      translate(Q0) {
	hull(){
	  circle(r=cr, $fn=20);
	  translate([-cr,10]) square([cr*2, 1]);
	}
      }
    }
  }
  translate([-opening_depth, -toph]) {
    difference(){
      translate([-5,-5])
	square([opening_depth+6, toph*2+10]);
      square([opening_depth+2, toph*2]);
    }
  }
}

module PsHull(ps) {
  hull(){
    for (p = ps) {
      translate(p)
	circle(r = ourcirc_r, $fn=10);
    }
  }
}

module LeverSect(top, inadj=false){
  P3 = inadj ? P3a : P3t;
  P6 = inadj ? P6a : P6t;
  P8 = top ? P8t : P8b;
  P9 = top ? P9t : P9b;
  difference(){
    union(){
      PsHull([P2,P3,P4]);
      PsHull([P0,P1,P2,P5,P8,P9]);
    }
    hull(){
      for (dp = [ [0,0],
		  (P6-P5),
		  (P3-P4)
		  ]) {
	translate(Q0 + 5*dp) circle(r=edge_or, $fn=20);
      }
    }
  }
}

module StrapSectTop(){
  translate(P9t + ourcirc_r * [+1,-1]) {
    difference(){
      circle(r = strap_below + strap_th, $fn=40);
      circle(r = strap_below,            $fn=40);
    }
  }
}

module StrapSectBot(inadj=false){
  mirror([0,1]){
    for (dx = [ -(strap_below + strap_th),
		0 ]) {
      translate(P9b + [ ourcirc_r + dx, -10 ]) {
	square([strap_th, 20]);
      }
    }
  }
}

module Ribbles(xmax, xmin, y){
  for (x = [ xmax + ourcirc_r - ribble_rad :
	     -ribble_rad * 4 :
	     xmin ]) {
    translate([x, y])
      circle(r = ribble_rad, $fn=20);
  }
}

module LeverSectTop(){
  difference(){
    union(){
      LeverSect(true, false);
      Ribbles(P8t[0],
	      Q0[0] + edge_or + ribble_rad*2,
	      P5[1] + ourcirc_r);
    }
    translate([pivot_x,0]) circle(r= pivot_r + pivot_slop, $fn=20);
  }
}

module LeverSectBot(inadj=false){
  P6 = inadj ? P6a : P6t;
  mirror([0,1]) {
    LeverSect(false, inadj);
    PsHull([P5,P6,P7]);
    PsHull([P8b,P8eb,P9eb,P9b]);
    Ribbles(P8eb[0],
	    P9b[0],
	    P8eb[1]);
    translate([pivot_x,0]) circle(r=pivot_r, $fn=20);
  }
}

module Demo(){
  translate([0,0,-5]) color("white") ExtrusionSect();
  LeverSectTop();
  translate([0,0,5]) LeverSectBot();
  color("black") LeverSectBot(true);
  color("blue") translate([0,0,10]) StrapSectTop();
  color("purple") translate([0,0,-10]) StrapSectBot();
}

module SomeLever() {
  // SomeLever(){ LeverBot(inadj); LeverSectBot(); }
  difference(){
    linear_extrude(height=width, convexity=100) children(0);
    for (i = [ 0 : nstraps - 1 ]) {
      translate([0,0, (i + 0.5) / nstraps * width - strap_width/2])
	linear_extrude(height=strap_width, convexity=10)
	children(1);
    }
  }
}

module Test(){
  linear_extrude(height=test_width, convexity=100) {
    translate([0,2,0]) LeverSectTop();
    LeverSectBot();
    translate([0,kit_adj_shift]) LeverSectBot(true);
  }
}

module LeverTop(){ ////toplevel
  SomeLever(){
    LeverSectTop();
    StrapSectTop();
  }
}
module LeverBotOutside(){ ////toplevel
  SomeLever(){
    LeverSectBot();
    StrapSectBot();
  }
}
module LeverBotInside(){ ////toplevel
  SomeLever(){
    LeverSectBot(true);
    StrapSectBot(true);
  }
}

module KitOutside(){ ////toplevel
  translate([0,2,0]) LeverTop();
  LeverBotOutside();
}

module KitInside(){ ////toplevel
  translate([0,2,0]) LeverTop();
  LeverBotInside();
}

//LeverSectBot(true);
//Demo();
//LeverTop();
//Test();
//Kit();
//KitInside();
