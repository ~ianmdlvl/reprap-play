// -*- C -*-

// Infill density: 20%

include <funcs.scad>
include <utils.scad>

nook_th = 12.41 + 0.50 - 1.50 + 1.35 - .25;
nook_w = 127.12 + 0.75 - .95 - .50;
nook_h = 123.44 + 21.88 + 21.05 + 0.75 - 1.90 - 0.50 - 0.50;

edge_ledge_w = 9.60;
edge_ledge_h = 2.44 - .25;
edge_ledge_inc_ang = 10; // degrees

usb_w = 14.5;
usb_below = 1.5;

open_recess_w = 12.5;
open_recess_h = 2.5;

open_recess_2_len = 15;
open_recess_2_d_tooth = 30;

nook_cnr_rad = 10;

case_th = 2.5;
ledge_w = 4;
tape_th = 1.75;
tape_inside = 2.0;

gap = 0.5 * [1,1];

tape_w = 15;

test_pillar = 4;

engage_l0 = 10;
engage_l1 = 10;
engage_l2 = 3;

tooth_inward = gap[0] * 1.0 + 0.25 + 0.25;
tooth_w = 15;

diag_near_hinge_slope = 0.5;

$test = false;

$fa = $test ? 10 : 3;
$fs = $test ? 0.1 : 1;

// calculated

tooth_height = nook_th;
ledge_h = case_th;
lid_th = case_th;
tooth_th = case_th;

spp0 = [0,0];
spp1 = spp0 + case_th * [-1,0];
spp9 = spp0 + ledge_h * [0,-1];
spp8 = spp9 + nook_th * [0,-1];
spp7 = spp8 + case_th * [-1,-1];

spp11y = spp1[1] - tape_th;
spp4y  = 0.5 * (spp0[1] + spp7[1]);
spp3y = spp4y + tape_inside/2;  spp5y = spp4y - tape_inside/2;
spp2y = spp3y + tape_th;        spp6y = spp5y - tape_th;

spp20 = spp8 + nook_cnr_rad * [1,0];
spp20x = spp20[0];

tppA = spp0 + gap;
tppB = spp1 + [0, gap[1]];
tppC = tppB + lid_th * [0,1];
tppD = [ spp20x, tppC[1] ];
tppE = [ spp20x, tppB[1] ];
tppF = tppA + ledge_w * [1,0];
tppG = tppF + ledge_h * [0,-1];
tppH = [ tppA[0], tppG[1] ];

tppJx = tppA[0] + tape_th;

tppK = [ tppC[0], tppG[1] ];
spp31 = tppK - [0, gap[1]];
spp30 = [ spp8[0], spp31[1] ];

nom_cnr = 0.5 * [nook_w, nook_h, 0] - nook_cnr_rad * [1,1,0];

tooth_y = nom_cnr[1] - tooth_w/2;

etxa = nom_cnr[0] - engage_l2;
etxb = etxa - engage_l1;
etxc = -(nom_cnr[0] - engage_l2);

tapa = nom_cnr[1] - engage_l2;
tapb = tapa - tape_w;

opra = nom_cnr[1] - engage_l2;
oprb = opra - open_recess_w;

opqa = tooth_y - open_recess_2_d_tooth + open_recess_2_len/2;
opqb = opqa - open_recess_2_len;

tppS = tppB + [-gap[0], 0];
tppP = [ tppS[0] - tooth_th, tppC[1] ];
tppQ = tppP + tooth_height * [0,-1] + tooth_inward * [1,0];
tppR = [ tppS[0] + tooth_inward, tppQ[1] ];
tppM = (tppQ + tppR) * 0.5 + tooth_th * 0.5 * [0,1];

edge_ledge_rad = edge_ledge_h;

module RightSideMainProfile() {
  rectfromto(spp7, spp0);
  rectfromto(spp7, spp20);
  EdgeLedgeProfile();
}

module LeftSideMainProfile() {
  rectfromto(spp7, spp30);
  rectfromto(spp7, spp20);
  EdgeLedgeProfile();
}

module EdgeLedgeProfile() {
  intersection(){
    hull(){
      for (t=[[0,0], [-20,0], [0,-10]]) {
	translate(spp8
		  + [edge_ledge_w, edge_ledge_h]
		  + edge_ledge_rad * [ -sin(edge_ledge_inc_ang),
				       -cos(edge_ledge_inc_ang) ]
		  + t)
	  circle(edge_ledge_rad);
      }
    }
    translate(spp7)
      square(30);
  }
}

module TopTapeCutout() {
  polygon([ tppA,
	    tppA + [-40, 0],
	    tppG + [-40,-1],
	    [ tppJx, tppH[1]-1 ],
	    [ tppJx, tppC[1]+1 ],
	    [ tppA[0], tppC[1]+1 ]]);
}

module RightTopMainProfile() {
  l = [ tppA, tppB, tppC, tppD, tppE, tppF, tppG, tppH ];
  polygon(l);
}

module LeftTopMainProfile() {
  l = [ tppC, tppD, tppE, tppF, tppG, tppK ];
  polygon(l);
}

module SideTapeCutout1(y0,y1) {
  rectfromto([ spp7[0]-1, y0 ],
	     [ spp8[0]+1, y1 ]);
}

module SideTapeCutout() {
  SideTapeCutout1(spp6y, spp5y);
  SideTapeCutout1(spp3y, spp2y);
  SideTapeCutout1(spp3y, spp2y);
  SideTapeCutout1(spp11y, spp1[1] + 1); // obsolete I think
}

module ToothProfile(){
  polygon([tppA,
	   tppB,
	   tppS + [-0.1,0],
	   tppP,
	   tppC]);
  hull(){
    polygon([tppP,
	     tppM,
	     tppS]);
    translate(tppM)
      circle(r= tooth_th/2);
  }
}

module Demo(){ ////toplevel
  translate([-1,0,0]) {
    translate([0,0,-2]) LeftSideMainProfile(); 
    translate([0,0,-2]) color("yellow") LeftTopMainProfile();
    color("red") difference(){
      LeftSideMainProfile();
      SideTapeCutout();
    }
    translate([0,0,-4]) color("brown") EdgeLedgeProfile();
    translate(concat(spp8 + [edge_ledge_w, edge_ledge_h], [2]))
      rotate(-edge_ledge_inc_ang) {
      color("blue") square(3);
      color("lightblue") mirror([1,0]) square(3);
    }
  }
  translate([0,0,0]) color("purple") difference(){
    LeftTopMainProfile();
    TopTapeCutout();
  }
  translate([nook_cnr_rad*2 + 5, 0,0]) mirror([1,0,0]) {
    color("red") RightSideMainProfile();
    color("purple") RightTopMainProfile();
    color("grey") translate([0,0,-2]) ToothProfile();
  }
  //%SideTapeCutout();
}

module FaceCore(z0,z1, extra_left, extra_right){
  difference(){
    for (mx=[0,1]) mirror([mx,0,0]) {
	for (my=[0,1]) mirror([0,my,0]) {
	    translate(-nom_cnr) {
	      rotate_extrude(angle=90, convexity=10) {
		intersection(){
		  translate(-[1,0,0] * nook_cnr_rad)
		    children(mx);
		  rectfromto([-100,-100], [0,100]);
		}
	      }
	    }
	  }
	translate([nook_w/2, 0,0])
	  linextr_y_xz(-nom_cnr[1]-0.1, nom_cnr[1]+0.1)
	  children(1-mx);
      }
    for (my=[0,1]) mirror([0,my,0]) {
	translate([-nook_w/2, 0,0])
	  mirror([1,0,0])
	  linextr_y_xz(tapb, tapa)
	  children(2);
      }
  }
  for (my=[0,1]) mirror([0,my,0]) {
      translate([0, -nook_h/2, 0]) {
	linextr_x_yz(-nom_cnr[0]-0.1,    etxc + extra_left)  children(0);
	linextr_x_yz(etxc - extra_right, etxb + extra_right) children(1);
	linextr_x_yz(etxb - extra_left,  etxa + extra_left)  children(0);
	linextr_x_yz(etxa - extra_right, nom_cnr[0]+0.1)     children(1);
      }
    }
  if (!$test) {
    linextr(z0,z1)
      rectfromto(-nom_cnr, nom_cnr);
  }
}

module DiagonaliseNearHinge(wider){
  sz = spp0[1] - spp30[1] + gap[1];

  for (my=[0,1]) mirror([0,my,0]) {
      translate([-etxa, -nook_h/2, 0])
	mirror([1,0,0])
	linextr_y_xz(spp31[0] - wider, spp30[0] + gap[0] + 0.1)
	translate([ 0, spp30[1] ])
	polygon([[  -1, 0 ],
		 [   0, 0 ],
		 [  sz/diag_near_hinge_slope, sz ],
		 [  sz/diag_near_hinge_slope, sz + 0.1 ],
		 [  -1, sz + 0.1 ]]);
    }
}    

module Base(){ ////toplevel
  difference(){
    FaceCore(spp7[1],spp8[1], 0.3, 0) {
      LeftSideMainProfile();
      RightSideMainProfile();
      SideTapeCutout();
    }
    translate([0, -nook_h/2, 0])
      mirror([0,1,0])
      linextr_x_yz(-usb_w/2, usb_w/2)
      rectfromto(spp8 + [-40, usb_below], [40, 40]);
    translate([ gap[0], 0,0 ])
      DiagonaliseNearHinge(10);
/*
    translate([nook_w/2, 0, 0])
      linextr_y_xz(oprb, opra)
      translate(spp0)
      rectfromto([-40, -open_recess_h], [40, 1]);
*/
  }
  if ($test) {
    linextr(spp7[1], spp8[1]) {
      hull(){
	for (r=[0,180])
	  rotate([0,0,r])
	    translate(nom_cnr + -1 * nook_cnr_rad*[1,1])
	    square(12);
      }
    }
  }
}

module Top(){ ////toplevel
  difference(){
    FaceCore(tppE[1],tppD[1], -gap[0], gap[0] + 0.3) {
      LeftTopMainProfile();
      RightTopMainProfile();
      TopTapeCutout();
    }
    translate([nook_w/2, 0,0])
      linextr_y_xz(opqb, opqa)
      rectfromto(spp8, tppC + [-1,1]);
  }
  translate([0,0, gap[1]])
    DiagonaliseNearHinge(0);
  translate([nook_w/2, tooth_y, 0])
    linextr_y_xz(-tooth_w/2, +tooth_w/2)
    ToothProfile();
}
module TopPrint(){ ////toplevel
  rotate([0,0,90]) rotate([180,0,0]) Top();
}
module BasePrint(){ ////toplevel
  rotate([0,0,90]) Base();
}

module TestExtrude(){
  difference(){
    linextr_y_xz(-test_pillar, tape_w+test_pillar) children(0);
    linextr_y_xz(           0, tape_w            ) children(1);
  }
}

module BaseTestRest(){ ////toplevel
  cube([30,15, spp8[1]-spp7[1]]);
}

module Demo3(){ ////toplevel
  color("purple") Top();
  color("red") Base();
}

module TestSide(){ ////toplevel
  TestExtrude() { LeftSideMainProfile(); SideTapeCutout(); }
}

module TestTop(){ ////toplevel
  TestExtrude() { LeftTopMainProfile(); TopTapeCutout(); }
}
module TestTopPrint(){ ////toplevel
  rotate([180,0,0]) TestTop();
}
