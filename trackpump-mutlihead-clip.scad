// -*- C -*-

include <commitid.scad>

pump_main_dia = 38;
pump_side_width = 5;
pump_side_thick = 4;
pump_shaft_dia = 14;
baseplate = 3;

pump_protr_flat = 3;
pump_protr_slope = 0.9;

hose_inner_dia = 20;
hose_aperture = 11;
hose_side_width = 5;
hose_base_offset = 30;

hose_side_thick = 6;
hose_side_stalk_width = 6;

pump_protr_protr = 3;
pump_side_height = 20;

// calculated
pump_protr_slheight = pump_protr_protr / pump_protr_slope;

pump_side_outer_rad = pump_side_width + pump_main_dia/2;

baseplate_width_rad =
  sqrt( pow(pump_side_outer_rad, 2)
	-pow( pump_main_dia/2 - pump_protr_protr, 2) );

xm = baseplate + pump_main_dia/2;

pump_side_total_height =
  pump_side_thick + pump_side_height + pump_protr_slheight + pump_protr_flat;

$fa=5;

module PumpSidePlan() {
  or = pump_side_outer_rad;
  difference(){
    union(){
      intersection(){
	translate([-xm, 0]) circle(r=or);
//	translate([-(xm+or), -or]) square([xm+or, or*2]);
      }
    }
    translate([-xm-or, 0])
      square(center=true, [pump_side_width*4, pump_shaft_dia]);
  }
}

module PumpSideElevation(){
  x3 = 0;
  x2 = x3 - baseplate;
  x1 = x2 - pump_main_dia;
  x0 = x1 - pump_side_width;
  x2a = x2 - pump_protr_protr;
  x4 = x2 + pump_side_width;

  z0 = 0;
  z1 = z0 - pump_side_thick;
  z2 = z1 - pump_side_height;
  z2a = z2 - pump_protr_slheight;
  z2b = z2a - pump_protr_flat;

  arcx = x2-x1;
  arcy = z1-z2;

  translate([x0,z1]) square([x1-x0, z0-z1]);

  difference(){
    translate([x1,z2]) square([x3-x1, z0-z2]);
    translate([x1,z2]) scale([1,arcy/arcx]) circle(r=arcx);
  }

  translate([x2,z2a]) square([x4-x2, z0-z2a]);

  hull(){
    translate([x2,z2a]) square([x4-x2, z2-z2a]);
    translate([x2a,z2b]) square([x3-x2a, z2a-z2b]);
  }
}

module PumpSide(){
  br = baseplate_width_rad;
  brs = hose_side_stalk_width/2;
  echo(brs);

  difference(){
    intersection(){
      translate([0,100,0])
	rotate([90,0,0])
	linear_extrude(height=200, convexity=10)
	PumpSideElevation();
      union(){
	translate([0,0,-100])
	  linear_extrude(height=200, convexity=10)
	  PumpSidePlan();
	// baseplate
	hull(){
	  mirror([0,0,1])
	    translate([-xm, -brs, 0])
	    cube([pump_main_dia/2 + pump_side_width,
		  brs*2,
		  1]);
	  translate([-xm, -br, -pump_side_total_height])
	    cube([xm,
		  br*2,
		  pump_protr_flat]);
	}
      }
    }
    translate([-(baseplate + pump_main_dia/2), 0,
		-(pump_side_thick + pump_side_height)])
      cylinder(r=pump_main_dia/2, h=200);
  }
  rotate([0,0,180])
    mirror([0,0,1])
    translate([-0,
	       -br,
	       pump_side_total_height])
    Commitid_BestCount_M([baseplate + pump_protr_protr,
			  br*2]);
}

module HoseSidePlan(){
  ro = hose_inner_dia/2 + hose_side_width;
  ri = (hose_inner_dia/2);
  st = hose_side_stalk_width/2;

  apx = sqrt( ri*ri - (hose_aperture*hose_aperture)/4 );
  apsq = hose_base_offset + apx - hose_aperture/2;
  echo(apx,apsq);

  difference(){
    union(){
      translate([-1, -st]) square([hose_base_offset+1, st*2]);
      translate([hose_base_offset, 0]) circle(r= ro);
    }
    translate([hose_base_offset, 0]) circle(r= hose_inner_dia/2);
    translate([apsq, 0])
      rotate(-45)
      square([50,50]);
  }

  //%translate([hose_base_offset + apx, 0]) square([50,50]);
  //%square(center=true, [100, hose_aperture]);
}

module HoseSide(){
  mirror([0,0,1])
    linear_extrude(height=hose_side_thick, convexity=10)
    HoseSidePlan();
}

module Clip(){
  PumpSide();
  HoseSide();
}

module ClipPrint(){
  rotate([180,0,0])
    Clip();
}

//PumpSidePlan();
//PumpSideElevation();
//PumpSide();
//HoseSide();
//Clip();
ClipPrint();
