// -*- C -*-

include <threads.scad>
include <utils.scad>

// https://en.wikipedia.org/wiki/ISO_metric_screw_thread

// M6
thread_nom = 4;
thread_pitch = 0.70;
thread_act = thread_nom + 0.375;
head_size = 10;

thread_len = 12.5;
base_th = 1.5;
base_sz = [40, head_size];

$test = false;

// calculated

base_dia = head_size / cos(30);

module ScrewThread(){
  translate([0, 0, -0.1])
    render()
    metric_thread(diameter=thread_act, pitch=thread_pitch,
		  leadin=1, internal=true,
		  test=$test, length=thread_len + 0.1);
}

module TestThread(){
  difference(){
    union(){
      linextr(-base_th, 0)
	square(center=true, base_sz);

      linextr(-base_th, thread_len - 0.1)
	circle(r= base_dia/2, $fn=6);
    }

    ScrewThread();
  }
}

TestThread();
