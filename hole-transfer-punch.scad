// -*- C -*-

include <utils.scad>

dia = 6;

point = 3;
solid = 3;

$fa= 1;
$fs = 0.1;

// calculated

depth = dia * 1.5;

module Blivet(){
  rotate_extrude(){
    polygon([[ 0,0 ],
	     [ point, 0 ],
	     [ point, solid ],
	     [ 0, solid + point]]);
  }
  linextr(0, solid){
    square(center=true, [ dia*3, dia ]);
  }
  linextr(-depth, solid){
    circle(r= dia/2);
  }
}

Blivet();
