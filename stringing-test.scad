// -*- C -*-

height = 15;
$fn= 20;

cylinder(r=7, h=height);

for (r= [0:4]) {
  rotate(r * 72) {
    d = 10 * pow(1.5, r);
    cube([d, 3, 2]);
    translate([d, 0,0])
      cube([7, 7, height]);
  }
}
