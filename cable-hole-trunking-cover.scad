// -*- C -*-

holedia = 25;
tapethick = 1.5;
cutoutsz= 15;
innerz = 11;

sidesflatbase = 2;
endsflatbase = 8;

basex = holedia + endsflatbase*2;
basey = holedia + sidesflatbase*2;

bevely = 2.75;
bevelslope = 0.75;
bevelz = bevely / bevelslope;;
basebevelt = 3;

sideslop = 0.5;

basebaset = 2;
sidewallt = 2;

lidt = 1.3;
endwallt = 2;
zslop = 0.75;
endslop = 0.75;

module sheared_cube(sz, xperz, yperz) {
  multmatrix([[1,0,xperz,0],
	      [0,1,yperz,0],
	      [0,0,1,    0],
	      [0,0,0,    1]])
    cube(sz);
}

module Base(cutouty){
  echo(cutouty);
  difference(){
    union(){
      for (mir=[0,1]) mirror([0,mir,0]) {
	translate([0, basey/2 - basebevelt, 0])
	  sheared_cube([basex, basebevelt, bevelz], 0, bevelslope);
	cube([basex, basey/2, basebaset]);
      }
    }
    translate([basex/2, 0, -1])
      cylinder(r=holedia/2, h=bevelz+2);
  }
  rotate([90, 0, 90]) {
    linear_extrude(height=endwallt) {
      difference(){
	for (mir=[0,1]) mirror([mir,0,0]) {
	    polygon([[-0.1,             0],
		     [basey/2,          0],
		     [basey/2 + bevely, bevelz],
		     [basey/2 + bevely, innerz],
		     [-0.1,             innerz]]);
	}
	translate([cutouty, 0])
	  square(size=[cutoutsz, 3*innerz], center=true);
      }
    }
  }
}

module Lid(){
  lidx = basex + endslop + endwallt;
  for (mir=[0,1]) mirror([0,mir,0]) {
    translate([0, basey/2 + sideslop + bevely, 0])
      rotate([90,0,90])
      linear_extrude(height = lidx)
      polygon([[0,         0],
	       [-bevely,   0],
	       [0,         bevelz],
	       [0,         innerz + lidt + zslop],
	       [sidewallt, innerz + lidt + zslop],
	       [sidewallt, -tapethick],
	       [0,         -tapethick]]);
    translate([0, -1, innerz + zslop])
      cube([lidx, 1 + basey/2 + sideslop + bevely + sidewallt, lidt]);
    translate([basex + endslop, -1, -tapethick])
      cube([endwallt, 1 + basey/2 + sideslop + bevely + sidewallt,
	    tapethick + innerz + zslop + 0.1]);
  }
}

module LidT(){ ////toplevel
  rotate([180,0,0]) Lid();
}

module BaseCMid(){ ////toplevel
  Base(0);
}

module BaseCTop(){ ////toplevel
  Base(basey/2 + bevely - cutoutsz/2);
}

module BaseCBot(){ ////toplevel
  Base(-(basey/2 + bevely - cutoutsz/2));
}

module BaseCNone(){ ////toplevel
  Base(basey);
}

module Demo(){ ////toplevel
  BaseCTop();
  %Lid();
}

//BaseCTop();
//BaseCMid();
//BaseCBot();
//BaseCNone();
//Lid();
//LidT();
//Demo();
