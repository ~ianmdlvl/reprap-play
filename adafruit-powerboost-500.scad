// -*- C -*-

psu_sz_nom = [ 21.59, 35.56 ];

//// toplevels-from:
include <adafruit-powerboost-common.scad>

psu_baffle_cnr_y = 7.45; // from connector end
psu_baffle_th = [ 0.8, 3.5 ];
psu_usbend_led_x = 4.5;
psu_innerend_led_depth = 10;

// ----- calculated -----

psu_usbend_led_depth = psu_baffle_cnr_y*2 - psu_usbend_led_x;


module PsuLedBafflePlan(){
  baffle_tr = [0, psu_baffle_cnr_y]
    + 0.5 * [psu_baffle_th[1], psu_baffle_th[0]];
  translate([0, -psu_sz[1]/2]) {
    mirror([1,0,0]) {
      rectfromto([-psu_baffle_th[1]/2, 0],
		 baffle_tr);
      rectfromto([-psu_sz[0]/2 - psu_board_support_wall *2,
		  baffle_tr[1] - psu_baffle_th[0]],
		 baffle_tr);
    }
  }
}

module PsuLedLegendsPlan(){
}

module PsuLedWindowsPlanCore(){
  difference(){
    union(){
      // Two LEDs incl "Chrg", one side of inlet connector
      AtPsuMountCorner(1,0) {
	rectfromto([ -(psu_board_support_wall + 0.1),
		     +psu_usbend_led_x ],
		   [ psu_sz[0]/2,
		     +psu_usbend_led_depth ]);
      }

      // One LED, "Low", other side of inlet connector
      AtPsuMountCorner(0,0) {
	sz = psu_baffle_cnr_y - psu_board_support_wall - psu_baffle_th[0];
	translate([0, psu_baffle_cnr_y])
	  rectfromto([ -(psu_board_support_wall + 0.1),
		       -sz/2 ],
		     [ psu_sz[0]/2,
		       +sz/2 ]);
      }

      // One LED, PWR, near outlet USB pads
      AtPsuMountCorner(0,1){
	rectfromto([0,0],
		   [psu_sz[0]/2 - psu_hole_pos[1] - psu_hole_dia/2,
		     psu_innerend_led_depth]);
      }
    }
    translate([0, -psu_sz[1]/2])
      square(center=true, [psu_baffle_th[1], psu_sz[1]]);;
  }
}
