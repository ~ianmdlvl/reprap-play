// -*- C -*-
//
// Print (fine detail settings):
//
//   * Bottom
//   * MiddlePrint
//   * CoverPrint

include <utils.scad>

usb_w = 12.01 + 0.19;
usb_wall_w = 0.51;
usb_tongue_d = 8.97 - 0.2;
usb_tongue_w_slop = +0.5;
usb_wall_h = 4.54 - 2.04;
usb_ceil_th = 0.425;

wall_th = 1.5; // XXXX rename wall_th

board_l = 17.56 + 0.2;
board_w = 19.14 + 0.2;
board_th = 1.92 + 0.1;

sw_to_edge = board_w/2 + 0.1;

front_wall_th = 0.75;
// egress_w = 8.0;

wall_y_min = -board_l - wall_th;
ceil_y_min = wall_y_min - 5;;

small_walls = [
	       [ [0, 0], [-sw_to_edge, -1.0] ],
	       [ [sw_to_edge-4.5, -4.5], [sw_to_edge, -5.7] ],
//	       [ [3.0, -11.72],              [sw_to_edge, -13.38] ],
	       [ [-sw_to_edge+3.85, -14.90], [sw_to_edge, -13.38] ],
	       ];
chip_cutout = [[ -sw_to_edge + 4.20,    -3.75 ],
	       [ -sw_to_edge + 11.95,  -11.90 ]];

strain_w = 3.5 + 0.5;
strain_t = 1.5 + 0.5;
strain_pitch_across = 5;
strain_pitch_along = 10;
strain_groove_d = 2;
strain_groove_w = 4.5;
strain_around = [2.25, 2.00];

cover_strap_c_d_y = 5.5; // from front of board
cover_registration_sz_y = 2;
cover_registration_sz_z = 3;
midbot_registraton_sz_x = 3;

cable_space_z = 6;
cable_dia = 6;
bottom_floor_th = 1.5;

fit_gap_z = 0.5;
fit_gap_y = 0.25;
side_x_gap = 0.5;

cover_ceil_th = 0.9;

cover_strap_sz_x = wall_th * 3.5;

// calculated

strap_w = strain_w;
cover_strap_cutout_z = wall_th;

middle_top_z = usb_wall_h;
middle_base_z = -board_th;
bottom_base_z = middle_base_z - cable_space_z - bottom_floor_th;;

front_y_max = front_wall_th;
main_y_min = -board_l - wall_th;

strain_0_y_c = main_y_min - strain_w/2;
strain_1_y_c = strain_0_y_c - strain_pitch_along;
total_y_min = strain_1_y_c - strain_w/2 - wall_th;

bottom_wall_top_z = (middle_top_z + middle_base_z) * 0.5 - fit_gap_z/2;
cover_wall_bot_z  = (middle_top_z + middle_base_z) * 0.5 + fit_gap_z/2;
cover_top_z  = middle_top_z + cover_ceil_th;

middle_side_wall_x = +board_w/2 + wall_th;
total_side_wall_x = middle_side_wall_x + wall_th + side_x_gap;

cover_registration_c_dy = -cover_strap_c_d_y - strap_w/2
  - wall_th - cover_registration_sz_y/2;

midbot_registration_sz_y = cover_registration_sz_y;
midbot_registration_sz_z = cover_registration_sz_z;
midbot_registration_y_min = total_y_min + wall_th*2;
midbot_registration_y_max = midbot_registration_y_min
  + midbot_registration_sz_y;
midbot_registration_y_around_max = midbot_registration_y_max
  + wall_th*2;
midbot_registration_bottom_x = board_w/2 - midbot_registraton_sz_x;

midbot_strap_c_y = 0.5 * (strain_0_y_c + strain_1_y_c);

module BothSides(){
  for (m=[0,1]) {
    mirror([m,0,0]) {
      children();
    }
  }
}

module NormalStrapCutouts(y_c, z, rot){
  BothSides(){
    translate([ -total_side_wall_x, y_c, z ])
      rotate([0, rot, 0])
      cube([ wall_th,
	     strap_w,
	     10 ],
	   center=true);
  }
}
module BottomStrapCutouts(y_c){
  NormalStrapCutouts(y_c, bottom_base_z, -45);
}
module CoverStrapCutouts(){
  BothSides(){
    translate([ -total_side_wall_x, -cover_strap_c_d_y, cover_top_z ])
      cube([ cover_strap_sz_x*2,
	     strap_w,
	     cover_strap_cutout_z*2 ],
	   center=true);
  }
}

module FrontWallsPlan(slop) {
  BothSides(){
    rectfromto([ -board_w/2 - wall_th,    0             ],
               [ -usb_w/2 - slop,         front_wall_th ]);
  }
}
module MiddleSmallWallsPlan() {
  for (m=[0,1]) {
    mirror([m,0]) {
      rectfromto([ -usb_w/2,              -0.01        ],
		 [ -usb_w/2 + usb_wall_w, usb_tongue_d ]);
    }
  }
  FrontWallsPlan(0);
  for (w=small_walls) {
    rectfromto(w[0], w[1]);
  }
}
module MiddleCeilPlan() {
  difference(){
    BothSides(){
      rectfromto([ -usb_w/2,              -0.01        ],
		 [ 0.1,                   usb_tongue_d ]);
      rectfromto([ -board_w/2 - wall_th, 0            ],
		 [ 0.1,                  ceil_y_min   ]);
    }
    rectfromto(chip_cutout[0], chip_cutout[1]);
  }
}
module MiddleMainWallsPlan() {
  BothSides(){
    rectfromto([ -board_w/2 - wall_th, 0          ],
	       [ -board_w/2,           wall_y_min ]);
  }
  FrontWallsPlan(usb_tongue_w_slop);
  rectfromto([ -board_w/2 - wall_th + 0, -      board_l   ],
	     [ +board_w/2 + wall_th,            total_y_min ]);
}

module RegistrationsMinkowski(){
  minkowski(){
    cube([ 1, fit_gap_y*2, fit_gap_z*2 ], center=true);
    children();
  }
}
module CoverRegistrations(){
  linextr_y_xz(cover_registration_c_dy - strap_w/2,
	       cover_registration_c_dy + strap_w/2) {
    difference(){
      rectfromto([ -total_side_wall_x,
		          cover_wall_bot_z - cover_registration_sz_z  ],
		 [ +total_side_wall_x, cover_top_z ]);
      hull(){
	MiddleElevationForCutout();
	translate([0, -20]) MiddleElevationForCutout();
      }
    }
  }
}
module MidBotRegistrations(){
  linextr_y_xz(midbot_registration_y_min,
	       midbot_registration_y_max) {
    BothSides(){
      rectfromto([ midbot_registration_bottom_x, middle_base_z + 0.1 ],
		 [ middle_side_wall_x, middle_base_z
		        - midbot_registration_sz_z ]);
    }
  }
}

module MiddleStrainHoles(){
  BothSides(){
    for (y_c = [strain_0_y_c, strain_1_y_c]) {
      translate([strain_pitch_across/2, y_c, 0])
	square([ strain_t, strain_w ], center=true);
    }
  }
}
module Middle(){ ////toplevel
  difference(){
    union(){
      linextr(0, usb_wall_h)
	MiddleSmallWallsPlan();
      linextr(usb_wall_h - usb_ceil_th, usb_wall_h)
	MiddleCeilPlan();
      linextr(-board_th, usb_wall_h)
	MiddleMainWallsPlan();
      BothSides()
	linextr(cover_wall_bot_z, middle_top_z)
        rectfromto([ -(board_w/2 + 0.1),  total_y_min             ],
		   [ -total_side_wall_x,  main_y_min - fit_gap_y  ]);
      MidBotRegistrations();
    }

    linextr(-20, 20)
      MiddleStrainHoles();
    linextr_y_xz(total_y_min-1, main_y_min)
      translate([0, middle_base_z])
      scale([1, strain_groove_d/strain_groove_w])
      circle(strain_groove_w/2, $fn = 8);
    NormalStrapCutouts(midbot_strap_c_y,
		       middle_top_z, 45);
  }
}
module MiddlePrint(){ ////toplevel
  rotate([180,0,0]) Middle();
}

module MiddleElevationForCutout(){
    rectfromto([ -(middle_side_wall_x + side_x_gap),
		    middle_base_z - fit_gap_z ],
	       [ +(middle_side_wall_x + side_x_gap), middle_top_z  ]);
}
module BottomMainElevation(){
  difference(){
    rectfromto([ -total_side_wall_x, bottom_base_z        ],
	       [ +total_side_wall_x, bottom_wall_top_z    ]);

    MiddleElevationForCutout();
  }
}
module Bottom(){ ////toplevel
  difference(){
    union(){
      linextr_y_xz(total_y_min, front_y_max)
	BottomMainElevation();
    }

    linextr_y_xz(midbot_registration_y_around_max,
		 front_y_max - wall_th)
      rectfromto([ -board_w/2, bottom_base_z + bottom_floor_th ],
		 [ +board_w/2, 20         ]);

    linextr_y_xz(total_y_min + wall_th,
		 front_y_max - wall_th)
      rectfromto([ -midbot_registration_bottom_x,
		     bottom_base_z + bottom_floor_th ],
		 [ +midbot_registration_bottom_x, 20         ]);

    linextr_y_xz(total_y_min - 1,
		 total_y_min + wall_th + 1){
      translate([ 0, middle_base_z ]){
	hull(){
	  translate([ 0, -cable_dia/2 ])
	    circle(r = cable_dia/2, $fa = 10, $fs = 1);
	  square([ cable_dia, 0.1 ], center=true);
	}
      }
    }
    RegistrationsMinkowski()
      CoverRegistrations();
    RegistrationsMinkowski()
      MidBotRegistrations();
    BottomStrapCutouts(-cover_strap_c_d_y);
    BottomStrapCutouts(midbot_strap_c_y);
  }
}

module CoverMainElevation(){
  difference(){
    rectfromto([ -total_side_wall_x, cover_wall_bot_z        ],
	       [ +total_side_wall_x, cover_top_z    ]);

    MiddleElevationForCutout();
  }
}
module Cover(){ ////toplevel
  difference(){
    union(){
      linextr_y_xz(main_y_min, front_y_max)
	CoverMainElevation();
      CoverRegistrations();
    }
    CoverStrapCutouts();
    linextr(-20,20) {
      minkowski(){
	square(strain_around * 2, center=true);
	hull() MiddleStrainHoles();
      }
    }
  }
}
module CoverPrint(){ ////toplevel
  rotate([180,0,0]) Cover();
}

module BottomDemo(){ ////toplevel
  translate([0, 0, -0.25]) Bottom();
  %Middle();
  translate([0, 0, +0.25]) Cover();
}
module ImpressionDemo(){ ////toplevel
  color("black") translate([0, 0, -0.25]) Bottom();
  %Middle();
  %translate([0, 0, +0.25]) Cover();
}
