// -*- C -*-

radius = 25/2;
wall = 1.0;
height = 40;
floor = 1.0;

difference(){
  cylinder(r=radius+wall, h=height);
  translate([0,0, floor])
    cylinder(r=radius, h=height);
}
