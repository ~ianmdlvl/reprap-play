// -*- C -*-

psu_sz_nom = [ 11.43*2, 36.20 ];

//// toplevels-from:
include <adafruit-powerboost-common.scad>

psu_hole_pos = [ 2.05, // from back edge of psu_sz[0]
		 0.55 * 0.5 * 25.4, // from centreline
		 ];

psu_led_low_x = 4;

psu_led_usbend_y_min = 4.5;
psu_led_chrg_min_x = -1.0;
psu_led_chrg_max_x = 3.0;
psu_led_chrg_both_sz_y = 9.4;

psu_led_low_sz_x = 4.5;
psu_led_low_min_y = -1.5;
psu_led_low_max_y = 3.25;

psu_led_baffle_th = 0.8;
psu_led_baffle_ends = 1.5;

psu_baffle_th = [ 0.8, 3.5 ];
psu_innerend_led_depth = 7;

psu_led_legend_line = 0.75;

psu_led_legend_battery_l = 6.0;
psu_led_legend_battery_w = 4.0;
psu_led_legend_battery_nub_l = 0.75;
psu_led_legend_battery_nub_w = 1.5;

psu_led_legend_power_dia = 5.0;

psu_led_legend_gap = 1.25;

// ----- calculated -----

psu_led_legend_battery_edge = psu_led_legend_line;

psu_led_legend_power_tick_l =
  psu_led_legend_power_dia * 0.65;
psu_led_legend_power_tick_dy = psu_led_legend_line;

psu_led_legend_power_sz_y =
  psu_led_legend_power_dia/2 + psu_led_legend_power_tick_l/2
  - psu_led_legend_power_tick_dy;

psu_innerend_led_x_midder =  - psu_hole_pos[1] - psu_hole_dia/2;

module PsuLedBafflePlan(){
  AtPsuMountCorner(0,0) {
    translate([ (psu_led_chrg_min_x + psu_led_chrg_max_x)/2,
	       psu_led_usbend_y_min + psu_led_chrg_both_sz_y/2 ])
      square(center=true,
	     [ psu_led_chrg_max_x - psu_led_chrg_min_x
	       + psu_led_baffle_ends*2,
	       psu_led_baffle_th ]);
  }
}

module PsuLedWindowsPlanCore(){
  difference(){
    union(){
      // Two LEDs one side of inlet connector
      // "Full" (near edge) and "Chrg" (inner)
      AtPsuMountCorner(0,0) {
	translate([0, psu_led_usbend_y_min ])
	  rectfromto([ psu_led_chrg_min_x, 0 ],
		     [ psu_led_chrg_max_x,
		       psu_led_chrg_both_sz_y ]);
      }

      // One LED, "Low", other side of inlet connector
      AtPsuMountCorner(1,0) {
	translate([0, psu_led_usbend_y_min ])
	  rectfromto([ 0,
		       psu_led_low_min_y ],
		     [ psu_led_low_sz_x,
		       psu_led_low_max_y ]);
      }

      // One LED, PWR, near outlet USB pads
      AtPsuMountCorner(0,1){
	rectfromto([0,0],
		   [psu_sz[0]/2 + psu_innerend_led_x_midder,
		    psu_innerend_led_depth]);
      }
    }
  }
}

module PsuLedLegendBattery(percent=50){
  e = psu_led_legend_battery_edge;
  empty_l = (100-percent)/100 * (psu_led_legend_battery_l - e*2);
  difference(){
    union(){
      square([psu_led_legend_battery_l,
	      psu_led_legend_battery_w], center=true);
      translate([psu_led_legend_battery_l/2, 0])
	square([psu_led_legend_battery_nub_l*2,
		psu_led_legend_battery_nub_w], center=true);
    }
    if (empty_l > 0)
      translate([-(psu_led_legend_battery_l/2-e),
		 -(psu_led_legend_battery_w/2-e)])
	square([empty_l, psu_led_legend_battery_w - e*2]);
  }
}  

module PsuLedLegendPowerSymbol(){
  $fn=30;
  tick_mid = [0, psu_led_legend_power_dia/2 - psu_led_legend_power_tick_dy];

  cut_slope = ( psu_led_legend_gap + psu_led_legend_line/2 ) / tick_mid[1];
  cut_y = psu_led_legend_power_dia + 1;

  translate(tick_mid)
    square([psu_led_legend_line, psu_led_legend_power_tick_l], center=true);
  
  difference(){
    circle(r= psu_led_legend_power_dia/2);
    circle(r= psu_led_legend_power_dia/2 - psu_led_legend_line);

    polygon([[0, 0],
	     [-cut_y * cut_slope, cut_y],
	     [ cut_y * cut_slope, cut_y]]);

    if(0) translate(tick_mid)
      square([psu_led_legend_line, psu_led_legend_power_tick_l]
	     + [psu_led_legend_gap*2, 0.1],
	     center=true);
  }
}

module PsuLedLegendsPlan(){
  translate([psu_led_legend_power_dia/2
	     + psu_innerend_led_x_midder
	     + psu_led_legend_gap,
	     psu_sz[1]/2
	     - psu_innerend_led_depth/2
	     ])
    rotate([0,0,180])
    PsuLedLegendPowerSymbol();

  for (full=[0,1]) {
    translate([-psu_sz[0]/2
	       + psu_led_legend_battery_l/2
	       + psu_led_chrg_max_x
	       + psu_led_legend_gap,
	       -psu_sz[1]/2
	       + psu_led_usbend_y_min
	       + psu_led_chrg_both_sz_y * 0.5
	       + max(
		     psu_led_legend_battery_w + psu_led_legend_gap,
		     psu_led_chrg_both_sz_y * 0.5
		     ) * (0.5 - full)
	       ])
      PsuLedLegendBattery(full ? 100 : 50);
  }

  translate([psu_sz[0]/2
	     - psu_led_legend_battery_nub_l
	     - psu_led_legend_battery_l/2,
	     -psu_sz[1]/2
	     + psu_led_legend_gap
	     + psu_led_usbend_y_min
	     + psu_led_low_max_y
	     + psu_led_legend_battery_w/2
	     ])
    PsuLedLegendBattery(0);
}
