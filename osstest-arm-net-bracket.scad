// -*- C -*-

holedist = 64;
tonguewidth = 10;
tongue2width = 15;
totaldepth = 26;
tongue2depth = 35;
thick = 4;
tabover = 7+6;
tabunder = 15;

tabsidel = 7+1;
tabsider = 7+10;

tonguethick = 4;
tongue2thick = 5;
strapthick = 2;
strapwidth = 5 + 0.35;
ridgewidth = 2;

hstrengthick = 2.5;
hstrengdepth = strapwidth;

cutoutover = 7;
rcutoutside = 7+2 - 0.5;
lcutoutside = 7-6.5 - 0.5;

t2strengwidth = 10;
t2strengwidtht = 4;
t2strenglen = tongue2depth + 5;
t2strengthicker = 1;

strapholethicker = 1.5;

holedia = 3.5;

tongue2x = tongue2width - holedist;

module Tongue(tw,tt,ad,slots=2){
  y0=thick+0.1;
  yn=ad-ridgewidth-strapwidth;
  difference(){
    union(){
      translate([-tw, 0, 0])
	cube([tw, ad, tt+strapthick]);
      child();
    }
    for (yi=[1:slots-1])
      translate([-tw-1, y0 + (yn-y0)*yi/(slots-1), tt])
	cube([tw+2, strapwidth, strapthick+strapholethicker]);
  }
}

module Body(){
  translate([-holedist-tabsider, 0, 0]) {
    cube([tabsidel+tabsider+holedist, thick, tabunder+tabover]);
    cube([tabsidel+tabsider+holedist, thick+hstrengdepth, hstrengthick]);
  }
  Tongue(tonguewidth,tonguethick,totaldepth,3);
  translate([tongue2x,0,0])
    Tongue(tongue2width,tongue2thick,tongue2depth+thick,5) {
    mirror([1,0,0]) hull(){
      translate([-(tongue2width-t2strengwidth)*0.05, 0,0]) 
	cube([t2strengwidth,t2strenglen,
	      tongue2thick+strapthick+strapholethicker+t2strengthicker]);
      cube([t2strengwidtht,thick+0.1,tabunder+tabover]);
    }
  }
}

module Object(){
  difference(){
    Body();
    translate([0,-25,tabunder+cutoutover]) {
      translate([-(holedist+rcutoutside), 0,0])
	mirror([1,0,0])
	cube([50,50,50]);
      translate([lcutoutside, 0,0])
	cube([50,50,50]);
    }
    for (x=[-holedist,0])
      translate([x, 0, tabunder]) {
	translate([0, -1, 0]) {
	  rotate([-90,0,0]) {
	    cylinder(r= holedia/2+0.5, h=thick+2, $fn=20);
	  }
	}
	translate([0, 19.95 + thick, 0])
	  cube(center=true,[10,40,10]);
      }
  }
}

Object();
