#!/bin/bash
#
# usage:
#    ./quacks-ingredients-make-copy-gcodes Tests_L
# Uses
#    quacks-ingredients-L$l,Tests_L.auto.stl
# to make
#    quacks-ingredients-L$l,Tests_L.auto.gcode
# and then edits them and copies them to the SD card as
#    PREPARED/Q$l.G

set -e

f=$1
shift

lhs=quacks-ingredients-L

gh () {
	g=$lhs$l.auto.gcode
	h=/media/sd/PREPARED/Q$l.G
}

for l in 1 2 3 4 5; do
	gh
	qi=quacks-L$l.auto.ini

	cp quacks.ini $qi

	case $l in
	2|4|5)
		perl -i~ -pe '
			s/^(retraction_hop *= *.*)/retraction_hop = 0.6/m
		' $qi
		;;
	esac

	cura -i $qi -s $lhs$l,$f.auto.stl -o $g
	case $l in
	1|2|3|4)
		perl -i~ -pe 's/^/;/ if m/^M140 S0\b.*\n/' $g
		;;
	esac

	perl -i~ -ne '
		$l =~ s/^/;/ if m/^M400/;
		$l .= "G91\nG1 Z5\nG90\n" if m/^M84/;
		print $l or die $!;
		$l = $_;
		END { print $l or die $!; }
	' $g
done

for l in 2 4 5; do
	gh
	: perl -i~ -0777 -ne '
		@l = split m{^(?=;LAYER:\d+\n)}m, $_;
		foreach my $i (0..$#l) {
			$l[$i] =~
				s{
				(	^G1 \ Z\S+ \s*\n	
				  |	^\;LAYER:\d+ \s*\n
				    (?:	^M10[67].* \s*\n )?
				)
					^G0 \ F(\d+) \ ( X\S+ \ Y\S+ )
							 \ Z(\S+) \s*\n
				}{
					die "$& $2" unless $2 > 9000;
					$1 .
					"G1 Z$4\n".
					"G0 F$2 $3\n".
					"G1 Z$4\n"
				}mxe	or $i==0
					or die "$l[$i] $i";
			$l[$i] =~
				s{
					^G1 \ Z([0-9.]+) \s*\n
				(	^G0 \ F(\d+) \ X\S+ \ Y\S+ \s*\n
					(?: ; .* \s*\n )?
					^G1 \ Z([0-9.]+) \s*\n	)
				}{
					die "$& $3" unless $1 >= $4;
					die "$& $3" unless $3 > 9000;
					my $z = $i == $#l ? $1 : $4 + 0.5;
					"G0 F$3\n".
					"G1 Z$z\n" .
					$2
				}gmxe or $i==0 or die "$l[$i] $i";
		}
		print or die $! foreach @l;
	' $g
done

exec 3>${lhs}234.auto.gcode
for l in 2 3; do
	gh
	perl -pe 's/^/;/ if m/^M104 S0\b/ || (m/^M84/..0)' $g >&3
done
for l in 4; do
	gh
	cat $g >&3
done

copies="3:5 2:234 1:1"
copyls=""
for copy in $copies; do
	l=-P-${copy%:*}
	ci=$lhs${copy#*:}.auto.gcode
	copyls+=" $l"
	gh
	rm -f $g
	ln -vs $ci $g
done

umount /media/sd >/dev/null 2>&1 ||:
mount /media/sd

for l in 1 2 3 4 5 234 $copyls; do
	gh
	cp $g $h
done

sleep 0.5

umount /media/sd
mount /media/sd

for l in 1 2 3 4 5 234 $copyls; do
	gh
	cmp $g $h
	ls -l $h
done
sleep 0.5

umount /media/sd
