//// toplevels-from:sewing-table.scad
include <sewing-table.scad>
TEST = true;
test_tile_th = 0.67;
test_edge = interlock_dia * 0.5 + interlock_fine + 2;

leg_n_fins = 2;

leg_top_thick = 3;
leg_bot_thick = 4;
leg_top_flat_z = 0.5;
leg_fin_top_w = 2;
leg_fin_bot_w = 3;
leg_fin_bot_rad = 15;
leg_bot_mid_dia = 9;
