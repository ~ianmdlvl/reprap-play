// -*- C -*-

include <utils.scad>

hole_dia = 10;
around_hole = 4;
thick = 3;
lever_len = 50;

teeth_n = 4;
teeth_bite = 4;
teeth_pitch = 4;
teeth_gap = 3;
teeth_back = 1;
teeth_height = 12;
teeth_chamfer = 3;

// calculated

teeth_x_mid = lever_len/2 - hole_dia/2 - teeth_bite - teeth_gap*1.5;
teeth_height_total = teeth_height + teeth_chamfer;

module Circles(r) {
  for (x = [-1,+1] * 0.5 * lever_len) {
    translate([x, 0])
      circle(r);
  }
}

module Plan() {
  difference(){
    hull(){
      Circles(hole_dia/2 + around_hole);
    }
    Circles(hole_dia/2);
    translate([ teeth_x_mid - teeth_bite - teeth_back - teeth_gap - hole_dia/2,
		0 ])
      circle(hole_dia/2);
  }
}

module TeethPlan(){
  translate([
	     teeth_x_mid,
	     -0.5 * teeth_n * teeth_pitch,
	     ]) {
    for (m=[0,1]) {
      mirror([m,0]) {
	translate([ teeth_bite + teeth_gap/2, 0 ])
	  rectfromto([ -0.1, 0 ],
		     [ teeth_back, teeth_n * teeth_pitch ]);
	for (i= [ 0: teeth_n-1 ]) {
	  translate([teeth_gap/2, teeth_pitch*i])
	    polygon([[ 0,0 ],
		     [ teeth_bite, 0 ],
		     [ teeth_bite, teeth_pitch ]]);
	}
      }
    }
  }
}

module Adjuster(){
  linextr(0,thick)
    Plan();
  difference(){
    linextr(thick - 0.1, thick + teeth_height_total)
      TeethPlan();
    translate([ teeth_x_mid, 0, thick + teeth_height_total + 1])
      linextr_y_xz(-teeth_pitch * teeth_n,
		 +teeth_pitch * teeth_n)
      rotate(45)
      square(sqrt(2) * (teeth_gap/2 + teeth_chamfer + 1), center=true);
  }
}

Adjuster();
