// -*- C -*-

thick = 3.5;
width = 5;

centre_dia = 21;

// calculated

ellipse_width = width / 0.90;

module Profile(){
  intersection(){
    scale([thick, ellipse_width]) circle(r=0.5, $fn=50);
    square([thick*2, width], center=true);
  }
}

module Ring(dia){
  rotate_extrude($fn=100){
    translate([dia/2 + thick/2, 0]){
      Profile();
    }
  }
}

//Profile();
//Ring(centre_dia);

module Tests(){
  min_dia = 19;
  max_dia = 22;
  dia_step = .25;
  n_dias = (max_dia - min_dia)/dia_step;
  for (i= [ 0 : n_dias ]) {
    echo(i);
    rotate([0,0, i*360 / (n_dias+2)])
      translate([80,0,0])
      Ring(min_dia + (max_dia-min_dia)*(i/n_dias));
  }
}

Tests();
