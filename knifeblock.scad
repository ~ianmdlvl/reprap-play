// -*- C -*-

// properties of the knives
nknives = 3;
widths = [15.5, 15.8, 19.0];
handlelenbase = 75;
handlelendelta = [-15, 0, 10];
locations = [-35, 0, 37];
bladew = 5; // 2.5
maxhandledepth = 45;

templatescale = 27.2 / 19.6;

coverlonglen = 120; // xxx
covershortlen = 70; // xxx

// other tuneables
front = 5;
back = 5;
height = 50;
minsidein = 4;
minsideout = 4;

frontbackslop = 0.25;

knifewidthslop = 2.0;

screwbackdepth = 6.0 - 1.0;
screwdia =       4.0 + 0.5;
screwcsinkdia =  9.8 + 1.0;

screwabove = 15;

coverthick = 2.4;
coverside = coverthick;

covertopwing = 15;
covertopwingbase = 20;
coveredge = 3;

holesize = 12.5;
holestrut = 7;
holeedge = 4;

holeoffx = 0.33;
holeoffy = 0.23;

indentdepth = 1;
indentoutersize = holesize + 2.15;
indentinnersize = indentoutersize - indentdepth * 3.0;

pegstem = 3.5;
peghead = 10;
pegstemheight = 2;
pegheight = 9;
peglen = 12;

recessblockwidth = peghead + pegstem*3;
recessblockheight = peglen/2 + pegstem*1.5;

pegsloph = 0.5;
pegslopv = 0.5;
pegslopl = 0.5;

pegdepthproportion = 0.80;

// computed

function width(k) = widths[k] + knifewidthslop;

side = minsidein + screwcsinkdia + minsideout;
totaldepth = front + maxhandledepth + back;

minkx = locations[0] -         width(0)        /2;
maxkx = locations[nknives-1] + width(nknives-1)/2;

minx = minkx - side;
maxx = maxkx + side;

holepitch = holesize+holestrut;

pegrecess = pegdepthproportion*totaldepth - 0.5*peglen;

module ImportTemplate(w,k,t) {
  fn = str("knifeblock-knives-t",k,t,".dxf");
  echo(fn);
  translate([0,0, -w/2])
    linear_extrude(height=w)
    scale(templatescale) import(file=fn, convexity=100);
}

module Knife(k){
  ImportTemplate(bladew, k,"bl");
  hull(){
    ImportTemplate(width(k), k,"hl");
    translate([-100,0,0])
      ImportTemplate(width(k), k,"hl");
  }
}

module DoKnife(k){
  translate([locations[k],0,0]){
    rotate([0,90,0])
      translate([-(handlelenbase + handlelendelta[k]),0,0])
      Knife(k);
  }
}

module DoKnives(){
  for (k=[0:nknives-1])
    DoKnife(k);
}

module ScrewHole(){
  translate([0,-50,0])
    rotate([-90,0,0])
    cylinder(r=screwdia/2, h=150, $fn=40);
  translate([0, totaldepth-front - screwbackdepth, 0])
    rotate([90,0,0])
    cylinder(r=screwcsinkdia/2 / (sqrt(3)/2), h=100, $fn=6);
}

module PegTemplate(apex){
  for (mx=[0,1]) for (my=[0,1]) {
      mirror([mx,0,0]) mirror([0,my,0])
	polygon([[-0.1,      -0.1],
		 [pegstem/2, -0.1],
		 [pegstem/2, pegstemheight/2],
		 [peghead/2, pegheight    /2],
		 [-0.1,      pegheight    /2 + apex]]);
    }
}

module AtSides(){
  translate([minx,0,0])                 child(0);
  translate([maxx,0,0]) mirror([1,0,0]) child(1);
}

module BlockPegSlot(){
  translate([recessblockwidth/2, pegrecess - peglen, -height]){
    rotate([-90,0,0]) linear_extrude(height=totaldepth){
      PegTemplate(peghead/2 * 1.2);
    }
  }
}

module DecorativeIndents(){
  translate([0, -front, 0])
  rotate([90,0,0])
  HexGrid(-height, 0, minx,maxx) {
    hull(){
      translate([0, 0, -indentdepth])
	cylinder(r=indentinnersize/2, h=indentdepth, $fn=40);
      cylinder(r=indentoutersize/2, h=indentdepth, $fn=40);
    }
  }
}

module Block(){
  sidemidx = minsideout + screwcsinkdia/2;

  difference(){
    mirror([0,0,1]) {
      translate([minx, -front, 0])
	cube([maxx-minx, totaldepth, height]);
    }
    for (x=[minx + sidemidx, maxx - sidemidx]) {
      translate([x, 0, -screwabove])
	ScrewHole();
    }
    for (yshift=[-1,1])
      translate([0, yshift * frontbackslop, 0])
	DoKnives();
    AtSides() { BlockPegSlot(); BlockPegSlot(); }
    DecorativeIndents();
  }
}

module BlockPrint(){ ////toplevel
  rotate([0,0,90])
    Block();
}

module CoverTemplate(){
  linear_extrude(height=coverthick)
    polygon([[minx, 0],
	     [maxx, 0],
	     [maxx, coverlonglen+0.1],
	     [maxx - coverside, coverlonglen+0.1],
	     [minx, covershortlen+0.1]]);
}

module CoverSide(len){
  translate([0, 0 ,0]) {
    rotate([90,0,90])
      linear_extrude(height=coverside)
      polygon([[0,                      0],
	       [0,                      totaldepth],
	       [covertopwing,           totaldepth],
	       [covertopwingbase,       coverside + coverthick],
	       [len - covertopwingbase, coverside + coverthick],
	       [len - covertopwing,     totaldepth],
	       [len,                    totaldepth],
	       [len,                    0]]);
    cube([recessblockwidth, recessblockheight, totaldepth]);
  }
}

module Peg(){
  echo("peg angle slope (must be <1)",
       (peghead-pegstem)/(pegheight-pegstemheight));
  dx = pegsloph;
  dy = pegslopv;
  rotate([90,0,0]) {
    linear_extrude(height=peglen-pegslopl) {
      intersection(){
	translate([-dx,-dy,0]) PegTemplate(0);
	translate([-dx,+dy,0]) PegTemplate(0);
	translate([+dx,+dy,0]) PegTemplate(0);
	translate([+dx,-dy,0]) PegTemplate(0);
      }
    }
  }
}

module CoverPegSlot(coverlen){
  translate([recessblockwidth/2, 0, -1]){
    linear_extrude(height= 1 + pegrecess + 0.5*peglen){
      PegTemplate(0);
    }
  }
}

module HolesScope(){
  intersection_for (dx=[-1,+1]) {
    intersection_for (dy=[-1,+1]) {
      translate([dx * holeedge, dy * holeedge, -5])
	scale([1,1,10])
	CoverTemplate();
    }
  }
}

module HexGrid(xa,xb,ya,yb) {
  imin = floor(xa / holepitch);
  imax =  ceil(xb / holepitch);
  jmin = floor(ya / (sqrt(3)*holepitch));
  jmax =  ceil(yb / (sqrt(3)*holepitch));
  echo("HexGrid ",imin,imax,jmin,jmax);
  for (i=[imin:imax]) {
    for (j=[jmin:jmax]) {
      translate([(j * sqrt(3) + holeoffx) * holepitch,
		 (i +     0.5 + holeoffy) * holepitch,
		 0]) {
	child();
	translate([sqrt(3)/2 * holepitch, -0.5 * holepitch, 0])
	  child();
      }
    }
  }
}

module Hole(){
  cylinder(r=holesize/2, h=40, $fn=40);
}

module Holes(){
  intersection(){
    translate([0, 0, -20])
      HexGrid(0, coverlonglen, minx, maxx)
      Hole();
    HolesScope();
  }
}

module CoverCover(){
  difference(){
    CoverTemplate();
    Holes();
  }
}

module Cover(){
  difference(){
    union(){
      CoverCover();
      AtSides() { CoverSide(covershortlen); CoverSide(coverlonglen); }
    }
    AtSides() { CoverPegSlot(); CoverPegSlot(); }
  }
}

module CoverAligned(){
  translate([0,-front,-height])
    rotate([-90,0,0])
    Cover();
}

module DemoPeg(){
  translate([recessblockwidth/2, pegrecess, -height])
    Peg();
}

module Demo(){ ////toplevel
  %Block();
  DoKnives();
  color([0,0,1]) CoverAligned();
  color([1,0,0]) AtSides() { DemoPeg(); DemoPeg(); }
}

module Pegs(){ ////toplevel
  Peg();
  translate([-peghead-3, 0,0]) Peg();
}

module CoverParts(){ ////toplevel
  Cover();
  translate([0, coverlonglen, pegheight/2-pegslopv])
    Pegs();
}

module FrontDemo(){ ////toplevel
  color([1,0,1]) Block();
  color([1,0,1]) CoverAligned();
  color([0,0,0]) DoKnives();
}

module BlockFrontTest(){ ////toplevel
  intersection(){
    Block();
    translate([minx-10, -front-10, -height-10]) {
      cube([75,14,35]);
      cube([75,25,13]);
    }
  }
}

//Block();
//Demo();
//Cover();
//CoverParts();
//Peg();
//Cover();
//Holes();
//%CoverTemplate();
//Pegs();
