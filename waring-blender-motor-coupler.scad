// -*- C --*-

include <utils.scad>

slop = 0.35;

d1 = 21.40 - slop;
d2 = 11.90 - slop;
d3 = 10.90 - slop;

h2 = 6.60;
h3 = 10.00;
ht = 22.15;
hkey_shaft = 9;

sq = 4.50 + slop;

key_w = 3.40 + slop;
key_l = 15.05 + slop;
key_d = 7.50 + slop;

over_key = 0.5; // guessed
sq_len = h3; // guessed

$fs = 0.5;
$fa = 3;

// calculated

h1 = ht - h2 - h3;
hkey = h1 - over_key;

module Coupler() { ////toplevel
  difference(){
    union(){
      linextr(0, h1)      circle(r = d1/2);
      linextr(0, h1 + h2) circle(r = d2/2);
      linextr(0, ht)      circle(r = d3/2);
    }
    linextr(ht - sq_len, ht + 1) square(center=true, sq);
    linextr(-1, hkey) square(center=true, [ key_l, key_w ]);
    linextr(-1, hkey_shaft) circle(r = key_d/2);
  }
}

module DemoCutout() { ////toplevel
  intersection(){
    Coupler();
    linextr_y_xz(0, 100) square(200, center=true);
  }
}
