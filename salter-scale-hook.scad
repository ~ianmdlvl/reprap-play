// -*- C -*-

include <utils.scad>

rod_dia = 8+2;
thick = 8;
screw_dia = 3.5 + 0.75;
screw_head_dia = 8.2 + 1.0;
rod_offset = 14 + 2;
mainheight = 25;
width = 40;
rearthick = 4;
screw_head_depth = 2;

// calculated

d = rod_dia/2 + thick/2;
yminc = -d;
ymin = yminc-thick/2;
ymaxc = mainheight;
ymax = mainheight+thick/2;

cutdepth = rod_offset - rod_dia/2 - rearthick;

cut_z0 = screw_head_dia/2;
cut_z1 = width/2 - rearthick;

cutslopez = cutdepth * 0.5;

module C() {
  circle(r = thick/2, $fn=30);
}

module Profile() {
  e = rod_offset;
  hull(){
    translate([-d, 0]) C();
    translate([-d,-d]) C();
  }
  difference(){
    rectfromto([-d,ymin], [e,0]);
    circle(r= rod_dia/2, $fn=50);
  }
  hull(){
    for (y= [-d, +mainheight]) {
      translate([d, y]) C();
      rectfromto([d, y-thick/2], [e, y+thick/2]);
    }
  }
}

module CutProfile(){
  hull(){
    for (x = [rod_dia/2 + thick/2, 30]) {
      for (y= [yminc,ymaxc] ) {
	translate([x,y]) circle(r = (thick-rearthick)/2, $fn=20);
      }
    }
  }
}

module ProfileDemo(){
  Profile();
  color("red") translate([0,0,1]) CutProfile();
}

module Cut(less){
  translate([0,0, cut_z0 + less])
    linear_extrude(height = cut_z1 - cut_z0 - less*2)
    CutProfile();
}

module ScrewHole(){
  xd =  (screw_head_dia-screw_dia)/2;
  translate([0,0,-50])
    cylinder(h=100, r= screw_dia/2, $fn=20);
  hull(){
    translate([0,0,-xd])
      cylinder(h=1, r= screw_dia/2, $fn=50);
    cylinder(h=20, r= screw_head_dia/2, $fn=50);
  }
}

module Hook(){
  difference(){
    translate([0,0, -width/2])
      linear_extrude(height=width) Profile();

    for (m=[0,1]) {
      mirror([0,0,m]) {
	hull(){
	  Cut(cutslopez);
	  translate([cutdepth,0,0]) Cut(0);
	}
      }
    }

    translate([rod_dia/2 + screw_head_depth,
	       ymaxc - screw_head_dia,
	       0]) {
      rotate([0,-90,0])
	ScrewHole();
    }
  }
}

//ProfileDemo();
Hook();
