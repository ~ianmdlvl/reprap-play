/* -*- C -*- */

motorwidth=35.7;
motorheight=34.5;
totalheight=58;

pillarthick=8;
sidethick=2.5;
archthick=6.5;
frameextra=3.5;
framesplayx=5;
framesplayy=5;
botleftgap=4.5;
botleftstand=0.75;
archoutwards=(pillarthick-archthick)/sqrt(8);

topgluecubex=18;
topgluecubez=5;
clippairy=16;
clippairdz=-2.5;
topgluecubedy=1;

dovebasecutcylz=4;
dovebasecutcylr=10;

d=0.01;

mw2=motorwidth/2;

include <doveclip.scad>

module corner() {
  $fn=30;
  frameheight= motorheight + frameextra;
  slopeheight= totalheight - frameheight;
  slopex = (mw2 + archoutwards - framesplayx)/slopeheight;
  slopey = (mw2 + archoutwards - framesplayy)/slopeheight;
  echo(sqrt(slopex*slopex + slopey*slopey));

  translate([-mw2,-mw2,0]) union(){
    difference(){
      union(){
	cylinder(r=pillarthick/2, h=frameheight);
	translate([0,0,frameheight])
	  sphere(r=pillarthick/2);
      }
      translate([d,d,-1])
	cube([mw2-1,mw2-1,frameheight+pillarthick+2]);
    }
    intersection(){
      multmatrix
	([	[	1,	0,	slopey,	-archoutwards ],
		[	0,	1,	slopex,	-archoutwards ],
		[	0,	0,	1, frameheight	],
		[	0,	0,	0,	1 	]])
	translate([0,0,-frameextra])
	cylinder(r=archthick/2,
		 h=slopeheight+frameextra);
      union() {
	cylinder(r=pillarthick/2, h=frameheight);
	translate([-100,-100,frameheight])
	  cube([200,200,100]);
      }
    }
  }
}

module halfside() {
  spacesz = (motorwidth - pillarthick/2*2) / 4;
  panelheight = spacesz + sidethick;
  panelbasez = motorheight+pillarthick/4-panelheight;
  translate([0,-mw2,0]) {
    translate([-mw2,-sidethick,0])
      cube([motorwidth,sidethick,sidethick]);
    difference(){
      translate([-mw2,-sidethick, panelbasez])
	cube([mw2,sidethick,panelheight]);
      translate([-mw2+pillarthick/3, -sidethick, panelbasez])
	rotate([0,45,0])
	translate([0,-1,0])
	cube([spacesz * sqrt(2),
	      sidethick+2,
	      spacesz * sqrt(2)]);
    }
    intersection(){
      for (xz=[[-mw2+pillarthick/3-sidethick, 0,
		panelbasez+sidethick],
	       [0, 0, panelbasez + sidethick/sqrt(2)]]) {
	translate(xz)
	  translate([0,-sidethick,0])
	  rotate([0,55,0])
	  translate([0,0,-sidethick])
	  cube([100, sidethick, sidethick]);
      }
      translate([-mw2,-sidethick,0])
	cube([motorwidth,sidethick,
	      motorheight+pillarthick]);
    }
  }
}

module towerbase() {
  difference(){
    union(){
      for (mirx=[0,1]) for (miry=[0,1])
			 mirror([mirx,0,0]) mirror([0,miry,0]) corner();
      for (angle=[0,90,180]) {
	rotate([0,0,angle]) halfside();
	rotate([0,0,angle]) mirror([1,0,0]) halfside();
      }
    }
//    multmatrix([[	-1,	0,	0, -mw2 - botleftstand ],
//		[	0,	1,	0,	-100	],
//		[	1,	0,	1, -100 + botleftgap ],
//		[	0,	0,	0,	1	] ])
//      cube([100,200,100]);
  }
  translate([clippairy/2,0,totalheight]) {
    difference(){
      translate([-clippairy+topgluecubedy/2,-topgluecubex/2,0])
	cube([clippairy-topgluecubedy,topgluecubex,topgluecubez]);
    }
    translate([0,0,topgluecubez+clippairdz+DoveClip_depth()]) rotate([0,-90,0])
//      DoveClipPair(h=clippairy);
      DoveClipPairSane(h=clippairy, count=3);
  }
}

if (towerbase_demo) {
  intersection(){
    translate([0,0,-50]) towerbase();
    translate([-100,-100,0]) cube([200,200,32]);
  }

  intersection(){
    translate([40,0,-60]) towerbase();
    translate([-100,-100,0]) cube([200,200,32]);
  }

  translate([60,-90,0]) {
    DoveClipPairSane(h=clippairy, count=3);
    mirror([1,0,0]) translate([DoveClip_depth()-0.1,0,0]) cube([20,8,6]);
  }

  for (x=[0,20,40]) {
    translate([x,-50,0]) DoveClipPin(h=clippairy);
    translate([x+10,-50,0]) DoveClipPin(h=clippairy/2);
    translate([x+10,-30,0]) DoveClipPin(h=clippairy/2);
  }
} else {
  towerbase();
}
