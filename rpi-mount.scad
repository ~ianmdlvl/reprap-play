// -*- C -*-

include <utils.scad>

pi_board_gap = 0.5;
pi_board_support_z = 2;
pi_board_support_wall = 2;

pi_sz   = [ 66.0, 30.5 ] + pi_board_gap * [1,1];

pi_nom_sz = [ 65, 30 ];
pi_nom_centres_in = 3.5;
pi_solder_side_gap = 1.5 * 2;
pi_screw_hole_dia  = 2.3;
pi_screw_hole_wall = 2.3;

pi_sz_z_incl_ribbon = 18.0;

pi_mount_z_min = 1.75;
pi_mount_z_def = 2.50;

// calculated, output

function pi_ribbon_top_z(pi_mount_z= pi_mount_z_def)
  = pi_mount_z + pi_sz_z_incl_ribbon;

module PiMount(pi_mount_z= pi_mount_z_def){
  sxy = pi_nom_sz/2 - [1,1] * pi_nom_centres_in;
  for (mx=[0,1]) mirror([mx,0,0]) for (my=[0,1]) mirror([0,my,0]) {
    difference(){
      union(){
	linextr(-0.1, pi_mount_z + pi_board_support_z, convexity=1)
	  rectfromto( pi_nom_sz/2 - 2 * [1,1] * pi_nom_centres_in,
		      pi_sz/2 + [1,1] * pi_board_support_wall);
	linextr(-0.1, pi_mount_z - pi_solder_side_gap, convexity=1)
	  translate(sxy)
	  square(center=true, pi_screw_hole_dia + pi_screw_hole_wall*2);
      }
      linextr(pi_mount_z, pi_mount_z + 5, convexity=1)
	rectfromto(-[10,10], pi_sz/2);
      translate( sxy )
	linextr(-1, pi_mount_z + 10, convexity=1)
	circle(r= pi_screw_hole_dia/2);
    }
  }
}
