#!/usr/bin/perl -w
use strict;
die unless @ARGV==1 && $ARGV[0] =~ m/^\d+/;
my $num = $ARGV[0];
$num /= 1000;
printf <<END, $num or die $!;
%%!
/Helvetica-Bold findfont
15 scalefont
setfont
0 0 moveto
(%.1f) show
showpage
END
