// -*- C -*-

h=2;

$fa=1;
$fs=0.1;

label=true;

spc= 7;
l = 50;
w = 10;

lt = 0.5;
lw = 10;

// calculated

// Actual sizes (according to calipers) of things that fit
//  A
//  C - 1.88mm (M3 screw)
//  E - 2.97mm (3mm HSS bit shank)
//  G - 
//  I - 3.15mm tight fit (Yale padlock from extra padlocks tray)
//  K - 3.33mm (M3.5 screw)

ly0 = -w/2 -lw;

difference(){
  union(){
    cube([l,w,h], center=true);
    if (label)
      translate([-l/2, ly0, -h/2])
	cube([l, lw, lt]);
  }

  for (i=[0:2:10]) {

    sz = 3 + 0.5 * i/10;

    echo(sz);
    translate([(i-5)/2 * spc, 0, -7 ]) {
      cylinder(r= sz/2, h=14);
      linear_extrude(height=14, convexity=100) {
	translate([0, ly0 + lw * .2])
	  text(halign="center",
	       size= lw * .6, font="DejaVu Sans:style=Bold",
	       chr(i + 65));
      }
    }
  }
}
