th=3;
holesz=6;
outsz=15;

module small(sz=holesz,dsz=0,dz=0) {
	cube([sz+dsz,sz+dsz,th+dz], center=true);
}

module osmall() {
	translate([0,outsz/2 + 10,0]) small();
}
module obig() {
	difference() {
		cube([outsz,outsz,th], center=true);
		small(dz=1);
	}
}

osmall();
obig();
