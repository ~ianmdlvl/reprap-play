// -*- C -*-

mainlen = 33;
straps = [10,23];
width = 60;
endwall = 5;
sidewall = 8;
basethick = 3;
endwallheight = 20;
morebase = 20;

plugwidth = 35;
plugstartheight = 10;

strapthick = 4;
strapwidth = 7;
strapbotgap = 1;
overstrap = 6;

discdia = 60;
discoff_rear = 10;
discoff_front = 50;

sidewallraise = strapbotgap + strapthick + overstrap;

module Sides(){
  difference(){
    for (y=[0, width-sidewall]) {
      translate([0,y,0])
	cube([mainlen, sidewall, basethick + sidewallraise]);
    }
    for (x=straps) {
      translate([x, 0, basethick + strapbotgap + strapthick/2])
	cube([strapwidth, width*3, strapthick], center=true);
    }
  }
}

module Ell(baseoff){
  translate([-endwall,0,0]) {
    translate([baseoff,0,0])
      cube([mainlen + endwall + morebase, width, basethick]);
    cube([endwall+0.1, width, endwallheight + sidewallraise + basethick]);
  }
}

module Plug(){
  translate([0, width/2,
	     basethick + sidewallraise + plugstartheight + 50])
    cube([endwall*3, plugwidth, 100], center=true);
}

module Disc(discoff){
  translate([discoff + discdia/2, width/2, -1])
    cylinder(r=discdia/2, h=50, $fn=100);
}

module Main(baseoff){
  difference(){
    union(){
      Ell(baseoff);
      Sides();
    }
    Plug();
  }
}

module RearBlock(){
  difference(){
    Main(-morebase);
    Disc(discoff_rear);
  }
}

module FrontBlock(){
  difference(){
    Main(0);
    Disc(discoff_front - endwall);
  }
}

module Both(){
  RearBlock();
  translate([mainlen + endwall + 10, 0, 0])
    FrontBlock();
}

Both();
