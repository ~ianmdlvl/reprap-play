// -*- C -*-

outdia=15.1;
india=13.0;
depth=10;

eoutrad = outdia/2 + 1.0;
einrad = india/2 - 1.0;
edepth = depth + 3;

handledepth = 5;
handlewidth = 20;
handlelength = 70;

module FlatSplines(){
  for (rot=[0:7]) {
    rotate([0,0, rot*360/8])
      for (m=[0,1]) {
	mirror([m,0,0])
	  polygon([[-0.1, 0],
		   [-0.01, eoutrad],
		   [einrad * sin(22.5), einrad * cos(22.5)],
		   [einrad * sin(22.5), einrad * cos(22.5) - 3],
		   [1, 0]]);
      }
  }
}

translate([0,0,-1])
  linear_extrude(height=edepth+1)
  FlatSplines();

translate([0,0,-handledepth/2])
  cube([handlelength,handlewidth,handledepth], center=true);
