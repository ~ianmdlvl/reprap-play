#!/usr/bin/perl -w
use strict;

our @xm = qw(4.7 6.8 8.2 10 12 15 18 22 27 33 47 68 100);
our @ym = qw(3.9 5.6 8.2 12 18 27 39 56);

sub p { print @_ or die $!; }

p "include <commitid.scad>\n";
p "p = Commitid_pixelsz();\n";

my $x = 0;
foreach my $xm (@xm) {
    my $y = 0;
    foreach my $ym (@ym) {
	p " translate([$x,$y] * p) {\n";
	p "  difference(){\n";
	p "   translate(-0.5*p*[1,1]) square([$xm+1,$ym+1]*p);\n";
	p "   square([$xm,$ym]*p);\n";
	p "  }\n";
	p "  Commitid_BestCount_2D([$xm,$ym] * p, margin=0.2);\n";
	p " }\n";
	$y += $ym + 2;
    }
    $x += $xm + 2;
}
