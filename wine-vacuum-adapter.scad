// -*- C -*-

india_nom = 27.0;
india_slop = 0.63;

middia_nom = 31.0;
middia_slop = 0.10;

outdia = 44.0;

wall = 4;

htop = 5;
hbot = 7;

slope = 0.65;

$fa=3;
$fs=0.1;

// calculated

india_use = india_nom + india_slop;
middia_use = middia_nom - middia_slop;

//echo("MIN WALL", (middia_use - india_use)/2);

ppA = [middia_use/2, 0];
ppB = ppA + [0, 1] * htop;
ppC = ppB + [-1,0] * wall;
ppD = [ppC[0], ppA[1] - wall/slope];
ppE = [india_use/2, ppD[1] - (india_use/2 - ppD[0])/slope];
ppF = ppE + [0,-1] * (htop + hbot);
ppG = ppF + [1, 0] * wall;
ppK = [outdia/2, ppA[1]];
ppJ = ppK + [0,-1] * wall;
ppH = [ppG[0], ppJ[1] - (ppJ[0]-ppG[0])/slope];

module Plan1() {
  polygon([[ india_use/2,    -hbot ],
	   [ outdia/2,       -hbot ],
	   [ outdia/2,       0     ],
	   [ middia_use/2,   0     ],
	   [ middia_use/2,   htop  ],
	   [ india_use/2,    htop  ]]);
}

module Plan3() {
  p = [ ppA,
	ppB,
	ppC,
	ppD,
	ppE,
	ppF,
	ppG,
	ppH,
	ppJ,
	ppK ];
  echo(p);
  polygon(p);
}

module Demo(){
  color("blue") translate([0,0,1]) Plan1();
  Plan3();
}

module Adapter(){
  rotate_extrude(convexity=5)
    Plan1();
}

//Demo();
Adapter();
