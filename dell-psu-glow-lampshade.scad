// -*- C -*-

india = 11.01 + 0.50;
t = 0.9;
l = 7.5;

$fa = 3;
$fs = 0.2;

linear_extrude(height=l, convexity=10) {
  difference(){
    circle(r = india/2 + t);
    circle(r = india/2);
  }
}

