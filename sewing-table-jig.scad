//// toplevels-from:sewing-table.scad
include <sewing-table.scad>

JIG = true;

tile_th=0.8;
interlock_dia=5;

jig_pencil_rad = 1;
jig_pencil_slotlen = 10;
jig_min_th = 0.3;
jig_post_hole_slop = 0.5;

test_tile_th = -0.1;

test_edge = interlock_dia * 0.5 + interlock_fine + 2;
round_edge_rad = tile_th/2;
frontcurve_z_slop = 15;
rearcurve_z_slop = 20;

interlock_fine = tile_th/8;
