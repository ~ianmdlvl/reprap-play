This directory contains source code a number of objects of various
kinds, and a simple build system.

In general for a simple file FOO.scad you can say `make FOO.stl'.


More complicated files contain source code for multiple related parts.
The actual objects which might be printed or previewed are indicated
in the source code with `///toplevel'.  For a complicated file
BAR.scad you can generate simple template openscad files, one for each
such object, with `make BAR.auto.scads'.  And you can make all the
corresponding .stl files with `make BAR.auto.stls'.


Many objects have `slop' in them, which represents an amount by which
holes or gaps are bigger, in order to make things fit well.  You will
need to examine the openscad source or play around with previews to
see which slop does what.


Unless otherwise noted every object and file here is:

  Copyright Ian Jackson
  Licenced under the GNU General Public Licence, version 3, or
  (at your option) any later version.  There is NO WARRANTY.

