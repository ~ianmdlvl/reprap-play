// -*- C -*-

include <utils.scad>

body_depth = 40.15 + 4;
body_height = 16.78 + 0.50;

back_round_depth = 2.0;
back_round_rad = 8.0;
back_above_height = 5.3;
back_thick = 3.0;

conn_thick = 6.42 + 2.25;
wire_thick = 6.00 + 0.75;
total_depth = 63.82 - 1.0;

body_front_overlap = 3;

prong_higher = 1.5;
prong_depth = 5.0;
prong_width = 2.0;

base_thick = 4;

$fa= 3;
$fs= 0.3;

// calculated

epp0 = [ 0, -body_height/2 ];
epp1 = [ 0, -conn_thick/2 ];
epp2 = epp1 + [ -body_front_overlap, 0 ];
epp3 = [ +body_depth -total_depth, epp2[1] ];
epp4 = [ epp3[0], +conn_thick/2 +prong_higher ];
epp4a = epp4 + prong_higher * [0,-1];
epp4b = epp4 + prong_higher * [1,0];
epp5 = epp4 + [ -prong_depth, 0 ];
epp6 = [ epp5[0], epp0[1] -base_thick ];
epp7 = [ epp2[0], epp6[1] ];
epp12 = [ +body_depth +back_round_rad, 0 ];
epp11 = [ +body_depth +back_round_depth, epp0[1] ];
epp10 = [ epp11[0], +back_above_height ];
epp9 = epp10 + [ +back_thick, 0 ];
epp8 = [ epp9[0], epp7[1] ];
  
y1 = wire_thick/2;
y2 = y1 + prong_width;

module MainElevation(){
  polygon([epp0,
	   epp1,
	   epp3,
	   epp4a,
	   epp4b,
	   epp5,
	   epp6,
	   epp8,
	   epp9,
	   epp10,
	   epp11
	   ]);
  intersection(){
    translate(epp12) circle(r= back_round_rad);
    rectfromto(epp8,
	       epp10 + [-back_round_rad, 0]);
  }
}

module Retainer(){
  difference(){
    linextr_y_xz( -y2, +y2 ) {
      MainElevation();
    }
    linextr_y_xz( -y1, +y1 ) {
      rectfromto( epp7 + [+1, -1],
		  epp5 + [-1, +1] );
    }
  }
}

module RetainerPrint(){
  rotate([0,0,180]) Retainer();
}

//MainElevation();

RetainerPrint();
