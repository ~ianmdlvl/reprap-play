// -*- C -*-

arm_depth = 25;
arm_innerwidth = 9.60 - 0.50;
arm_innerheight = 8.90 - 0.50;
arm_pin_depth = 18.50 + 1.0;
arm_pin_dia = 1.5 + 0.7;

armpart_hex_rad = 15;
armpart_main_thick = 8;

hingepin_dia = 3 + 1.0;
hingenut_width = 6 + 1.0;
hingenut_depth = 4;
hingenut_clear = 5;

headpart_main_dia = 15 + 0.3;
headpart_main_len = 16 + 1.1;
headpart_stub_protrude = 2;
headpart_stub_width = 11.7 - 0.6;

headpart_flatten_angle = 45;

// computed

armpart_hinge_height = arm_innerheight + hingenut_width/2 + hingenut_clear;
armpart_main_height = armpart_hinge_height + headpart_stub_width / 2;
armpart_main_width = headpart_stub_width;
armpart_x_unit = armpart_hex_rad * tan(30);
headpart_flatten_z = headpart_main_dia/2 * cos(headpart_flatten_angle);
headpart_stub_support_x = headpart_stub_width * cos(59) / 2;
headpart_stub_len = headpart_stub_protrude + headpart_main_dia/2;
hingenut_depth_y =
  sqrt(headpart_main_dia*headpart_main_dia/4 - hingenut_width*hingenut_width/4)
  - hingenut_depth;

module ArmPart(){ ////toplevel
  difference(){
    translate([-arm_innerwidth/2, 1, 0])
      mirror([0,-1,0])
      cube([arm_innerwidth, arm_depth+1, arm_innerheight]);
    translate([0, -arm_pin_depth, -50])
      cylinder(r=arm_pin_dia/2, h=100, $fn=20);
  }
  difference(){
    translate([-armpart_main_width/2, 0, 0])
      cube([armpart_main_width, armpart_main_thick, armpart_main_height]);
    translate([0,50,armpart_hinge_height])
      rotate([90,0,0])
      cylinder(r=hingepin_dia/2, h=100, $fn=20);
  }
}

module HeadPart(){ ////toplevel
  difference(){
    union(){
      translate([-headpart_main_len/2, 0,0])
	rotate([0,90,0])
	cylinder(r=headpart_main_dia/2, h=headpart_main_len, $fn=40);
      rotate([90,0,0])
	cylinder(h = headpart_stub_len,
		 r = headpart_stub_width/2,
		 $fn = 6);
      translate([-headpart_stub_support_x,
		 -headpart_stub_len,
		 -headpart_main_dia/2])
	cube([headpart_stub_support_x*2,
	      headpart_stub_len,
	      headpart_main_dia/2]);
    }
    translate([-100,-100,-100])
      cube([200,200, 100 - headpart_flatten_z]);
    rotate([90,0,0])
      translate([0,0, -100])
      cylinder(r=hingepin_dia/2, h = 200, $fn=20);
    translate([0,hingenut_depth_y,0])
      rotate([90,0,180])
      cylinder(r=hingenut_width/2/cos(30), h=20, $fn=6);
  }
}

//ArmPart();
//HeadPart();
