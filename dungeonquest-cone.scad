// -*- C -*-

basesz=12;
height=14.7;
topsz=0.5;
dsz=0;

module One(){ ////toplevel
  cylinder(h=height, r1=basesz/2-dsz, r2=topsz/2-dsz, $fn=50);
}

module Four(){ ////toplevel
  for (x=[0,1]) {
    for (y=[0,1]) {
      translate([x*(basesz+3), y*(basesz+3), 0])
	One();
    }
  }
}
