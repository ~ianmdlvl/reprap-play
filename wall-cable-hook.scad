// -*- C -*-

circle_inner_rad = 10 + 0.5;

thick = 3;

tab_sz = 20;
width = 20;

screw_hole_dia = 4.5 + 0.5;

// calculated

circle_outer_rad = circle_inner_rad + thick;

module Plan() {
  difference(){
    circle(r=circle_outer_rad, $fn=150);
    circle(r=circle_inner_rad, $fn=150);
    mirror([1,0]) square([50,50]);
  }
  translate([-circle_outer_rad, -0.1])
    multmatrix([[1,0,0,0],
		[-1,1,0,0],
		[0,0,1,0],
		[0,0,0,1]])
    square([thick, tab_sz + circle_outer_rad + thick]);
}

module Hook(){
  difference(){
    linear_extrude(height=width) Plan();
    if (false)
      translate([-50, circle_outer_rad + tab_sz - width/2, width/2])
      rotate([0,90,0])
      cylinder(r= screw_hole_dia / 2, h=100, $fn=50);
  }
}

Hook();
