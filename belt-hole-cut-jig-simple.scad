// -*- C -*-

strap_thick = 3;
strap_width = 26.75 + 0.7;

punch_dia = 11.10;

punch_slop = 0.5;

jig_interval = 20;

reg_blocks = 3;

jig_iters = 7;

roof_thick = 4;
regblock_thick = 4;
punchtube_thick = 1.8;

total_h = 33;
punchfree_h = 8;

reg_prot_width = 4;

// computed:

punchhole_r = punch_dia/2 + punch_slop;
mainframe_l = jig_interval * jig_iters;

mainframe_w = strap_width + reg_prot_width*2;

echo(mainframe_l);

module RegBlockOutline(){
  difference(){
    translate([0, -mainframe_w/2])
      mirror([1,0])
      square([total_h, mainframe_w]);
    translate([1, -strap_width/2])
      mirror([1,0])
      square([strap_thick+1, strap_width]);
  }
}

module RegBlock(){
  translate([regblock_thick/2,0,total_h])
    rotate([0,-90,0])
    linear_extrude(height=regblock_thick)
    RegBlockOutline();
}

module MainFrame(){
  translate([jig_interval/2, -mainframe_w/2, 0])
    mirror([1,0,0])
    cube([mainframe_l, mainframe_w, roof_thick]);
  for (rbi=[0:reg_blocks-1]) {
    translate([0 +
	       -(mainframe_l-jig_interval)/(reg_blocks-1) * rbi,
	       0,0])
      RegBlock();
  }
}

module PerHole(){
  for (holei=[0:jig_iters-1]) {
    translate([-jig_interval * holei, 0, 0])
      child(0);
  }
}

module Shells(){
  PerHole(){
    cylinder(r=punchhole_r+punchtube_thick, h=total_h-punchfree_h, $fn=50);
  }
}

module Punches(){
  PerHole(){
    translate([0,0,-1]){
      cylinder(r=punchhole_r, h=total_h+2, $fn=100);
      %cylinder(r=punch_dia/2, h=total_h);
    }
  }
}

module Jig(){
  difference(){
    union(){
      MainFrame();
      Shells();
    }
    Punches();
  }
}

Jig();
