// -*- C -*-

include <doveclip.scad>

for (y=[0,-15]) translate([0,y,0]) {
  DoveClipPair();

  translate([-8,0,0])
    cube([16,5,7]);
  translate([15,0,0])
    DoveClipPin();
}

translate([0,20,0])
  DoveClipPairBase();
