// -*- C -*-

include <utils.scad>

height = 60;
curl = 10;
width = 85;
sides_depth = 50;
th = 6;
th2 = 4;

$fa = 3;
$fs = 0.3;

// calculated

upper_r = th/2;
upper_ctr_maj_r = curl/2 + upper_r;

zmin = curl/2 + th;

module UpperPlan(){
  circle(r = upper_r);
}

module EndCurl(){
  rotate([90,0,0])
    rotate_extrude(angle=180)
    translate([upper_ctr_maj_r, 0])
    UpperPlan();
  translate([-upper_ctr_maj_r, 0,0])
    sphere(r= upper_r);
}

module Upper(){
  translate([upper_ctr_maj_r, 0, 0])
    linextr(-0.1, height + 0.1)
    UpperPlan();
  translate([0, 0, height])
    EndCurl();
}

module Lower(){
  rotate([180,0,0])
    EndCurl();
  linextr(-zmin, -zmin + th) {
    square(center=true, [th2, width]);
    for (m=[0,1])
      mirror([0,m])
	hull()
      {
	for (x= sides_depth/2 * [-1,+1])
	  translate([ x, width/2 - th2/2 ])
	    circle(r= th2/2);
      }
  }
}

module Hook(){
  Upper();
  Lower();
}

Hook();
