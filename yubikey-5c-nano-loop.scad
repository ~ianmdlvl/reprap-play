// -*- C -*-

include <utils.scad>

base = [ 8.4, 4.1 ];
base_th = 0.7;
base_slope = 2.0;

hoop_th = 2.1;
hoop_inner_dia = 3.0;

time_square = 8;

$fa = 3;
$fs = 0.1;

// caclulated

loop_post_z = hoop_inner_dia/2;
max_z = hoop_inner_dia + hoop_th;

module Base() {
  hull(){
    linextr(-base_th, -base_th + 0.01)
      square(base, center=true);
    linextr(-base_th, 0) {
      square(base - 2 * base_th * base_slope * [1,1], center=true);
      LoopPlan2();
    }
  }
}

module LoopPlan() {
  translate([hoop_inner_dia/2 + hoop_th/2, 0])
    circle(r = hoop_th/2);
}

module LoopPlan2() {
  for (m=[0,1]) {
    mirror([m,0,0]) LoopPlan();
  }
}

module Loop() {
  translate([0,0, loop_post_z]) {
    intersection(){
      rotate([90, 0,0]){
	rotate_extrude(){
	  LoopPlan();
	}
      }
      linextr(-0.1, hoop_inner_dia*2)
	square(hoop_inner_dia*4, center=true);
    }
  }
  linextr(0, loop_post_z)
    LoopPlan2();
}

module UseUpTime() {
  linextr(-base_th, max_z)
    translate([0, base[0] * 3, 0])
    square(time_square, center=true);
}

module Whole() {
  rotate([0,0, 90]) {
    Base();
    Loop();
  }
  UseUpTime();
}

Whole();
