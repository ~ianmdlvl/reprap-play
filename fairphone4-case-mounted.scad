// -*- C -*-

include <bike-phone-mount.scad>

module CaseMounted(){ ////toplevel
  Case();
  translate([ phone_width/2,
	      -phone_height/2, epp3[1] - case_th_bottom ])
    Mount();
}

//// toplevels-from:
include <fairphone4-case.scad>
$suppress_forward_holes = true;
$suppress_hinge = true;
