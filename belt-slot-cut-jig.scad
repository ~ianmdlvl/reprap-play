// -*- C -*-

// todo
//  various registration marks
//   protrustions at ends at strap width and middle
//   grooves on top face at 1/4,1/2,3/4 length and 1/2 width

strap_thick = 3;
strap_width = 26.75 + 0.7;

jig_interval = 25;

edgewall_width = 3;
crewpunch_slop = 0.3;
main_slop = 0.25;

holder_min_wall = 2;
holder_attach_near_wall = 5;

holder_attach_xsz = 5;
holder_ctie_width = 4.0 + 0.5;
holder_ctie_thick = 3.0 + 0.5;
holder_attach_walls = 3;
holder_attach_roof = 2.5;

holder_corner_round = 2.0;

punch_travel = 8;

// from careful measurement

crewpunch_shape =
  [[  6, [0.6, 6.0], [1.6, 12.3] ],
   [  8, [1.1, 6.2], [1.9, 12.5] ],
   [ 10, [1.6, 6.5], [2.1, 12.8] ],
   [ 12, [1.8, 6.6], [2.3, 12.7] ],
   [ 14, [2.1, 6.8], [2.6, 13.0] ],
   [ 16, [2.4, 6.9], [2.7, 13.2] ],
   [ 18, [2.5, 7.0], [2.9, 13.3] ],
   [ 22, [3.1, 7.1], [3.2, 13.4] ],
   [ 26, [3.3, 7.2], [3.5, 13.6] ], 
   ];

crewpunch_shaft_max_y = 7.5;

crewpunch_systematic_size_error = +0.36;

crewpunch_smallest_shape = crewpunch_shape[0];
crewpunch_biggest_shape = crewpunch_shape[len(crewpunch_shape)-1];

crewpunch_skew_angle = 3.5; //degrees
crewpunch_skew_yoff = +1.1; //mm

// computed

punch_dx = 0.5 * (-crewpunch_biggest_shape[2][0]
		  +crewpunch_biggest_shape[2][1]);
punch_dy = 0.5 * (+crewpunch_biggest_shape[1][1]
		  -crewpunch_biggest_shape[1][0]) + crewpunch_skew_yoff;

attach_ysz = holder_attach_walls*2 + holder_ctie_width;

holder_block_zsz = crewpunch_biggest_shape[0] - crewpunch_smallest_shape[0];
holder_xsz = crewpunch_biggest_shape[2][0] + crewpunch_biggest_shape[2][1] +
  holder_min_wall*2;

holder_skewangle_yextra = holder_xsz/2 * sin(abs(crewpunch_skew_angle));

holder_max_y = punch_dy + crewpunch_biggest_shape[1][0] + holder_min_wall
  + crewpunch_systematic_size_error + holder_skewangle_yextra;

holder_attach_max_y = punch_dy
  - max(crewpunch_biggest_shape[1][1], crewpunch_shaft_max_y)
  - crewpunch_systematic_size_error - holder_skewangle_yextra;

holder_block_min_y = punch_dy
  - crewpunch_biggest_shape[1][1] - holder_attach_near_wall +
  - crewpunch_systematic_size_error - holder_skewangle_yextra;

holder_all_min_y = holder_attach_max_y - attach_ysz;

jig_max_y = max(holder_max_y + main_slop, strap_width/2) + edgewall_width;
jig_min_y = min(holder_all_min_y - main_slop, -strap_width/2) - edgewall_width;

jig_main_zsz = holder_block_zsz + punch_travel;

// common stuff

include <belt-cut-jig-common.scad>

// objects

module CrewPunch(){
  ourslop = crewpunch_slop - crewpunch_systematic_size_error;
  hull(){
    for(layer=crewpunch_shape){
      translate([0,0, layer[0]]){
	for(xind=[0,1]) //translate([xind?0:1,0,0])
	  for(yind=[0,1]) //translate([0,yind?0.5:0,0])
	    mirror([xind?1:0,0,0]) mirror([0,yind?0:1,0]){
	      translate([-0.1,-0.1,-0.1])
		cube([0.1 + layer[2][xind] + ourslop,
		      0.1 + layer[1][1-yind] + ourslop,
		      0.2]);
	    }
      }
    }
  }
}

module MaybeRoundedCube(sizes, roundedness){
  if (roundedness > 0) {
    translate([roundedness, roundedness, 0]){
      minkowski(){
	cube([sizes[0] - roundedness*2,
	      sizes[1] - roundedness*2,
	      sizes[2]]);
	cylinder(h=0.05, r=roundedness, $fn=20);
      }
    }
  } else {
    cube(sizes);
  }
}

module PunchHolder(cutouts=true){
  roundedness = cutouts ? holder_corner_round : 0;
    difference(){
      translate([-holder_xsz/2, holder_block_min_y, 0])
	MaybeRoundedCube([holder_xsz,
			  holder_max_y - holder_block_min_y,
			  holder_block_zsz],
			 roundedness);
      if (cutouts)
	rotate([0,0,-crewpunch_skew_angle])
	translate([punch_dx,
		   punch_dy,
		   -crewpunch_smallest_shape[0]])
	  CrewPunch();
    }
    difference(){
      translate([-holder_attach_xsz/2, holder_all_min_y, 0])
	MaybeRoundedCube([holder_attach_xsz,
			  attach_ysz,
			  holder_block_zsz + punch_travel
			  + holder_ctie_thick + holder_attach_roof + 1],
			 roundedness);
      if (cutouts)
	translate([-30,
		   holder_all_min_y + holder_attach_walls,
		   holder_block_zsz + punch_travel])
	  cube([60, holder_ctie_width, holder_ctie_thick]);
    }
}

module OneJigCutout(){
  minkowski(){
    cube([main_slop*2, main_slop*2, 50], center=true);
    PunchHolder(false);
  }
}

module JigT(){ ////toplevel
  JigPrint();
}

module PunchHolderT(){ ////toplevel
  PunchHolder(true);
}

module Demo(){ ////toplevel
  %PunchHolder();
  Jig();
}

module Kit(){ ////toplevel
  JigT();
  rotate([0,0,-45]){
    translate([(jig_iters-1)*jig_interval/2,
	       jig_min_y - holder_max_y - 5,
	       0])
      PunchHolder();
  }
}

//CrewPunch();
//PunchHolder();
//PunchHolder(false);
//OneJig();
//Jig();
Demo();
//JigT();
//RegistrationProtrusion();
//PunchHolderT();
//Kit();
