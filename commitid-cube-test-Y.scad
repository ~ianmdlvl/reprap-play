// -*- C -*-

$Commitid_depth = 1.0;
$Commitid_pixelsz = 1.5;

include <commitid.scad>

baseh= 1;

fdo = [1, 3, 0];

w = 3;
t = 4;

fdsz = Commitid_FontDemo_sz();
d = Commitid_depth();
ru = Commitid_pixelsz();
echo($Commitid_pixelsz, ru, fdsz);

sz = max( fdsz[0], fdsz[1] ) + ru;

module FD () {
    translate(fdo)
      Commitid_FontDemo();
}

module TC () { ////toplevel
  difference(){
    cube([sz,sz,sz]);
    translate([0,0, sz]) mirror([0,0,1]) FD();
    rotate([90,0,0]) translate([0,0,0]) FD();
    translate([sz,0,0]) mirror([1,0,0]) rotate([90,0,90]) FD();
    translate([sz,sz,0]) rotate([0,0,180]) FD();
  }
  translate([sz,sz,0]) rotate([-90,0,0]) rotate([0,0,180]) FD();
  translate([0,sz,0]) rotate([-90,0,90]) rotate([0,0,180]) FD();
}

module TTWall () {
  difference(){
    translate([0, 0, -0.1])
      cube([w, sz, sz - 2 + 0.1]);

    translate([0,sz,0]) rotate([90,0,-90]) FD();
    translate([0, sz, 0])
      rotate([90, 0, -90])
      translate(fdo + [0, -ru*2, -d]) cube([fdsz[0], ru, d*2]);
  }
  translate([w,0,0]) rotate([90,0,90]) FD();

  translate([0, sz+d, 0])
    rotate([90,0,0])
    translate([0, fdo[1], 0]) cube([d*2, fdsz[1], ru]);
}

module TT () { ////toplevel
  difference(){
    translate([-sz, 0, -t])
      cube([sz*2 + w, sz, t]);

    translate([0,0,-t]) rotate([0,180,0]) FD();
    translate([w,0,0]) rotate([0,0,0]) FD();

    translate([(sz+w), 0, -t]) rotate([0,180,0])
      Commitid_BestCount([sz+w, sz]);
  }
  translate([-sz,0,0]) rotate([0,0,0]) FD();

  TTWall();
  translate([0,0,-t]) rotate([90,0,0]) TTWall();
}

echo("pixelsz:", str(Commitid_pixelsz()),
     "depth:", Commitid_depth(),
     "sz:", Commitid_FontDemo_sz());

//TC();
TT();
