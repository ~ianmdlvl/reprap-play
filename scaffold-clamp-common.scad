// -*- C -*-

include <utils.scad>

tube_dia = 48.3;

th = 7;

pin_gap = 1.5; // around

smooth_r = 15;

bolt_dia = 5;
bolt_flat = 10 + 1;

nbolts = 2;

open_gap = 10;

hinge_unit = 10;
hinge_z_gap = 1;

hinge_units = 4;

bolt_gap = 1.0; // total, on both sides

// ---------- vhook ----------

vhook_th = 14;

// ---------- cleat ----------

cleat_frames = 10;
cleat_curve_r = 200;
cleat_horn_l = 40;
cleat_horn_d_min = [10, 12];
cleat_horn_d_max = [12, 14];
cleat_height = 25;
cleat_stem_l = 20;

cleat_overlap = (1-cos(60));

// ---------- hhook ----------

hhook_inside = 40;
hhook_th = 4;
hhook_l = 40;

// ---------- linear bracket ----------

linear_bracket_h = 50;
linear_bracket_l = 100;
linear_bracket_t = 15;
linear_bracket_hole_offset = 20;
linear_bracket_hole_dia = 5 + 1.00;

// ========== defaults ==========

pin_head_th = th/2;
pin_dia = th;
pin_hole_dia = pin_dia/2;
pin_tail = pin_hole_dia + pin_head_th + hinge_z_gap*3;

// ========== calculated ==========

TAU = PI*2;

hole_dia = th + pin_gap;

bolt_hole_r = (bolt_dia + bolt_gap)/2;

main_r = tube_dia/2 + th;

hinge_gap = pin_gap;

hinge_o_r = 0.5 * hole_dia + th;

hinge_x = -0.5 * tube_dia - hinge_o_r;
bolt_x = 0.5 * tube_dia + th + bolt_flat * 0.5;
max_x = bolt_x + max(bolt_hole_r + th, 0.5 * bolt_flat/2);

flats_y = open_gap/2 + th;

stride_z = hinge_unit*2 + hinge_z_gap*2;
total_z = hinge_units * stride_z - hinge_z_gap;

min_z = -total_z/2;
max_z = +total_z/2;

pin_flatten = pin_dia/2 * (1 - cos(45));

bolt_stride = total_z / nbolts;

// calculated - vhook

vhook_inside = 15;

vhook_theta = atan2( smooth_r, main_r );

vhook_y0 = -max(main_r, (tube_dia/2 + vhook_th));
vhook_ctr = vhook_y0 - vhook_inside/2;
vhook_outer_dia = vhook_inside + vhook_th*2;

// calculated - cleat

cleat_horn_tl = cleat_horn_l + cleat_stem_l/2;

vcleat_dz = max(0,
		cleat_horn_tl
		+ cleat_horn_d_min[0]/2
		- cleat_horn_d_min[0]/2 * cleat_overlap
		- total_z/2
		);

// calculated - hhook

hhook_outer_dia = hhook_inside + hhook_th*2;

hhook_ctr = -max(main_r + hhook_inside/2,
		 tube_dia/2 + hhook_outer_dia/2);

$fa = 3;
$fs = 0.1;

module SmoothPlan(){
  offset(r=-smooth_r) offset(delta=smooth_r) children(0);
}

module TubePlan(){ circle(r = tube_dia/2); }
module MainCirclePlan(){ circle(r = main_r); }

module PlanWeldMainCircle(){
  intersection(){
    difference(){
      SmoothPlan(){
	union(){
	  MainCirclePlan();
	  children(0);
	}
      }
      TubePlan();
    }
    rotate(-135) square(100);
  }
}

module MainPlan(flatten=false) {
  difference(){
    SmoothPlan()
      union(){
      translate([hinge_x, 0]) circle(r= hinge_o_r);
      MainCirclePlan();
      rectfromto([0,           -flats_y],
		 [max_x,       +flats_y]);
    }
    TubePlan();
    rectfromto([0,       -open_gap/2],
	       [max_x+1, +open_gap/2]);
    translate([hinge_x, 0]) {
      intersection(){
	circle(r= hole_dia/2);
	if (flatten)
	  translate([ pin_flatten, 0 ])
	  square(center=true, [hole_dia, hole_dia + 1]);
      }
    }
  }
}

module Portion(d=0) {
  translate([hinge_x, 0]) circle(r= hinge_o_r + d);
  rectfromto([hinge_x*2, 0],
	     [max_x+10, -(tube_dia/2+th+10)]);
}

module MainPlanA(flatten){
  intersection(){
    MainPlan(flatten);
    Portion(0);
  }
}

module MainPlanB(flatten){
  difference(){
    MainPlan(flatten);
    Portion(hinge_gap);
  }
}

module HalfClampXPositive(flatten=false){
  translate([0,0, min_z]) {
    linextr(0, total_z) mirror([0,1]) MainPlanB();
    for (i=[0 : hinge_units-1]) {
      translate([0,0, stride_z*i])
	linextr(0, hinge_unit) MainPlanA(flatten);
    }
  }
}

module HalfClampXNegative(){
  for (j=[0:nbolts-1]) {
    translate([ bolt_x, 0, min_z + (j + 0.5) * bolt_stride ]) {
      translate([0, -tube_dia/2, 0])
	rotate([-90,0,0])
	cylinder(r= bolt_hole_r, h= tube_dia);
      translate([0, -flats_y, 0])
	rotate([90,0,0])
	cylinder(r= bolt_flat/2, h= tube_dia/2);
    }
  }
}

module HalfClampX(flatten=false){
  difference(){
    HalfClampXPositive(flatten);
    HalfClampXNegative();
  }
}

// ---------- vhook ----------

module VHookProfile() {
  translate([0, -vhook_inside/2 - vhook_th/2])
    circle(r = vhook_th/2);
}

module VHookHookMain(outer=false){ ////toplevel
  rotate([0,90,0])
    rotate_extrude(convexity=10)
    rotate([0,0,90])
    hull(){
      VHookProfile();
      if (outer) {
	translate([0,-vhook_outer_dia]) square(center=true, vhook_th);
      }
    }
}

module VHookA(){ ////toplevel
  DummyA();

  translate([0, vhook_ctr, 0]){
    for (m=[0,1]) {
      mirror([0, m, 0]) {
	linextr(-0.1, vhook_outer_dia/2)
	  VHookProfile();
	translate([0, -vhook_inside/2 -vhook_th/2, vhook_outer_dia/2])
	  sphere(r= vhook_th/2);
      }
    }

    intersection(){
      VHookHookMain(outer=true);
      linextr_y_xz(0, vhook_outer_dia/2) hull(){
	VHookProfile();
	translate([0,-0.1]) square(center=true, [vhook_th, 0.2]);
      }
    }

    intersection(){
      VHookHookMain();
      translate([0,0, -vhook_outer_dia])
	cube(center=true, vhook_outer_dia*2);
    }
  }

  //translate([0, vhook_y0, 50]) rotate([0,0,-90]) color("black") cube(10);
  // translate([0,0,-150]) rotate([0,0,180 + theta]) color("blue") cube(100);
}

module VHookPlanDemo(){
  MainPlanA();
  translate([0, vhook_ctr, 5])
    for (m=[0,1]) {
      mirror([0,m])
	color("blue") VHookProfile();
    }
}

// ---------- cleat ----------

function cleat_frame_theta(s) = s * cleat_horn_tl / cleat_curve_r * 360/TAU;
function cleat_frame_z(s) = cleat_curve_r * (1 - cos(cleat_frame_theta(s)));
function cleat_frame_x(s) = cleat_curve_r * sin(cleat_frame_theta(s));
function cleat_frame_r(s) = ( cleat_horn_d_min * s +
			      cleat_horn_d_max * (1-s) ) * 0.5;

module CleatFrameSphere(r) {
  scale([1, r[1]/r[0], 1])
    sphere(r= r[0]);
}

module CleatFrame(s) {
  r = cleat_frame_r(s);
  translate([cleat_frame_x(s), 0, cleat_frame_z(s)])
    rotate([0, 90, 0])
    CleatFrameSphere(r);
}


module CleatHorn(){
  for (si=[0 : cleat_frames-2]) {
    s0 = si / (cleat_frames-1);
    s1 = (si+1) / (cleat_frames-1);
    hull(){
      CleatFrame(s0);
      CleatFrame(s1);
    }
  }
}

module CleatBase(){
  frames = cleat_frames/2;
  se = cleat_stem_l/2 / cleat_horn_tl;
  r = cleat_frame_r(se);

  hull(){
    for (s = [-se, se]) {
      for (z= [-(cleat_height + tube_dia/2),
	       cleat_frame_z(s)]) {
	translate([cleat_frame_x(s), 0, z])
	  linear_extrude(height=0.1)
	  scale([1, r[1]/r[0]])
	  circle(r=r[0]);
      }
    }
  }
}

module VCleat(){
  intersection(){
    translate([0,0, vcleat_dz]){
      difference(){
	translate([0, -(main_r + cleat_height), 0]) {
	  rotate([0, -90, 90]) {
	    CleatBase();
	    for (m=[0,1]) {
	      mirror([m,0,0]) {
		CleatHorn();
	      }
	    }
	  }
	}
	linextr(-cleat_stem_l, +cleat_stem_l)
	  circle(r = tube_dia/2 + 0.1);
      }
    }
      translate([0,0, total_z * 0.5])
	cube(center=true,
	     (main_r + cleat_stem_l)*4 * [1,1,0] +
	     total_z * [0,0,2]);
  }
}

module VCleatA(){ ////toplevel
  DummyA();
  VCleat();
}

// ---------- hhook ----------

module HHookHookPlan(){
  translate([0, hhook_ctr]){
    difference(){
      circle(r = hhook_outer_dia/2);
      circle(r = hhook_inside/2);
      rectfromto([+hhook_outer_dia, -hhook_outer_dia],
		 [0,                +hhook_outer_dia]);
    }
    translate([0, -(hhook_inside/2 + hhook_th/2)]){
      hull(){
	for (x=[-0.1, hhook_l]) {
	  translate([x,0]) square(center=true, hhook_th);
	}
      }
    }
  }
}

module HHookA(){ ////toplevel
  DummyA();
  linextr(min_z, max_z) {
    HHookHookPlan();
  }
}

module HHookPlanDemo(){
  MainPlanA();
  HHookHookPlan();
}

// ---------- linear bracket ----------

module LinearBracketA(){ ////toplevel
  difference(){
    union(){
      HalfClampXPositive();
      mirror([1,0,0])
      linextr_y_xz(-open_gap/2 - linear_bracket_t, -open_gap/2)
	rectfromto([0, min_z],
		   [max_x + linear_bracket_l, min_z + linear_bracket_h]);
    }
    HalfClampXNegative();
    linextr(-1000,1000)
      TubePlan();
    mirror([1,0,0])
      linextr_y_xz(-100,100) {
      for (t = [
		[1,1] * linear_bracket_hole_offset,
		-[1,1] * linear_bracket_hole_offset +
		[linear_bracket_l, linear_bracket_h]
		]) {
	translate([ max_x, min_z ] + t)
	  circle(r= linear_bracket_hole_dia/2);
      }
    }
  }
}

// ---------- misc ----------

module PinSitu(){ ////toplevel
  difference(){
    union(){
      translate([0,0, -pin_head_th])
	cylinder(r= pin_dia/2, h = total_z + pin_head_th + pin_tail);
      mirror([0,0,1])
	cylinder(r= hinge_o_r - pin_gap, h = pin_head_th);
    }
    translate([0,0, total_z + pin_tail/2])
      rotate([0,90,0])
      translate([0,0, -pin_dia])
      cylinder(r= pin_hole_dia/2, h=pin_dia*2);
    translate([pin_dia/2 * cos(45), -50, -pin_head_th*2])
      cube([50,100, total_z*2]);
  }
}

module Pin(){ ////toplevel
  rotate([0,90,0]) {
    PinSitu();
  }
}

module GeneralB(){ ////toplevel
  HalfClampX(true);
}

module DummyA(){ ////toplevel
  HalfClampX();
}

module PlanDemo(){ ////toplevel
  MainPlan();
  translate([0,0,-4]) color("red") Portion(1);
  translate([0,0,-2]) color("grey") Portion(0);

  translate([0, tube_dia*1.5]) {
    MainPlanB();
    MainPlanA();
  }

  translate([0, -tube_dia*1.5]) {
    VHookPlanDemo();
  }
  translate([tube_dia*4, 0]) {
    HHookPlanDemo();
  }
//  translate([max_x - hinge_x + 20, 0]) color("blue") MainPlanA();
}

module DemoA(){ DummyA(); }

module Demo(){ ////toplevel
  color("red") rotate([180,0,0]) GeneralB();
  color("blue") DemoA();
  color("orange") translate([hinge_x, 0, min_z - hinge_z_gap])
    rotate([0,0,180]) PinSitu();
}

module DemoPair(){ ////toplevel
  color("red") rotate([180,0,0]) DemoA();
  color("blue") DemoA();
  color("orange") translate([hinge_x, 0, min_z - hinge_z_gap])
    rotate([0,0,180]) PinSitu();
}

//PlanDemo();
//HalfClamp();
