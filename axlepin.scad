// -*- C -*-
//
// axlepin.scad
//
// 3D designs for for securing things on axles
// Copyright 2012,2016 Ian Jackson
//
// This work is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This work is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this work.  If not, see <http://www.gnu.org/licenses/>.



function AxlePin_holerad() = 2;
function AxlePin_zoffset(holerad=2, slop=0.5) = (holerad - slop)*0.7;

module AxlePin(axlerad, pinlen, holerad=2, tabthick=5, slop=0.5){
  pinr = holerad - slop;
  intersection(){
    union(){
      translate([0, -pinlen/2, 0]) rotate([-90,0,0])
	cylinder(r=pinr, h=pinlen, $fn=10);
      translate([-tabthick, axlerad, -holerad])
	cube([tabthick*2, holerad*2, holerad*2]);
    }
    translate([-50,-50,-AxlePin_zoffset(holerad,slop)])
      cube([100,100,50]);
  }
}

function Washer_thick() = 1.2;

module Washer(axlerad, washerrad, thick=1.2, slop=0.5){
  difference(){
    cylinder(h=thick, r=washerrad);
    translate([0,0,-1]) cylinder(h=thick+2, r=axlerad+slop);
  }
}
