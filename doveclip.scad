// -*- C -*-
//
// doveclip.scad
//
// 3D design for a fastener suitable for Reprapss
// Copyright 2012,2016 Ian Jackson
//
// This work is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This work is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this work.  If not, see <http://www.gnu.org/licenses/>.


toothheight = 1.2;
webthick = 1.8;
height = 7;
pinlengthfact = 1.2;
nomrad = height/2 - toothheight;
minrad = nomrad - 0.75;
maxrad = nomrad + 0.25;
jawthick = 1.5;

webgap = 0.4;
basepinclear = 1.0;

toothgap = webthick + webgap*2;
basethick = toothheight;

module DoveClipPin(h=height) {
  pinh = h * pinlengthfact;
  pinheight = nomrad*2 + jawthick*2;
  translate([0,0, pinheight/2]) intersection(){
    union(){
      for (m=[0,1]) {
	mirror([0,0,m]) translate([0,0,pinheight/2]) rotate([90,0,0])
	  cylinder($fn=20, r1=minrad, r2=maxrad, h=pinh);
      }
      translate([-webthick/2, -pinh, -pinheight/2-1])
	cube([webthick, pinh, pinheight+2]);
    }
    translate([-maxrad-1, -pinh-1, -pinheight/2])
      cube([maxrad*2+2, pinh+2, pinheight]);
  }
}

function DoveClip_depth() =
  basethick + nomrad*2 + toothheight;

module DoveClipEnd(baseextend=1, h=7) {
  cubex = nomrad*2 + jawthick*2;
  cube0y = -basethick-nomrad*2-toothheight;
  centrey = -basethick-nomrad;
  difference(){
    translate([-cubex/2, cube0y, 0])
      cube([cubex, -cube0y+baseextend, h]);
    translate([0, centrey, -1])
      cylinder($fn=20, r=nomrad, h=h+2);
    translate([-toothgap/2, cube0y-1, -1])
      cube([toothgap, toothheight+nomrad+1, h+2]);
  }
}

module DoveClipPair(baseextend=1, h=7) {
  delta =  nomrad*2 + jawthick*2 + toothgap;
  for (x=[-delta/2,delta/2])
    translate([x,0,0])
      DoveClipEnd(baseextend=baseextend, h=h);
}

module DoveClipPairBase(baseextend=0.1, h=7, count=2) {
  delta = nomrad*2 + jawthick;
  intrude = nomrad + basethick - basepinclear;
  for (i=[0:count-1]) {
    translate([(i - (count-1)/2) * delta, 0, 0])
      DoveClipEnd(baseextend=baseextend, h=h);
  }
  translate([-delta * count/2, -intrude, 0])
    cube([delta * count, intrude+0.1, h]);
}

module DoveClipPairSane(baseextend=0, h=7, count=2) {
  rotate([0,0,90])
    translate([0, DoveClip_depth(), 0])
    DoveClipPairBase(baseextend=baseextend, h=h, count=count);
}

function DoveClipPairSane_width(count=2) =
  2 * (nomrad + jawthick + ((nomrad*2 + jawthick) * (count-1)/2));

module ExtenderPillar(length, height,
		      pillarw=3.5, pillarslope=0.75, webthick=1) {
  pillarr=pillarw/2;
  d = 0.25;

  intangle = atan(pillarslope);
  polyjx = sin(intangle)*pillarr;
  polyjy = cos(intangle)*pillarr;
  polyex = -tan(intangle+90)*pillarr;
  webmidy = height/2+d;

  for (xmir=[0,1])
    translate([0,0,height/2]) mirror([0,0,xmir])
      translate([0,0,-height/2]) {
      intersection() {
	translate([-1, -pillarr-5, 0.01])
	  cube([length+2, height+pillarr*2+10, height]);
	mirror([1,0,0]) rotate([0,-90,0])
	  linear_extrude(height=length) union(){
	  circle(r=pillarr, $fn=20);
	  polygon([[polyjx,polyjy-0.1], [polyex, 0],
		   [polyjx,-(polyjy-0.1)]]);
	  polygon([[0,-webthick/2], [0,webthick/2],
		   [webmidy,webthick/2], [webmidy,-webthick/2]]);
	}
      }
    }
}

module ExtenderPillars(length, width, height,
		       pillarw=3.5, pillarslope=0.75, webthick=1,
		       baseweb=false, basewebthick=1) {
  pilesw = width - pillarw;

  for (ymir=[0,1]) mirror([0,ymir,0]) translate([0,-pilesw/2,0]) {
      ExtenderPillar(length, height, pillarw, pillarslope, webthick);
    }

  if (baseweb) {
    translate([0, -pilesw/2, 0])
      cube([length, pilesw, basewebthick]);
  }
}

module DoveClipExtender(length, ha=7, hb=7, counta=2, countb=2,
			pillarw=3.5, pillarslope=0.75, webthick=1) {

  mirror([1,0,0])
    DoveClipPairSane(h=ha, count=counta);
  translate([length,0,0])
    DoveClipPairSane(h=hb, count=countb);
  pillarlen = length - DoveClip_depth() * 2 + 2;

  pilesw = min(DoveClipPairSane_width(counta), DoveClipPairSane_width(countb))
    - 0.5;
  pilesh = min(ha, hb) - 0.5;

  translate([DoveClip_depth() - 1, 0, 0])
    ExtenderPillars(pillarlen, pilesw, pilesh,
		    pillarw=pillarw, pillarslope=pillarslope,
		    webthick=webthick);
}

//DoveClipExtender(length=100, ha=16, hb=20, counta=3, countb=4);
