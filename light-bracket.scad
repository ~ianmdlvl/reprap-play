shrinkage = 1.0126; // width of 56.2 gives 55.5
remote_width= 56.35 * shrinkage;
remote_height=124.7 * shrinkage;
remote_thick=6.1; // height of 6.8 gives 6.3
mainhole_thick=remote_thick+1;
hook_hook_thick=1.5;
hook_stem_thick=1.5;
hook_hook_len=1.5;
base_thick=3.5;
base_margin=3.0;
base_width=remote_width-base_margin*2;
base_height=remote_height-base_margin*2;
base_edgewidth=4;

screw_ys=[ 28, remote_height-28 ];

// origin is base of mainhole

module mainhole() {
	translate([ -remote_width/2, 0, 0 ])
	cube(center=false,
		size=[ remote_width, remote_height, mainhole_thick ] );
}

module hhook(extent) {
	translate([ -hook_stem_thick, 0, -base_thick*2 ])
		cube(center=false,
			size=[
				hook_stem_thick,
				extent,
				base_thick*2 + mainhole_thick + hook_hook_thick
			]);
	translate([ -hook_stem_thick, 0, -base_thick*2 ])
		cube(center=false,
			size=[
				hook_stem_thick+base_margin+base_edgewidth-1,
				extent,
				base_thick*2
			]);
	translate([ -hook_stem_thick+1.0, 0, mainhole_thick ])
		rotate(v=[0,1,0], a=-30)
		cube(center=false,
			size=[
				3,
				extent,
				hook_hook_thick
			]);
	//difference() {
	//	#translate([ -hook_stem_thick, 0, -base_thick*2 ])
	//		cube(center=false,
	//			size=[
	//		hook_stem_thick+base_margin+base_edgewidth-1,
	//		extent,
	//		base_thick*2 + mainhole_thick + hook_hook_thick
	//				]);
	//	translate([hook_hook_len, -5, 0])
	//		 cube(center=false, size=[ 20, extent+10, 30 ]);
	//}
}

module hhookside(extent) {
	translate([ -remote_width/2, 0, 0 ])
		hhook(extent);
}

module hhookbot(extent) {
	rotate(a=90, v=[0,0,1]) hhook(extent);
}

module hstuff() {
	translate([0,70,0]) hhookside(15);
	translate([0,10,0]) hhookside(15);
	translate([-10,0,0]) hhookbot(15);
}

module slashes() {
	for (y=[-30 : 60 : +40])
		translate([0,y,0])
			rotate(v=[0,0,1],a=45)
				cube(center=true, [ 5,200,200 ]);
}

module base() {
	translate([ 0, base_height/2 + base_margin, -base_thick/2 ])
	intersection() {
		cube(center=true,
			[ base_width, base_height, base_thick+10 ]);
		union() {
			difference() {
				cube(center=true, [ 200,200,200 ]);
				cube(center=true,
					[ base_width - base_edgewidth*2,
					  base_height - base_edgewidth*2,
					  base_thick + 15 ]);
				
			}
			slashes();
			mirror([1,0,0]) slashes();
		}
	}
//	translate([-base_width/2, base_margin, -base_thick*2])
//		cube(center=false, [base_width,base_height,base_thick+10]);
}

module stuff() {
	hstuff();
	mirror([1,0,0]) hstuff();
	base();
	for (y=screw_ys) translate([0, y, -20])
		cylinder(r=6.5, h=21);
}

module screwhole(holedia, csdia) {
	// screw goes along z axis downwards
	// origin is top of head
	// results are positive, so this should be subtracted
	translate([0,0,-100]) cylinder(h=200, r=holedia/2);
	csdist=(csdia-holedia)/2;
	translate([0,0,-csdist]) cylinder(h=csdist, r1=holedia/2, r2=csdia/2);
	cylinder(h=100, r=csdia/2);
}

module bracket() {
	// this is the actual thing we want
	difference() {
		stuff();
		mainhole();
		for (y=screw_ys) translate([0, y, 0])	
			screwhole(5.4,10); //dia=4 gives 2.9
					 //holedia=10 gives 9.0 want 7.0
		translate([0,0,-50 - base_thick])
			cube(center=true,[300,300,100]); // print bed
	}
}

intersection() {
	!bracket();
	translate([-50,6,-50]) #cube(center=false, [100,27,100]);
}
