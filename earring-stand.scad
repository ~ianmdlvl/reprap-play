// -*- C -*-

include <commitid.scad>

front_height = 80;
front_width = 120;
front_setback = 30;
front_thick = 2.4;

front_hex_stride = 12.5;
front_hex_dia = 9.5;

front_hex_y_fudge = -0.65;

front_surround_lr =3;

back_thick = 3;
back_pillarw = 6;

base_thick = 2.4;

eclip_inner_rad = 2.5;
eclip_gap_rad = 0.1;
eclip_prong_th = 2.25;
eclip_outer_strt = 0.5;
eclip_inner_xstrt = 0.5;

eclip_ult_angle = 44;
eclip_base_epsilon = 0.5;

eclip_each_len = 6;
eclip_each_every = 29;

test_alpha = 10;
test_main_th = 1.5;
test_eclips = 5;
test_base_th = 2.5;
test_len = eclip_each_len + eclip_each_every*(test_eclips-1);

num_eclips = 5;

// calculated

include <utils.scad>

eclip_inner_strt = eclip_outer_strt + eclip_inner_xstrt;

r0 = eclip_inner_rad;
r1 = r0 + eclip_gap_rad;
r2 = r1 + eclip_prong_th;
r2e = r1 + eclip_base_epsilon;

ppxl = -(r0 / sqrt(2)) + (eclip_inner_strt / sqrt(2));

rgap = eclip_gap_rad;

eclip_base_offset = r1;
eclip_wall_offset = -ppxl;

eclip_ra_offset = r2 - 0.1;

eclip_recept_height = r2;

eclip_rhs_offset = ppxl + rgap + eclip_prong_th;
// does not include main_th

$fn=70;

module EclipLPlanCore(alpha){
  FArcSegment(0,0, r1,r2,
	      180-eclip_ult_angle, eclip_ult_angle-alpha +1);

  difference(){
    hull(){
      intersection(){
	circle(r2);
	rotate(-alpha) mirror([1,1]) square([r2e, 50]);
     }
      rotate(-alpha) mirror([1,1]) square([r2e, r2]);
    }
    circle(r1);
  }
}

module EclipRPlan(alpha, main_th){
  intersection(){
    rotate(alpha)
      translate([ppxl + main_th + rgap, -r2*2])
      square([eclip_prong_th, r2*(2 + 1/sqrt(2))]);
    translate([-r2, -r2e])
      square([r2*3, eclip_base_epsilon + r2*4]);
  }
}

module EclipLPlan(alpha){
  rotate(alpha) EclipLPlanCore(alpha);
}

module EclipPPlan(main_th){
  intersection(){
    hull(){
      circle(r0);
      rotate(90-eclip_ult_angle) square([r0,r0]);
    }
    translate([-(r0+.1), -(r0+.1)])
      square([(r0+.1) + main_th + ppxl, r2*2]);
  }
  translate([ppxl, 0]) square([main_th, r2]);
}

module TestBase(){ ////toplevel
  translate([0,0, eclip_base_offset]){
    for (i=[1 : 2: test_eclips-2]) {
      translate([0, i*eclip_each_every])
	rotate([90,0,0])
	linear_extrude(height=eclip_each_len)
	EclipLPlan(test_alpha);
    }
    for (j=[0 : 2: test_eclips-1]) {
      translate([0, j*eclip_each_every])
	rotate([90,0,0])
	linear_extrude(height=eclip_each_len)
	EclipRPlan(test_alpha, test_main_th);
    }
  }
  translate([-r2, -eclip_each_len, -test_base_th]){
    difference(){
      cube([r2*2,
	    test_len,
	    test_base_th]);
      mirror([0,0,1]) Commitid_BestCount_M([r2*2, test_len]);
    }
  }
}

module TestProtr(){ ////toplevel
  difference(){
    translate([0,0, test_main_th - eclip_wall_offset])
      rotate([0,90,0])
      linear_extrude(height=test_len)
      EclipPPlan(test_main_th);
    mirror([0,0,1]) Commitid_BestCount_M([test_len, r2]);
  }
}

module TestRAProtr(){ ////toplevel
  rotate([-90,0,0]) TestProtr();
  mirror([1,0,0])
    translate([-test_len,
	       -r2,
	       -(eclip_ra_offset + test_base_th)])
    cube([test_len,
	  r2*2,
	  test_base_th]);
}

module TestPlanDemo(){
  color("red") EclipLPlan(test_alpha);
  color("blue") rotate(test_alpha) EclipPPlan(test_main_th);
  color("green") EclipRPlan(test_alpha, test_main_th);
}

beta = asin(front_setback / front_height);

uf = [-sin(beta), cos(beta)];
ur = [ -uf[1], uf[0]];

pp = [0, 0];
pq = pp + uf*front_height + ur*eclip_ra_offset;
pr = [ pq[0] - eclip_base_offset - eclip_wall_offset,
       0 ];

echo("uf ur P Q R", uf, ur, pp, pq, pr);

module Sketch(){
  polygon([pq, pp, pr]);
}

thicks = [ base_thick, front_thick, back_thick ];

module Joins(alpha, objnum, objnum_f, objnum_m) {
  pitch = (front_width - eclip_each_len) / (num_eclips-1);
  
  thm = thicks[objnum_m];
  stride = (front_width - eclip_each_len) / (num_eclips-1);

  if (objnum==objnum_f) {
    for (i=[ 1 : 2 : num_eclips-1 ]) {
      translate([0, i*stride + eclip_each_len, 0]) {
	rotate([90,0,0])
	linear_extrude(height=eclip_each_len)
	  EclipLPlan(alpha);
      }
    }
    for (i=[ 0 : 2 : num_eclips-1 ]) {
      translate([0, i*stride + eclip_each_len, 0]) {
	rotate([90,0,0])
	linear_extrude(height=eclip_each_len)
	  EclipRPlan(alpha, thm);
      }
    }
  }
  if (objnum==objnum_m)
    mirror([0,1,0])
      rotate([90,0,0])
      linear_extrude(height=front_width)
      rotate(alpha)
      EclipPPlan(thm);
}

function r3(pc) = [ pc[0], 0, pc[1] ];

module ObjectJoins(objnum){
  translate(r3(pp))                   Joins(beta, objnum, 0,1);
  translate(r3(pr)) mirror([1,0,0])   Joins(0,    objnum, 0,2);
  translate(r3(pq)) rotate([0,90,0]) mirror([1,0,0]) Joins(-beta, objnum, 2,1);
}

module Base(){
  xmin = pr[0] - eclip_rhs_offset - thicks[2];
  xmax = pp[0] + eclip_rhs_offset + thicks[1]
    + eclip_prong_th * (1/cos(beta) - 1)
    + eclip_base_offset * tan(beta);
  intersection(){
    ObjectJoins(0);
    translate([xmin,
	       -1,
	       -50])
      cube([xmax - xmin,
	    front_width + 2,
	    300]);
  }
  translate([xmin,
	     0,
	     -eclip_base_offset - thicks[0]]){
    difference(){
      cube([xmax - xmin,
	    front_width,
	  thicks[0]]);
      translate([xmax-xmin, front_width]/2)
	rotate([0,0,270])
	Commitid_Full16_M();
    }
  }
}

module FrontPattern(){
  totalh = front_height - eclip_wall_offset + thicks[1];

  ystride = front_hex_stride;
  xstride = front_hex_stride * cos(30) * 2;

  difference(){
    square([front_width, totalh]);
    translate([ front_surround_lr,
		eclip_recept_height ])
      square([ front_width - front_surround_lr*2,
	       totalh - eclip_recept_height*2
	       ]);
  }
    
  difference(){
    square([front_width, totalh]);
    for (xi=[ -5 : 5 ]) {
      translate([front_width/2 +
		 xi * xstride,
		 0]) {
	for (yi=[ 0 : 10 ]) {
	  //echo(yi);
	  translate([0, yi * ystride +
		     front_hex_dia*front_hex_y_fudge]) {
	    for (dv=[ [0,0],
		      [-xstride/2, -ystride/2]
		      ])
	      translate(dv)
		circle(r= front_hex_dia/2, $fn=6);
	  }
	}
      }
    }
  }
}

module Front(){
  ObjectJoins(1);
  rotate([0, 90-beta, 0])
    translate([0, 0, ppxl])
    rotate([0,0,90]) {
    linear_extrude(height=thicks[1])
      FrontPattern();
  }
}

module Back(){
  ObjectJoins(2);

  zmin = pr[1];
  zmax = pq[1] + eclip_prong_th;
  height = zmax - zmin;

  translate([pr[0] + eclip_wall_offset - thicks[2],
	     0, 0])
    rotate([0,90,0])
    rotate([0,0,90]) {
    difference(){
      cube([front_width,
	    height,
	    thicks[2]]);
      translate([back_pillarw,
		 eclip_recept_height,
		 -10])
	cube([front_width - back_pillarw*2,
	      height - eclip_recept_height*2 - eclip_prong_th,
	      20]);
    }
  }
}

module BackPrint(){ ////toplevel
  rotate([0,-90,0]) Back();
}

module FrontPrint(){ ////toplevel
  rotate([0, 90+beta, 0]) Front();
}

module BasePrint(){ ////toplevel
  Base();
}

module Demo(){ ////toplevel
  color("red") Base();
  color("blue") Front();
  color("black") Back();
}

//PlanDemo();
//TestBase();
//TestProtr();
//TestRAProtr();
//Sketch();
//Demo();
//BackPrint();
//FrontPrint();
//BasePrint();
