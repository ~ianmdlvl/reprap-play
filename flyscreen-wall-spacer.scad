// -*- C -*-

include <utils.scad>

bracket_th = 2.70;
left_inboard_to_wall = 9.78;
right_inboard_to_wall = 13.21;

plug_dia = 10;
screw_dia = 5;
bucket_wall = 2.5;
bucket_floor = 2.5;
whole_dia = plug_dia + bucket_wall *2;

min_spacing = 8;
max_spacing = 19;

general_spacer_height = 10;

// calculated

module Oval(r, dc) {
  hull(){
    circle(r);
    translate([0, dc])
      circle(r);
  }
}

module MainCircle() {
  difference(){
    circle(r = whole_dia/2);
    circle(r = screw_dia/2);
  }
}

module MultiSpacer() {
  difference(){
    linextr(0, $inboard_to_wall - bracket_th){
      Oval(whole_dia/2, max_spacing);
    }

    linextr(bucket_floor, 100) {
      Oval(plug_dia/2, max_spacing);
    }

    linextr(-1, 100) {
      circle(screw_dia/2);

      translate([0, min_spacing])
	Oval(screw_dia/2, max_spacing - min_spacing);
    }
  }
}

module AnySpacer(max_z) {
  linextr(0, bucket_wall)
    MainCircle();
  linextr(0, max_z){
    difference(){
      MainCircle();
      circle(r = plug_dia/2);
    }
  }
}

module Spacer($inboard_to_wall) {
  AnySpacer($inboard_to_wall - bracket_th);
}

module Spacers1() {
  for (dy = [0, 30]) {
    translate([0,dy,0]) {
      Spacer($inboard_to_wall = left_inboard_to_wall);
      translate([0, 70, 0])
	Spacer($inboard_to_wall = right_inboard_to_wall);
    }
  }

  translate([40, 0, 0])
    MultiSpacer($inboard_to_wall = left_inboard_to_wall);
  translate([40, 70, 0])
    MultiSpacer($inboard_to_wall = right_inboard_to_wall);
}

module Spacers2() {
  for (dy = 30 * [0]) {
    echo(dy);
    translate([0, dy, 0])
      AnySpacer(general_spacer_height);
  }
}

module Spacers3() {
  AnySpacer(6.08);
  translate([0, 30, 0])
    AnySpacer(8.18);
}

Spacers3();
