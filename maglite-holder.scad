// -*- C -*-

dxf_off = [ -40, -85 ];

torch_lit_dia = 37.5;
torch_big_dia = 56.5;
torch_tot_len = 256;
torch_big_len = 60;

torch_clear = 30;
torch_clear_below = 10;

stem_width = 20;
stem_thick = 8;

torch_recess = 11;
arm_width = 10;
block_thick = 15;

torch_out_more = 10;

brace = [ 40, 20, 20 ];

hole_dia = 4 + 0.5;
hole_slot = 5;

movement_extra_angle = 5;

slop = 4; // total, not each side

torch_min_xcoord_fig_cm = -2.7; // coordinates of bottom left of curve
torch_min_ycoord_fig_cm = -5.9; // & big part in fig file.  mid top is origin
torch_smm_xcoord_fig_cm = -1.9; // x coord of lhs of narrow part

miniature = 1; // can adjust this to get commitid size right and rescale *Print

$fa=5;

// calculated

include <commitid.scad>

torch_dxf_scale =
  [ (torch_big_dia - torch_lit_dia) /
    (-(torch_min_xcoord_fig_cm - torch_smm_xcoord_fig_cm) * 10 * 2),
    torch_big_len / (-torch_min_ycoord_fig_cm * 10) ];

echo(torch_dxf_scale);

above = torch_big_len + torch_clear + torch_clear_below;

holes = [ 172, 265 ];

stem_below = stem_width/2;

stem_len = holes[1] - above + stem_below;

torch_out = stem_thick + torch_big_dia/2 + torch_out_more;

block_width = arm_width*2 + torch_big_dia;

block_out = torch_out + torch_big_dia/2/sqrt(2);

module TorchOrig(){
  mirror([0,0,1]){
    hull(){
      rotate_extrude()
	translate([-torch_lit_dia/2, 0])
	scale(torch_dxf_scale)
	translate(dxf_off)
	translate([-torch_smm_xcoord_fig_cm * 10, 0])
	import(file="maglite-holder-torch-curve.dxf",
	       convexity=10, center=true);
    }
    translate([0,0, -1])
      cylinder(r=torch_lit_dia/2, h= torch_tot_len - torch_big_len + 1);
  }
}

module Torch(){
  scale(slop/torch_lit_dia + 1.0)
    TorchOrig();
}

module ScrewHole(y, rot) {
  translate([0,0, above -y]){
    rotate([0,rot,0]){
      hull(){
	for (d= [-1,+1] * hole_slot/2) {
	  translate([d,0,0])
	    rotate([90,0,0])
	    translate([0,0,-stem_thick*2])
	    cylinder(r= hole_dia/2, h= stem_thick*4);
	}
      }
    }
  }
}    

module TorchMovement(){
  translate([0, -torch_out, 0]) {
    translate([0, 0, -torch_recess])
      Torch();
    for (as=[-1,+1]) {
      rotate([0,0, as*movement_extra_angle])
	rotate([90,0,0])
	linear_extrude(height= block_out)
	projection() rotate([-90,0,0]) Torch();
    }
  }
}

module Bracket(){ ////toplevel
  cid_w = stem_width * .75;
  hole_near = hole_slot + hole_dia;

  difference(){
    mirror([0,1,0]) {
      translate([-stem_width/2, 0, -stem_len])
	cube([stem_width, stem_thick, stem_len]);
      translate([0,0, -block_thick]) hull(){
	translate([-stem_width/2, 0, -brace[2]])
	  cube([stem_width, stem_thick, 1]);
	translate([-brace[0]/2, 0, 0])
	  cube([brace[0], brace[1], 1]);
      }
    }
    ScrewHole(holes[0], 90);
    ScrewHole(holes[1], 0);
    translate([-cid_w/2, 0, above - holes[0] - hole_near])
      rotate([-90,0,0])
      scale([miniature, miniature, 1])
      Commitid_BestCount([cid_w, holes[1]-holes[0] - hole_near*2]
			 / miniature);
  }
  difference(){
    mirror([0,1,0])
      translate([-block_width/2, 0, -block_thick])
      cube([block_width, block_out, block_thick]);
    TorchMovement();
  }
}

module BracketPrint(){ ////toplevel
  scale(1/miniature)
    rotate([-90,0,0])
    Bracket();
}

module TestTorchPrint(){ ////toplevel
  scale(1/miniature)
  intersection(){
    translate([0,0, torch_lit_dia / 2 / sqrt(2)])
      rotate([-90,0,0])
      Torch();
    translate([-100, -torch_tot_len*2, 0])
      cube([200, torch_tot_len*4, 200]);
  }
}

module Demo(){ ////toplevel
  color("red")
    translate([0, -torch_out, 0])
    TorchOrig();
  color("blue")
    translate([0, -torch_out, above])
    cylinder(r=torch_big_dia/2, h=1);
  Bracket();
}

//Demo();
//BracketPrint();
//TestTorchPrint();
