// -*- C -*-

// print Stem and Wingnut on High Detail
// but adjust shell thickness to 2mm

// others on Standard

include <utils.scad>
include <threads.scad>
include <camera-mount.scad>

positive_dia = inch * 3/8. - 0.375;
positive_l = inch * 1/2.;

positive_blank_dia = 8.12;
blank_l = 17;
blank_taper = 1.0;

stem_l = 40;
stem_dia = 12;
stem_th = 3;
stem_ceil = 2;
stem_base_th  = 4;
stem_base_dia = 25;
stem_inner_l = 15;

thread_nom = 8;
thread_pitch = 1.25;
thread_act = thread_nom + 0.600;

clamp_l = 40;
clamp_top_th = 7;
clamp_bot_th = 10;
clamp_bot_tooth_h = 2.5;
clamp_bot_tooth_d  = 10;
clamp_bot_collar = 20;
clamp_bot_collar_th = 3.0;
clamp_reg_sz1 = 3;
clamp_reg_sz2 = 5;
clamp_w = 15;
clamp_max_table_th = 35;

clamp_hole_dia = thread_nom + 0.30;

clamp_reg_clear_x = 2.5;
clamp_reg_clear_y = 0.75; // each side
clamp_reg_extra_x = 4;

//ct_h = 7;

wingnut_th = 5;
wingnut_wall = 4;
wingnut_wing_mindia = 17.0;
wingnut_wing_xrad = 8;
wingnut_wing_xh = 5;
wingnut_wing_th = 3;

//$test= true;
$test= false;

$fa= 3;
$fs= 0.2;

// calculated

wingnut_cnr = wingnut_wing_th/2 -0.1;

clamp_reg_bot_x_min = -stem_base_dia/2 -clamp_reg_clear_x -clamp_reg_sz2;
clamp_collar_r = thread_nom/2 + clamp_bot_collar_th;

module OurThread(l){
  translate([0,0,-0.01])
    metric_thread(diameter=thread_act, pitch=thread_pitch,
		  leadin=3, internal=true,
		  test=$test, length=l);
}

module StemWith(){
  translate([0,0, stem_l -0.1])
    children();

  difference(){
    union(){
      cylinder(r= stem_dia/2 * 1/(0.5 * sqrt(3)),
	       h = stem_l,
	       $fn=6);
      cylinder(r= stem_base_dia/2,
	       h = stem_base_th);
    }
    OurThread(stem_inner_l);
  }
}  

module Stem(){ ////toplevel
  StemWith()
    english_thread(diameter=positive_dia/inch, threads_per_inch=16,
		   leadin=1, test=$test,
		   length= (positive_l + 0.1) / inch);
}

module StemBlankPart(){
  hull(){
    cylinder(h = blank_l + 0.1 - blank_taper,
	     r = positive_blank_dia/2);
    cylinder(h = blank_l + 0.1,
	     r = positive_blank_dia/2 - blank_taper);
  }
}

module BlankStem(){ ////toplevel
  StemWith()
    StemBlankPart();
}

module Wingnut(){ ////toplevel
  difference(){
    union(){
      cylinder(r= (thread_nom+wingnut_wall)/2,
	       h= wingnut_th);
      minkowski(){
	sphere(r= wingnut_cnr);
	translate([0,0, wingnut_cnr*0.5])
	  linear_extrude(height= wingnut_wing_xh + wingnut_th
			 - wingnut_cnr*1.5)
	  square([wingnut_wing_mindia + wingnut_wing_xrad*2 - wingnut_cnr*2,
		  wingnut_wing_th - wingnut_cnr*2],
		 center=true);
      }
    }
    translate([0,0, wingnut_th])
      linear_extrude(height= wingnut_wing_xh+1)
      square(wingnut_wing_mindia, center=true);
    translate([0,0, wingnut_th])
      rotate([180,0,0])
      OurThread(wingnut_th+3);
    mirror([0,0,1])
      linear_extrude(height=5)
      square(center=true, wingnut_wing_mindia*2);
  }
}

module ClampCollarPlan(){
  circle(r= clamp_collar_r);
}
module ClampHolePlan(){
  circle(r= clamp_hole_dia/2);
}
module ClampArmPlan(){
  r = clamp_collar_r;
  hull(){
    rectfromto([r,       -clamp_w/2],
	       [clamp_l, +clamp_w/2]);
    ClampCollarPlan();
  }
}

module ClampTop(){ ////toplevel
  linear_extrude(height = clamp_top_th, convexity=4) {
    difference(){
      union(){
	ClampArmPlan();
	ClampCollarPlan();
      }
      ClampHolePlan();
    }
  }
  linear_extrude(height = clamp_reg_sz1, convexity=4) {
    difference(){
      for (m=[0,1]){
	mirror([0,m,0])
	  translate([0, clamp_reg_sz2/2 + clamp_reg_clear_y, 0])
	  rectfromto([clamp_reg_bot_x_min - clamp_reg_extra_x, 0 ],
		     [0,                           clamp_reg_sz1 ]);
      }
      ClampHolePlan();
    }
  }
}

module ClampBot(){ ////toplevel
  linear_extrude(height = clamp_bot_th, convexity=4) {
    difference(){
      ClampArmPlan();
      ClampHolePlan();
    }
  }
  translate([clamp_l, 0, clamp_bot_th-0.1])
    linear_extrude(height = clamp_bot_tooth_h +0.1)
    rectfromto([ -clamp_bot_tooth_d, -clamp_w/2 ],
	       [  0,                 +clamp_w/2 ]);
  translate([0,0, clamp_bot_th])
    mirror([0,0,1])
    linear_extrude(height = clamp_bot_collar)
    difference(){
    ClampCollarPlan();
    ClampHolePlan();
  }
  translate([0, 0, clamp_bot_th]) {
    linextr(-clamp_reg_sz2, clamp_max_table_th+clamp_reg_sz2) {
      translate([clamp_reg_bot_x_min, 0]) {
	rectfromto([ 0,             -clamp_reg_sz2/2 ],
		   [ clamp_reg_sz2, +clamp_reg_sz2/2 ]);
      }
    }
    linextr(-clamp_reg_sz2, 0) {
      difference(){
	rectfromto([ clamp_reg_bot_x_min, -clamp_reg_sz2/2 ],
		   [  0,                  +clamp_reg_sz2/2 ]);
	ClampHolePlan();
      }
    }
  }
}

module StemBlankTest(){ ////toplevel
  StemBlankPart();
  linextr(-1.5,0) square(center=true, [10,35]);
}

module Demo(){ ////toplevel
  color("blue") translate([0,0, clamp_top_th+0.5]) BlankStem();
  color("red") ClampTop();
  color("grey") translate([0,0, -(clamp_bot_th + 5)]) ClampBot();
  translate([0,0, -(clamp_bot_collar +10)])
    rotate([180,0,0]) Wingnut();
}

//Wingnut();
//Stem();
