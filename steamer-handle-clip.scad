// -*- C -*-

include <funcs.scad>
include <utils.scad>

width = 30 - 2;
cup = 2.5;
jaw = 32.36 - 2.00 - 2.00 - 3.00;
th = 3.0;
l = 15;

a = cup;
b = width/2;
alpha = atan2(a, b);
c = vectorlen2d([a, b]);
r = a / (1 - cos(2*alpha));
C = [0, a-r];

$fa = 1;

module HalfBaseline() {
  intersection(){
    translate(C + [0, jaw/2])
      circle(r=r);
    rectfromto([ -width/2, -1, ],
	       [  width/2, jaw ]);
  }
}

module Baseline(){
  HalfBaseline();
  mirror([0,1]) HalfBaseline();
}

module Plan(){
  difference(){
    offset(delta=th) Baseline();
    Baseline();
    rectfromto([-width, -jaw/2],
	       [0,       jaw/2]);
  }
}

module Thing(){
  linextr(0,l) Plan();
}

//HalfPlan();
//Plan();
Thing();
