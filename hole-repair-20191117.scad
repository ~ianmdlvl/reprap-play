// -*- C -*-

post_dia = 23.0;
post_height = 20;

th = 4;

nom_hole = 25;
min_r = 15 + nom_hole/2;
maj_r = 15 + nom_hole/2;

postwall_th = 2;

$fa=1;
$fs=1;

module Profile(r) {
  polygon([ [0,    0],
	    [r,    0],
	    [r-th, th],
	    [0,    th] ]);
}

module Body(){
  hull(){
    for (x= [-1,+1] * (maj_r-min_r))
      translate([x,0,0])
	rotate_extrude()
	Profile(min_r);
  }
}

module Post(){
  difference(){
    translate([0,0,-1])
      cylinder(r= post_dia/2, h= post_height+1);
    translate([0,0,-2])
      cylinder(r= post_dia/2 - postwall_th, h= post_height+3);
  }
}

module Cover(){
  rotate([0,180,0]) Body();
  Post();
}

//Body();
//Post();
Cover();
