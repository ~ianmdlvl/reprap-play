// -*- C -*-

main_sz = 22.5;

wall_th = 3;

front_hook = 3;
front_slope = 1.0;

front_th = 2;
gap_th = 6.5 + 0.5;

back_tot_l = 35.5;
back_cut_l = 6;
back_cut_w = 15.0 + 1.0;
back_prong_w = 3;
back_hole_d = 2;
back_hole_dia = 1 + 1.5;

back_th = back_hole_dia + 2.4;

module MidPlan(){
  polygon([[0,            0],
	   [0,            main_sz],
	   [wall_th,      main_sz],
	   [wall_th,      wall_th],
	   [main_sz,      wall_th],
	   [main_sz,      0]]);
}

module FrontElevation(){
  hook_z = front_hook / front_slope;
  translate([0, back_th+gap_th]) {
    hull(){
      square([wall_th, hook_z + 0.01]);
      translate([front_hook, hook_z])
	square([wall_th, 0.01]);
    }
  }
}

module FrontEdge(){
  rotate([90,0,0]) linear_extrude(height=main_sz) FrontElevation();
}

module Front(){
  mirror([0,1,0]) FrontEdge();
  rotate([0,0,90]) FrontEdge();
}

module BackPlan(){
  sqmid = main_sz * sqrt(0.5);
  prlen = back_tot_l - sqmid;
  prx = 0.5*back_cut_w + back_prong_w;
  difference(){
    union(){
      square(main_sz);
      rotate(-45) translate([-prx, sqmid])
	square([prx*2,prlen]);
    }
    rotate(-45) translate([-back_cut_w/2, back_tot_l-back_cut_l])
      square([back_cut_w, back_cut_l+1]);
  }
}

module Hook(){
  difference(){
    union(){
      linear_extrude(height=back_th)
	BackPlan();
      linear_extrude(height=back_th+gap_th+front_th)
	MidPlan();
      Front();
    }
    rotate([0,0,-45])
      translate([0, back_tot_l - back_hole_d, back_th/2])
      rotate([0,90,0]) translate([0,0,-50])
      cylinder(h=100, r=back_hole_dia/2, $fn=40);
  }
}

//MidPlan();
//FrontPlan();
//BackPlan();
//Front();
Hook();
