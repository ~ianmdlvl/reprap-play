// -*- C -*-

whole_depth = 90;
whole_width = 120;

antifoot_width = 15;
antifoot_height = 15;
antifoot_slope = 1.0;
antifoot_depth = 10;
antifoot_base = 12;
antifoot_front = 5;

leg_width = 8;
leg_thick = 8;

post_height = 50;
orifice_dia = 22.1 + 0.3;
post_thick = 8;

stretcher_thick = 5;
stretcher_width = 8;

antifoot_back = antifoot_depth + antifoot_height/antifoot_slope;
post_rad = orifice_dia/2 + post_thick;

module AntiFoot(){
  translate([-antifoot_front-antifoot_back, antifoot_width/2, 0])
    rotate([90,0,0])
    translate([antifoot_front, antifoot_base, 0])
    linear_extrude(height=antifoot_width)
    polygon([[-antifoot_front, -antifoot_base],
	   [-antifoot_front,  antifoot_height],
	   [0,               antifoot_height],
	   [0,               0],
	   [antifoot_depth,  0],
	   [antifoot_back, antifoot_height],
	   [antifoot_back, -antifoot_base]]);
  translate([-antifoot_back, 0, 0])
    cube([stretcher_width, whole_width*0.55, stretcher_width]);
}

module LeftLeg(){
  effective_depth = whole_depth - antifoot_back;
  translate([-effective_depth, -whole_width/2, 0])
    AntiFoot();
  hull(){
    translate([-effective_depth-leg_width/2, -whole_width/2, 0])
      cylinder(r=leg_width/2, h=antifoot_base);
    cylinder(r=leg_width/2, h=post_height);
  }
}

module RightLeg(){
  mirror([0,1,0]) LeftLeg();
}

module Post(){
  cylinder(h=post_height, r=post_rad, $fn=70);
}

module Stand(){
  difference(){
    union(){
      LeftLeg();
      RightLeg();
      Post();
    }
    translate([0,0,-1]) cylinder(h=post_height+2, r=orifice_dia/2);
  }
}

Stand();
