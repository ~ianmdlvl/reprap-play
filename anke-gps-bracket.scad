// -*- C -*-

include <doveclip.scad>

// Dimensions of the main GPS body
outerw = 120;
outerh =  75;
outert =  15;
outerbackbevel = 3;

// Dimensions for the holder
holder_outerw = outerw - 0.0;
holder_outerh = outerh + 0.0;
holder_outert = outert + 0.0;

// Dimensions for the model
model_outerw = outerw + 2.5;
model_outerh = outerh - 0.2;
model_outert = outert - 1.0;

// Dimensions of the bezel area round the edges
bezelw =    11 - 0.5;
bezelboth = 11 - 0.5;
bezeltoph =  7 - 0.5;

// Dimensions of the speaker at the back
spkrdia =  22;
spkr2bot = 19;
spkr2rhs = 25;

// Dimensions of the plug and wire
plugw =      12;
plugh =       9;
plug2bot =   11;
plug2lhs =   11;
plugtotald = 15;
pluggapd =    5;

// Dimensions of the hole in the tray
//   width and height (vertical) at the top
nestleh = 53;
nestlew = 60.9;
//   depths (back to front distance):
nestledl = 40.2;
nestledr = 43.9;
//   differences in width, depth, at bottom:
nestledwl = 2.1;
nestledwr = 1.4;
nestleddf = 4.0;
nestleddbl = 5.7;
nestleddbr = 5.2;

// Adjustment for the GPS attitude and position
gpsazimuth = 45;
gpselevation = 40;
gpsrightwardoffset = 5;
gpsrearwardoffset = 2;
gpsrightwardoffsetonbar = 0;

// Amount of wire protrusion to allow for
plugwiremoreh = 25;

// Slops and steps etc.
plugslop = 0.5;
plughstep = 1.5;
bodylhsrhsslop = 0.5;
holderhgap = 5;
holderbezelmore = 2;
nestlebevel = 1;

// Dimensions for strength only
screent = 1.0;
plugstrutw = 4;
plugstrutt = min(model_outert, 5);
nestledoveclipw = 20;
holderh = model_outerh * 0.5;
holderwallt = 2.5;
holderbackt = 2.8;
holderdccount = 2;
holderdoveclipl = 15;
chassish = 13;
chassist = 13;
nestlefloorh = 4.7;
nestleceilh = 6.0;
nestlewallmin = 10.0;
nestlearchslope = 0.75 * sqrt(0.5);

// Consequential values
holderdcw = DoveClipPairSane_width(holderdccount);

module GpsPlugPlug(slop){
  effhslop = slop - plughstep;
  effplugw = plugw + slop*2;
  effplugh = plugh + effhslop*2;
  translate([plug2lhs-slop, plug2bot-effhslop, -1])
    cube([effplugw, effplugh, model_outert+2]);
}

module GpsBodyOuterBevel(len){
  translate([0,-1,0]) {
    rotate([-90,0,0]) {
      linear_extrude(height=len+2) {
	polygon([[-outerbackbevel, 0],
		 [ 0, outerbackbevel],
		 [outerbackbevel, 0],
		 [ 0, -outerbackbevel]]);
      }
    }
  }
}

module GpsBody() { ////toplevel
  difference(){
    union(){
      difference(){
	cube([model_outerw, model_outerh, model_outert]);
	translate([bezelw, bezelboth, screent])
	  cube([model_outerw-bezelw*2,
		model_outerh-bezelboth-bezeltoph,
		model_outert]);
	translate([model_outerw-spkr2rhs, spkr2bot, -1])
	  cylinder(r=spkrdia/2, h=model_outert+2);
      }
      translate([plug2lhs+plugw/2, plug2bot+plugh/2, 0])
	cylinder(r=(plugw+plugh)/2, h=model_outert);
      for (x=[plug2lhs-plugstrutw, plug2lhs+plugw])
	translate([x, 0.1, 0])
	  cube([plugstrutw, model_outerh-0.2, plugstrutt-0.10]);
    }
    GpsPlugPlug(0);
    for (x=[0,model_outerw]) translate([x,0,0]) GpsBodyOuterBevel(model_outerh);
    for (y=[0,model_outerh]) translate([0,y,0])
      rotate([0,0,-90]) GpsBodyOuterBevel(model_outerw);
  }
}

module GpsPlug() {
  plugwireh = plug2bot + plugwiremoreh;
  translate([-plugslop,0,0]) GpsPlugPlug(-plugslop);
  mirror([0,0,1]) translate([plug2lhs, plug2bot, 0]) {
    cube([plugw, plugh, plugtotald-0.05]);
    translate([0, -plugwireh, pluggapd])
      cube([plugw, plugwireh+0.05, plugtotald-pluggapd]);
  }
}

lhsteethu = 2;

module GpsLHSMask(xslop=0){
  translate([plug2lhs + plugw+plugh+plugstrutw,
	     0,
	     -50]) {
    for (iter=[-100/lhsteethu : 100/lhsteethu]) {
      translate([0, iter*lhsteethu*2, 0]) {
	linear_extrude(height=100) {
	  polygon([[-300,     0],
		   [   0,     0],
		   [lhsteethu,lhsteethu],
		   [   0,     lhsteethu*2],
		   [-300,     lhsteethu*2+0.1]]);
	}
      }
    }
  }
}

module GpsAssembled(){ ////toplevel
  GpsBody();
  GpsPlug();
}

module GpsBodyLT(){
  intersection(){
    GpsBody();
    GpsLHSMask();
  }
}

module GpsBodyRT(){
  difference(){
    GpsBody();
    GpsLHSMask(bodylhsrhsslop);
  }
}

module GpsPlugT(){ ////toplevel
  rotate([0,-90,0]) GpsPlug();
}

module NestleCubeCutout(ca,cb,d){
  dist = cb - ca;
  cuth = -nestleh + nestlefloorh;
  mirror([0,1,0]){
    translate([0,1,0])
    rotate([90,0,0]){
      linear_extrude(height=d+2){
	polygon([[ca+nestlebevel, cuth],
		 [ca, cuth+nestlebevel*2],
		 [ca, -dist/2/nestlearchslope-nestleceilh],
		 [(ca+cb)/2, -nestleceilh],
		 [cb, -dist/2/nestlearchslope-nestleceilh],
		 [cb, cuth+nestlebevel*2],
		 [cb-nestlebevel, cuth]]);
      }
    }
  }
}

module NestleCube(){ ////toplevel
  midw = nestlew/2;
  midd = min(nestledl,nestledr);
  midddb = max(nestleddbl,nestleddbr);

  based0 = nestleddf;
  based1 = midd - midddb;
  basew0 = -nestledwr;
  basew1 = +nestledwl-nestlew;

  echo("wl,wr=", basew1, basew0);
  echo("df,dbl,dbm,dbr",
       based0, nestledl-nestleddbl, based1, nestledr-nestleddbr);

  cutd0 = based0 + nestlewallmin;
  cutd1 = based1 - nestlewallmin;
  cutw0 = basew0 - nestlewallmin;
  cutw1 = basew1 + nestlewallmin;

  bevth = -nestleh + nestlebevel*2;
  bevw = nestlebevel;
  bevd = nestlebevel;

  translate([-(basew0+basew1)/2, -(based0+based1)/2, 0]) {
    difference(){
      polyhedron
	(points=
	 [[          +0      ,            +0,        0], // 0
	  [          +0      ,            +nestledr, 0], // 1
	  [          -midw   ,            +midd,     0], // 2
	  [          -nestlew,            +nestledl, 0], // 3
	  [          -nestlew,            +0,        0], // 4
	  [-nestledwr+0      , +nestleddf +0,        bevth], // 5
	  [-nestledwr+0      , -nestleddbr+nestledr, bevth], // 6
	  [          -midw   , -midddb    +midd,     bevth], // 7
	  [+nestledwl-nestlew, -nestleddbl+nestledl, bevth], // 8
	  [+nestledwl-nestlew, +nestleddf +0,        bevth], // 9
	  [-nestledwr+0      -bevw, +nestleddf +0       +bevd, -nestleh], // 10
	  [-nestledwr+0      -bevw, -nestleddbr+nestledr-bevd, -nestleh], // 11
	  [          -midw        , -midddb    +midd    -bevd, -nestleh], // 12
	  [+nestledwl-nestlew+bevw, -nestleddbl+nestledl-bevd, -nestleh], // 13
	  [+nestledwl-nestlew+bevw, +nestleddf +0       +bevd, -nestleh]], // 14
	 triangles=[// main side panels
		    [0,1,6],[6,5,0],
		    [1,2,7],[7,6,1],
		    [2,3,8],[8,7,2],
		    [3,4,9],[9,8,3],
		    [4,0,5],[5,9,4],
		    // bevels
		    [6,7,12],[12,11,6],
		    [7,8,13],[13,12,7],
		    [8,9,14],[14,13,8],
		    [9,5,10],[10,14,9],
		    [5,6,11],[11,10,5],
		    // top and bottom
		    [4,3,2],[2,1,0],[0,4,2],
		    [12,13,14],[10,11,12],[12,14,10]],
	 convexity=3);
      union(){
	#NestleCubeCutout(cutw1, cutw0, max(nestledl,nestledr));
	#rotate([0,0,90]) NestleCubeCutout(cutd0, cutd1, nestlew);
      }
    }
  }

  translate([gpsrightwardoffset,-gpsrearwardoffset,0])
    rotate([0,0,90+gpsazimuth])
    translate([nestledoveclipw/2,0,DoveClip_depth()-0.5])
    rotate([0,-90,0])
    DoveClipPairSane(count=3, h=nestledoveclipw);
}

module NestleCubeBaseTest(){ ////toplevel
  intersection(){
    translate([0,0,nestleh]) NestleCube();
    translate([-100,-100,0]) cube([200,200,nestlebevel*5]);
  }
  cube([5,5,10]);
}

module NestleCubeCeilTest(){ ////toplevel
  intersection(){
    translate([0,0,3]) NestleCube();
    translate([-100,-100,0]) cube([200,200,5.5]);
  }
  cube([5,5,10]);
}

module NestleCubePin(){ ////toplevel
  DoveClipPin(nestledoveclipw*0.4);
}

module HolderSideL(){ ////toplevel
  minz = -(bezelw - holderbezelmore) - holderbackt;
  holdert = holder_outert + holderwallt*2;
  cylr = 0.5*sqrt(holderdcw*holderdcw + holderdoveclipl*holderdoveclipl);
  difference(){
    translate([-holderh,
	       -holderwallt,
	       minz]) {
      cube([holderh + holderhgap + cylr,
	    holdert,
	    -minz]);
      translate([holderh + holderhgap + cylr, holdert/2, 0]) {
	cylinder(r=cylr, h=-minz);
	rotate([0,0,gpselevation])
	  translate([0, -holderdoveclipl/2, -minz + DoveClip_depth()])
	  rotate([0,-90,-90])
	  DoveClipPairSane(count=holderdccount, h=holderdoveclipl);
      }
    }
    translate([-holderh-1,
	       0,
	       minz + holderbackt])
      cube([holderh+1,
	    holder_outert,
	    bezelw]);
  }
}

module HolderSideR(){ ////toplevel
  mirror([0,1,0]) HolderSideL();
}

module ChassisBar(){ ////toplevel
  dist = holder_outerw - 2*((bezelw - holderbezelmore) + DoveClip_depth());
  cliph = holderdcw;
  for (mir=[0,1]) {
    mirror([mir,0,0]) {
      translate([dist/2, cliph/2, 0])
	DoveClipPairSane(h=holderdoveclipl, count=holderdccount);
      translate([-1, 0, 0])
	cube([dist/2 - DoveClip_depth() + 1.1, chassish, chassist]);
    }
  }
  translate([-gpsrightwardoffsetonbar, -DoveClip_depth(), 0])
    rotate([0,0,-90])
    DoveClipPairSane(h=nestledoveclipw, count=3,
		     baseextend=chassist/2);
}

module HolderSidePin(){ ////toplevel
  DoveClipPin(holderdoveclipl*0.5);
}

module Pins(){ ///toplevel
  for (i=[1:4*holderdccount]) {
    translate([i*10, 0, 0]) HolderSidePin();
  }
  for (i=[1:6]) {
    translate([i*10, 20, 0]) NestleCubePin();
  }
}

//GpsPlugT();
//GpsAssembled();
//GpsBody();
//NestleCube();
//NestleCubeBaseTest();
//NestleCubeCeilTest();
//NestleCubePin();
//HolderSideL();
//HolderSideR();
//HolderSidePin();
//ChassisBar();
//Pins();
