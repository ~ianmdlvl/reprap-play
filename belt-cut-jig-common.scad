// -*- C -*-

jig_max_len = 160; // print diagonally
//jig_max_len = 30;

registrationgroove_width = 0.8;
registrationgroove_depth = 1.2;

registrationprotrusion_poke = 3;
registrationprotrusion_slope = 0.75;

jig_overlap = 1;

jig_ends_extra = 2;

jig_iters = floor((jig_max_len - jig_ends_extra) / jig_interval);
//jig_iters=2;
echo(jig_iters);

module RegistrationGroove(l){
  // runs along the +ve X axis for length l but at correct z pos
  translate([0, 0, jig_main_zsz + 0.1]) {
    rotate([90,0,90])
      linear_extrude(height=l)
      polygon([[-registrationgroove_width/2, 0],
	       [ +registrationgroove_width/2, 0],
	       [ 0, -registrationgroove_depth ]]);
  }
}

module OneJig(){
  difference(){
    translate([-(jig_interval/2 + jig_overlap),
	       jig_min_y,
	       -strap_thick])
      cube([jig_interval + 2,
	    jig_max_y - jig_min_y,
	    jig_main_zsz + strap_thick]);
    OneJigCutout();
    translate([-100, -strap_width/2, -10])
      cube([200, strap_width, 10]);
   translate([-100,0,0])
     RegistrationGroove(200);
   for (xfrac=[-1/4,0,+1/4])
     translate([jig_interval * xfrac, -100, 0])
       rotate([0,0,90])
       RegistrationGroove(200);
  }
}

module RegistrationProtrusion(){
  // points towards the positive x axis
  xsz = registrationprotrusion_poke;
  ysz = registrationprotrusion_poke;
  diag_sz = xsz * sqrt(2);
  zsz = diag_sz / registrationprotrusion_slope;
  hull(){
    translate([0, 0, 0.1]){
      linear_extrude(height=0.1)
	polygon([[   0, -ysz ],
		 [ xsz,    0 ],
		 [   0,  ysz ]]);
      translate([-0.1, 0, zsz ])
	rotate([0,0,45])
	cube(0.1);
    }
  }
}

module Jig(){
  for(end=[0,1]){
    for(yfrac=[-1/2, 0, 1/2]){
      translate([ end
		  ? jig_interval * (jig_iters - 0.5)
		  : -jig_interval/2,
		 yfrac * strap_width,
		 0])
	rotate([0,0, end ? 0 : 180])
	translate([ jig_overlap, 0, 0 ])
	RegistrationProtrusion();
    }
  }
  for (i=[0:jig_iters-1]) {
    translate([jig_interval * i, 0, 0])
      OneJig();
  }
}

module JigPrint(){
  rotate([0,0,-45])
    translate([0,0,jig_main_zsz])
    rotate([180,0,0])
    Jig();
}

