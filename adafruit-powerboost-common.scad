// -*- C -*-

include <nutbox.scad>
include <utils.scad>

psu_sz  = psu_sz_nom + [ 0.11, 0.44 ] + [ 0.25, 0.25 ];

psu_hole_pos = [ 2.05, // from back edge of psu_sz[0]
		 0.55 * 0.5 * 25.4, // from centreline
		 ];

psu_th = 1.70 + 0.25;
psu_th_for_clamp = 1.50;

psu_hole_dia = 2.5 - 0.5;
psu_connector_z = 2.9 + 0.1;
psu_connector_z_overlap = 0.15;
psu_connector_depth = 6.25;
psu_connector_w = 8.0 + 0.5;
psu_usb_protr = 0.6;

psu_clamp_th = 4.0 + 0.75;
psu_clamp_w = 8.0;
psu_clamp_gap = 0.4;

psu_board_clamp_ovlp = 4.5;
psu_board_nutbox = nutbox_data_M3;

psu_board_gap = 0.5;
psu_board_support_wall = 2;
psu_board_support_ovlp = 4.5;
psu_board_support_ovlp_ceil = 2;
psu_board_support_z = 2;

psu_baffle_gap = 1.0 + 0.5;

psu_y = +psu_sz[1]/2 + psu_usb_protr;

psu_usba_v_apart = 7.0;
psu_usba_v_from_edge = 4.86;
psu_usba_v_space_below = 1.5;
psu_usba_v_space_w = 1.7;
psu_usba_v_space_l = 3.0;

psu_test_ceil = 2.5;

// ----- calculated -----

psu_z = NutBox_h_base(psu_board_nutbox);
psu_z_down = psu_z + 0.1;
psu_fix_sz = NutBox_outer_size(psu_board_nutbox);
psu_board_nutbox_y = psu_sz[1]/2 + psu_board_nutbox[0]/2;

psu_mount_outer_sz_x = psu_sz[0] + psu_board_support_wall * 2; // centred
psu_mount_outer_sz_y = psu_y + max(psu_board_support_wall, // at psu_y
				   psu_board_nutbox_y + psu_fix_sz/2);

module PsuBoardRepresentation(){
  linear_extrude(height= psu_th)
    square(center=true, [psu_sz[0],psu_sz[1]]);
}

module PsuRepresentation(){
  PsuBoardRepresentation();
  translate([0, -psu_sz[1]/2, -psu_connector_z])
    linear_extrude(height= psu_connector_z + psu_connector_z_overlap)
    rectfromto([ -psu_connector_w/2, -10 ],
	       [ +psu_connector_w/2, psu_connector_depth ]);
}

module AtPsuMountCorner(mx,my){
  mirror([mx,0,0])
    mirror([0,my,0])
      translate(-0.5 * [psu_sz[0], psu_sz[1], 0]
		-1 * [0,0, psu_z_down])
	children();
}

module PsuMountCornerExtrude(mx,my, plus_z=psu_board_support_z){
  AtPsuMountCorner(mx,my){
    linear_extrude(height= psu_z_down + plus_z, convexity=10) {
      children();
    }
  }
}

module PsuUsbAVSpacePlan(){
  for (x= [-1,+1] * psu_usba_v_apart/2) {
    translate([x, -psu_usba_v_from_edge ]) {
      hull(){
	for (y= [-1,+1] * 0.5 * (psu_usba_v_space_l - psu_usba_v_space_w)) {
	  translate([0,y])
	    circle(r= psu_usba_v_space_w);
	}
      }
    }
  }
}

module PsuMountPositiveMain(){
  for (mx=[0,1]) {
    for (my=[0,1]) {
      PsuMountCornerExtrude(mx,my){
	rectfromto(-[1,1]*psu_board_support_wall,
		   +[1,1]*psu_board_support_ovlp);
      }
    }
    // mount above at plug end
    PsuMountCornerExtrude(mx,0, psu_th + psu_board_support_wall){
      rectfromto(-[1,1]*psu_board_support_wall,
		 [psu_board_support_ovlp,
		  psu_board_support_ovlp_ceil]);
    }
  }
  translate([0,0, -psu_z_down])
    linear_extrude(psu_z_down - psu_baffle_gap, convexity=10)
      PsuLedBafflePlan();
}

module PsuMountNegative(){
  axis = [0, -psu_sz[1]/2, psu_th];
  PsuRepresentation();
  translate(axis)
    rotate([atan(2 * psu_board_support_z / psu_sz[1]),
	    0,0])
    translate(-axis)
    PsuBoardRepresentation();
}

module PsuMountPositive(){
  difference(){
    intersection(){
      PsuMountPositiveMain();
      linextr_y_xz(-psu_y, psu_sz[1]*2) square(100, center=true);
    }
    PsuMountNegative();
    intersection(){
      hull(){
	PsuBoardRepresentation();
	translate([0,0,5]) PsuBoardRepresentation();
      }
      translate([-20,0,-20]) cube(40);
    }
  }
  for (mx=[0,1]) {
    PsuMountCornerExtrude(mx,1){
      translate([psu_sz[0]/2 - psu_hole_pos[1],
		 psu_hole_pos[0]]
		+ psu_board_gap * [1,1] )
	circle(r= psu_hole_dia/2);
    }
  }
  difference(){
    translate([0, psu_board_nutbox_y, 0])
      rotate([0,0,180])
      NutBox(psu_board_nutbox, psu_z_down);
    translate([0, psu_sz[1]/2, 0])
      linextr(-psu_usba_v_space_below, +10)
      PsuUsbAVSpacePlan();
  }
}

module PsuClamp(){ ////toplevel
  rotate([180,0,0]) difference(){
    linear_extrude(height=psu_clamp_th + psu_th_for_clamp, convexity=5) {
      difference(){
	hull(){
	  circle(r = psu_fix_sz/2);
	  translate([ -psu_board_nutbox[0]/2, 0])
	    square(center=true, [ psu_board_clamp_ovlp*2, psu_clamp_w ]);
	}
	circle(r = psu_board_nutbox[0]/2);
      }
    }
    translate([0,0,-1]) linear_extrude(height=psu_th_for_clamp+1) {
      translate([ -psu_board_nutbox[0]/2 + psu_clamp_gap, 0 ])
	mirror([1,0])
	translate([0,-20]) square(40);
    }
    linextr(-10,10) {
      rotate(-90)
	translate([0, -psu_board_nutbox[0]/2])
	PsuUsbAVSpacePlan();
    }
  }
}

module PsuLedWindowsPlan(){
  difference(){
    PsuLedWindowsPlanCore();
    PsuLedBafflePlan();
  }
}

module PsuLedWindowsWindows(ceil){
  translate([0,0, -psu_z - ceil])
    linextr(0, psu_initial_layer_thick)
    offset(delta=psu_window_ledge)
    PsuLedWindowsPlan();
}

module PsuFirstLayerNegative(ceil){
  translate([0, 0, -psu_z - ceil])
    linextr(-1, psu_initial_layer_thick)
    children();
}

module PsuMountWindowsNegative(ceil){
  linextr(-10, 0.1)
    PsuLedWindowsPlan();
  PsuFirstLayerNegative(ceil)
    offset(delta= psu_window_ledge + psu_multicolour_gap)
    PsuLedWindowsPlan();
}

module PsuLedLegendsNegative(ceil){
  PsuFirstLayerNegative(ceil)
    offset(delta= psu_multicolour_gap)
    PsuLedLegendsPlan();
}

module PsuMountDemo() { ////toplevel
  ceil = psu_test_ceil;

  translate([0, psu_y, psu_z]) {
    difference(){
      PsuMountPositive();
      linextr(-20, 0.1)
	PsuLedWindowsPlan();
    }
    %PsuMountNegative();

    color("yellow") translate([0,0, -psu_z - ceil])
      linear_extrude(height=0.4, convexity=10)
      PsuLedWindowsPlan();

    color("blue") translate([0,0, -psu_z - ceil])
      linear_extrude(height=0.4, convexity=10)
      PsuLedLegendsPlan();

    translate([0, psu_board_nutbox_y, 10])
      rotate([180,0,0])
      rotate([0,0,-90])
      PsuClamp();
  }
}

module PsuMountTest() { ////toplevel
  ceil = psu_test_ceil;
  $fs = 0.1;
  $fa = 3;
  difference(){
    union(){
      translate([0, psu_y, psu_z])
	PsuMountPositive();
      difference(){

	// rectangular box with wall
	linextr_x_yz(-psu_mount_outer_sz_x/2,
		     +psu_mount_outer_sz_x/2) {
	  difference(){
	    rectfromto([0, -ceil],
		       [psu_mount_outer_sz_y, psu_z + 10]);
	    rectfromto([ceil,0], 400*[1,1]);
	  }
	}

	translate([0, psu_y, psu_z]) {
	  PsuMountNegative();
	}
      }
    }
    translate([0, psu_y, psu_z]) {
      PsuMountWindowsNegative(ceil);
      PsuLedLegendsNegative(ceil);
    }
  }
}

psu_multicolour_gap = 0.075;
psu_initial_layer_thick = 0.400;
psu_initial_layer_width = 0.750;
psu_window_ledge = 0.50; // each side

psu_frame_gap = 1.0;

module PsuMountLayerFrame(bl, tr, ix) {
  gap0 = [1,1] * (psu_frame_gap + psu_initial_layer_width*(ix+0));
  gap1 = [1,1] * (psu_frame_gap + psu_initial_layer_width*(ix+1));
  linextr(0, psu_initial_layer_thick) {
    difference(){
      rectfromto(bl-gap1, tr+gap1);
      rectfromto(bl-gap0, tr+gap0);
    }
  }
}

module PsuMountTestFullLayerFrame(ix) {
  PsuMountLayerFrame([-0.5 * psu_mount_outer_sz_x, 0],
		     [+0.5 * psu_mount_outer_sz_x,
		      psu_mount_outer_sz_y],
		     ix);
}

module PsuMountTestFullMain() { ////toplevel
  ceil = psu_test_ceil;

  PsuMountTestFullLayerFrame(2);
  
  difference(){
    translate([0,0, ceil])
      PsuMountTest();
  }
}

module PsuMountTestFullOneLayer(ix) {
  PsuMountTestFullLayerFrame(ix);
  linextr(0, psu_initial_layer_thick) {
    translate([0, psu_y]) children();
  }
}

module PsuMountTestFullText() { ////toplevel
  PsuMountTestFullOneLayer(0)
    PsuLedLegendsPlan();
}
module PsuMountTestFullWindows() { ////toplevel
  PsuMountTestFullLayerFrame(1);
  translate([0, psu_y, psu_z + psu_test_ceil])
    PsuLedWindowsWindows(psu_test_ceil);
}

module PsuMountTestFullDemo() { ////toplevel
  color("blue") PsuMountTestFullMain();
  color("yellow") PsuMountTestFullText();
  %PsuMountTestFullWindows();
}
