// -*- C -*-
//
// For holding the spring while reassembling a candle holder.

include <utils.scad>

spring_body_w = 5.0;
spring_body_l = 6.0;
axle_dia = 2.0;
recess_d = 13.0;
total_len = 45.0;

th_y = 1.5;
th_x = 2;
handle_th = 2.5;

// calculated

outer_sz = [spring_body_l + th_x*2, spring_body_w + th_y*2];
handle_sz = [outer_sz[0], handle_th];
th_z = th_x;

echo(outer_sz);

module OuterElevation(){
  square(center=true, outer_sz);
}

module Elevation(){
  difference(){
    OuterElevation();

    square(center=true, [spring_body_l, spring_body_w]);
    square(center=true, [outer_sz[0] + 10, axle_dia]);
  }
}

module Clip(){
  linextr(-th_z, recess_d) Elevation();
  linextr(-th_z, 0) OuterElevation();
  linextr(recess_d - total_len, 0) square(center=true, handle_sz);
}

module Print(){
  rotate([0, 90, 0]) Clip();
}

Print();
