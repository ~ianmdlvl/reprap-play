// -*- C -*-

//$fs=0.1;
//$fa=3;
$fs=0.2;
$fa=6;

rearwallthick = 3;
basethick = 2;
mainframeendthick = 2.5;

tubedia = 16 + 0.8;
tubetubethick=2;
tubetubetopslop=1;

boltholedia = 6.5 + 0.5;
boltholeslotshorter = 6;
mainframeholedia = 5 + 1.0;

// "slot" refers to the things in the base of the drill press stand
backslotedgespace = 59;
slotwidth = 11.5 - 0.5;
backslotmid2screwhole = 17;
slotplugheight = 5.5;
slotplugshorterlen =10;
slotpluglongerlen = 20;

//slotslope = 11.0 / 18.5;
slotslope = 1 / tan(51);

// "keepslot" refers to the screws in the wooden jig block
keepslotstartz = 20;
keepslotlen = 18;
keepslotx = backslotedgespace / 2;
keepslotwidth = 4;

mainframeextraside = 12;
mainframeextrafront = 15;

rearwallstrengthwidth = 10;
keepslotclear = 10;

// computed values

slotslopediag = sqrt(1 + slotslope*slotslope);
slotwidthx = slotwidth * slotslopediag;

slotxperlen = slotslope / slotslopediag;
slotyperlen =         1 / slotslopediag;

mainframeholex = backslotedgespace/2 + slotpluglongerlen * slotxperlen
  + 0.5 * slotwidth * slotyperlen;

mainframeholey = -slotpluglongerlen * slotyperlen
  + 0.5 * slotwidth * slotxperlen;

mainframemaxx = mainframeholex + mainframeextraside;
mainframeminy = mainframeholey - mainframeextrafront;
mainframemaxz = keepslotstartz + keepslotlen;

module MainFrame(){
  for (m=[0,1]) {
    mirror([m,0,0]) {
      translate([-1, mainframeminy, 0])
	cube([mainframemaxx+1, -mainframeminy, basethick]);
      translate([-1, -rearwallthick, 0])
	cube([mainframemaxx+1, rearwallthick, mainframemaxz]);

      for (x=[keepslotx - keepslotclear, mainframemaxx - 0.5]) {
	translate([x,0,0])
	  rotate([90,0,-90])
	  linear_extrude(height=mainframeendthick)
	  polygon([[-mainframeminy, 0],
		   [0, mainframemaxz],
		   [0, 0]]);
      }

      translate([backslotedgespace/2, 0, 1])
	mirror([0,0,1])
	linear_extrude(height=slotplugheight+1)
	polygon([[0,0],
		 [slotwidthx, 0],
		 [slotwidthx + slotplugshorterlen * slotxperlen,
		  -slotplugshorterlen * slotyperlen],
		 [slotpluglongerlen * slotxperlen,
		  -slotpluglongerlen * slotyperlen]]);
      translate([-1,
		 -rearwallthick - boltholeslotshorter + 0.2,
		 tubedia + tubetubetopslop + tubetubethick + 4])
	cube([keepslotx - keepslotclear + 1,
	      boltholeslotshorter + 0.5,
	      rearwallstrengthwidth]);
    }
  }
}

module TubeThing(extralen, dia, extraheight, underheight){
  effheight = tubetubetopslop + extraheight + underheight;
  len = -mainframeminy + extralen * 2;
  translate([0, mainframeminy - extralen, -underheight]) {
    translate([0,0, dia/2 + effheight])
      rotate([-90,0,0]) cylinder(h=len, r=dia/2);
    translate([-dia/2, 0, 0])
      cube([dia, len, effheight + dia/2]);
  }
}

module Jig(){
  difference(){
    union(){
      MainFrame();
      TubeThing(0, tubedia+tubetubethick*2, -tubetubethick, 0);
    }
    union(){
      translate([0,0,-0.1])
	TubeThing(10, tubedia, 0, 5);
      translate([-boltholedia/2, mainframeminy - 1, -5])
	cube([boltholedia,
	      -mainframeminy + 1 - rearwallthick - boltholeslotshorter,
	      mainframemaxz + 10]);
      for (m=[0,1]) {
	mirror([m,0,0]) {
	  translate([mainframeholex, mainframeholey, -30])
	    cylinder(h=basethick+40, r=mainframeholedia/2);
	  translate([keepslotx - keepslotwidth/2,
		     -10, keepslotstartz])
	    cube([keepslotwidth, 20, keepslotlen + 10]);
	}
      }
    }
  }
}

//MainFrame();
//TubeThing(0, tubedia);

rotate([-90,0,0])
  Jig();
