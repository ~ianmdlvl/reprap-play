// -*- C -*-

include <threads.scad>

inch = 25.4;

negative_dia = inch * 1/4. + 0.375;
negative_default_l =   10.0;

negative_tpi = 20;
negative_pitch = inch/negative_tpi;
negative_chamfer = negative_pitch/2;

module CameraMountThread(l){
  rotate([0,180,0])
    english_thread(diameter=negative_dia/inch,
		   threads_per_inch=negative_tpi,
		   leadin=0, internal=true, test=$test,
		   length= (l + inch/19) / inch);
  hull(){
    translate([0,0, negative_chamfer])
      cylinder(r= negative_dia/2 + negative_chamfer*2,
	       h=1);
    mirror([0,0,1])
      cylinder(r= negative_dia/2 - negative_chamfer*2,
		   h= negative_chamfer*3);
  }
}
