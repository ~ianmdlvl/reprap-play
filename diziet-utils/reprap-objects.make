# reprap-objects Makefile, reuseable parts
#
# Build scripts for various 3D designs
#
# Copyright 2012-2023 Ian Jackson
#
# This work is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This work is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this work.  If not, see <http://www.gnu.org/licenses/>.


M4=m4

CWD := $(shell pwd)
DUTILS ?= $(CWD)/diziet-utils
PLAY ?= $(CWD) # deprecated, for compatibility only

AUTO_TOPLEVELS := $(foreach m,$(USING_AUTOS),$(shell $(DUTILS)/toplevel-find $m))

AUTO_INCS += funcs.scad

default:	autoincs scads

$(shell set -xe; $(DUTILS)/commitid.scad.pl >commitid.scad.tmp; cmp commitid.scad.tmp commitid.scad || mv -f commitid.scad.tmp commitid.scad )

autoincs:	$(AUTO_INCS) $(AUTO_STLS_INCS)
scads:		$(addsuffix .auto.scad, $(AUTO_TOPLEVELS))
stls:		$(addsuffix .auto.stl, $(AUTO_TOPLEVELS))

%.auto.scads: %.scad
	$(MAKE) $(addsuffix .auto.scad, $(shell $(DUTILS)/toplevel-find $*))
%.auto.stls:
	$(MAKE) $(addsuffix .auto.stl, $(shell $(DUTILS)/toplevel-find $*))

i=mv -f $@.tmp $@
o= >$@.tmp && $i

-include .*.d

%.stl:		%.scad $(AUTO_INCS)
		openscad -d .$@.d.tmp -o $*.tmp.stl $<
		@rm -f $@
		@sed -e 's/\.tmp\.stl:/.stl:/' <.$@.d.tmp >.$@.d
		@rm .$@.d.tmp
	        mv -f $*.tmp.stl $@

AUTOBASE=$(shell echo $(1) | perl -pe 's/,\w+\.auto$$//')

%:		%.cpp
		cpp -nostdinc -P <$< $o

funcs.scad:	diziet-utils/funcs.scad.cpp
		cpp -nostdinc -P $< $o

%.gcode:	manual-gcode-generator %.m-g
		$(CWD)/$^ $o

%.dxf:		%.eps
		pstoedit -dt -f "dxf: -polyaslines -mm" $< $@

%:		%.pl
		./$< $o

%:		%.m4
		$(M4) -P >$@.tmp $< && $i

.PRECIOUS: %.auto.scad
%.auto.scad: $(DUTILS)/toplevel-make Makefile $(DUTILS)/toplevel-find
		@echo ' write $@'
		$< $@ >$@.tmp
	        @$i

.PRECIOUS:	%.stl %.gcode %.eps %.dxf

clean:
		rm -f *~ *.stl *.auto.scad *.gcode .*.d $(AUTO_INCS)

