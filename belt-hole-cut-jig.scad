// -*- C -*-

strap_thick = 3;
strap_width = 26.75 + 0.7;

jig_interval = 20;

edgewall_width = 3;

jig_ywidth = 17;

jig_min_y = -jig_ywidth;
jig_max_y = +jig_ywidth;


jig_main_zsz = 28;
punch_dia = 12.75;

punch_slop = 0.5;

// common stuff

include <belt-cut-jig-common.scad>

module OneJigCutout(){
  translate([0,0,-10])
  cylinder(r= punch_dia/2 + punch_slop, h=100, $fn=50);
}

module Demo(){ ////toplevel
  Jig();
}

module JigT(){ ////toplevel
  JigPrint();
}

JigT();
