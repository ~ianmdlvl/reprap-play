// -*- C -*-

dy= 145;
dx=  65;

h1= 8;
h2= 14;
ratio = 0.8;

module Plan(){
  polygon([[ -dx/2,  0    ],
	   [     0,  dy/2 ],
	   [  dx/2,  0    ],
	   [     0, -dy/2 ]]);
}

module Solid(){
  rotate([0,0, -45]) {
    hull(){
      linear_extrude(height= h1) {
	Plan();
      }
      linear_extrude(height= h2) {
	scale(ratio) Plan();
      }
    }
  }
}

Solid();
