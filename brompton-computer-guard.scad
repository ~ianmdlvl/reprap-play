// -*- C -*-

arch_height = 18;
arch_width = 75;
end_width = 25;

arch_thick = 4;

arch_breadth = 25;

hole_dia = 4 + 0.5;

pbase_tab = 12;
pbase_thick = 4;
inner_pbase_thick = 12;
inner_pbase_rad_mul = 3;

// computed

arch_alpha = atan(arch_height / (arch_width/2));
arch_beta = 2*arch_alpha;
echo(arch_alpha,arch_beta);
arch_in_rad = arch_width/2 / sin(arch_beta);
arch_to_chord = arch_in_rad * cos(arch_beta);

echo(inner_pbase_thick);

inner_pbase_rad = arch_in_rad * inner_pbase_rad_mul;

end_thick = arch_thick;

holes = [[[  5  , 5  ], [16  , 21]], // left
	 [[ 18.5, 4.5], [ 4.5, 21]]]; // right

module ArchCircle(rad){
  translate([0,-arch_to_chord])
    circle(rad, $fa=0.1);
}

module ArchProfile(pbase){
  intersection(){
    translate([-200,0])
      square([400,200]);
    difference(){
      union(){
	ArchCircle(arch_in_rad + arch_thick);
	for (m=[0,1])
	  mirror([m,0])
	    translate([arch_width/2,0])
	    multmatrix([[1,pbase ? -0.75 : 0,0,0],
			[0,1,0,0],
			[0,0,1,0],
			[0,0,0,1]])
	    square([end_width, pbase ? pbase_tab : end_thick]);
      }
    }
  }
}

module Holes(){
  for (m=[0,1]) {
    mirror([1-m,0])
      translate([arch_width/2, 50, 0])
      rotate([90,0,0])
      for (h=holes[m]) {
	translate(h)
	  cylinder(r=hole_dia/2, h=100, $fn=20);
      }
  }
}

module MainCutout(){
  ArchCircle(arch_in_rad);
}

module Arch(){
  difference(){
    rotate([0,0,180]){
      linear_extrude(height=arch_breadth) {
	difference(){
	  ArchProfile(false);
	  MainCutout();
	}
      }
      difference(){
	translate([0,0, arch_breadth - pbase_thick])
	linear_extrude(height=pbase_thick){
	  difference(){
	    hull(){
	      ArchProfile(true);
	      ArchProfile(false);
	    }
	    intersection(){
	      MainCutout();
	      translate([0, -inner_pbase_thick
			 - (inner_pbase_rad - arch_in_rad)])
		ArchCircle(inner_pbase_rad);
	    }
	  }
	}
      }
    }
    Holes();
  }
}

rotate([0,0,45]) translate([0,0,arch_breadth]) rotate([0,180,0]) Arch();
