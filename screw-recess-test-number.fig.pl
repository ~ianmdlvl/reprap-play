#!/usr/bin/perl

my $number = shift @ARGV;
die unless $number =~ m/^\d+$/;

my $fontsz = $number * 6 + 12;

print <<END or die $!;
#FIG 3.2  Produced by xfig version 3.2.5b
Landscape
Center
Metric
A4      
100.00
Single
-2
1200 2
4 0 0 50 -1 18 $fontsz 0.0000 4 285 225 3600 4500 $number\001
END
