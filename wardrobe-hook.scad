// -*- C -*-

include <funcs.scad>
include <utils.scad>

tubeslop = 0.5;
tubeheight = 30 + tubeslop;
tubewidth = 15 + tubeslop;
mainthick = 4;

clipthick = 2;
clipang = 135;

stemlen = 40;

topwidth = 20;

hookinrad = 7.5;
hookcurl = 60;
hookwidth = 4;

tuberad = tubewidth/2;
bend = atan(tuberad/stemlen);
mainoutrad = tuberad + mainthick;
hookoutrad = hookinrad + hookwidth;
hookcy = -(stemlen - hookoutrad);

eltop = [topwidth/2, -tuberad + tubeheight + mainthick + 0.1];
elmid = [topwidth/2, -tuberad];
ellow = tangent_intersect_b([0,hookcy], hookinrad, elmid);
ellowextra = 180 - tangent_intersect_beta([0,hookcy], hookinrad, elmid);

module ClipPlan(qbend, qstemleny){
  dy = tubeheight - tuberad*2;
  FArcSegment(0, dy, tuberad, mainoutrad, -1, 181);
  FArcSegment(0, 0,  tuberad, mainoutrad, -qbend, qbend+1);
  translate([tuberad, 0]) square(center=false, size=[mainthick,dy]);
  FArcSegment(0, 0, tuberad, tuberad + clipthick, 360-clipang, clipang+1);
  rotate(-qbend) translate([tuberad, 0]) mirror([0,1])
    square(center=false, size=[mainthick, qstemleny/cos(qbend)]);
}

module Plan(){
  ClipPlan(bend,stemlen);
}

module ElevationCore(){
  FArcSegment(0, hookcy, hookinrad, hookoutrad,
	      180 - ellowextra,
	      90 + hookcurl + ellowextra);
  translate([-hookoutrad*sqrt(0.5),
	      hookcy - hookoutrad*sqrt(0.5) + 0.1])
    mirror([1,0])
    square(center=false, size=[topwidth, stemlen + tubeheight + 20]);
  polygon([[-hookoutrad, ellow[1]],
	   reflect_in_y(eltop),
	   eltop,
	   elmid,
	   ellow]);
}

// after here is all 3D

module Primary(){
  intersection(){
    translate([0,0, -(topwidth+10)/2])
      linear_extrude(height=topwidth+10) Plan();
    translate([50,0])
      rotate([0,-90,0])
      linear_extrude(height=100)
      ElevationCore();
  }
}

module PlaneAbove(){
  translate([-100,-100,0]) cube(center=false,size=[200,200,200]);
}

taperangle = -270 + tangent_intersect_beta([-hookcy, 0],
					  hookoutrad,
					  [-eltop[1], -eltop[0]]);
module HookL(){ ////toplevel
  difference(){
    rotate([taperangle,0,0])
      translate([0,-eltop[1],0])
      Primary();
    translate([0,0,topwidth/2])
      rotate([taperangle*2,0,0])
      PlaneAbove();
    translate([0,0,-topwidth/2])
      mirror([0,0,1]) PlaneAbove(0);
  }
}

// straight-on version, everything prefixed with s or S

shookcy = -(stemlen-hookoutrad);
sstemleny = -shookcy;
sbend_raw = tangents_intersect_beta([0,0],tuberad,
				    [0,shookcy],hookinrad);
sbend = angle_map_range(360-sbend_raw, -180);

module SPlan(){
  ClipPlan(sbend, sstemleny);
  FArcSegment(0,shookcy, hookinrad,hookoutrad,
	      270 - hookcurl,
	      hookcurl + 90 - sbend);
}

module SElevation(){
  boty = shookcy - hookoutrad - 1;
  polygon([[-1,         tubeheight],
	   [topwidth,   tubeheight],
	   [topwidth,   elmid[1]],
	   [hookwidth,  shookcy],
	   [hookwidth,  boty],
	   [-1,         boty]]);
}

module SElevationPlaced(){
  rotate([0,-90,0]) translate([0,0,-100]) linear_extrude(height=200)
    SElevation();
}

module SHookL(){ ///toplevel
  intersection(){
    linear_extrude(height=topwidth) SPlan();
    SElevationPlaced();
  }
}

// straight-on version, reversed, everything prefixed with t or T

tjoinrad = mainoutrad * 0.7;
tstem0leny = tuberad - tjoinrad*0.5;
tjoinoutrad = tjoinrad + mainthick;

thookcy = shookcy;

tjoin0c = [tuberad - tjoinrad, -tstem0leny];
tjoin1c = [0,                      thookcy];

tbend_raw = tangents_intersect_beta(tjoin0c, tjoinrad,
				    tjoin1c, -hookoutrad);
tbend0 = angle_map_range(tbend_raw,       0);
tbend1 = angle_map_range(tbend_raw + 180, -180);

tbend0p = circle_point(tjoin0c, tjoinrad, tbend_raw);
tbend1p = circle_point(tjoin1c, -hookoutrad, tbend_raw);

module TPlan(){
  ClipPlan(0, tstem0leny);
  FArcSegment(tjoin0c[0],tjoin0c[1], tjoinrad,tjoinoutrad,
	      tbend0, 360-tbend0);
  FArcSegment(0,shookcy, hookinrad,hookoutrad,
	      tbend1, 270+hookcurl - tbend1);
  translate(tbend0p) {
    rotate(tbend_raw+180) mirror([1,0]) {
      translate([0,-0.1]) square(size=[mainthick, dist2d(tbend0p,tbend1p)+0.2]);
    }
  }
}

module THookR(){ ///toplevel
  intersection(){
    linear_extrude(height=topwidth) TPlan();
    SElevationPlaced();
  }
}

// other toplevels etc.

module HookR(){ ////toplevel
  mirror([1,0,0]) HookL();
}

module SHookR(){ ////toplevel
  mirror([1,0,0]) SHookL();
}

module THookL(){ ////toplevel
  mirror([1,0,0]) THookR();
}

module Demo(){ ////toplevel
  translate([-30,tubeheight,0]) HookL();
  translate([  0,tubeheight,0]) HookR();
  translate([ 30,         0,0]) SHookL();
  translate([ 60,         0,0]) SHookR();
  translate([ 90,         0,0]) THookL();
  translate([120,         0,0]) THookR();
}
