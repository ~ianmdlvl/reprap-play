len	= 54.3 - 1;
bigdep	= 10.7 - 1;
smalldep=  4.9 - 1;
tallw1	=  3.5 - 1.5;
tallw2	=  3.5 - 1.5; // ish
totalw	= 11.6 - 2;

gapw = totalw - tallw1 - tallw2;

difference() {
  cube([len, totalw, bigdep]);
  translate([-1, tallw2, smalldep])
    cube([len+2, gapw, bigdep - smalldep + 1]);
}
