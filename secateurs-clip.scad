// -*- C -*-

l = 47;
d = 18;

w = 15;
t = 10;
te = 7;

linear_extrude(height=15, convexity=4) {
  for (m=[0,1]) {
    mirror([m,0]) {
      polygon([[ -1,      0 ],
	       [ -1,     -t ],
	       [ l/2+te, -t ],
	       [ l/2+te,  d ],
	       [ l/2,     d ],
	       [ l/2,     0 ],
	       ]);
    }
  }
}
