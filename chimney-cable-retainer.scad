// -*- C -*-

include <utils.scad>

inrear_d_real = 20;
inrear_d = 15;
bar_th = 5;
general_th = 5;
between_cables = 150;
around_cables = 20;
cable_dia = 15;

total_d = 40;
above_h = 40;

// calculated

cable_x = around_cables + cable_dia/2;
total_x = cable_x * 2 + between_cables;

below_h = above_h;

sit_angle = atan2(inrear_d_real - inrear_d, below_h);

module CoreElevation(){
  rotate(-sit_angle)
    rectfromto([ 0, 0 ],
	       [ total_d, general_th ]);
  rectfromto([ 0, 0 ],
	     [ general_th, above_h ]);
  translate([ inrear_d, -above_h ])
    rectfromto([ 0,0 ],
	       [ -bar_th, bar_th ]);
}

module BarMountElevation(){
  hull(){
    rotate(-sit_angle)
      rectfromto([ 0, 0 ],
		 [ inrear_d, general_th ]);
    translate([ 0, -below_h ])
      rectfromto([ 0,0 ],
		 [ inrear_d, bar_th ]);
  }
}

module Retainer(){ ////toplevel
  difference(){
    union(){
      linextr_x_yz(0, total_x)
	mirror([1,0])
	CoreElevation();

      for (x = [0, 0.5, 1] * (total_x - general_th))
	translate([ x, 0,0 ])
	linextr_x_yz(0, general_th)
	mirror([1,0])
	BarMountElevation();
    }

    for (x = [cable_x, total_x - cable_x])
      translate([x, 0, 0])
      linextr(-below_h/2, 100)
      hull(){
        translate([ 0, -(general_th + cable_dia/2) ])
	  circle(r = cable_dia/2);
	translate([ 0, -(total_d + 1)])
	  square([ cable_dia, 1], center=true);
      }
  }
}
