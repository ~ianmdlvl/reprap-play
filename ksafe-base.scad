// -*- C -*-

// from actual ksafe
bolt_above = 8.50 - 0.50;
bolthole_height = 4.24 + 1.00;
wall_thick = 4.50;
bolthole_width = 16.62 + 1.00;
main_sz = 149.06 - 0.25;
cnr_rad = 19.5;
lidinner_thick_allow = 20.78 + 0.50;
display_width = 69.81 - 0.50;

dpp3 = [ -5.5, 8.5 ];
dpp2 = [ -11.0, 7.0 ];
dpp1 = [ -34.0, 14.0 ];

// other parameters
web_thick = 4;
web_height = 20; // excluding wall and base thick
bolthole_rhs = 20;
bolthole_lhs = 20;
boltreinf_thick = 6;
anchor_wall_space = 25;
base_thick = 4;
space = 25;
anchor_thick = 4;
anchor_rad = 4;
bevel = 8;
string_slot = 3.0;
string_depth = 6.0;
thumbslot_depth = 5.0;
thumbslot_width = 15.0;
thumbslot_between = 10;
ksafecover_lip = 4.62;
display_rhs_secs = 15;
dcover_endthick = 3.0;
dcover_mainthick = 5.0;
dcover_slop_height = 0.35;
dcover_slop_depth = 0.25;
dcover_slop_inside = 1.50;
dcover_commonvertoff = 0.00; // slop_height or slop_inside is added too
dcover_edge_gap_more_width = 2.0; // each side

// ----- calculated -----

hsz = main_sz/2;
cut = main_sz + 20;

gppA = [0,0];
gppB = gppA - [ wall_thick, 0 ];

gppL = [ gppB[0], -(lidinner_thick_allow + space + base_thick) ];

yw1 = -web_thick/2;
yw2 = yw1 - bolthole_rhs;
yw3 = yw2 - anchor_thick;
yw4 = yw3 - anchor_wall_space;
yw5 = yw4 - wall_thick;
yw6 = -(hsz - cnr_rad + 0.1);

yw10 = web_thick/2;
yw11 = yw2 + anchor_wall_space;
yw12 = yw11 + wall_thick;
yw13 = -yw6;

cpp1 = dpp1 + [  dcover_slop_depth, dcover_slop_height ];
cpp2 = [ dpp2[0] - dcover_slop_depth, dpp3[1] + dcover_slop_height ];
cppH = cpp1 + [ 0, dcover_endthick ];
cppA = [ cpp2[0], dpp3[1] + dcover_slop_inside ];
cppK = cppA + [ 0, dcover_mainthick ];
cppZ = [ -ksafecover_lip, -dcover_commonvertoff ];
cppD = cppZ + [ 0, -dcover_slop_inside ];
cppC = [ dcover_slop_inside, cppD[1] ];
cppF = cppC + dcover_mainthick * [1,-1];
cppB = [ cppC[0], cppA[1] ];
cppG = [ cppF[0], cppK[1] ];
cppE = [ cppD[0], cppF[1] - (cppF[0] - cppD[0]) ];

// anchor

anchor_b = anchor_thick + anchor_rad;
appM = gppL + anchor_b * [1,1];

a_bevel = 2 * anchor_b * (1 + sqrt(0.5));

module upp_app_Vars(t_bevel){
  $xppE = gppL + t_bevel * [0,1];
  $xppF = gppL + t_bevel * [1,0];

  $xppJ = $xppE + wall_thick * [ 1, tan(22.5) ];
  $xppI = $xppF + base_thick * [ tan(22.5), 1 ];

  // must pass a_bevel for t_bevel for these to be valid
  $gppP = gppA + [0,-1] * lidinner_thick_allow;
  $gppQ = $gppP + [1,-1] * web_height;
  $gppR = $xppJ + [ 1, tan(22.5) ] * web_height;
  $gppS = $xppI + [ tan(22.5), 1 ] * web_height;
  $gppT = [ $gppQ[0], $xppE[1] ];

  children();
}

module upp_app_Profile(){
  polygon([ gppA,
	    gppB,
	    $xppE,
	    $xppF,
	    $xppF + [1,0],
	    $xppI + [1,0],
	    $xppJ ],
	  convexity=10);
}


module UsualProfile(){
  upp_app_Vars(bevel) upp_app_Profile();
}

module NearAnchorProfile(){
  upp_app_Vars(a_bevel) upp_app_Profile();
}

module AnchorProfile(){
  upp_app_Vars(a_bevel) {

    upp_app_Profile();

    difference(){
      hull(){
	polygon([ $xppE,
		  $xppF,
		  $xppF + [0,1],
		  $xppE + [1,0] ],
	  convexity=10);
	translate(appM) circle(r= anchor_b);
      }
      translate(appM) circle(r= anchor_rad);
    }
  }
}

module AnchorWallProfile(){
  UsualProfile();
  NearAnchorProfile();
  hull(){
    for (bev = [bevel, a_bevel]) {
      upp_app_Vars(bev) {
	polygon([ $xppE,
		  $xppF,
		  $xppI,
		  $xppJ ],
	  convexity=10);
      }
    }
  }
}

module WebProfile(){
  upp_app_Vars(a_bevel){
    if ($gppR[1] <= $gppP[1]) {
      polygon([ $gppP,
		$xppE,
		$gppT,
		$gppQ ]);
      polygon([ $gppP,
		$xppE,
		$xppF,
		$gppS,
		$gppR ],
	  convexity=10);
    } else {
      polygon([ $gppP,
		$xppE,
		$xppF,
		$gppS,
		$gppP + web_height * [1,0] ],
	  convexity=10);
    }
    polygon([ $gppS,
	      $xppF,
	      $xppF + [1,0],
	      $gppS + [1,0] ],
	  convexity=10);
  }
}

module SomeBaseProfile(I, F){
  polygon([ I,
	    F,
	    [ hsz+1, F[1] ],
	    [ hsz+1, I[1] ] ]);
}

module BaseProfile(){
  SomeBaseProfile($xppI, $xppF);
}

module DCoverProfileRaw(){
  polygon([ cpp1,
	    cpp2,
	    cppA,
	    cppB,
	    cppC,
	    cppD,
	    cppE,
	    cppF,
	    cppG,
	    cppK,
	    cppH ],
	  convexity = 10);
}

module DCoverProfile(){
  mirror([1,0])
    translate(-cppZ)
    DCoverProfileRaw();
}

module SWalls(ymin, ymax, t_bevel) {
  upp_app_Vars(t_bevel) {
    translate([0,ymin,0])
      mirror([0,1,0])
      rotate([90,0,0])
      linear_extrude(height= ymax-ymin, convexity=10)
      for (xm=[0,1])
	mirror([xm,0])
	  translate([-hsz, 0])
	    children();
  }
}

module AtTwoCorners(){
  for (xm=[0,1]) {
    mirror([xm,0,0]) 
    translate((hsz - cnr_rad) * [1,1])
    intersection(){
      rotate_extrude(convexity=10)
	translate([-cnr_rad,0])
	children();
      translate([0,0,-250])
	cube([50,50,500]);
    }
  }
}

module Box(){
  /// corners, and front and back of base
  for (ym=[0,1]) mirror([0,ym,0]) {
    AtTwoCorners(){
      UsualProfile();
    }
    hull() AtTwoCorners(){
      upp_app_Vars(bevel){
	polygon([ $xppI,
		  $xppF,
		  $xppF + [0.1, 0],
		  $xppI + [0.1, 0]
		  ]);
      }
    }
  }

  // side walls and base
  SWalls(yw6 , yw4 , bevel  ) { UsualProfile();      BaseProfile(); }
  SWalls(yw5 , yw4 , a_bevel) { AnchorWallProfile(); BaseProfile(); }
  SWalls(yw5 , yw12, a_bevel) { NearAnchorProfile(); BaseProfile(); }
  SWalls(yw3 , yw2 , a_bevel) { AnchorProfile();     BaseProfile(); }
  SWalls(yw11, yw12, a_bevel) { AnchorWallProfile(); BaseProfile(); }
  SWalls(yw11, yw13, bevel  ) { UsualProfile();      BaseProfile(); }
  SWalls(yw1,  yw10, a_bevel) { WebProfile(); SomeBaseProfile($gppS, $xppF); }

  // front and rear walls
  rotate([0,0,90]) SWalls(yw6, yw13, bevel) UsualProfile();
}

module DCover(){ ////toplevel
  translate([ -display_width/2, -hsz, 0 ])
    rotate([0,90,0])
    rotate([0,0,90])
    linear_extrude( display_width - display_rhs_secs, convexity = 10)
    DCoverProfile();
}

module DCoverSupportAllowance(){
  translate([0, -hsz, 0])
    cube(center=true,
	 [ display_width + 2 * dcover_edge_gap_more_width,
	   wall_thick * 2,
	   dcover_slop_inside * 2 + 0.01 ]);
}

module BoltHoles(){
  translate([0,0, -(bolt_above + 0.5 * bolthole_height)])
    cube(center=true, [ cut, bolthole_width, bolthole_height ]);
}

module KsafeBase(){ ////toplevel
  DCover();

  difference(){
    Box();

    BoltHoles();

    // string slot
    translate([ -cut,
	        -(bolthole_width/2 + bolthole_rhs),
	        1 ])
      mirror([0,1,0]) mirror([0,0,1])
      cube([ cut*2,
	     string_slot,
	     lidinner_thick_allow + string_depth + 1 ]);

    // thumb slots
    for (mx=[0,1]) mirror([mx,0,0]) {
      translate([ thumbslot_between/2,
		  0,
		  -thumbslot_depth ])
	cube([ thumbslot_width,
	       cut,
	       thumbslot_depth+1 ]);
    }

    DCoverSupportAllowance();
  }
}

module DemoProfiles(){ ////toplevel
  translate([0,0,-2]) color("yellow") AnchorWallProfile();
  color("red") AnchorProfile();
  translate([0,0,2]) color("black") NearAnchorProfile();
  translate([0,0,4]) color("blue") UsualProfile();
  translate([0,0,-4]) color("pink") WebProfile();
  translate([0,0,6]) color("purple") DCoverProfile();
}

module RimTest(){ ////toplevel
  intersection(){
    Box();
    cube(center=true, [ main_sz*2, main_sz*2,
			2.5 ]);
  }
}

module DCoverTest(){ ////toplevel
  intersection(){
    difference(){
      union(){
	Box();
	DCover();
      }
      DCoverSupportAllowance();
      BoltHoles();
    }
    translate([0,0,60])
    cube(center=true, [ main_sz*2, main_sz*2,
			2 * (60 + 10) ]);
  }
}

module BoltTest(){ ////toplevel
  dy = 0.5 * (bolthole_width+4);
  intersection(){
    KsafeBase();
    translate([ 0, -dy, -(bolt_above + bolthole_height + 1) ])
      cube([ main_sz, dy*2, 50 ]);
  }
}

//DemoProfiles();
//Box();
//KsafeBase();
//RimTest();
