// -*- C -*-

include <threads.scad>
include <utils.scad>

// https://en.wikipedia.org/wiki/ISO_metric_screw_thread

// M6
thread_nom = 6;
thread_pitch = 1.00;
thread_act = thread_nom - 0.300;
head_size = 10;

thread_len = 12.5;
base_th = 1.5;

$test = false;

// calculated

base_dia = head_size / cos(30);

module MachineScrew(){
  translate([0, 0, -0.1])
    render()
    metric_thread(diameter=thread_act, pitch=thread_pitch,
		  leadin=1, internal=false,
		  test=$test, length=thread_len + 0.1);
  linextr(-base_th, 0)
    circle(r= base_dia/2, $fn=6);
}

MachineScrew();
