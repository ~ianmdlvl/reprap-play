// -*- C -*-

// print on High Detail
// but adjust infill to 50%, shell thickness to 2mm

include <threads.scad>
include <camera-mount.scad>

positive_dia = inch * 3/8. - 0.375;
positive_l = inch * 1/2.;

negative_l = negative_default_l;

negative_wall = 4;
midsection = 4;

spanner = 12;

base_dia = 35;
base_th_min = 1;
base_th_max = 4;

//$test = true;
$test = false;
$fs=0.1;
$fa=5;

module Adapter(){
  translate([0,0,-0.1])
    english_thread(diameter=positive_dia/inch, threads_per_inch=16,
		   leadin=1, test=$test,
		   length= (positive_l + 0.1) / inch);
  rotate([180,0,0]) {
    difference(){
      union(){
	cylinder(r= spanner/2 * 1/(0.5 * sqrt(3)),
		 h = negative_l + midsection,
		 $fn=6);
	translate([0,0, midsection+negative_l]) {
	  mirror([0,0,1]) {
	    hull(){
	      cylinder(r= base_dia/2, h = base_th_min);
	      cylinder(r= 0.1,        h = base_th_max);
	    }
	  }
	}
      }
      translate([0,0, midsection + negative_l])
	CameraMountThread(negative_l);
    }
  }
}

Adapter();
